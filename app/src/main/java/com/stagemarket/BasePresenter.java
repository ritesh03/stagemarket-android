package com.stagemarket;

public interface BasePresenter {
    public void showProgressDialog();
    public void showErrorDialog(String msg);
    public void showMessageDialog(String msg);
    public void hideProgressDialog();
}
