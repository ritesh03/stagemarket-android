package com.stagemarket.facebook;

/**
 * Created by Ashish on 26/04/15.
 */
public interface OnSocialLoginListener {

    /**
     * Override this method to get Login Data
     *
     * @param type
     * @param loginData
     */
    public void onSocialLogin(int type, SocialLoginData loginData);
}
