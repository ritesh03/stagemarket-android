package com.stagemarket.facebook;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.stagemarket.R;
import com.stagemarket.utils.CommonListeners;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.DialogPopUps;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;



public class SocialLogin{


    public static final int FACEBOOK_LOGIN = 6;

    private Activity activity;
    private SocialLoginData loginData;
    private CallbackManager callbackManager;

    private ImageView btnLoginViaFacebook;

    public SocialLogin(Activity activity) {
        this.activity = activity;
    }

    /**
     * Method to login the user through facebook
     */
    public CallbackManager loginViaFacebook(int viewId) {

        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.e("ACCESS TOKEN", loginResult.getAccessToken().getToken());
                        loginData = new SocialLoginData();
                        loginData.setAccess_token(loginResult.getAccessToken().getToken());

                        requestFacebookUserData(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        Log.e("CANCEL", "CANCEL");

                        loginData = new SocialLoginData();

                      //  requestFacebookUserData(AccessToken.getCurrentAccessToken());
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Log.e("ERROR", "ERROR");
                        try {

                            if (!CommonUtils.isConnectedToInternet(activity)) {
                                // CustomDialogClass custom_dialog=new CustomDialogClass(activity, activity.getResources().getString(R.string.network_connection));
                                // custom_dialog.show();
                               // Toast.makeText(activity, activity.getResources().getString(R.string.network_connection), Toast.LENGTH_SHORT).show();

                                DialogPopUps.alertPopUp(activity, activity.getResources().getString(R.string.check_your_network),
                                        activity.getResources().getString(R.string.alert), activity.getResources().getString(R.string.ok)
                                        , new CommonListeners.AlertCallBackInterface() {
                                            @Override
                                            public void neutralClick() {

                                            }
                                        });
                            } else {

                            }

                            Log.v("ERROR", exception.getMessage());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //new DialogPopup().alertPopup(activity, "Error", "Please try other alternatives.");
                    }
                });


        btnLoginViaFacebook = (ImageView) activity.findViewById(viewId);
        btnLoginViaFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                LoginManager.getInstance().logInWithReadPermissions(activity,
                        Arrays.asList("email"));
            }
        });

        return callbackManager;
    }

    /**
     * Method to request the UserData from Facebook
     *
     * @param token
     */
    public void requestFacebookUserData(AccessToken token) {

        GraphRequest request = GraphRequest.newMeRequest(
                token, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        // Application code
                        try {
                            Log.v("GRAPH RESPONSE", response.toString());
                            Log.v("JSON OBJECT", object.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        try {

                            String key;

                            key = "id";
                            loginData.setSocialUserID(getValue(key, object));

                            key = "birthday";
                            loginData.setBirthday_date(getValue(key, object));

                            key = "gender";
                            loginData.setSex(getValue(key, object));

                            key = "name";
                            String name = getValue(key, object);

                            key = "email";
                            loginData.setEmail(getValue(key, object));

                            key = "first_name";
                            loginData.setFirst_name(getValue(key, object));

                            key = "last_name";
                            loginData.setLast_name(getValue(key, object));

                            key = "about";
                            loginData.setAbout_me(getValue(key, object));

                            returnFacebookUserData();
                        } catch (JSONException jEx) {

                        }
                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, name, email, gender, birthday, about, first_name, last_name");
        request.setParameters(parameters);
        request.executeAsync();
    }

    /**
     * Method to safely set the the values to the Fields
     *
     * @param key
     * @param jObj
     * @return
     * @throws JSONException
     */
    private String getValue(String key, JSONObject jObj) throws JSONException {

        if (jObj.has(key))
            return jObj.getString(key);

        return "";
    }

    /**
     * Return the User data to the User
     */
    private void returnFacebookUserData() {

      //  String pic = Profile.getCurrentProfile().getProfilePictureUri(400, 400).toString();

        try {
            loginData.setPic_big("https://graph.facebook.com/"+loginData.getSocialUserID()+"/picture?width=640&height=640");
            loginData.setUserType("facebook");

            if (activity instanceof OnSocialLoginListener)
                ((OnSocialLoginListener) activity).onSocialLogin(FACEBOOK_LOGIN, loginData);
        } catch (Exception e) {
            e.printStackTrace();
            loginData.setPic_big("");
            loginData.setUserType("F");
            if (activity instanceof OnSocialLoginListener)
                ((OnSocialLoginListener) activity).onSocialLogin(FACEBOOK_LOGIN, loginData);
        }
    }



    public void releaseResources(){
        btnLoginViaFacebook=null;
        activity=null;
    }

}
