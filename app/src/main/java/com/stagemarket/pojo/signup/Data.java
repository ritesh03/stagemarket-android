
package com.stagemarket.pojo.signup;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Data {

    @SerializedName("blockStatus")
    private Long mBlockStatus;
    @SerializedName("companyAddress")
    private String mCompanyAddress;
    @SerializedName("companyName")
    private String mCompanyName;
    @SerializedName("companyPhone")
    private String mCompanyPhone;
    @SerializedName("companyWebsite")
    private String mCompanyWebsite;
    @SerializedName("createdAt")
    private String mCreatedAt;
    @SerializedName("deviceToken")
    private String mDeviceToken;
    @SerializedName("deviceType")
    private Long mDeviceType;
    @SerializedName("dob")
    private String mDob;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("emailStatus")
    private Long mEmailStatus;
    @SerializedName("emailToken")
    private String mEmailToken;
    @SerializedName("fbId")
    private String mFbId;
    @SerializedName("firstName")
    private String mFirstName;
    @SerializedName("gender")
    private String mGender;
    @SerializedName("id")
    private Long mId;
    @SerializedName("image_url")
    private String mImageUrl;
    @SerializedName("lastName")
    private String mLastName;
    @SerializedName("mobile")
    private String mMobile;
    @SerializedName("organizationType")
    private Long mOrganizationType;
    @SerializedName("password")
    private String mPassword;
    @SerializedName("remember_token")
    private String mRememberToken;
    @SerializedName("resetPasswordToken")
    private String mResetPasswordToken;
    @SerializedName("role")
    private Long mRole;
    @SerializedName("updatedAt")
    private String mUpdatedAt;
    @SerializedName("userType")
    private Long mUserType;
    @SerializedName("visiblePwd")
    private String mVisiblePwd;

    public Long getBlockStatus() {
        return mBlockStatus;
    }

    public void setBlockStatus(Long blockStatus) {
        mBlockStatus = blockStatus;
    }

    public String getCompanyAddress() {
        return mCompanyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        mCompanyAddress = companyAddress;
    }

    public String getCompanyName() {
        return mCompanyName;
    }

    public void setCompanyName(String companyName) {
        mCompanyName = companyName;
    }

    public String getCompanyPhone() {
        return mCompanyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        mCompanyPhone = companyPhone;
    }

    public String getCompanyWebsite() {
        return mCompanyWebsite;
    }

    public void setCompanyWebsite(String companyWebsite) {
        mCompanyWebsite = companyWebsite;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public String getDeviceToken() {
        return mDeviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        mDeviceToken = deviceToken;
    }

    public Long getDeviceType() {
        return mDeviceType;
    }

    public void setDeviceType(Long deviceType) {
        mDeviceType = deviceType;
    }

    public String getDob() {
        return mDob;
    }

    public void setDob(String dob) {
        mDob = dob;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public Long getEmailStatus() {
        return mEmailStatus;
    }

    public void setEmailStatus(Long emailStatus) {
        mEmailStatus = emailStatus;
    }

    public String getEmailToken() {
        return mEmailToken;
    }

    public void setEmailToken(String emailToken) {
        mEmailToken = emailToken;
    }

    public String getFbId() {
        return mFbId;
    }

    public void setFbId(String fbId) {
        mFbId = fbId;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getGender() {
        return mGender;
    }

    public void setGender(String gender) {
        mGender = gender;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public String getMobile() {
        return mMobile;
    }

    public void setMobile(String mobile) {
        mMobile = mobile;
    }

    public Long getOrganizationType() {
        return mOrganizationType;
    }

    public void setOrganizationType(Long organizationType) {
        mOrganizationType = organizationType;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getRememberToken() {
        return mRememberToken;
    }

    public void setRememberToken(String rememberToken) {
        mRememberToken = rememberToken;
    }

    public String getResetPasswordToken() {
        return mResetPasswordToken;
    }

    public void setResetPasswordToken(String resetPasswordToken) {
        mResetPasswordToken = resetPasswordToken;
    }

    public Long getRole() {
        return mRole;
    }

    public void setRole(Long role) {
        mRole = role;
    }

    public String getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        mUpdatedAt = updatedAt;
    }

    public Long getUserType() {
        return mUserType;
    }

    public void setUserType(Long userType) {
        mUserType = userType;
    }

    public String getVisiblePwd() {
        return mVisiblePwd;
    }

    public void setVisiblePwd(String visiblePwd) {
        mVisiblePwd = visiblePwd;
    }

}
