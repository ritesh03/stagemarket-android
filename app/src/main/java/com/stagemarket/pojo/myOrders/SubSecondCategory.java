package com.stagemarket.pojo.myOrders;

import com.google.gson.annotations.SerializedName;

public class SubSecondCategory {
    private boolean isSelected =false;


    @SerializedName("id")
    private String mId;
    @SerializedName("subSecondCatName")
    private String subSecondCatName;
    @SerializedName("catId")
    String catId;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getSubSecondCatName() {
        return subSecondCatName;
    }

    public void setSubSecondCatName(String subSecondCatName) {
        this.subSecondCatName = subSecondCatName;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }
}
