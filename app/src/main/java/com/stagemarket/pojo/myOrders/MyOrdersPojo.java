package com.stagemarket.pojo.myOrders;

public class MyOrdersPojo {

    private String order_id = "";
    private String product_name = "";
    private String product_price = "";
    private String product_status = "";
    private String product_timimgs = "";
    private int product_image;


    public MyOrdersPojo(String order_id, String product_name, String product_price, String product_status, String product_timimgs, int product_image) {
        this.order_id = order_id;
        this.product_name = product_name;
        this.product_price = product_price;
        this.product_status = product_status;
        this.product_timimgs = product_timimgs;
        this.product_image = product_image;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_price() {
        return product_price;
    }

    public void setProduct_price(String product_price) {
        this.product_price = product_price;
    }

    public String getProduct_status() {
        return product_status;
    }

    public void setProduct_status(String product_status) {
        this.product_status = product_status;
    }

    public String getProduct_timimgs() {
        return product_timimgs;
    }

    public void setProduct_timimgs(String product_timimgs) {
        this.product_timimgs = product_timimgs;
    }

    public int getProduct_image() {
        return product_image;
    }

    public void setProduct_image(int product_image) {
        this.product_image = product_image;
    }
}
