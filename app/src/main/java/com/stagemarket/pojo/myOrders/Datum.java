
package com.stagemarket.pojo.myOrders;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class Datum {

    @SerializedName("addressId")
    private Long mAddressId;
    @SerializedName("billingAddress")
    private String mBillingAddress;
    @SerializedName("billingAmount")
    private String mBillingAmount;
    @SerializedName("billingCity")
    private String mBillingCity;
    @SerializedName("billingContact")
    private String mBillingContact;
    @SerializedName("billingName")
    private String mBillingName;
    @SerializedName("buyerId")
    private Long mBuyerId;
    @SerializedName("buyer_name")
    private String mBuyerName;
    @SerializedName("catId")
    private Long mCatId;
    @SerializedName("categoryName")
    private String mCategoryName;
    @SerializedName("commision")
    private String mCommision;

    public String getmQuantity() {
        return mQuantity;
    }

    public void setmQuantity(String mQuantity) {
        this.mQuantity = mQuantity;
    }

    @SerializedName("quantity")
    private String mQuantity;
    @SerializedName("companyId")
    private Long mCompanyId;
    @SerializedName("cost")
    private double mCost;
    @SerializedName("createdAt")
    private String mCreatedAt;
    @SerializedName("deliveredDate")
    private String mDeliveredDate;
   /* @SerializedName("getCompanyInfo")
    @Expose
    public GetCompanyInfo getCompanyInfo;*/
    @SerializedName("id")
    private Long mId;
    @SerializedName("orderStatus")
    private Long mOrderStatus;
    @SerializedName("orderType")
    private Long mOrderType;
    @SerializedName("orderedDate")
    private String mOrderedDate;
    @SerializedName("paymentStatus")
    private Long mPaymentStatus;
    @SerializedName("productId")
    private Long mProductId;
    @SerializedName("productImage")
    private String mProductImage;
    @SerializedName("productImages")
    private List<ProductImage> mProductImages;
    @SerializedName("productName")
    private String mProductName;
    @SerializedName("role")
    private Long mRole;
    @SerializedName("sellerId")
    private Long mSellerId;
    @SerializedName("seller_name")
    private String mSellerName;
    @SerializedName("status")
    private Long mStatus;
    @SerializedName("subCategoryName")
    private String mSubCategoryName;
    @SerializedName("subSecondCategoryName")
    private String mSubSecondCategoryName;
    @SerializedName("subcatId")
    private Long mSubcatId;
    @SerializedName("updatedAt")
    private String mUpdatedAt;
    @SerializedName("vat")
    private String mVat;

    public Long getAddressId() {
        return mAddressId;
    }

    public void setAddressId(Long addressId) {
        mAddressId = addressId;
    }

    public String getBillingAddress() {
        return mBillingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        mBillingAddress = billingAddress;
    }

    public String getBillingAmount() {
        return mBillingAmount;
    }

    public void setBillingAmount(String billingAmount) {
        mBillingAmount = billingAmount;
    }

    public String getBillingCity() {
        return mBillingCity;
    }

    public void setBillingCity(String billingCity) {
        mBillingCity = billingCity;
    }

    public String getBillingContact() {
        return mBillingContact;
    }

    public void setBillingContact(String billingContact) {
        mBillingContact = billingContact;
    }

    public String getBillingName() {
        return mBillingName;
    }

    public void setBillingName(String billingName) {
        mBillingName = billingName;
    }

    public Long getBuyerId() {
        return mBuyerId;
    }

    public void setBuyerId(Long buyerId) {
        mBuyerId = buyerId;
    }

    public String getBuyerName() {
        return mBuyerName;
    }

    public void setBuyerName(String buyerName) {
        mBuyerName = buyerName;
    }

    public Long getCatId() {
        return mCatId;
    }

    public void setCatId(Long catId) {
        mCatId = catId;
    }

    public String getCategoryName() {
        return mCategoryName;
    }

    public void setCategoryName(String categoryName) {
        mCategoryName = categoryName;
    }

    public String getCommision() {
        return mCommision;
    }

    public void setCommision(String commision) {
        mCommision = commision;
    }

    public Long getCompanyId() {
        return mCompanyId;
    }

    public void setCompanyId(Long companyId) {
        mCompanyId = companyId;
    }

    public double getCost() {
        return mCost;
    }

    public void setCost(double cost) {
        mCost = cost;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public String getDeliveredDate() {
        return mDeliveredDate;
    }

    public void setDeliveredDate(String deliveredDate) {
        mDeliveredDate = deliveredDate;
    }

  /*  public GetCompanyInfo getGetCompanyInfo() {
        return getCompanyInfo;
    }

    public void setGetCompanyInfo(String getCompanyInfo) {
        getCompanyInfo = getCompanyInfo;
    }*/

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Long getOrderStatus() {
        return mOrderStatus;
    }

    public void setOrderStatus(Long orderStatus) {
        mOrderStatus = orderStatus;
    }

    public Long getOrderType() {
        return mOrderType;
    }

    public void setOrderType(Long orderType) {
        mOrderType = orderType;
    }

    public String getOrderedDate() {
        return mOrderedDate;
    }

    public void setOrderedDate(String orderedDate) {
        mOrderedDate = orderedDate;
    }

    public Long getPaymentStatus() {
        return mPaymentStatus;
    }

    public void setPaymentStatus(Long paymentStatus) {
        mPaymentStatus = paymentStatus;
    }

    public Long getProductId() {
        return mProductId;
    }

    public void setProductId(Long productId) {
        mProductId = productId;
    }

    public String getProductImage() {
        return mProductImage;
    }

    public void setProductImage(String productImage) {
        mProductImage = productImage;
    }

    public List<ProductImage> getProductImages() {
        return mProductImages;
    }

    public void setProductImages(List<ProductImage> productImages) {
        mProductImages = productImages;
    }

    public String getProductName() {
        return mProductName;
    }

    public void setProductName(String productName) {
        mProductName = productName;
    }

    public Long getRole() {
        return mRole;
    }

    public void setRole(Long role) {
        mRole = role;
    }

    public Long getSellerId() {
        return mSellerId;
    }

    public void setSellerId(Long sellerId) {
        mSellerId = sellerId;
    }

    public String getSellerName() {
        return mSellerName;
    }

    public void setSellerName(String sellerName) {
        mSellerName = sellerName;
    }

    public Long getStatus() {
        return mStatus;
    }

    public void setStatus(Long status) {
        mStatus = status;
    }

    public String getSubCategoryName() {
        return mSubCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        mSubCategoryName = subCategoryName;
    }

    public String getSubSecondCategoryName() {
        return mSubSecondCategoryName;
    }

    public void setSubSecondCategoryName(String subSecondCategoryName) {
        mSubSecondCategoryName = subSecondCategoryName;
    }

    public Long getSubcatId() {
        return mSubcatId;
    }

    public void setSubcatId(Long subcatId) {
        mSubcatId = subcatId;
    }

    public String getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        mUpdatedAt = updatedAt;
    }

    public String getVat() {
        return mVat;
    }

    public void setVat(String vat) {
        mVat = vat;
    }

}
