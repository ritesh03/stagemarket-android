
package com.stagemarket.pojo.myOrders.purchased;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class PurchasedResponse {

    @SerializedName("data")
    private Data mData;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("result")
    private String mResult;
    @SerializedName("return")
    private Long mReturn;

    public Data getData() {
        return mData;
    }

    public void setData(Data data) {
        mData = data;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

    public Long getReturn() {
        return mReturn;
    }

    public void setReturn(Long reeturn) {
        mReturn = reeturn;
    }

}
