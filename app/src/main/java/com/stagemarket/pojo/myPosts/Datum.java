
package com.stagemarket.pojo.myPosts;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Datum {

    @SerializedName("catId")
    private Long mCatId;
    @SerializedName("CatName")
    private String mCategoryName;
    @SerializedName("cost")
    private Long mCost;
    @SerializedName("createdAt")
    private String mCreatedAt;
    @SerializedName("currency")
    private String mCurrency;
    @SerializedName("deleteStatus")
    private Long mDeleteStatus;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("firstName")
    private String mFirstName;
    @SerializedName("id")
    private String mId;
    @SerializedName("latitude")
    private String mLatitude;
    @SerializedName("location")
    private String mLocation;
    @SerializedName("longitude")
    private String mLongitude;
    @SerializedName("mobile")
    private String mMobile;
    @SerializedName("name")
    private String mName;
    @SerializedName("productImages")
    private List<ProductImage> mProductImages;
    @SerializedName("role")
    private Long mRole;
    @SerializedName("status")
    private Long mStatus;
    @SerializedName("subCatName")
    private String mSubCatName;
    @SerializedName("subcatId")
    private Long mSubcatId;
    @SerializedName("title")
    private String mTitle;

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    @SerializedName("quantity")
    private String quantity;
    @SerializedName("updatedAt")
    private String mUpdatedAt;
    @SerializedName("userId")
    private Long mUserId;
    @SerializedName("companyId")
    private Long companyId;

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getCatId() {
        return mCatId;
    }

    public void setCatId(Long catId) {
        mCatId = catId;
    }

    public String getCategoryName() {
        return mCategoryName;
    }

    public void setCategoryName(String categoryName) {
        mCategoryName = categoryName;
    }

    public Long getCost() {
        return mCost;
    }

    public void setCost(Long cost) {
        mCost = cost;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public String getCurrency() {
        return mCurrency;
    }

    public void setCurrency(String currency) {
        mCurrency = currency;
    }

    public Long getDeleteStatus() {
        return mDeleteStatus;
    }

    public void setDeleteStatus(Long deleteStatus) {
        mDeleteStatus = deleteStatus;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public void setLatitude(String latitude) {
        mLatitude = latitude;
    }

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String location) {
        mLocation = location;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public void setLongitude(String longitude) {
        mLongitude = longitude;
    }

    public String getMobile() {
        return mMobile;
    }

    public void setMobile(String mobile) {
        mMobile = mobile;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public List<ProductImage> getProductImages() {
        return mProductImages;
    }

    public void setProductImages(List<ProductImage> productImages) {
        mProductImages = productImages;
    }

    public Long getRole() {
        return mRole;
    }

    public void setRole(Long role) {
        mRole = role;
    }

    public Long getStatus() {
        return mStatus;
    }

    public void setStatus(Long status) {
        mStatus = status;
    }

    public String getSubCatName() {
        return mSubCatName;
    }

    public void setSubCatName(String subCatName) {
        mSubCatName = subCatName;
    }

    public Long getSubcatId() {
        return mSubcatId;
    }

    public void setSubcatId(Long subcatId) {
        mSubcatId = subcatId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        mUpdatedAt = updatedAt;
    }

    public Long getUserId() {
        return mUserId;
    }

    public void setUserId(Long userId) {
        mUserId = userId;
    }

}
