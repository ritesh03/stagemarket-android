package com.stagemarket.pojo.deleteProduct;

import com.google.gson.annotations.SerializedName;

public class DeleteProductResponse{

    @SerializedName("result")
    private String result;

    @SerializedName("message")
    private String message;

    @SerializedName("return")
    private int jsonMemberReturn;

    public void setResult(String result){
        this.result = result;
    }

    public String getResult(){
        return result;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage(){
        return message;
    }

    public void setJsonMemberReturn(int jsonMemberReturn){
        this.jsonMemberReturn = jsonMemberReturn;
    }

    public int getJsonMemberReturn(){
        return jsonMemberReturn;
    }
}