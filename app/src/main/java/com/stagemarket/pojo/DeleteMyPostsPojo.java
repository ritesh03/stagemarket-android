package com.stagemarket.pojo;

public class DeleteMyPostsPojo {
    private int position;

    public DeleteMyPostsPojo(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}