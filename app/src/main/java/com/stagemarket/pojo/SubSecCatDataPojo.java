package com.stagemarket.pojo;

public class SubSecCatDataPojo {

    String subId = "";

    public SubSecCatDataPojo(String subId) {
        this.subId = subId;
    }

    public String getSubId() {
        return subId;
    }

    public void setSubId(String subId) {
        this.subId = subId;
    }
}
