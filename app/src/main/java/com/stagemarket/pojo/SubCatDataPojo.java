package com.stagemarket.pojo;

public class SubCatDataPojo {

    String subId = "";

    public SubCatDataPojo(String subId) {
        this.subId = subId;
    }

    public String getSubId() {
        return subId;
    }

    public void setSubId(String subId) {
        this.subId = subId;
    }
}
