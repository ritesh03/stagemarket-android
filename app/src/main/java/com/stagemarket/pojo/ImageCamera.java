package com.stagemarket.pojo;

public class ImageCamera {
    int position;

    public ImageCamera(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
