package com.stagemarket.pojo.categorySell;

import com.stagemarket.pojo.category.CategoriesPojo;

public class CategorySellPojo {

    private String id = "";
    private int image;
    private String title = "";

    public CategorySellPojo(String id, int image, String title) {
        this.id = id;
        this.image = image;
        this.title = title;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
