
package com.stagemarket.pojo.categorySell;

import com.google.gson.annotations.SerializedName;
import com.stagemarket.pojo.myOrders.SubSecondCategory;

import java.util.List;

@SuppressWarnings("unused")
public class SubCategory {
    private boolean isSelected =false;

    @SerializedName("id")
    private String mId;
    @SerializedName("subCatName")
    private String mSubCatName;
    @SerializedName("catId")
String catId;
@SerializedName("subSecondCategories")
    List<SubSecondCategory> mArrayListSubSecCat ;

    public List<SubSecondCategory> getmArrayListSubSecCat() {
        return mArrayListSubSecCat;
    }

    public void setmArrayListSubSecCat(List<SubSecondCategory> mArrayListSubSecCat) {
        this.mArrayListSubSecCat = mArrayListSubSecCat;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getSubCatName() {
        return mSubCatName;
    }

    public void setSubCatName(String subCatName) {
        mSubCatName = subCatName;
    }

}
