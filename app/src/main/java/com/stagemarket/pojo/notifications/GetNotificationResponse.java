
package com.stagemarket.pojo.notifications;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class GetNotificationResponse {

    @SerializedName("data")
    private ArrayList<NotificationData> mData;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("result")
    private String mResult;
    @SerializedName("return")
    private Long mReturn;

    public ArrayList<NotificationData> getData() {
        return mData;
    }

    public void setData(ArrayList<NotificationData> data) {
        mData = data;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

    public Long getReturn() {
        return mReturn;
    }

    public void setReturn(Long returnn) {
        mReturn = returnn;
    }

}
