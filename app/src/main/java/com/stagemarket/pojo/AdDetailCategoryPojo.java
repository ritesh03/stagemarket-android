package com.stagemarket.pojo;

public class AdDetailCategoryPojo {
    String catName="";

    public AdDetailCategoryPojo(String catName) {

        this.catName = catName;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }
}
