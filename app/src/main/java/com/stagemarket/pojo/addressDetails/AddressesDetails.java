package com.stagemarket.pojo.addressDetails;

public class AddressesDetails {

    private String addressCount = "";
    private String addressUsername = "";
    private String addressLine1 = "";
    private String addressLine2 = "";
    private String addressContactDetails = "";


    public AddressesDetails(String addressCount, String addressUsername, String addressLine1,
                            String addressLine2, String addressContactDetails) {
        this.addressCount = addressCount;
        this.addressUsername = addressUsername;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.addressContactDetails = addressContactDetails;
    }

    public String getAddressCount() {
        return addressCount;
    }

    public void setAddressCount(String addressCount) {
        this.addressCount = addressCount;
    }

    public String getAddressUsername() {
        return addressUsername;
    }

    public void setAddressUsername(String addressUsername) {
        this.addressUsername = addressUsername;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getAddressContactDetails() {
        return addressContactDetails;
    }

    public void setAddressContactDetails(String addressContactDetails) {
        this.addressContactDetails = addressContactDetails;
    }
}
