package com.stagemarket.pojo.addressDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetAddressResponse {
    @SerializedName("return")
    @Expose
    public Integer _return;
    @SerializedName("result")
    @Expose
    public String result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;
    public class Datum {

        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("userId")
        @Expose
        public Integer userId;
        @SerializedName("billingName")
        @Expose
        public String billingName;
        @SerializedName("billingContact")
        @Expose
        public String billingContact;
        @SerializedName("billingddress")
        @Expose
        public String billingddress;
        @SerializedName("billingCity")
        @Expose
        public String billingCity;
        @SerializedName("createdAt")
        @Expose
        public String createdAt;
        @SerializedName("updatedAt")
        @Expose
        public String updatedAt;

    }
}
