package com.stagemarket.pojo;

public class SubCatPojo {
    int position;
    String catName = "";
    String subCatId = "";

    public SubCatPojo(String catName) {
        this.catName = catName;
    }

    public SubCatPojo(int position, String catName,String subCatId) {
        this.position = position;
        this.catName = catName;
        this.subCatId = subCatId;
    }

    public String getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(String subCatId) {
        this.subCatId = subCatId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
