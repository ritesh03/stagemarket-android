
package com.stagemarket.pojo.changePassword;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ChangePwdResponse {

    @SerializedName("message")
    private String mMessage;
    @SerializedName("result")
    private String mResult;
    @SerializedName("return")
    private Long mReturn;

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

    public Long getReturn() {
        return mReturn;
    }

    public void setReturn(Long rreturn) {
        mReturn = rreturn;
    }

}
