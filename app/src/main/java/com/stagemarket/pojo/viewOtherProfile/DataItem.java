package com.stagemarket.pojo.viewOtherProfile;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class DataItem {

    @SerializedName("cost")
    private float cost;

    @SerializedName("role")
    private int role;
    @SerializedName("commision")
    private String commision;
    @SerializedName("category_name")
    private String categoryName;

    @SerializedName("latitude")
    private String latitude;

    @SerializedName("mobile")
    private String mobile;

    public String getWishlistStatus() {
        return WishlistStatus;
    }

    public void setWishlistStatus(String wishlistStatus) {
        WishlistStatus = wishlistStatus;
    }

    @SerializedName("WishlistStatus")
    private String WishlistStatus;

    @SerializedName("description")
    private String description;

    @SerializedName("title")
    private String title;

    @SerializedName("userId")
    private int userId;

    @SerializedName("subcatId")
    private int subcatId;

    @SerializedName("catId")
    private int catId;

    @SerializedName("createdAt")
    private String createdAt;

    @SerializedName("firstName")
    private String firstName;

    @SerializedName("productImages")
    private List<ProductImagesItem> productImages;

    @SerializedName("sub_cat_name")
    private String subCatName;

    @SerializedName("CatName")
    private String catName;

    @SerializedName("username")
    private String username;
    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    @SerializedName("quantity")
    private String quantity;

    @SerializedName("userImage")
    private String userImage;

    @SerializedName("deleteStatus")
    private int deleteStatus;

    @SerializedName("name")
    private String name;

    @SerializedName("currency")
    private String currency;

    @SerializedName("location")
    private String location;

    @SerializedName("id")
    private String id;

    @SerializedName("longitude")
    private String longitude;

    @SerializedName("status")
    private int status;

    public int getProductType() {
        return productType;
    }

    public void setProductType(int productType) {
        this.productType = productType;
    }

    @SerializedName("productType")
    private int productType;

    @SerializedName("updatedAt")
    private String updatedAt;

 /*   @SerializedName("getCompanyInfo")
    private GetCompanyInfo getCompanyInfo;

    public GetCompanyInfo getGetCompanyInfo() {
        return getCompanyInfo;
    }

    public void setGetCompanyInfo(GetCompanyInfo getCompanyInfo) {
        this.getCompanyInfo = getCompanyInfo;
    }*/

    public String getCommision() {
        return commision;
    }

    public void setCommision(String commision) {
        this.commision = commision;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public float getCost() {
        return cost;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public int getRole() {
        return role;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMobile() {
        return mobile;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public void setSubcatId(int subcatId) {
        this.subcatId = subcatId;
    }

    public int getSubcatId() {
        return subcatId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public int getCatId() {
        return catId;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setProductImages(List<ProductImagesItem> productImages) {
        this.productImages = productImages;
    }

    public List<ProductImagesItem> getProductImages() {
        return productImages;
    }

    public void setSubCatName(String subCatName) {
        this.subCatName = subCatName;
    }

    public String getSubCatName() {
        return subCatName;
    }

    public void setDeleteStatus(int deleteStatus) {
        this.deleteStatus = deleteStatus;
    }

    public int getDeleteStatus() {
        return deleteStatus;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocation() {
        return location;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public String toString() {
        return
                "DataItem{" +
                        "cost = '" + cost + '\'' +
                        ",role = '" + role + '\'' +
                        ",category_name = '" + categoryName + '\'' +
                        ",latitude = '" + latitude + '\'' +
                        ",mobile = '" + mobile + '\'' +
                        ",description = '" + description + '\'' +
                        ",title = '" + title + '\'' +
                        ",userId = '" + userId + '\'' +
                        ",subcatId = '" + subcatId + '\'' +
                        ",catId = '" + catId + '\'' +
                        ",createdAt = '" + createdAt + '\'' +
                        ",firstName = '" + firstName + '\'' +
                        ",productImages = '" + productImages + '\'' +
                        ",sub_cat_name = '" + subCatName + '\'' +
                        ",deleteStatus = '" + deleteStatus + '\'' +
                        ",name = '" + name + '\'' +
                        ",currency = '" + currency + '\'' +
                        ",location = '" + location + '\'' +
                        ",id = '" + id + '\'' +
                        ",longitude = '" + longitude + '\'' +
                        ",status = '" + status + '\'' +
                        ",updatedAt = '" + updatedAt + '\'' +
                        "}";
    }
}