package com.stagemarket.pojo.viewOtherProfile;

import com.google.gson.annotations.SerializedName;

public class UserProfile{

	@SerializedName("deviceType")
	private int deviceType;

	@SerializedName("lastName")
	private String lastName;

	@SerializedName("emailToken")
	private String emailToken;

	@SerializedName("role")
	private int role;

	@SerializedName("gender")
	private String gender;

	@SerializedName("image_url")
	private String imageUrl;

	@SerializedName("mobile")
	private String mobile;

	@SerializedName("blockStatus")
	private int blockStatus;

	@SerializedName("deviceToken")
	private String deviceToken;

	@SerializedName("firstName")
	private String firstName;

	@SerializedName("resetPasswordToken")
	private String resetPasswordToken;

	@SerializedName("createdAt")
	private String createdAt;

	@SerializedName("password")
	private String password;

	@SerializedName("emailStatus")
	private int emailStatus;

	@SerializedName("dob")
	private String dob;

	@SerializedName("visiblePwd")
	private String visiblePwd;

	@SerializedName("fbId")
	private String fbId;

	@SerializedName("id")
	private int id;

	@SerializedName("userType")
	private int userType;

	@SerializedName("remember_token")
	private String rememberToken;

	@SerializedName("email")
	private String email;

	@SerializedName("updatedAt")
	private String updatedAt;

	public void setDeviceType(int deviceType){
		this.deviceType = deviceType;
	}

	public int getDeviceType(){
		return deviceType;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setEmailToken(String emailToken){
		this.emailToken = emailToken;
	}

	public String getEmailToken(){
		return emailToken;
	}

	public void setRole(int role){
		this.role = role;
	}

	public int getRole(){
		return role;
	}

	public void setGender(String gender){
		this.gender = gender;
	}

	public String getGender(){
		return gender;
	}

	public void setImageUrl(String imageUrl){
		this.imageUrl = imageUrl;
	}

	public String getImageUrl(){
		return imageUrl;
	}

	public void setMobile(String mobile){
		this.mobile = mobile;
	}

	public String getMobile(){
		return mobile;
	}

	public void setBlockStatus(int blockStatus){
		this.blockStatus = blockStatus;
	}

	public int getBlockStatus(){
		return blockStatus;
	}

	public void setDeviceToken(String deviceToken){
		this.deviceToken = deviceToken;
	}

	public String getDeviceToken(){
		return deviceToken;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setResetPasswordToken(String resetPasswordToken){
		this.resetPasswordToken = resetPasswordToken;
	}

	public String getResetPasswordToken(){
		return resetPasswordToken;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setEmailStatus(int emailStatus){
		this.emailStatus = emailStatus;
	}

	public int getEmailStatus(){
		return emailStatus;
	}

	public void setDob(String dob){
		this.dob = dob;
	}

	public String getDob(){
		return dob;
	}

	public void setVisiblePwd(String visiblePwd){
		this.visiblePwd = visiblePwd;
	}

	public String getVisiblePwd(){
		return visiblePwd;
	}

	public void setFbId(String fbId){
		this.fbId = fbId;
	}

	public String getFbId(){
		return fbId;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setUserType(int userType){
		this.userType = userType;
	}

	public int getUserType(){
		return userType;
	}

	public void setRememberToken(String rememberToken){
		this.rememberToken = rememberToken;
	}

	public String getRememberToken(){
		return rememberToken;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	@Override
 	public String toString(){
		return 
			"UserProfile{" + 
			"deviceType = '" + deviceType + '\'' + 
			",lastName = '" + lastName + '\'' + 
			",emailToken = '" + emailToken + '\'' + 
			",role = '" + role + '\'' + 
			",gender = '" + gender + '\'' + 
			",image_url = '" + imageUrl + '\'' + 
			",mobile = '" + mobile + '\'' + 
			",blockStatus = '" + blockStatus + '\'' + 
			",deviceToken = '" + deviceToken + '\'' + 
			",firstName = '" + firstName + '\'' + 
			",resetPasswordToken = '" + resetPasswordToken + '\'' + 
			",createdAt = '" + createdAt + '\'' + 
			",password = '" + password + '\'' + 
			",emailStatus = '" + emailStatus + '\'' + 
			",dob = '" + dob + '\'' + 
			",visiblePwd = '" + visiblePwd + '\'' + 
			",fbId = '" + fbId + '\'' + 
			",id = '" + id + '\'' + 
			",userType = '" + userType + '\'' + 
			",remember_token = '" + rememberToken + '\'' + 
			",email = '" + email + '\'' + 
			",updatedAt = '" + updatedAt + '\'' + 
			"}";
		}
}