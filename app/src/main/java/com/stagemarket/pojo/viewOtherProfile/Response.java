package com.stagemarket.pojo.viewOtherProfile;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Response{

	@SerializedName("result")
	private String result;

	@SerializedName("data")
	private ArrayList<DataItem> data;

	@SerializedName("message")
	private String message;

	@SerializedName("return")
	private int jsonMemberReturn;

	@SerializedName("userProfile")
	private UserProfile userProfile;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setData(ArrayList<DataItem> data){
		this.data = data;
	}

	public ArrayList<DataItem> getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setJsonMemberReturn(int jsonMemberReturn){
		this.jsonMemberReturn = jsonMemberReturn;
	}

	public int getJsonMemberReturn(){
		return jsonMemberReturn;
	}

	public void setUserProfile(UserProfile userProfile){
		this.userProfile = userProfile;
	}

	public UserProfile getUserProfile(){
		return userProfile;
	}

	@Override
 	public String toString(){
		return 
			"Response{" + 
			"result = '" + result + '\'' + 
			",data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",return = '" + jsonMemberReturn + '\'' + 
			",userProfile = '" + userProfile + '\'' + 
			"}";
		}
}