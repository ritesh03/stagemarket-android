package com.stagemarket.pojo.placeOrder;

import com.google.gson.annotations.SerializedName;

public class ProdData {


    @SerializedName("billingAddress")
    private String mBillingAddress;
    @SerializedName("billingAmount")
    private String mBillingAmount;
    @SerializedName("billingCity")
    private String mBillingCity;
    @SerializedName("billingContact")
    private String mBillingContact;
    @SerializedName("billingName")
    private String mBillingName;
    @SerializedName("buyerId")
    private String mBuyerId;

    public String getmQuantity() {
        return mQuantity;
    }

    public void setmQuantity(String mQuantity) {
        this.mQuantity = mQuantity;
    }

    @SerializedName("quantity")
    private String mQuantity;
    @SerializedName("categoryName")
    private String mCategoryName;
    @SerializedName("commision")
    private String mCommision;
    @SerializedName("cost")
    private String mCost;
    @SerializedName("createdAt")
    private String mCreatedAt;

    @SerializedName("deliveredDate")
    private String mDeliveredDate;
    @SerializedName("orderStatus")
    private Long mOrderStatus;
    @SerializedName("orderType")
    private String mOrderType;
    @SerializedName("orderedDate")
    private String mOrderedDate;
    @SerializedName("productId")
    private String mProductId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @SerializedName("orderId")
    private String orderId;
    @SerializedName("productName")
    private String mProductName;
    @SerializedName("sellerId")
    private String mSellerId;
    @SerializedName("status")
    private Long mStatus;
    @SerializedName("subCategoryName")
    private String mSubCategoryName;
    @SerializedName("updatedAt")
    private String mUpdatedAt;
    @SerializedName("vat")
    private String mVat;

    public String getmBillingAddress() {
        return mBillingAddress;
    }

    public void setmBillingAddress(String mBillingAddress) {
        this.mBillingAddress = mBillingAddress;
    }

    public String getmBillingAmount() {
        return mBillingAmount;
    }

    public void setmBillingAmount(String mBillingAmount) {
        this.mBillingAmount = mBillingAmount;
    }

    public String getmBillingCity() {
        return mBillingCity;
    }

    public void setmBillingCity(String mBillingCity) {
        this.mBillingCity = mBillingCity;
    }

    public String getmBillingContact() {
        return mBillingContact;
    }

    public void setmBillingContact(String mBillingContact) {
        this.mBillingContact = mBillingContact;
    }

    public String getmBillingName() {
        return mBillingName;
    }

    public void setmBillingName(String mBillingName) {
        this.mBillingName = mBillingName;
    }

    public String getmBuyerId() {
        return mBuyerId;
    }

    public void setmBuyerId(String mBuyerId) {
        this.mBuyerId = mBuyerId;
    }

    public String getmCategoryName() {
        return mCategoryName;
    }

    public void setmCategoryName(String mCategoryName) {
        this.mCategoryName = mCategoryName;
    }

    public String getmCommision() {
        return mCommision;
    }

    public void setmCommision(String mCommision) {
        this.mCommision = mCommision;
    }

    public String getmCost() {
        return mCost;
    }

    public void setmCost(String mCost) {
        this.mCost = mCost;
    }

    public String getmCreatedAt() {
        return mCreatedAt;
    }

    public void setmCreatedAt(String mCreatedAt) {
        this.mCreatedAt = mCreatedAt;
    }

    public String getmDeliveredDate() {
        return mDeliveredDate;
    }

    public void setmDeliveredDate(String mDeliveredDate) {
        this.mDeliveredDate = mDeliveredDate;
    }

    public Long getmOrderStatus() {
        return mOrderStatus;
    }

    public void setmOrderStatus(Long mOrderStatus) {
        this.mOrderStatus = mOrderStatus;
    }

    public String getmOrderType() {
        return mOrderType;
    }

    public void setmOrderType(String mOrderType) {
        this.mOrderType = mOrderType;
    }

    public String getmOrderedDate() {
        return mOrderedDate;
    }

    public void setmOrderedDate(String mOrderedDate) {
        this.mOrderedDate = mOrderedDate;
    }

    public String getmProductId() {
        return mProductId;
    }

    public void setmProductId(String mProductId) {
        this.mProductId = mProductId;
    }

    public String getmProductName() {
        return mProductName;
    }

    public void setmProductName(String mProductName) {
        this.mProductName = mProductName;
    }

    public String getmSellerId() {
        return mSellerId;
    }

    public void setmSellerId(String mSellerId) {
        this.mSellerId = mSellerId;
    }

    public Long getmStatus() {
        return mStatus;
    }

    public void setmStatus(Long mStatus) {
        this.mStatus = mStatus;
    }

    public String getmSubCategoryName() {
        return mSubCategoryName;
    }

    public void setmSubCategoryName(String mSubCategoryName) {
        this.mSubCategoryName = mSubCategoryName;
    }

    public String getmUpdatedAt() {
        return mUpdatedAt;
    }

    public void setmUpdatedAt(String mUpdatedAt) {
        this.mUpdatedAt = mUpdatedAt;
    }

    public String getmVat() {
        return mVat;
    }

    public void setmVat(String mVat) {
        this.mVat = mVat;
    }
}
