
package com.stagemarket.pojo.placeOrder;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Data {

    @SerializedName("data")
    private ProdData mData;
    @SerializedName("payment_url")
    private String mPaymentUrl;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @SerializedName("orderId")
    private String orderId ;


    public ProdData getmData() {
        return mData;
    }

    public void setmData(ProdData mData) {
        this.mData = mData;
    }

    public String getPaymentUrl() {
        return mPaymentUrl;
    }

    public void setPaymentUrl(String paymentUrl) {
        mPaymentUrl = paymentUrl;
    }



}
