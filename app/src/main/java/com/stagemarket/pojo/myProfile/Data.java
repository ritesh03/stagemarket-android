
package com.stagemarket.pojo.myProfile;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Data {

    @SerializedName("blockStatus")
    private String mBlockStatus;
    @SerializedName("createdAt")
    private String mCreatedAt;
    @SerializedName("deviceToken")
    private String mDeviceToken;
    @SerializedName("deviceType")
    private String mDeviceType;
    @SerializedName("dob")
    private String mDob;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("emailStatus")
    private String mEmailStatus;
    @SerializedName("emailToken")
    private String mEmailToken;
    @SerializedName("fbId")
    private String mFbId;
    @SerializedName("firstName")
    private String mFirstName;
    @SerializedName("gender")
    private String mGender;
    @SerializedName("id")
    private String mId;
    @SerializedName("image_url")
    private String mImageUrl;
    @SerializedName("lastName")
    private String mLastName;
    @SerializedName("mobile")
    private String mMobile;
    @SerializedName("password")
    private String mPassword;
    @SerializedName("remember_token")
    private String mRememberToken;
    @SerializedName("resetPasswordToken")
    private String mResetPasswordToken;
    @SerializedName("role")
    private String mRole;
    @SerializedName("updatedAt")
    private String mUpdatedAt;
    @SerializedName("userType")
    private String mUserType;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyWebsite() {
        return companyWebsite;
    }

    public void setCompanyWebsite(String companyWebsite) {
        this.companyWebsite = companyWebsite;
    }

    @SerializedName("companyName")
    private String companyName;
    @SerializedName("companyAddress")
    private String companyAddress;
    @SerializedName("companyWebsite")
    private String companyWebsite;

    public String getOrganizationType() {
        return organizationType;
    }

    public void setOrganizationType(String organizationType) {
        this.organizationType = organizationType;
    }

    @SerializedName("organizationType")
    private String organizationType;
    @SerializedName("visiblePwd")
    private String mVisiblePwd;

    public String getBlockStatus() {
        return mBlockStatus;
    }

    public void setBlockStatus(String blockStatus) {
        mBlockStatus = blockStatus;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public String getDeviceToken() {
        return mDeviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        mDeviceToken = deviceToken;
    }

    public String getDeviceType() {
        return mDeviceType;
    }

    public void setDeviceType(String deviceType) {
        mDeviceType = deviceType;
    }

    public String getDob() {
        return mDob;
    }

    public void setDob(String dob) {
        mDob = dob;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getEmailStatus() {
        return mEmailStatus;
    }

    public void setEmailStatus(String emailStatus) {
        mEmailStatus = emailStatus;
    }

    public String getEmailToken() {
        return mEmailToken;
    }

    public void setEmailToken(String emailToken) {
        mEmailToken = emailToken;
    }

    public String getFbId() {
        return mFbId;
    }

    public void setFbId(String fbId) {
        mFbId = fbId;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getGender() {
        return mGender;
    }

    public void setGender(String gender) {
        mGender = gender;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public String getMobile() {
        return mMobile;
    }

    public void setMobile(String mobile) {
        mMobile = mobile;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getRememberToken() {
        return mRememberToken;
    }

    public void setRememberToken(String rememberToken) {
        mRememberToken = rememberToken;
    }

    public String getResetPasswordToken() {
        return mResetPasswordToken;
    }

    public void setResetPasswordToken(String resetPasswordToken) {
        mResetPasswordToken = resetPasswordToken;
    }

    public String getRole() {
        return mRole;
    }

    public void setRole(String role) {
        mRole = role;
    }

    public String getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        mUpdatedAt = updatedAt;
    }

    public String getUserType() {
        return mUserType;
    }

    public void setUserType(String userType) {
        mUserType = userType;
    }

    public String getVisiblePwd() {
        return mVisiblePwd;
    }

    public void setVisiblePwd(String visiblePwd) {
        mVisiblePwd = visiblePwd;
    }

}
