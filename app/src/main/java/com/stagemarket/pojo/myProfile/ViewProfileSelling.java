package com.stagemarket.pojo.myProfile;

public class ViewProfileSelling {

    private String id = "";
    private String selling_price = "";
    private String selling_product_name = "";
    private int image;

    public ViewProfileSelling(String id, String selling_price, String selling_product_name) {
        this.id = id;
        this.selling_price = selling_price;
        this.selling_product_name = selling_product_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getSelling_product_name() {
        return selling_product_name;
    }

    public void setSelling_product_name(String selling_product_name) {
        this.selling_product_name = selling_product_name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getSelling_price() {
        return selling_price;
    }

    public void setSelling_price(String selling_price) {
        this.selling_price = selling_price;
    }
}
