
package com.stagemarket.pojo.discover;

import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.stagemarket.pojo.myPosts.ProductImage;

@SuppressWarnings("unused")
public class ProductData {

    @SerializedName("catId")
    private Long mCatId;
    @SerializedName("companyId")
    private Long companyId;
    @SerializedName("cost")
    private float mCost;
    @SerializedName("createdAt")
    private String mCreatedAt;
    @SerializedName("currency")
    private String mCurrency;
    @SerializedName("deleteStatus")
    private Long mDeleteStatus;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("distance")
    private Float mDistance;
    @SerializedName("id")
    private String mId;
    @SerializedName("latitude")
    private String mLatitude;
    @SerializedName("location")
    private String mLocation;
    @SerializedName("longitude")
    private String mLongitude;
    @SerializedName("name")
    private String mName;
    @SerializedName("productImages")
    private List<ProductImage> mProductImages;
    @SerializedName("role")
    private Long mRole;
    @SerializedName("status")
    private Long mStatus;

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    @SerializedName("quantity")
    private String quantity;
    @SerializedName("CatName")
    private String CatName;
    @SerializedName("subCatName")
    private String subCatName;
    @SerializedName("commision")
    private String commision;

    public int getProductType() {
        return productType;
    }

    public void setProductType(int productType) {
        this.productType = productType;
    }

    @SerializedName("productType")
    private int productType;

    public String getWishlistStatus() {
        return WishlistStatus;
    }

    public void setWishlistStatus(String wishlistStatus) {
        WishlistStatus = wishlistStatus;
    }

    @SerializedName("WishlistStatus")
    private String WishlistStatus;
    @SerializedName("username")
    private String username;

    @SerializedName("userId")
    private String mUserId;
    @SerializedName("userImage")
    private String userImage;

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getCommision() {
        return commision;
    }

    public void setCommision(String commision) {
        this.commision = commision;
    }

    public List<ProductImage> getmProductImages() {
        return mProductImages;
    }

    public void setmProductImages(List<ProductImage> mProductImages) {
        this.mProductImages = mProductImages;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    @SerializedName("subcatId")

    private Long mSubcatId;
    @SerializedName("title")
    private String mTitle;
    @SerializedName("updatedAt")
    private String mUpdatedAt;

    public Float getmDistance() {
        return mDistance;
    }

    public void setmDistance(Float mDistance) {
        this.mDistance = mDistance;
    }

    public String getCatName() {
        return CatName;
    }

    public void setCatName(String catName) {
        CatName = catName;
    }

    public String getSubCatName() {
        return subCatName;
    }

    public void setSubCatName(String subCatName) {
        this.subCatName = subCatName;
    }


    public Long getCatId() {
        return mCatId;
    }

    public void setCatId(Long catId) {
        mCatId = catId;
    }

    public float getCost() {
        return mCost;
    }

    public void setCost(float cost) {
        mCost = cost;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public String getCurrency() {
        return mCurrency;
    }

    public void setCurrency(String currency) {
        mCurrency = currency;
    }

    public Long getDeleteStatus() {
        return mDeleteStatus;
    }

    public void setDeleteStatus(Long deleteStatus) {
        mDeleteStatus = deleteStatus;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }


    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public void setLatitude(String latitude) {
        mLatitude = latitude;
    }

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String location) {
        mLocation = location;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public void setLongitude(String longitude) {
        mLongitude = longitude;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }


    public Long getRole() {
        return mRole;
    }

    public void setRole(Long role) {
        mRole = role;
    }

    public Long getStatus() {
        return mStatus;
    }

    public void setStatus(Long status) {
        mStatus = status;
    }

    public Long getSubcatId() {
        return mSubcatId;
    }

    public void setSubcatId(Long subcatId) {
        mSubcatId = subcatId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        mUpdatedAt = updatedAt;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

}
