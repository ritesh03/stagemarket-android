package com.stagemarket.pojo.wishlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddWishlistResponse {
    @SerializedName("return")
    @Expose
    public Integer _return;
    @SerializedName("result")
    @Expose
    public String result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public Data data;

    public class Data {

        @SerializedName("status")
        @Expose
        public Integer status;

    }
}
