package com.stagemarket.activity.editAdDetail;

import android.app.Activity;
import android.content.Context;

import com.stagemarket.R;
import com.stagemarket.pojo.SaveLoginDetailsModel;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.retrofit.RestClient;
import com.stagemarket.utils.ApiGlobal;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.SharedPreff;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.disposables.CompositeDisposable;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.mime.TypedFile;


public class EditAdDetailPresenterImpl implements EditAdDetailPresenter {
    EditAdDetailView editAdDetailView;
    private final CompositeDisposable disposables = new CompositeDisposable();
    private final SaveLoginDetailsModel saveLoginDetailsModel;
    NetWorkingService netWorkingService;

    public EditAdDetailPresenterImpl(EditAdDetailView editAdDetailView) {
        this.editAdDetailView = editAdDetailView;
        this.netWorkingService = netWorkingService;
        saveLoginDetailsModel = SharedPreff.getLoginDetailsModel();

    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        if (disposables != null) {
            disposables.clear();
        }
        editAdDetailView = null;
        netWorkingService = null;
    }

    @Override
    public void onSubmitAdClick(String title, String price, String description
            , List<String> productImages, String productId,String qty,int productType) {
        if (title.isEmpty()) {
            CommonUtils.showTopSnackBar((Context) editAdDetailView, ((Activity) editAdDetailView).findViewById(android.R.id.content)
                    , ((Activity) editAdDetailView).getResources().getString(R.string.please_enter_title));
        } else if (price.isEmpty()) {
            CommonUtils.showTopSnackBar((Context) editAdDetailView, ((Activity) editAdDetailView).findViewById(android.R.id.content)
                    , ((Activity) editAdDetailView).getResources().getString(R.string.please_enter_price));
        } else if (description.isEmpty()) {
            CommonUtils.showTopSnackBar((Context) editAdDetailView, ((Activity) editAdDetailView).findViewById(android.R.id.content)
                    , ((Activity) editAdDetailView).getResources().getString(R.string.please_enter_description));
        } else if (qty.isEmpty()) {
            CommonUtils.showTopSnackBar((Context) editAdDetailView, ((Activity) editAdDetailView).findViewById(android.R.id.content)
                    , ((Activity) editAdDetailView).getResources().getString(R.string.please_enter_quantity));
        }else if (Integer.parseInt(qty)==0) {
            CommonUtils.showTopSnackBar((Context) editAdDetailView, ((Activity) editAdDetailView).findViewById(android.R.id.content)
                    , "Quantity must be greater than zero.");
        } else {
            if (CommonUtils.isConnectedToInternet(((Context) editAdDetailView))) {
                editAdDetailView.showProgressDialog();

                Map<String, TypedFile> files = new HashMap<>();
                for (int i = 0; i < productImages.size(); i++) {
                    File file = new File(productImages.get(i));
                    files.put("image[" + i + "]", new TypedFile("multipart/form-data", file));
                }

                RestClient.get(60).updateProduct(files, saveLoginDetailsModel.getId()
                        ,
                        description, price, title,qty, productId,productType
                        , new retrofit.Callback<ApiGlobal>() {
                            @Override
                            public void success(ApiGlobal apiGlobal, retrofit.client.Response response) {
                                if (editAdDetailView != null) {
                                    editAdDetailView.hideProgressDialog();
                                }
                                if (apiGlobal.getResult().equalsIgnoreCase(Constants.SUCCESS)) {
                                    editAdDetailView.showMessageDialog(((Context) editAdDetailView).getString(R.string.ad_updated));
                                } else {
                                    editAdDetailView.showErrorDialog(apiGlobal.getMessage());
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                if (editAdDetailView != null) {
                                    editAdDetailView.hideProgressDialog();
                                    editAdDetailView.showErrorDialog(((Context) editAdDetailView)
                                            .getResources().getString(R.string.something_went_wrong));
                                }
                            }
                        });
            } else {
                CommonUtils.check_your_network_available((Context) editAdDetailView);
            }
        }
    }
}
