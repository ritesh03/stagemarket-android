package com.stagemarket.activity.editAdDetail;

import com.stagemarket.CommonPresenter;

import java.util.List;

public interface EditAdDetailPresenter  extends CommonPresenter {
    void onSubmitAdClick(String title, String price, String description, List<String> productImages,
                          String productId,String qty,int productType);
}
