package com.stagemarket.activity.editAdDetail;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stagemarket.R;
import com.stagemarket.activity.categories.CategoriesActivity;
import com.stagemarket.activity.changePassword.ChangePasswordActivity;
import com.stagemarket.activity.home.HomeActivity;
import com.stagemarket.activity.sell.categorySell.ad_details_sell.AdDetaillsSellActivity;
import com.stagemarket.activity.sell.categorySell.ad_details_sell.ViewPagerAdapterSell;
import com.stagemarket.activity.sell.categorySell.camera.CameraFragmentMainActivity;
import com.stagemarket.activity.sell.categorySell.category_sell.CategorySellActivity;
import com.stagemarket.activity.sell.categorySell.select_category_sell.SelectCategorySellActivity;
import com.stagemarket.adapter.ViewPagerAdapter;
import com.stagemarket.pojo.discover.LatLongBean;
import com.stagemarket.pojo.myPosts.Datum;
import com.stagemarket.pojo.myPosts.ProductImage;
import com.stagemarket.utils.CommonListeners;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.DialogPopUps;
import com.stagemarket.utils.SharedPreff;

import org.greenrobot.eventbus.EventBus;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EditAdDetailActivity extends AppCompatActivity implements EditAdDetailView {

    @BindView(R.id.title_name)
    TextView titleName;
    private static final int IMAGES_REQUEST = 1000;
    @BindView(R.id.img_header_back)
    ImageView imgHeaderBack;
    @BindView(R.id.right_text)
    TextView rightText;
    @BindView(R.id.header)
    View header;
    @BindView(R.id.tv_locationName)
    TextView tv_locationName;
    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.img_slider)
    ImageView imgSlider;
    @BindView(R.id.left_text)
    TextView leftText;
    @BindView(R.id.relyt)
    RelativeLayout relyt;
    @BindView(R.id.price_value)
    EditText priceValue;
    @BindView(R.id.qty_value)
    EditText qty_value;
    @BindView(R.id.tv_product_description)
    EditText tvProductDescription;
    private ViewPagerAdapter viewPagerAdapter;
    @BindView(R.id.left_nav)
    ImageView leftNav;
    @BindView(R.id.right_nav)
    ImageView rightNav;
    @BindView(R.id.tv_categoryName)
    TextView tv_categoryName;
    @BindView(R.id.et_name)
    EditText et_name;
    @BindView(R.id.tv_photos_count)
    TextView tv_photos_count;
    @BindView(R.id.lytPhotosCount)
    LinearLayout lytPhotosCount;
    @BindView(R.id.tv_edit_photo)
    TextView tv_edit_photo;
    @BindView(R.id.lyt_main)
    FrameLayout lyt_main;
    @BindView(R.id.radio_group)
    RadioGroup radio_group;
    private String productDetail;
    private Datum prodDetail;
    private ArrayList<ProductImage> mArrayListProducts = new ArrayList<>();
    EditAdDetailPresenter editAdDetailPresenter;
    private String longg = "", lat = "";
    private List<String> productImages = new ArrayList<>();
   int ProductType = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_ad_detail);

        ButterKnife.bind(this);
        initToolbar();

        editAdDetailPresenter = new EditAdDetailPresenterImpl(this);
        if (getIntent().getExtras() != null) {
            productDetail = getIntent().getStringExtra(Constants.PRODUCT_DATA);
            Gson gson = new Gson();
            Type type = new TypeToken<Datum>() {
            }.getType();
            prodDetail = gson.fromJson(productDetail, type);

            if(prodDetail.getProductImages().size()!=0){

                new GetBitmap().execute();
            }


        }

        radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.btn_new_visit:
                        ProductType = 1;
                        break;
                    case R.id.btn_new_consult:
                        ProductType = 2;
                        break;
                }
            }
        });
        initViews(prodDetail);
    }

    public class GetBitmap extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {

            for (int i = 0; i < prodDetail.getProductImages().size(); i++) {
                Bitmap b = getBitmapFromURL(Constants.IMAGE_BASE_URL
                        + prodDetail.getProductImages().get(i).getImage());

                // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                if(b!=null)
                {
                Uri tempUri = getImageUri(getApplicationContext(), b);

                // CALL THIS METHOD TO GET THE ACTUAL PATH
                File finalFile = new File(getRealPathFromURI(tempUri));
                productImages.add(CommonUtils.getPath(EditAdDetailActivity.this, tempUri));}
            }

            return null;
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void initToolbar() {
        header.setBackgroundColor(ContextCompat.getColor(this, R.color.greynew));
        imgHeaderBack.setVisibility(View.GONE);
        imgHeaderBack.setImageResource(R.drawable.ic_back);
        DrawableCompat.setTint(imgHeaderBack.getDrawable(), ContextCompat.getColor(this, R.color.black));

        leftText.setVisibility(View.VISIBLE);
        leftText.setText(R.string.cancel);
        leftText.setTextColor(ContextCompat.getColor(EditAdDetailActivity.this, R.color.black));

        rightText.setVisibility(View.VISIBLE);
        rightText.setText(R.string.update_ad);
        rightText.setTextColor(ContextCompat.getColor(EditAdDetailActivity.this, R.color.black));

        titleName.setText(R.string.edit);
        titleName.setTextColor(ContextCompat.getColor(this, R.color.black));
    }

    @OnClick(R.id.left_text)
    public void onCancelAdClick() {
        finish();
    }

    @OnClick(R.id.right_text)
    public void onSubmitAdClick() {

        if (CommonUtils.isConnectedToInternet(EditAdDetailActivity.this)) {
            String title = et_name.getText().toString().trim();
        String price = priceValue.getText().toString().trim();
        String desc = tvProductDescription.getText().toString().trim();
        String qty = qty_value.getText().toString().trim();

        editAdDetailPresenter.onSubmitAdClick(title, price, desc, productImages,
                prodDetail.getId(),qty,ProductType);
    } else {
        CommonUtils.showTopSnackBar(EditAdDetailActivity.this,
                findViewById(android.R.id.content), getString(R.string.please_check_internet));

    }
    }

    private void setUpViewPager() {
        leftNav.setVisibility(View.GONE);
        viewPagerAdapter = new ViewPagerAdapter(this, mArrayListProducts,"");
        viewpager.setAdapter(viewPagerAdapter);
    }


    @OnClick(R.id.tv_edit_photo)
    public void onEditPhotoClick() {
        if(productImages.size()!=0){
            Intent intent = new Intent(this, CameraFragmentMainActivity.class)
                    .putExtra(Constants.PRODUCT_IMAGES, new Gson().toJson(productImages));
            startActivityForResult(intent, IMAGES_REQUEST);
        }
    }

    private void initViews(Datum prodDetail) {
        tv_edit_photo.setVisibility(View.VISIBLE);
        if (prodDetail != null) {
            tv_photos_count.setText(prodDetail.getProductImages().size() + getString(R.string.photos));
            if(prodDetail.getProductImages().size()!=0){
                viewpager.setVisibility(View.VISIBLE);
                mArrayListProducts.addAll(prodDetail.getProductImages());

                if (mArrayListProducts.size() == 1) {
                    lytPhotosCount.setVisibility(View.GONE);
                    leftNav.setVisibility(View.GONE);
                    rightNav.setVisibility(View.GONE);
                } else {
                    lytPhotosCount.setVisibility(View.VISIBLE);
                    leftNav.setVisibility(View.GONE);
                    rightNav.setVisibility(View.VISIBLE);
                }

            }else{

                lyt_main.setBackgroundResource(R.drawable.not_available);
                viewpager.setVisibility(View.GONE);
                rightNav.setVisibility(View.GONE);
            }
            setUpViewPager();

            priceValue.setText("" + prodDetail.getCost());
            qty_value.setText("" + prodDetail.getQuantity());
            et_name.setText(prodDetail.getTitle() /*+ getString(R.string.for_sale)*/);
            et_name.setSelection(et_name.getText().toString().trim().length());
            tv_categoryName.setText(getString(R.string.category_product_title) + prodDetail.getCategoryName());
            tvProductDescription.setText(prodDetail.getDescription());
            tv_locationName.setText(prodDetail.getLocation());


            viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {

                    if (position == 0) {
                        leftNav.setVisibility(View.GONE);
                        rightNav.setVisibility(View.VISIBLE);
                    } else if (position >= 1 && position < mArrayListProducts.size() - 1) {
                        leftNav.setVisibility(View.VISIBLE);
                        rightNav.setVisibility(View.VISIBLE);
                    } else if (position == 1 && mArrayListProducts.size() == 2) {
                        leftNav.setVisibility(View.VISIBLE);
                        rightNav.setVisibility(View.GONE);
                    } else {
                        rightNav.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });
        }

    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        CommonUtils.hideSoftKeyboard(EditAdDetailActivity.this);
        return super.dispatchTouchEvent(ev);

    }

    @OnClick(R.id.left_nav)
    public void OnLeftNavClick() {
        rightNav.setVisibility(View.VISIBLE);
        int tab = viewpager.getCurrentItem();
        if (tab == 1) {
            leftNav.setVisibility(View.GONE);
        }
        if (tab > 0) {
            tab--;
            viewpager.setCurrentItem(tab);
        } else if (tab == 0) {
            viewpager.setCurrentItem(tab);
        }
    }

    @OnClick(R.id.img_header_back)
    public void OnImageBackClick() {
        finish();
    }

    @OnClick(R.id.right_nav)
    public void OnRightNavClick() {
        leftNav.setVisibility(View.VISIBLE);
        int tab = viewpager.getCurrentItem();
        tab++;
        viewpager.setCurrentItem(tab);

        if (tab == mArrayListProducts.size() - 1) {
            rightNav.setVisibility(View.GONE);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void showProgressDialog() {
        DialogPopUps.showProgressDialog(this, getResources().getString(R.string.please_wait));
    }

    @Override
    public void showErrorDialog(String msg) {
        CommonUtils.showTopSnackBar(this, findViewById(android.R.id.content), msg);

    }

    @Override
    public void showMessageDialog(String msg) {
        msg = getString(R.string.ads_updated);
        DialogPopUps.alertPopUp(this, msg, "",
                getResources().getString(R.string.ok)
                , new CommonListeners.AlertCallBackInterface() {
                    @Override
                    public void neutralClick() {
                        Intent splash = new Intent(EditAdDetailActivity.this, HomeActivity.class);
                        splash.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(splash);
                        finish();
                    }
                });

    }

    @Override
    public void hideProgressDialog() {
        DialogPopUps.hideDialog();
    }

    int PLACE_PICKER_REQUEST = 1;

    @OnClick(R.id.lytChangeLocation)
    public void onChangeLocationClick() {
       /* try {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }*/
    }

  /*  @OnClick(R.id.tv_categoryName)
    public void onCategoryClick() {
        startActivity(new Intent(this, CategoriesActivity.class));
    }
*/

    @Override
    protected void onResume() {
        super.onResume();

        if (SharedPreff.getCatName() != null) {
            tv_categoryName.setText(SharedPreff.getCatName().getName() + " > " + SharedPreff.getSubCatName().getSubCatName());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);
                getLocationAddress(place.getLatLng().latitude, place.getLatLng().longitude, false);
                /*SharedPreff.saveLng(String.valueOf(place.getLatLng().longitude));
                SharedPreff.saveLat(String.valueOf(place.getLatLng().latitude));
*/
                lat = String.valueOf(place.getLatLng().latitude);
                longg = String.valueOf(place.getLatLng().longitude);

                SharedPreff.saveTempLat(String.valueOf(place.getLatLng().latitude));
                SharedPreff.saveTempLng(String.valueOf(place.getLatLng().longitude));
                EventBus.getDefault().post(new LatLongBean(place.getLatLng().latitude, place.getLatLng().longitude));
            }
        } else if (requestCode == IMAGES_REQUEST) {
            if (resultCode == RESULT_CANCELED) {
                return;
            } else if (resultCode == RESULT_OK) {
                String prodImages = data.getStringExtra(Constants.PRODUCT_IMAGES);

                Gson gson = new Gson();
                Type type = new TypeToken<ArrayList<String>>() {
                }.getType();
                List<String> list = gson.fromJson(prodImages, type);
                productImages.clear();
                productImages.addAll(list);

mArrayListProducts.clear();
for (int i=0;i<list.size();i++) {
    ProductImage productImage = new ProductImage();

    productImage.setImage(list.get(i));



    mArrayListProducts.add(productImage);
}
                Log.e("list",mArrayListProducts.get(0).getImage()+"");

                viewPagerAdapter = new ViewPagerAdapter(this, mArrayListProducts,"edit");
                viewpager.setAdapter(viewPagerAdapter);
            }
        }
    }

    private void getLocationAddress(double lat, double lng, boolean isCurrent) {
        Address address = CommonUtils.getAddressObjectFromLocation(lat, lng, this);
        StringBuilder sb = new StringBuilder();
        sb.append(address.getAddressLine(0)).append("\n");
        tv_locationName.setText(sb.toString());
    }

}
