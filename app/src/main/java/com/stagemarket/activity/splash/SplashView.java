package com.stagemarket.activity.splash;

import com.stagemarket.BasePresenter;

public interface SplashView extends BasePresenter {

    public void goToNextScreen();
    public void showLoginView();

    void moveToDrawerActivity();

    void moveToCreateAccountActivity();


}
