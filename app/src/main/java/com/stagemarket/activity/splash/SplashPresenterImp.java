package com.stagemarket.activity.splash;

import com.stagemarket.pojo.SaveLoginDetailsModel;
import com.stagemarket.pojo.login.LoginResponse;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.SharedPreff;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SplashPresenterImp implements SplashPresenter {


    private static final String TAG = SplashPresenterImp.class.getSimpleName();
    private SplashView splashView = null;
    private NetWorkingService netWorkingService;
    private final CompositeDisposable disposables = new CompositeDisposable();
    SaveLoginDetailsModel saveLoginDetailsModel;

    public SplashPresenterImp(SplashView splashView, NetWorkingService netWorkingService) {
        this.splashView = splashView;
        this.netWorkingService = netWorkingService;
        saveLoginDetailsModel = SharedPreff.getLoginDetailsModel();
    }


    @Override
    public void timerFunction() {

        getObservable().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Long>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Long aLong) {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

                if (saveLoginDetailsModel != null) {
                    splashView.goToNextScreen();
                } else {
                    // SharedPreff.clearPreferences();
                    if (splashView != null) {
                        splashView.showLoginView();
                    }
                }

                //  splashView.showLoginView();

            }
        });
    }


    @Override
    public void makeSocialLoginRequest() {

    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        if (disposables != null) {
            disposables.clear();
        }
        splashView = null;
    }

    private Observable<? extends Long> getObservable() {
        return Observable.timer(3, TimeUnit.SECONDS);
    }


}
