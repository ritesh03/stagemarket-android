package com.stagemarket.activity.splash;

import com.stagemarket.CommonPresenter;

public interface SplashPresenter extends CommonPresenter {

    public void timerFunction();

    void makeSocialLoginRequest();


}
