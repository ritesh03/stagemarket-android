package com.stagemarket.activity.splash;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stagemarket.AppClass;
import com.stagemarket.BaseActivity;
import com.stagemarket.R;
import com.stagemarket.activity.home.HomeActivity;
import com.stagemarket.activity.login.LoginActivity;
import com.stagemarket.activity.product_detail.ProductDetailsActivity;
import com.stagemarket.pojo.discover.ProductData;
import com.stagemarket.pojo.viewOtherProfile.DataItem;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.DialogPopUps;
import com.stagemarket.utils.GPSTracker;
import com.stagemarket.utils.LocationAddress;
import com.stagemarket.utils.LocationTrackerClass;
import com.crashlytics.android.Crashlytics;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.fabric.sdk.android.Fabric;
import com.stagemarket.utils.SharedPreff;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Splash extends BaseActivity implements SplashView {

    private SplashPresenter splashPresenter;
    private NetWorkingService netWorkingService;
    final static int REQUEST_LOCATION = 199;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Fabric.with(this, new Crashlytics());
        getSignatures();
        initViews();
    }


    private void initViews() {
        netWorkingService = ((AppClass) getApplication()).getNetworkService();
        splashPresenter = new SplashPresenterImp(this, netWorkingService);

    }

    private void getSignatures() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.stagemarket",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    public void goToNextScreen() {

        Intent intent = new Intent(Splash.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void showLoginView() {
      /*  if (CommonUtils.isGpsEnabled(AppClass.getInstance())) {
            Intent intent = new Intent(Splash.this, LoginActivity.class);
            startActivity(intent);
            finish();
        } else {

        }*/
        Intent intent = new Intent(Splash.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }


    @Override
    public void moveToDrawerActivity() {

    }

    @Override
    public void moveToCreateAccountActivity() {

    }

    @Override
    public void showProgressDialog() {
        DialogPopUps.showProgressDialog(Splash.this, getResources().getString(R.string.please_wait));
    }

    @Override
    public void showErrorDialog(String msg) {
        DialogPopUps.alertPopUp(Splash.this, msg);
    }

    @Override
    public void showMessageDialog(String msg) {

    }

    @Override
    public void hideProgressDialog() {

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    //--
    private void enableLocation() {
        checkPermission();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int permissionCheck = ContextCompat.checkSelfPermission(Splash.this,
                    Manifest.permission.CAMERA);

            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                Log.e("permission", "granted");
                callLocation();
            } else {
                ActivityCompat.requestPermissions(Splash.this,
                        new String[]{Manifest.permission.CAMERA,
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
            }
        } else {
            Log.e("MainActivity", "Lower Than MarshMallow");
            callLocation();
        }

        // Todo Turn On GPS Automatically

        if (!hasGPSDevice(Splash.this)) {
            Toast.makeText(Splash.this, "Gps not Supported", Toast.LENGTH_SHORT).show();
        }
        final LocationManager manager = (LocationManager) Splash.this.getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice(Splash.this)) {
            Log.e("keshav", "Gps already enabled");
//            Toast.makeText(getActivity(),"Gps not enabled",Toast.LENGTH_SHORT).show();
            enableLoc();
        } else {
            Log.e("keshav", "Gps already enabled");
//            Toast.makeText(getActivity(),"Gps already enabled",Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        // enableLoc();
        splashPresenter.timerFunction();
    }

    private boolean hasGPSDevice(Context context) {
        final LocationManager mgr = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        if (mgr == null)
            return false;
        final List<String> providers = mgr.getAllProviders();
        if (providers == null)
            return false;
        return providers.contains(LocationManager.GPS_PROVIDER);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("onActivityResult()", Integer.toString(resultCode));

        //final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        switch (requestCode) {
            case REQUEST_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_OK: {
                        // All required changes were successfully made
                        Toast.makeText(Splash.this, "Location enabled by user!", Toast.LENGTH_LONG).show();
                        callLocation();
                        break;
                    }
                    case Activity.RESULT_CANCELED: {
                        // The user was asked to change settings, but chose not to
                        Toast.makeText(Splash.this, "Location not enabled, user cancelled.", Toast.LENGTH_LONG).show();

                        finish();

                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;
        }
    }

    public void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int permissionCheck = ContextCompat.checkSelfPermission(Splash.this,
                    Manifest.permission.CAMERA);

            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                //showing dialog to select image
                Log.e("keshav", "permission granted");
            } else {
                ActivityCompat.requestPermissions(Splash.this,
                        new String[]{Manifest.permission.CAMERA,
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
            }
        }
    }


    private GoogleApiClient googleApiClient;

    private void enableLoc() {
        // TODO Add this Dependency
        // compile 'com.google.android.gms:play-services-location:7.+'

        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(Splash.this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(Bundle bundle) {

                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                            googleApiClient.connect();
                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(ConnectionResult connectionResult) {
                            Log.d("Location error", "Location error " + connectionResult.getErrorCode());
                        }
                    }).build();
            googleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            builder.setAlwaysShow(true);

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();

                    Log.e("keshav", "status Called  -->" + status.getStatusCode());

                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            Log.e("keshav", "LocationSettingsStatusCodes.RESOLUTION_REQUIRED Called ....");
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(Splash.this, REQUEST_LOCATION);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SUCCESS:
                            callLocation();
                            break;
                    }
                }
            });
        }
    }

    GPSTracker gps;

    private void callLocation() {

        Log.e("keshav", "callLocation Called ... ");

        gps = new GPSTracker(Splash.this);

        // check if GPS enabled
        if (gps.canGetLocation()) {

            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();

            Log.e("MainActivity", "latitude -> " + latitude);
            Log.e("MainActivity", "longitude -> " + longitude);

            if (latitude != 0.0) {
                SharedPreff.saveLat(String.valueOf(latitude));
                SharedPreff.saveLng(String.valueOf(longitude));

            } else {

            }

            splashPresenter.timerFunction();
          //  showLoginView();
            //   LocationAddress locationAddress = new LocationAddress();
         /*   locationAddress.getAddressFromLocation(latitude, longitude,
                    getApplicationContext(), new GeocoderHandler());*/

        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings


//            gps.showSettingsAlert();
            enableLoc();
        }
    }


}
