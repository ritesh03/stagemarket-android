package com.stagemarket.activity.myOrders;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.stagemarket.AppClass;
import com.stagemarket.R;
import com.stagemarket.activity.viewProfile.ViewProfileActivity;
import com.stagemarket.pojo.SaveLoginDetailsModel;
import com.stagemarket.pojo.discover.GetProductsResponse;
import com.stagemarket.pojo.myOrders.purchased.PurchasedResponse;
import com.stagemarket.pojo.wishlist.GetWishlistProductsResponse;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.SharedPreff;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MyOrdersPresenterImp implements MyOrdersPresenter {
    MyOrdersView myOrdersView;
    private NetWorkingService netWorkingService;
    private final CompositeDisposable disposables = new CompositeDisposable();
    Context mContext;
    SaveLoginDetailsModel saveLoginDetailsModel;

    public MyOrdersPresenterImp(MyOrdersView myOrdersView) {
        this.myOrdersView = myOrdersView;
        mContext = AppClass.getInstance().getApplicationContext();
        saveLoginDetailsModel = SharedPreff.getLoginDetailsModel();
        netWorkingService = AppClass.getInstance().getNetworkService();

    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        if (disposables != null) {
            disposables.clear();
        }
        myOrdersView = null;
        netWorkingService = null;
    }

    @Override
    public void getMyOrders(int current_page, final String orderType,String type) {
        if (CommonUtils.isConnectedToInternet(mContext)) {
            myOrdersView.showProgressDialog();
if(type.equalsIgnoreCase("orders")) {
    getMyorders(orderType, current_page);
}
else
{
getMywishlist(orderType, current_page);
}


        } else {
            CommonUtils.showTopSnackBar(mContext,
                    ((MyOrdersActivity) myOrdersView).findViewById(android.R.id.content),
                    mContext.getString(R.string.please_check_internet));
        }
    }



    public void getMyorders(final String orderType, int current_page)
    {
        netWorkingService.getAPI().getMyOrders(
                saveLoginDetailsModel.getId(),
                orderType,
                current_page
        )
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PurchasedResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposables.add(d);
                    }

                    @Override
                    public void onNext(PurchasedResponse purchasedResponse) {
                        myOrdersView.hideProgressDialog();

                        if (myOrdersView != null) {
                            myOrdersView.hideProgressDialog();
                            if (purchasedResponse.getResult().equalsIgnoreCase(Constants.SUCCESS)) {
                                myOrdersView.setPurchaseData(purchasedResponse.getData().getData(),orderType);
                            } else {
                                myOrdersView.showError(purchasedResponse.getMessage());
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        myOrdersView.hideProgressDialog();
                        myOrdersView.showErrorDialog(mContext.getResources().getString(R.string.something_went_wrong));
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    public void getMywishlist(final String orderType, int current_page)
    {
        Log.e("in",orderType+","+current_page);
        netWorkingService.getAPI().getWishlistProdcuts(
                saveLoginDetailsModel.getId(),

                current_page
        )
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<GetWishlistProductsResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposables.add(d);
                    }

                    @Override
                    public void onNext(GetWishlistProductsResponse purchasedResponse) {
                        myOrdersView.hideProgressDialog();
                        Gson gson=new Gson();
                        Log.e("rr",""+gson.toJson(purchasedResponse));
                        Log.e("in","response");
                        if (myOrdersView != null) {
                            myOrdersView.hideProgressDialog();
                            if (purchasedResponse.getResult().equalsIgnoreCase(Constants.SUCCESS)) {
                                Log.e("in","response2");
                                myOrdersView.setWislistData(purchasedResponse.getData().getProductData(),orderType);
                            } else {
                                myOrdersView.showError(purchasedResponse.getMessage());
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        myOrdersView.hideProgressDialog();
                        myOrdersView.showErrorDialog(mContext.getResources().getString(R.string.something_went_wrong));
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }


}



