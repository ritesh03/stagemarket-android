package com.stagemarket.activity.myOrders;

import com.stagemarket.BasePresenter;
import com.stagemarket.pojo.discover.ProductData;
import com.stagemarket.pojo.myOrders.purchased.Datum;

import java.util.ArrayList;
import java.util.List;

public interface MyOrdersView extends BasePresenter {
    void setPurchaseData(List<Datum> data, String orderType);
    void setWislistData(ArrayList<ProductData> mData, String orderType);
    void  showError(String msg);
}
