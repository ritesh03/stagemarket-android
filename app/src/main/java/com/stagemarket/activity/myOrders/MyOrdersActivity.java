package com.stagemarket.activity.myOrders;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.stagemarket.BaseActivity;
import com.stagemarket.R;
import com.stagemarket.adapter.PurchasedAdapter;
import com.stagemarket.adapter.SoldAdapter;
import com.stagemarket.pojo.discover.ProductData;
import com.stagemarket.pojo.myOrders.purchased.Datum;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyOrdersActivity extends BaseActivity implements MyOrdersView,
        PurchasedAdapter.CallBackBottomReach, SoldAdapter.CallBackBottomReach {

    @BindView(R.id.title_name)
    TextView titleName;
    @BindView(R.id.img_header_back)
    ImageView imgHeaderBack;
    @BindView(R.id.right_text)
    TextView rightText;
    @BindView(R.id.header)
    View header;
    @BindView(R.id.radio_purchased)
    RadioButton radioPurchased;
    @BindView(R.id.radio_sold)
    RadioButton radioSold;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.recyclerViewPurchased)
    RecyclerView recyclerViewPurchased;
    @BindView(R.id.recyclerViewSold)
    RecyclerView recyclerViewSold;
    @BindView(R.id.swipe_sold)
    SwipeRefreshLayout swipe_sold;
    @BindView(R.id.swipe_purchased)
    SwipeRefreshLayout swipe_purchased;
    @BindView(R.id.tv_no_orders)
      TextView tv_no_orders;

    PurchasedAdapter mPurchasedAdapter;
    SoldAdapter mSoldAdapter;

    ArrayList<Datum> mArrayList_myOrders = new ArrayList<>();
    ArrayList<ProductData> mArrayList_myWishlist = new ArrayList<>();
    // ArrayList<MyOrdersPojo> mArrayList_myOrders = new ArrayList<>();
    ArrayList<Datum> mArrayList_myOrders_Sold = new ArrayList<>();

    MyOrdersPresenter myOrdersPresenter;
    int current_page = 1;
    private String orderType = "1";
    private boolean hasMore = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders);
        ButterKnife.bind(this);

        swipe_purchased.setVisibility(View.VISIBLE);
        swipe_sold.setVisibility(View.GONE);
        recyclerViewSold.setVisibility(View.GONE);
        recyclerViewPurchased.setVisibility(View.VISIBLE);

        myOrdersPresenter = new MyOrdersPresenterImp(this);


        myOrdersPresenter.getMyOrders(current_page, orderType,getIntent().getStringExtra("type"));

        initToolbar();
        initRecyclerViewForpurchased(getIntent().getStringExtra("type"));



        initRecyclerViewForSold(getIntent().getStringExtra("type"));
        clickRadioButton();
    }

    @OnClick(R.id.img_header_back)
    public void onBackClick() {
        finish();
    }

    private void clickRadioButton() {
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                View radioButton = radioGroup.findViewById(checkedId);
                int idx = radioGroup.indexOfChild(radioButton);
                if (idx == 0) {
                    current_page = 1;
                    orderType = "1";
                    myOrdersPresenter.getMyOrders(current_page, orderType,getIntent().getStringExtra("type"));
                    swipe_purchased.setVisibility(View.VISIBLE);
                    swipe_sold.setVisibility(View.GONE);
                    recyclerViewSold.setVisibility(View.GONE);
                    recyclerViewPurchased.setVisibility(View.VISIBLE);
                } else {
                    // initRecyclerViewForSold();
                    current_page = 1;
                    orderType = "2";
                    swipe_purchased.setVisibility(View.GONE);
                    swipe_sold.setVisibility(View.VISIBLE);
                    myOrdersPresenter.getMyOrders(current_page, orderType,getIntent().getStringExtra("type"));
                    recyclerViewSold.setVisibility(View.VISIBLE);
                    recyclerViewPurchased.setVisibility(View.GONE);

                }
            }
        });
    }

    private void initRecyclerViewForSold(String type) {
        //initData1();
        mSoldAdapter = new SoldAdapter(this, mArrayList_myOrders_Sold, this);
        recyclerViewSold.setLayoutManager(new GridLayoutManager(MyOrdersActivity.this, 2));
        recyclerViewSold.setAdapter(mSoldAdapter);
    }


    private void initRecyclerViewForpurchased(String type) {
        //initData();

        if(type.equalsIgnoreCase("orders"))
        {
            mPurchasedAdapter = new PurchasedAdapter(MyOrdersActivity.this,this, mArrayList_myOrders, this);

            recyclerViewPurchased.setLayoutManager(new GridLayoutManager(MyOrdersActivity.this, 2));}
        else
        {
            mPurchasedAdapter = new PurchasedAdapter(MyOrdersActivity.this,this, mArrayList_myWishlist, this,tv_no_orders);

            recyclerViewPurchased.setLayoutManager(new GridLayoutManager(MyOrdersActivity.this, 1));
        }
        recyclerViewPurchased.setAdapter(mPurchasedAdapter);
    }

 /*   private void initData() {
        mArrayList_myOrders.add(new MyOrdersPojo("Order ID 5421345", "Akg and shure Microphone", "$89.99",
                "Delivered on 22nd July", "10:32 am", R.drawable.guitar));

        mArrayList_myOrders.add(new MyOrdersPojo("Order ID 4535321", "Electric Guitar Model", "$759.99",
                "Delivered on 22nd Aug", "9:32 am", R.drawable.guitar));


    }*/

   /* private void initData1() {
        mArrayList_myOrders_Sold.add(new MyOrdersPojo("Order ID 4521569", "Victrola Vintage 3 Sple", "$459.99",
                "Sold on 22nd July", "9:32 am", R.drawable.guitar));

        mArrayList_myOrders_Sold.add(new MyOrdersPojo("Order ID 4535321", "Hercules DJ instinct", "$289.99",
                "Sold on 22nd Aug", "10:32 am", R.drawable.guitar));

    }
*/

    private void initToolbar() {
        header.setBackgroundColor(ContextCompat.getColor(this, R.color.greynew));

        imgHeaderBack.setImageResource(R.drawable.ic_back);
        DrawableCompat.setTint(imgHeaderBack.getDrawable(), ContextCompat.getColor(this, R.color.black));
if(getIntent().getStringExtra("type").equalsIgnoreCase("orders")) {
    titleName.setText(R.string.my_orders);
}
else
{
    tv_no_orders.setText("No items found");
    titleName.setText("Wishlist");
}
        titleName.setTextColor(ContextCompat.getColor(this, R.color.black));

    }


    @Override
    public void showProgressDialog() {
        if (orderType.equalsIgnoreCase("1")) {
            swipe_purchased.setRefreshing(true);
        } else {
            swipe_sold.setRefreshing(true);
        }
    }

    @Override
    public void showErrorDialog(String msg) {
      /*  if (orderType == "1") {
            tv_no_orders.setVisibility(View.VISIBLE);
        } else {
            tv_no_orders.setVisibility(View.VISIBLE);
        }*/
    }

    @Override
    public void showMessageDialog(String msg) {

    }

    @Override
    public void hideProgressDialog() {
        if (orderType.equalsIgnoreCase("1")) {
            swipe_purchased.setRefreshing(false);
        } else {
            swipe_sold.setRefreshing(false);
        }
    }

    @Override
    public void setPurchaseData(List<Datum> data, String orderType) {
        if (orderType == "1") {
            swipe_purchased.setRefreshing(false);
            if (current_page == 1) {
                mArrayList_myOrders.clear();
            }
            mArrayList_myOrders.addAll(data);
            mPurchasedAdapter.notifyDataSetChanged();
            if(data.size()<10){
                hasMore=true;
            }

            if(getIntent().getStringExtra("type").equalsIgnoreCase("orders"))
            {

            }
            else
            {
                tv_no_orders.setText("No items found");
            }
            if (data.size() != 0) {
                tv_no_orders.setVisibility(View.GONE);
            } else {
                tv_no_orders.setVisibility(View.VISIBLE);
            }
        } else {
            if(getIntent().getStringExtra("type").equalsIgnoreCase("orders"))
            {

            }
            else
            {
                tv_no_orders.setText("No items found");
            }
            swipe_sold.setRefreshing(false);
            if (current_page == 1) {
                mArrayList_myOrders_Sold.clear();
            }
            mArrayList_myOrders_Sold.addAll(data);
            mSoldAdapter.notifyDataSetChanged();

            if (data.size() != 0) {
                tv_no_orders.setVisibility(View.GONE);
            } else {
                tv_no_orders.setVisibility(View.VISIBLE);
            }
        }
    }
    @Override
    public void setWislistData(ArrayList<ProductData> mData, String orderType) {

        swipe_purchased.setRefreshing(false);
        if (current_page == 1) {
            mArrayList_myWishlist.clear();
        }
        mArrayList_myWishlist.addAll(mData);
        mPurchasedAdapter.notifyDataSetChanged();
        if(mData.size()<10){
            hasMore=true;
        }
        if(getIntent().getStringExtra("type").equalsIgnoreCase("orders"))
        {

        }
        else
        {
            tv_no_orders.setText("No items found");
        }
        if (mData.size() != 0) {
            tv_no_orders.setVisibility(View.GONE);
        } else {
            tv_no_orders.setVisibility(View.VISIBLE);
            tv_no_orders.setText("No items found");
        }

    /*    if (orderType == "1") {

        } else {
            swipe_sold.setRefreshing(false);
            if (current_page == 1) {
                mArrayList_myOrders_Sold.clear();
            }
            mArrayList_myOrders_Sold.addAll(data);
            mSoldAdapter.notifyDataSetChanged();

            if (data.size() != 0) {
                tv_no_orders.setVisibility(View.GONE);
            } else {
                tv_no_orders.setVisibility(View.VISIBLE);
            }
        }*/
    }

    @Override
    public void showError(String msg) {
        tv_no_orders.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBottomReach() {
        if(!hasMore){
            swipe_purchased.setRefreshing(false);
            current_page = current_page + 1;
            myOrdersPresenter.getMyOrders(current_page, orderType,getIntent().getStringExtra("type"));
        }

    }

    @Override
    public void onBottomReachSold() {
        swipe_sold.setRefreshing(false);
        current_page = current_page + 1;
        myOrdersPresenter.getMyOrders(current_page, orderType,getIntent().getStringExtra("type"));
    }
}
