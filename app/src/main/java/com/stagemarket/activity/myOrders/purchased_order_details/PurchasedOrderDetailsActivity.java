package com.stagemarket.activity.myOrders.purchased_order_details;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stagemarket.AppClass;
import com.stagemarket.BaseActivity;
import com.stagemarket.R;
import com.stagemarket.activity.login.LoginActivity;
import com.stagemarket.activity.viewProfile.ViewProfileActivity;
import com.stagemarket.pojo.SaveLoginDetailsModel;
import com.stagemarket.pojo.login.LoginResponse;
import com.stagemarket.pojo.myOrders.OrderDetalResponse;
import com.stagemarket.pojo.myOrders.purchased.Datum;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.DialogPopUps;
import com.stagemarket.utils.SharedPreff;

import java.lang.reflect.Type;
import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class PurchasedOrderDetailsActivity extends BaseActivity {

    @BindView(R.id.title_name)
    TextView titleName;
    @BindView(R.id.img_header_back)
    ImageView imgHeaderBack;
    @BindView(R.id.header)
    View header;

    @BindView(R.id.tv_prodName)
    TextView tv_prodName;
    @BindView(R.id.purchased_order_status)
    TextView purchased_order_status;
    @BindView(R.id.tvVat)
    TextView tvVat;
    @BindView(R.id.tv_purchased_product_price)
    TextView tv_purchased_product_price;
    @BindView(R.id.tv_purchased_product_quantity)
    TextView tv_purchased_product_quantity;
    @BindView(R.id.tv_purchased_paymentmode)
    TextView tv_purchased_paymentmode;
    @BindView(R.id.tvBillingAdd)
    TextView tvBillingAdd;
    @BindView(R.id.tvBillingCity)
    TextView tv_BillingCity;
    @BindView(R.id.tv_billingContact)
    TextView tvBillingContact;
    @BindView(R.id.tv_purchased_username)
    TextView tv_purchased_username;
    @BindView(R.id.tv_purchased_subtotal)
    TextView tv_purchased_subtotal;
    @BindView(R.id.tv_purchased_paidAmount)
    TextView tv_purchased_paidAmount;
    @BindView(R.id.img_purchased_product)
    ImageView img_purchased_product;
    @BindView(R.id.view_profile)
    TextView view_profile;
    @BindView(R.id.tvname)
    TextView tvname;
    @BindView(R.id.amount_txt)
    TextView amount_txt;
    @BindView(R.id.lytMain)
    LinearLayout lytMain;
    private String orderDetail = "";
    private Datum datsum;
    private NetWorkingService netWorkingService;
    private final CompositeDisposable disposables = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchased_order_details);
        ButterKnife.bind(this);

        netWorkingService = AppClass.getInstance().getNetworkService();
        if (getIntent().getExtras() != null) {
            orderDetail = getIntent().getStringExtra(Constants.ORDER_DETAIL);
            Gson gson = new Gson();
            Type type = new TypeToken<Datum>() {
            }.getType();
            datsum = gson.fromJson(orderDetail, type);
            getOrderdetail(datsum.getId());
            //  initViews(datum);
        }
    }

    private void getOrderdetail(Long id) {
        if (CommonUtils.isConnectedToInternet(PurchasedOrderDetailsActivity.this)) {
            showProgressDialog();
            netWorkingService.getAPI().getMyOrdersdetail(id + "", SharedPreff.getLoginDetailsModel().getId())
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<OrderDetalResponse>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            disposables.add(d);
                        }

                        @Override
                        public void onNext(OrderDetalResponse orderDetalResponse) {
                            hideProgressDialog();
                            Log.d("1","1");
                            if (orderDetalResponse.getResult().equalsIgnoreCase(Constants.SUCCESS)) {
                                Log.d("2","2");
                                lytMain.setVisibility(View.VISIBLE);
                                initViews(orderDetalResponse.getData().get(0));
                            } else {
                                Log.d("3","3");
                                showErrorDialog(orderDetalResponse.getMessage());
                            }

                        }

                        @Override
                        public void onError(Throwable e) {
                            hideProgressDialog();
                            Log.d("3","3:"+e.getMessage());
                        }

                        @Override
                        public void onComplete() {
                            Log.d("5","5");
                        }
                    });
        } else {
            CommonUtils.showTopSnackBar(PurchasedOrderDetailsActivity.this,
                    findViewById(android.R.id.content), getString(R.string.please_check_internet));
        }
    }

    public void showProgressDialog() {
        DialogPopUps.showProgressDialog(PurchasedOrderDetailsActivity.this, getResources().getString(R.string.please_wait));
    }

    public void showErrorDialog(String msg) {
        CommonUtils.showTopSnackBar(this, findViewById(android.R.id.content), msg);
    }

    public void showMessageDialog(String msg) {

    }

    public void hideProgressDialog() {
        DialogPopUps.hideDialog();

    }

    private void initViews(com.stagemarket.pojo.myOrders.Datum datum) {
        initToolbar();
        if (!SharedPreff.getLoginDetailsModel().getId().equalsIgnoreCase(String.valueOf(datum.getSellerId()))) {
            view_profile.setVisibility(View.INVISIBLE);
        } else {
            view_profile.setVisibility(View.INVISIBLE);
        }
        tvname.setText(datum.getBuyerName());
        tv_prodName.setText(datum.getProductName());

        if (String.valueOf(datum.getOrderType()).equalsIgnoreCase(Constants.COD)) {
            amount_txt.setText("Amoun to be Paid");
            tv_purchased_paymentmode.setText(Constants.CASH_ON_DELIVERY);
        } else {
            tv_purchased_paymentmode.setText(Constants.CREDIT_CARD);
        }
        tvBillingAdd.setText(datum.getBillingAddress());
        Log.e("commision",String.valueOf(datum.getCommision()));

        float pCost=Float.parseFloat(String.valueOf(datum.getCost()))*Integer.parseInt(datum.getmQuantity());
        float vvv=pCost*15;
        float vv=vvv/100;
        float v= pCost+ vv;

float total=Float.parseFloat(String.valueOf(datum.getVat()))+v;
        tv_purchased_paidAmount.setText(Constants.DOLlAR + ""+String.valueOf(new DecimalFormat(".##").format(total)));
        tv_purchased_subtotal.setText(Constants.DOLlAR + ""+new DecimalFormat(".##").format(v) + "");
        tvVat.setText(Constants.DOLlAR + ""+datum.getVat() + "");

        tv_purchased_username.setText(datum.getSellerName());
        tvBillingContact.setText(Constants.DOLlAR + ""+datum.getBillingContact());
        float pCostt=Float.parseFloat(String.valueOf(datum.getCost()));
        float vvvt=pCostt*15;
        float vvt=vvvt/100;
        float vt= pCostt+ vvt;
        tv_purchased_product_price.setText(Constants.DOLlAR + "" + new DecimalFormat(".##").format(vt) + "");
        tv_purchased_product_quantity.setText("Quantity: "+datum.getmQuantity());
        tv_BillingCity.setText(datum.getBillingCity());
        if (datum.getProductImages().size() != 0) {
            if (!datum.getProductImages().get(0).getImage().equalsIgnoreCase("")) {
                CommonUtils.setImage(this, img_purchased_product, datum.getProductImages().get(0).getImage());
            }
        }
        try {
            Log.e("timeee",datum.getOrderedDate());
          //  purchased_order_status.setText("Order Placed on : " +  CommonUtils.getMyDateFormat(CommonUtils.OrderDetailsconvertUTCtoLocalTime(datum.getOrderedDate()),"yyyy-MM-dd hh:mm:ss","hh:mm a"));
            purchased_order_status.setText("Order Placed on : " +  CommonUtils.OrderDetailsconvertUTCtoLocalTime(datum.getOrderedDate()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.view_profile)
    public void onViewProfileClick() {
        if (!SharedPreff.getLoginDetailsModel().getId().equalsIgnoreCase(String.valueOf(datsum.getSellerId()))) {
            startActivity(new Intent(this, ViewProfileActivity.class)
                    .putExtra(Constants.OTHER_PROFILE_USER_ID, String.valueOf(datsum.getSellerId()))

            );
        }
    }

    @OnClick(R.id.img_header_back)
    public void onBackClick() {
        finish();
    }


    private void initToolbar() {
        header.setBackgroundColor(ContextCompat.getColor(this, R.color.greynew));

        imgHeaderBack.setImageResource(R.drawable.ic_back);
        DrawableCompat.setTint(imgHeaderBack.getDrawable(), ContextCompat.getColor(this, R.color.black));


        titleName.setText("Order ID " + datsum.getId());
        titleName.setTextColor(ContextCompat.getColor(this, R.color.black));
    }
}
