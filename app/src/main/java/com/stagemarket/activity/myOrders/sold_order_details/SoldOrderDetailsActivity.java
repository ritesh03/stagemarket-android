package com.stagemarket.activity.myOrders.sold_order_details;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.stagemarket.R;

public class SoldOrderDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sold_order_details);
    }
}
