package com.stagemarket.activity.myOrders;

import com.stagemarket.CommonPresenter;

public interface MyOrdersPresenter extends CommonPresenter {
    void getMyOrders(int current_page,String orderType,String type);
}
