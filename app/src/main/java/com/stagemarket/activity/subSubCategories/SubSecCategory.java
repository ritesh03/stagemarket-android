package com.stagemarket.activity.subSubCategories;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stagemarket.R;
import com.stagemarket.activity.sell.categorySell.ad_details_sell.AdDetaillsSellActivity;
import com.stagemarket.activity.sell.categorySell.select_category_sell.SelectCategorySellActivity;
import com.stagemarket.adapter.SubCategoriesAdapter;
import com.stagemarket.adapter.SubCategoriesDataAdapter;
import com.stagemarket.adapter.SubSecCategoryAdapter;
import com.stagemarket.pojo.SubCatPojo;
import com.stagemarket.pojo.category.CategoriesPojo;
import com.stagemarket.pojo.categorySell.SubCategory;
import com.stagemarket.pojo.myOrders.SubSecondCategory;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.SharedPreff;

import org.greenrobot.eventbus.EventBus;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SubSecCategory extends AppCompatActivity {
    @BindView(R.id.header)
    View header;
    @BindView(R.id.title_name)
    TextView titleName;
    @BindView(R.id.img_header_back)
    ImageView imgHeaderBack;
    @BindView(R.id.recyclerViewSubCategories)
    RecyclerView recyclerViewSubCategories;
    private String cat_name="";
    private String subCatValues="";
    private ArrayList<SubSecondCategory> mArrayListCategory = new ArrayList<>();
    private SubSecCategoryAdapter mSubCategoriesAdapter;
    private String from="";
    @BindView(R.id.right_text)
    TextView right_text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_sec_category);
        ButterKnife.bind(this);

        if(getIntent().hasExtra(Constants.CAT_NAME)){
            cat_name = getIntent().getStringExtra(Constants.CAT_NAME);
        }

        if(getIntent().hasExtra(Constants.FROM)){
            from = getIntent().getStringExtra(Constants.FROM);
        }


        if(getIntent().hasExtra(Constants.SUB_SEC_DATA)){
            subCatValues = getIntent().getStringExtra(Constants.SUB_SEC_DATA);
            Gson gson = new Gson();
            Type type = new TypeToken<List<SubSecondCategory>>() {
            }.getType();
            List<SubSecondCategory> subCategories = gson.fromJson(subCatValues, type);
           // Toast.makeText(SubSecCategory.this, ""+subCategories.size(), Toast.LENGTH_SHORT).show();

if(Constants.PRODUCT_FOR.equalsIgnoreCase("1")) {
    SubSecondCategory subCategory1 = new SubSecondCategory();
    subCategory1.setCatId("0");
    subCategory1.setSelected(false);
    subCategory1.setSubSecondCatName("All");
    mArrayListCategory.add(0, subCategory1);
}

            mArrayListCategory.addAll(subCategories);
        }

        initToolbar();
        initRecyclerView();
    }

    @OnClick(R.id.img_header_back)
    public void onBackClick() {
        finish();
    }

    private void initRecyclerView() {
        mSubCategoriesAdapter = new SubSecCategoryAdapter( mArrayListCategory,this,from);
        recyclerViewSubCategories.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));
        recyclerViewSubCategories.setAdapter(mSubCategoriesAdapter);
    }

    private void initToolbar() {
        if(from.equalsIgnoreCase("2")){
            right_text.setVisibility(View.VISIBLE);
            right_text.setText(R.string.next);
            right_text.setTextColor(ContextCompat.getColor(SubSecCategory.this, R.color.black));

        }else{
            right_text.setVisibility(View.GONE);
        }
        header.setBackgroundColor(ContextCompat.getColor(this, R.color.greynew));
        imgHeaderBack.setImageResource(R.drawable.ic_back);
        DrawableCompat.setTint(imgHeaderBack.getDrawable(), ContextCompat.getColor(this, R.color.black));

        titleName.setText(cat_name);
        titleName.setTextColor(ContextCompat.getColor(this, R.color.black));
    }

    @OnClick(R.id.right_text)
    public void onNextClick() {
        if (mSubCategoriesAdapter.getPosSelectedVal() != -1) {

            SharedPreff.saveSubSecCatName(mArrayListCategory.get(mSubCategoriesAdapter.getPosSelectedVal()));

            Intent intent = new Intent(SubSecCategory.this, AdDetaillsSellActivity.class);
            startActivity(intent);

        } else {
            CommonUtils.showTopSnackBar(SubSecCategory.this,
                    findViewById(android.R.id.content), getString(R.string.please_select_cat));
        }

    }
}
