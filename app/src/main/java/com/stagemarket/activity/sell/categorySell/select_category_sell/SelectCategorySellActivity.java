package com.stagemarket.activity.sell.categorySell.select_category_sell;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stagemarket.BaseActivity;
import com.stagemarket.R;
import com.stagemarket.activity.editAdDetail.EditAdDetailActivity;
import com.stagemarket.activity.sell.categorySell.ad_details_sell.AdDetaillsSellActivity;
import com.stagemarket.activity.subSubCategories.SubSecCategory;
import com.stagemarket.adapter.SelectCategorySellAdapter;
import com.stagemarket.pojo.categorySell.SubCategory;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.SharedPreff;
import com.stagemarket.utils.SpacesItemDecoration;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SelectCategorySellActivity extends BaseActivity implements SelectCategorySellView {
    public static Context context;

    @BindView(R.id.img_header_back)
    ImageView imgHeaderBack;
    @BindView(R.id.title_name)
    TextView titleName;
    @BindView(R.id.header)
    View header;
    @BindView(R.id.img_slider)
    ImageView imgSlider;
    @BindView(R.id.select_category_sell_title)
    TextView selectCategorySellTitle;
    @BindView(R.id.recyclerViewSelectCategorySell)
    RecyclerView recyclerViewSelectCategorySell;
    @BindView(R.id.tv_noRecordFound)
    TextView tv_noRecordFound;
    @BindView(R.id.right_text)
    TextView right_text;
    SelectCategorySellAdapter mSelectCategorySellAdapter;
    ArrayList<SubCategory> mArrayListCategorySell = new ArrayList<>();
    private String catName = "";
    private String subCatValues = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_category_sell);
        ButterKnife.bind(this);
        context = SelectCategorySellActivity.this;
        catName = getIntent().getStringExtra(Constants.CAT_NAME);
        subCatValues = getIntent().getStringExtra(Constants.SUB_CAT_VALUES);


        Gson gson = new Gson();
        Type type = new TypeToken<List<SubCategory>>() {
        }.getType();
        List<SubCategory> subCategories = gson.fromJson(subCatValues, type);
        mArrayListCategorySell.addAll(subCategories);

        if (mArrayListCategorySell.size() != 0) {
            tv_noRecordFound.setVisibility(View.GONE);
            recyclerViewSelectCategorySell.setVisibility(View.VISIBLE);
        } else {
            tv_noRecordFound.setVisibility(View.VISIBLE);
            recyclerViewSelectCategorySell.setVisibility(View.GONE);
        }

        selectCategorySellTitle.setText(catName);
        initToolbar();
        initRecyclerView();
    }


    @OnClick(R.id.img_header_back)
    public void onBackClick() {
        finish();
    }


    private void initRecyclerView() {
        //  initListData();
        mSelectCategorySellAdapter = new SelectCategorySellAdapter(this, mArrayListCategorySell);
        recyclerViewSelectCategorySell.setLayoutManager(new LinearLayoutManager(this
                , LinearLayoutManager.VERTICAL, false));
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen._1sdp);

        recyclerViewSelectCategorySell.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
        recyclerViewSelectCategorySell.setAdapter(mSelectCategorySellAdapter);
    }

  /*  private void initListData() {
        mArrayListCategorySell.add(new SubCatData("Guitars", false));
        mArrayListCategorySell.add(new SubCatData("Pedals", false));
        mArrayListCategorySell.add(new SubCatData("Amplifiers", false));
        mArrayListCategorySell.add(new SubCatData("Keyboards", false));
        mArrayListCategorySell.add(new SubCatData("Trumpet", false));
        mArrayListCategorySell.add(new SubCatData("Violin", false));
        mArrayListCategorySell.add(new SubCatData("Drums", false));

    }*/

    private void initToolbar() {
        header.setBackgroundColor(ContextCompat.getColor(this, R.color.greynew));

        imgHeaderBack.setImageResource(R.drawable.ic_back);
        DrawableCompat.setTint(imgHeaderBack.getDrawable(), ContextCompat.getColor(this, R.color.black));

        right_text.setVisibility(View.VISIBLE);
        right_text.setText(R.string.next);
        right_text.setTextColor(ContextCompat.getColor(SelectCategorySellActivity.this, R.color.black));


        titleName.setText(R.string.select_sub_cat);
        titleName.setTextColor(ContextCompat.getColor(this, R.color.black));
    }

    @OnClick(R.id.right_text)
    public void onNextClick() {
        if (mSelectCategorySellAdapter.getPosSelectedVal() != -1) {
            SharedPreff.saveSubCatName(mArrayListCategorySell.get(mSelectCategorySellAdapter.getPosSelectedVal()));
   /*         Intent returnIntent = new Intent(context, SubSecCategory.class);
            returnIntent.putExtra(Constants.FROM,"2");
            returnIntent.putExtra(Constants.CAT_NAME,mArrayListCategorySell.get(mSelectCategorySellAdapter.getPosSelectedVal()).getSubCatName());
            returnIntent.putExtra(Constants.SUB_SEC_DATA,new Gson().toJson(mArrayListCategorySell.get(mSelectCategorySellAdapter.getPosSelectedVal()).getmArrayListSubSecCat()));
       startActivity(returnIntent);*/
           /* startActivity(new Intent(this, EditAdDetailActivity.class)
                    .putExtra(Constants.PRODUCT_DATA,new Gson().toJson(mArrayListCategorySell).toString()));*/


            Log.e("LENGTH",new Gson().toJson(mArrayListCategorySell.get(mSelectCategorySellAdapter.getPosSelectedVal()).getmArrayListSubSecCat()).length()+"");

            if(new Gson().toJson(mArrayListCategorySell.get(mSelectCategorySellAdapter.getPosSelectedVal()).getmArrayListSubSecCat()).length()>2)
            {

                Intent returnIntent = new Intent(context, SubSecCategory.class);
                returnIntent.putExtra(Constants.FROM,"2");
                returnIntent.putExtra(Constants.CAT_NAME,mArrayListCategorySell.get(mSelectCategorySellAdapter.getPosSelectedVal()).getSubCatName());
                returnIntent.putExtra(Constants.SUB_SEC_DATA,new Gson().toJson(mArrayListCategorySell.get(mSelectCategorySellAdapter.getPosSelectedVal()).getmArrayListSubSecCat()));
                startActivity(returnIntent);

            }
            else
            {
            Intent intent = new Intent(SelectCategorySellActivity.this, AdDetaillsSellActivity.class);
            startActivity(intent);}
        } else {
            CommonUtils.showTopSnackBar(SelectCategorySellActivity.this,
                    findViewById(android.R.id.content), getString(R.string.please_select_cat));
        }

    }

    @Override
    public void showProgressDialog() {

    }

    @Override
    public void showErrorDialog(String msg) {

    }

    @Override
    public void showMessageDialog(String msg) {

    }

    @Override
    public void hideProgressDialog() {

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
