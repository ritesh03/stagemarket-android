package com.stagemarket.activity.sell.categorySell.category_sell;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.stagemarket.AppClass;
import com.stagemarket.BaseActivity;
import com.stagemarket.R;
import com.stagemarket.activity.sell.categorySell.select_category_sell.SelectCategorySellView;
import com.stagemarket.adapter.CategorySellAdapter;
import com.stagemarket.pojo.categorySell.CategorySellPojo;
import com.stagemarket.pojo.categorySell.Datum;
import com.stagemarket.pojo.categorySell.GetCategoriesResponse;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.DialogPopUps;
import com.stagemarket.utils.SpacesItemDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CategorySellActivity extends BaseActivity implements CategorySellVIew {

    public static Context context ;

    @BindView(R.id.img_header_back)
    ImageView imgHeaderBack;
    @BindView(R.id.right_text)
    TextView right_text;
    @BindView(R.id.title_name)
    TextView titleName;
    @BindView(R.id.header)
    View header;
    CategorySellAdapter mCategorySellAdapter;
    ArrayList<CategorySellPojo> mArrayListCategorySellPojos = new ArrayList<>();
    @BindView(R.id.recyclerViewCategoriesSell)
    RecyclerView recyclerViewCategoriesSell;

    NetWorkingService netWorkingService;
    CategorySellPresenter categorySellPresenter;
    ArrayList<Datum> getCategoriesResponseData = new ArrayList<>();
    String categorySellName="",categorySellImage = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_sell);
        ButterKnife.bind(this);

        context = CategorySellActivity.this;
        initToolbar();

        netWorkingService = AppClass.getInstance().getNetworkService();
        categorySellPresenter = new CategorySellPresenterImp(this,netWorkingService);

        initRecyclerView();

        categorySellPresenter.fetchCategoriesSellList();

    }

    @OnClick(R.id.img_header_back)
    public void onBackClick() {
        finish();
    }

    private void initRecyclerView() {
       // initListData();
        mCategorySellAdapter = new CategorySellAdapter((CategorySellVIew) this, getCategoriesResponseData);
        recyclerViewCategoriesSell.setLayoutManager(new GridLayoutManager(this, 2));
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen._1sdp);

        recyclerViewCategoriesSell.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
        recyclerViewCategoriesSell.setAdapter(mCategorySellAdapter);
    }

    private void initListData() {
        mArrayListCategorySellPojos.add(new CategorySellPojo("1", R.drawable.cat_one, "PA Systems"));
        mArrayListCategorySellPojos.add(new CategorySellPojo("1", R.drawable.cat_two, "Studio Equipments"));
        mArrayListCategorySellPojos.add(new CategorySellPojo("1", R.drawable.cat_three, "Musical Instruments"));
        mArrayListCategorySellPojos.add(new CategorySellPojo("1", R.drawable.subcat_four, "Home Sound"));
    }


    private void initToolbar() {
        header.setBackgroundColor(ContextCompat.getColor(this, R.color.greynew));

        imgHeaderBack.setImageResource(R.drawable.ic_back);
        DrawableCompat.setTint(imgHeaderBack.getDrawable(), ContextCompat.getColor(this, R.color.black));

        right_text.setVisibility(View.GONE);
        right_text.setText(R.string.next);
        right_text.setTextColor(ContextCompat.getColor(CategorySellActivity.this, R.color.black));

        titleName.setText(R.string.select_category);
        titleName.setTextColor(ContextCompat.getColor(this, R.color.black));
    }


    @Override
    public void showProgressDialog() {
        DialogPopUps.showProgressDialog(CategorySellActivity.this, getResources().getString(R.string.please_wait));

    }

    @Override
    public void showErrorDialog(String msg) {
        CommonUtils.showTopSnackBar(CategorySellActivity.this, findViewById(android.R.id.content), msg);
    }

    @Override
    public void showMessageDialog(String msg) {
    }

    @Override
    public void hideProgressDialog() {
        DialogPopUps.hideDialog();
    }

    @Override
    public void getCategoriesSellResponse(ArrayList<Datum> getCategoriesResponseData) {

       /* Datum datum = new Datum();
        datum.setName("All Categories");
        datum.setId("-1");
        getCategoriesResponseData.add(0,datum);*/
        this.getCategoriesResponseData = getCategoriesResponseData;
        mCategorySellAdapter.addingCategoriesSellData(getCategoriesResponseData);
    }

}
