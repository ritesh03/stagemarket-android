package com.stagemarket.activity.sell.categorySell.ad_details_sell;

import com.stagemarket.CommonPresenter;

import java.util.List;

public interface AdDetailsSellPresenter extends CommonPresenter {
    void onSubmitAdClick(String title, String price, String description, List<String> productImages,
                         String locName, String lat, String longg, String catId, String subCatId,String qunatity,
                         String subSecCatID,int productType);
}
