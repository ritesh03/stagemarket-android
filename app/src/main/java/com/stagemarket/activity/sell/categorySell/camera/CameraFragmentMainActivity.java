package com.stagemarket.activity.sell.categorySell.camera;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresPermission;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.florent37.camerafragment.CameraFragment;
import com.github.florent37.camerafragment.CameraFragmentApi;
import com.github.florent37.camerafragment.configuration.Configuration;
import com.github.florent37.camerafragment.internal.utils.Utils;
import com.github.florent37.camerafragment.listeners.CameraFragmentControlsAdapter;
import com.github.florent37.camerafragment.listeners.CameraFragmentResultAdapter;
import com.github.florent37.camerafragment.listeners.CameraFragmentStateAdapter;
import com.github.florent37.camerafragment.listeners.CameraFragmentVideoRecordTextAdapter;
import com.github.florent37.camerafragment.widgets.CameraSettingsView;
import com.github.florent37.camerafragment.widgets.CameraSwitchView;
import com.github.florent37.camerafragment.widgets.FlashSwitchView;
import com.github.florent37.camerafragment.widgets.MediaActionSwitchView;
import com.github.florent37.camerafragment.widgets.RecordButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mlsdev.rximagepicker.RxImagePicker;
import com.mlsdev.rximagepicker.Sources;
import com.squareup.picasso.Picasso;
import com.stagemarket.R;
import com.stagemarket.activity.login.LoginActivity;
import com.stagemarket.activity.sell.categorySell.CameraAdapter;
import com.stagemarket.activity.sell.categorySell.category_sell.CategorySellActivity;
import com.stagemarket.activity.yourAccount.YourAccountActivity;
import com.stagemarket.pojo.ImageCamera;
import com.stagemarket.pojo.myPosts.Datum;
import com.stagemarket.utils.CommonListeners;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.DialogPopUps;
import com.stagemarket.utils.SharedPreff;
import com.yalantis.ucrop.UCrop;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.functions.Consumer;

@SuppressLint("MissingPermission")
public class CameraFragmentMainActivity extends AppCompatActivity {

    public static final String FRAGMENT_TAG = "camera";
    private static final int REQUEST_CAMERA_PERMISSIONS = 931;
    private static final int REQUEST_PREVIEW_CODE = 1001;
    @BindView(R.id.settings_view)
    CameraSettingsView settingsView;
    @BindView(R.id.flash_switch_view)
    FlashSwitchView flashSwitchView;
    @BindView(R.id.front_back_camera_switcher)
    CameraSwitchView cameraSwitchView;
    @BindView(R.id.record_button)
    RecordButton recordButton;
    @BindView(R.id.photo_video_camera_switcher)
    MediaActionSwitchView mediaActionSwitchView;
    @BindView(R.id.tv_done)
    TextView tv_done;
    @BindView(R.id.record_duration_text)
    TextView recordDurationText;
    @BindView(R.id.record_size_mb_text)
    TextView recordSizeText;
    @BindView(R.id.recyclerViewCamera)
    RecyclerView recyclerViewCamera;
    @BindView(R.id.imgCamera)
    ImageView imgCamera;
    @BindView(R.id.cameraLayout)
    View cameraLayout;

    @BindView(R.id.gallery_view)
    ImageView gallery_view;

    CameraAdapter cameraAdapter;
    ArrayList<String> filePathList = new ArrayList<>();
    private String productImages;
    private ArrayList<String> prodImages = new ArrayList<>();
//    @BindView(R.id.addCameraButton)
//    View addCameraButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camerafragment_activity_main);
        //to remove "information bar" above the action bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        ButterKnife.bind(this);

        if (getIntent() != null) {
            productImages = getIntent().getStringExtra(Constants.PRODUCT_IMAGES);
            if (productImages != null) {
                Gson gson = new Gson();
                Type type = new TypeToken<ArrayList<String>>() {
                }.getType();
                prodImages = gson.fromJson(productImages, type);
                filePathList.addAll(prodImages);
                tv_done.setText(getString(R.string.done));
                showUploadButton();
            }
        }

        if (Build.VERSION.SDK_INT > 15) {
            final String[] permissions = {
                    Manifest.permission.CAMERA,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE};

            final List<String> permissionsToRequest = new ArrayList<>();
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                    permissionsToRequest.add(permission);
                }
            }
            if (!permissionsToRequest.isEmpty()) {
                ActivityCompat.requestPermissions(this, permissionsToRequest.toArray(new String[permissionsToRequest.size()]), REQUEST_CAMERA_PERMISSIONS);
            } else addCamera();
        } else {
            addCamera();
        }

        initCameraRecyclerView();
    }

    private void initCameraRecyclerView() {
        cameraAdapter = new CameraAdapter(filePathList, CameraFragmentMainActivity.this);
        recyclerViewCamera.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerViewCamera.setAdapter(cameraAdapter);
    }

    @OnClick(R.id.tv_done)
    public void onDoneClick() {
        if (prodImages.size() != 0) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra(Constants.PRODUCT_IMAGES,new Gson().toJson(filePathList));
            setResult(RESULT_OK, returnIntent);
            finish();
        } else {
            if (filePathList.size() != 0) {
                SharedPreff.saveSellPhotos(filePathList);
                startActivity(new Intent(this, CategorySellActivity.class));
            } else {
                CommonUtils.showTopSnackBar(CameraFragmentMainActivity.this,
                        findViewById(android.R.id.content), getString(R.string.please_upload_photos_to_proceed_further));
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SharedPreff.clearSellPhotos();

        if (prodImages.size() != 0) {
            Intent returnIntent = new Intent();
            setResult(RESULT_CANCELED, returnIntent);
            finish();
        }

    }

    @OnClick(R.id.tv_cancel)
    public void onCancelClick() {
        if (prodImages.size() != 0) {
            Intent returnIntent = new Intent();
            setResult(RESULT_CANCELED, returnIntent);
            finish();
        }else{
            DialogPopUps.showAlertWithButtons(CameraFragmentMainActivity.this,
                    getString(R.string.alert), getString(R.string.are_you_sure_you_want_to_exit_posting), getString(R.string.exit),
                    getString(R.string.cancel),
                    "", false, false, new CommonListeners.AlertCallBackWithButtonsInterface() {
                        @Override
                        public void positiveClick() {
                            SharedPreff.clearSellPhotos();
                            finish();
                        }

                        @Override
                        public void neutralClick() {

                        }

                        @Override
                        public void negativeClick() {
                        }
                    });
        }


    }

    @OnClick(R.id.gallery_view)
    public void onGalleryClick() {

        RxImagePicker.with(CameraFragmentMainActivity.this).requestImage(Sources.GALLERY).subscribe(new Consumer<Uri>() {
            @Override
            public void accept(@io.reactivex.annotations.NonNull Uri uri) throws Exception { //Get image by uri using one of image loading libraries. I use Glide in sample app.
                UCrop.Options options = new UCrop.Options();
                options.setToolbarColor(ContextCompat.getColor(CameraFragmentMainActivity.this, R.color.colorPrimary));
                options.setStatusBarColor(ContextCompat.getColor(CameraFragmentMainActivity.this, R.color.colorPrimary));
                UCrop.of(uri, Uri.fromFile(new File(getCacheDir(), "temp_image" + System.currentTimeMillis() + ".jpeg")))
                        .withAspectRatio(1, 1).withMaxResultSize(700, 700).withOptions(options).start((CameraFragmentMainActivity.this));
            }
        });

    }

    @OnClick(R.id.flash_switch_view)
    public void onFlashSwitcClicked() {
        final CameraFragmentApi cameraFragment = getCameraFragment();
        if (cameraFragment != null) {
            cameraFragment.toggleFlashMode();
        }
    }

    @OnClick(R.id.front_back_camera_switcher)
    public void onSwitchCameraClicked() {
        final CameraFragmentApi cameraFragment = getCameraFragment();
        if (cameraFragment != null) {
            cameraFragment.switchCameraTypeFrontBack();
        }
    }

    @OnClick(R.id.record_button)
    public void onRecordButtonClicked() {
        final CameraFragmentApi cameraFragment = getCameraFragment();
        if (cameraFragment != null) {
            cameraFragment.takePhotoOrCaptureVideo(new CameraFragmentResultAdapter() {
                                                       @Override
                                                       public void onVideoRecorded(String filePath) {
                                                       }

                                                       @Override
                                                       public void onPhotoTaken(byte[] bytes, String filePath) {
                                                           if (filePathList.size() > 3) {
                                                               CommonUtils.showTopSnackBar(CameraFragmentMainActivity.this,
                                                                       findViewById(android.R.id.content), getString(R.string.max_4_photos));
                                                           } else {
                                                               filePathList.add(filePath);
                                                               cameraAdapter.notifyDataSetChanged();
                                                           }

                                                           showUploadButton();
                                                           //   startActivity(new Intent(CameraFragmentMainActivity.this, CategorySellActivity.class));
                                                       }
                                                   },
                    getCacheDir().getPath(),
                    null);
        }
    }

    @OnClick(R.id.settings_view)
    public void onSettingsClicked() {
        final CameraFragmentApi cameraFragment = getCameraFragment();
        if (cameraFragment != null) {
            cameraFragment.openSettingDialog();
        }
    }

    @OnClick(R.id.photo_video_camera_switcher)
    public void onMediaActionSwitchClicked() {
        final CameraFragmentApi cameraFragment = getCameraFragment();
        if (cameraFragment != null) {
            cameraFragment.switchActionPhotoVideo();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK
                && requestCode == UCrop.REQUEST_CROP) {
            Uri resultUri = UCrop.getOutput(data);

            final InputStream imageStream;
            try {
                imageStream = getContentResolver().openInputStream(resultUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                if (filePathList.size() > 3) {
                    CommonUtils.showTopSnackBar(CameraFragmentMainActivity.this,
                            findViewById(android.R.id.content), getString(R.string.max_4_photos));
                } else {
                    filePathList.add(CommonUtils.getPath(CameraFragmentMainActivity.this, resultUri));
                    cameraAdapter.notifyDataSetChanged();
                }

                showUploadButton();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }


        } else if (requestCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
            Log.d("uri", "error " + cropError);

        }
    }



   /* @OnClick(R.id.addCameraButton)
    public void onAddCameraClicked() {
        if (Build.VERSION.SDK_INT > 15) {
            final String[] permissions = {
                    Manifest.permission.CAMERA,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE};

            final List<String> permissionsToRequest = new ArrayList<>();
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                    permissionsToRequest.add(permission);
                }
            }
            if (!permissionsToRequest.isEmpty()) {
                ActivityCompat.requestPermissions(this, permissionsToRequest.toArray(new String[permissionsToRequest.size()]), REQUEST_CAMERA_PERMISSIONS);
            } else addCamera();
        } else {
            addCamera();
        }
    }*/

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length != 0) {
            addCamera();
        }
    }

    @RequiresPermission(Manifest.permission.CAMERA)
    public void addCamera() {
        // addCameraButton.setVisibility(View.GONE);
        // cameraLayout.setVisibility(View.VISIBLE);

        final CameraFragment cameraFragment = CameraFragment.newInstance(new Configuration.Builder()
                .setCamera(Configuration.CAMERA_FACE_REAR).build());
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content, cameraFragment, FRAGMENT_TAG)
                .commitAllowingStateLoss();

        if (cameraFragment != null) {
            //cameraFragment.setResultListener(new CameraFragmentResultListener() {
            //    @Override
            //    public void onVideoRecorded(String filePath) {
            //        Intent intent = PreviewActivity.newIntentVideo(CameraFragmentMainActivity.this, filePath);
            //        startActivityForResult(intent, REQUEST_PREVIEW_CODE);
            //    }
//
            //    @Override
            //    public void onPhotoTaken(byte[] bytes, String filePath) {
            //        Intent intent = PreviewActivity.newIntentPhoto(CameraFragmentMainActivity.this, filePath);
            //        startActivityForResult(intent, REQUEST_PREVIEW_CODE);
            //    }
            //});

            cameraFragment.setStateListener(new CameraFragmentStateAdapter() {

                @Override
                public void onCurrentCameraBack() {
                    cameraSwitchView.displayBackCamera();
                }

                @Override
                public void onCurrentCameraFront() {
                    cameraSwitchView.displayFrontCamera();
                }

                @Override
                public void onFlashAuto() {
                    flashSwitchView.displayFlashOff();

                    //  flashSwitchView.displayFlashAuto();
                }

                @Override
                public void onFlashOn() {
                    flashSwitchView.displayFlashOff();

                    // flashSwitchView.displayFlashOn();
                }

                @Override
                public void onFlashOff() {
                    flashSwitchView.displayFlashOff();
                }

                @Override
                public void onCameraSetupForPhoto() {
                    mediaActionSwitchView.displayActionWillSwitchVideo();

                    recordButton.displayPhotoState();
                    flashSwitchView.setVisibility(View.GONE);
                }

                @Override
                public void onCameraSetupForVideo() {
                    mediaActionSwitchView.displayActionWillSwitchPhoto();

                    recordButton.displayVideoRecordStateReady();
                    flashSwitchView.setVisibility(View.GONE);
                }

                @Override
                public void shouldRotateControls(int degrees) {
                    ViewCompat.setRotation(cameraSwitchView, degrees);
                    ViewCompat.setRotation(mediaActionSwitchView, degrees);
                    ViewCompat.setRotation(flashSwitchView, degrees);
                    ViewCompat.setRotation(recordDurationText, degrees);
                    ViewCompat.setRotation(recordSizeText, degrees);
                }

                @Override
                public void onRecordStateVideoReadyForRecord() {
                    recordButton.displayVideoRecordStateReady();
                }

                @Override
                public void onRecordStateVideoInProgress() {
                    recordButton.displayVideoRecordStateInProgress();
                }

                @Override
                public void onRecordStatePhoto() {
                    recordButton.displayPhotoState();
                }

                @Override
                public void onStopVideoRecord() {
                    recordSizeText.setVisibility(View.GONE);
                    //cameraSwitchView.setVisibility(View.VISIBLE);
                    settingsView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onStartVideoRecord(File outputFile) {
                }
            });

            cameraFragment.setControlsListener(new CameraFragmentControlsAdapter() {
                @Override
                public void lockControls() {
                    cameraSwitchView.setEnabled(false);
                    recordButton.setEnabled(false);
                    settingsView.setEnabled(false);
                    flashSwitchView.setEnabled(false);
                }

                @Override
                public void unLockControls() {
                    cameraSwitchView.setEnabled(true);
                    recordButton.setEnabled(true);
                    settingsView.setEnabled(true);
                    flashSwitchView.setEnabled(false);
                }

                @Override
                public void allowCameraSwitching(boolean allow) {
                    cameraSwitchView.setVisibility(allow ? View.VISIBLE : View.GONE);
                }

                @Override
                public void allowRecord(boolean allow) {
                    recordButton.setEnabled(allow);
                }

                @Override
                public void setMediaActionSwitchVisible(boolean visible) {
                    mediaActionSwitchView.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
                }
            });

            cameraFragment.setTextListener(new CameraFragmentVideoRecordTextAdapter() {
                @Override
                public void setRecordSizeText(long size, String text) {
                    recordSizeText.setText(text);
                }

                @Override
                public void setRecordSizeTextVisible(boolean visible) {
                    recordSizeText.setVisibility(visible ? View.VISIBLE : View.GONE);
                }

                @Override
                public void setRecordDurationText(String text) {
                    recordDurationText.setText(text);
                }

                @Override
                public void setRecordDurationTextVisible(boolean visible) {
                    recordDurationText.setVisibility(visible ? View.VISIBLE : View.GONE);
                }
            });
        }
    }

    private CameraFragmentApi getCameraFragment() {
        return (CameraFragmentApi) getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onImageDelete(ImageCamera imageCamera) {
        filePathList.remove(imageCamera.getPosition());
        cameraAdapter.notifyDataSetChanged();

        showUploadButton();
    }

    private void showUploadButton() {
        if (filePathList.size() != 0) {
            tv_done.setVisibility(View.VISIBLE);
        } else {
            tv_done.setVisibility(View.GONE);
        }


        if (filePathList.size() > 3) {
            gallery_view.setAlpha(0.5f);
            gallery_view.setClickable(false);
            imgCamera.setAlpha(0.5f);
            recordButton.setClickable(false);
        } else {
            gallery_view.setAlpha(1f);
            gallery_view.setClickable(true);
            imgCamera.setAlpha(1f);
            recordButton.setClickable(true);
        }
    }
}
