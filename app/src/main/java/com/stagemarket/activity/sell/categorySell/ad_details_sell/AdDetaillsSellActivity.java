package com.stagemarket.activity.sell.categorySell.ad_details_sell;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.ViewPager;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.stagemarket.AppClass;
import com.stagemarket.BaseActivity;
import com.stagemarket.R;
import com.stagemarket.activity.categories.CategoriesActivity;
import com.stagemarket.activity.home.HomeActivity;
import com.stagemarket.activity.login.LoginActivity;
import com.stagemarket.activity.sell.categorySell.category_sell.CategorySellActivity;
import com.stagemarket.activity.sell.categorySell.select_category_sell.SelectCategorySellActivity;
import com.stagemarket.adapter.ViewPagerAdapter;
import com.stagemarket.pojo.discover.LatLongBean;
import com.stagemarket.pojo.myPosts.ProductImage;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.CommonListeners;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.DialogPopUps;
import com.stagemarket.utils.SharedPreff;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AdDetaillsSellActivity extends BaseActivity implements AdDetailsSellView {

    @BindView(R.id.title_name)
    TextView titleName;
    @BindView(R.id.img_header_back)
    ImageView imgHeaderBack;
    @BindView(R.id.right_text)
    TextView rightText;
    @BindView(R.id.header)
    View header;
    @BindView(R.id.tv_locationName)
    TextView tv_locationName;
    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.img_slider)
    ImageView imgSlider;
    @BindView(R.id.left_text)
    TextView leftText;
    @BindView(R.id.relyt)
    RelativeLayout relyt;
    @BindView(R.id.price_value)
    EditText priceValue;
    @BindView(R.id.tv_product_description)
    EditText tvProductDescription;
    private ViewPagerAdapterSell viewPagerAdapter;
    @BindView(R.id.left_nav)
    ImageView leftNav;
    @BindView(R.id.right_nav)
    ImageView rightNav;
    @BindView(R.id.tv_categoryName)
    TextView tv_categoryName;
    @BindView(R.id.et_name)
    EditText et_name;
    @BindView(R.id.tv_photos_count)
    TextView tv_photos_count;
    @BindView(R.id.lytPhotosCount)
    LinearLayout lytPhotosCount;
    @BindView(R.id.et_quantity)
    EditText et_quantity;
    @BindView(R.id.radio_group)
    RadioGroup radio_group;
    int ProductType=1;
    /*
        List<ProductImage> productImages = new ArrayList<>();
    */
    AdDetailsSellPresenter adDetailsSellPresenter;
    List<String> productImages = new ArrayList<>();
    private NetWorkingService netWorkingService;
    private String longg = "", lat = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ad_detaills_sell);
        ButterKnife.bind(this);
        initToolbar();
        productImages = SharedPreff.getSellPhotos();
        netWorkingService = ((AppClass) getApplication()).getNetworkService();

        adDetailsSellPresenter = new AdDetailsSellPresenterImp(this, netWorkingService);
        setUpViews();
        setUpViewPager();

        radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.btn_new_visit:
                        ProductType = 1;
                        break;
                    case R.id.btn_new_consult:
                        ProductType = 2;
                        break;
                }
            }
        });

    }

    public class DecimalDigitsInputFilter implements InputFilter {

        Pattern mPattern;

        public DecimalDigitsInputFilter(int digitsBeforeZero,int digitsAfterZero) {
            mPattern=Pattern.compile("[0-9]{0," + (digitsBeforeZero-1) + "}+((\\.[0-9]{0," + (digitsAfterZero-1) + "})?)||(\\.)?");
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            Matcher matcher=mPattern.matcher(dest);
            if(!matcher.matches())
                return "";
            return null;
        }

    }

    @OnClick(R.id.tv_edit_photo)
    public void onEditPhotoClick() {
        ((Activity) CategorySellActivity.context).finish();
        ((Activity) SelectCategorySellActivity.context).finish();
        finish();
    }

    @OnClick(R.id.left_nav)
    public void OnLeftNavClick() {
        rightNav.setVisibility(View.VISIBLE);
        int tab = viewpager.getCurrentItem();

        if (tab == 1) {
            leftNav.setVisibility(View.GONE);
        }

        if (tab > 0) {
            tab--;
            viewpager.setCurrentItem(tab);
        } else if (tab == 0) {
            viewpager.setCurrentItem(tab);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        CommonUtils.hideSoftKeyboard(AdDetaillsSellActivity.this);
        return super.dispatchTouchEvent(ev);

    }

    /* @OnClick(R.id.tv_categoryName)
     public void onCategoryClick() {
         startActivity(new Intent(this, CategoriesActivity.class));
     }
 */
    @Override
    protected void onResume() {
        super.onResume();

        if (SharedPreff.getSubSecCatName() != null) {
             tv_categoryName.setText(SharedPreff.getCatName().getName() + " > " + SharedPreff.getSubCatName().getSubCatName()
                    + " > " +SharedPreff.getSubSecCatName().getSubSecondCatName());

        }
        else
        {
            tv_categoryName.setText(SharedPreff.getCatName().getName() + " > " + SharedPreff.getSubCatName().getSubCatName()
                      );
        }
    }

    @OnClick(R.id.right_nav)
    public void OnRightNavClick() {
        leftNav.setVisibility(View.VISIBLE);
        int tab = viewpager.getCurrentItem();
        tab++;
        viewpager.setCurrentItem(tab);

        if (tab == productImages.size() - 1) {
            rightNav.setVisibility(View.GONE);
        }
    }


    private void setUpViewPager() {

        if (productImages.size() == 1) {
            lytPhotosCount.setVisibility(View.GONE);
            leftNav.setVisibility(View.GONE);
            rightNav.setVisibility(View.GONE);
        } else {
            lytPhotosCount.setVisibility(View.VISIBLE);
        }

        leftNav.setVisibility(View.GONE);
        viewPagerAdapter = new ViewPagerAdapterSell(this, productImages);
        viewpager.setAdapter(viewPagerAdapter);
    }

    private void setUpViews() {
        priceValue.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(10,2)});
        tv_photos_count.setText(productImages.size() + " Photos");
        if (!SharedPreff.getLat().equalsIgnoreCase("")) {

            lat = SharedPreff.getLat();
            longg = SharedPreff.getLng();


            double lat = Double.parseDouble(SharedPreff.getLat());
            double lng = Double.parseDouble(SharedPreff.getLng());
            Address address = CommonUtils.getAddressObjectFromLocation(lat, lng, this);
            StringBuilder sb = new StringBuilder();
            if (address.getSubLocality() != null) {
                sb.append(address.getSubLocality()).append("\n");
            } else {
                sb.append(address.getLocality()).append("\n");
            }
            sb.append(address.getSubAdminArea()).append(",");
            sb.append(address.getCountryName());
            String result = sb.toString();
            tv_locationName.setText(result);
        }


        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if (position == 0) {
                    leftNav.setVisibility(View.GONE);
                    rightNav.setVisibility(View.VISIBLE);
                } else if (position >= 1 && position < productImages.size() - 1) {
                    leftNav.setVisibility(View.VISIBLE);
                    rightNav.setVisibility(View.VISIBLE);
                } else if (position == 1 && productImages.size() == 2) {
                    leftNav.setVisibility(View.VISIBLE);
                    rightNav.setVisibility(View.GONE);
                } else {
                    rightNav.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    int PLACE_PICKER_REQUEST = 1;

    @OnClick(R.id.lytChangeLocation)
    public void onChangeLocationClick() {
        try {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);
                getLocationAddress(place.getLatLng().latitude, place.getLatLng().longitude, false);
                /*SharedPreff.saveLng(String.valueOf(place.getLatLng().longitude));
                SharedPreff.saveLat(String.valueOf(place.getLatLng().latitude));
*/
                lat = String.valueOf(place.getLatLng().latitude);
                longg = String.valueOf(place.getLatLng().longitude);

                SharedPreff.saveTempLat(String.valueOf(place.getLatLng().latitude));
                SharedPreff.saveTempLng(String.valueOf(place.getLatLng().longitude));
                EventBus.getDefault().post(new LatLongBean(place.getLatLng().latitude, place.getLatLng().longitude));
                //  String toastMsg = String.format("Place: %s", place.getName());
                // Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
               /* if (place != null) {
                    gettingAddressFromLatLong(true, "" + place.getLatLng().latitude, "" + place.getLatLng().longitude);
                }*/
            }
        }
    }

    private void getLocationAddress(double lat, double lng, boolean isCurrent) {
        Address address = CommonUtils.getAddressObjectFromLocation(lat, lng, this);
      /*  StringBuilder sb = new StringBuilder();
        sb.append(address.getThoroughfare()).append("\n");
        sb.append(address.getSubAdminArea()).append(",");
        sb.append(address.getCountryName());
        String result = sb.toString();
      */
        StringBuilder sb = new StringBuilder();
        if(address!=null)
        {
        sb.append(address.getAddressLine(0)).append("\n");}
      /*  if (address.getSubLocality() != null) {
            sb.append(address.getSubLocality()).append(",");
        } else {
            sb.append(address.getLocality()).append(",");
        }
        if (address.getSubAdminArea() != null) {
            sb.append(address.getSubAdminArea()).append("\n");
        } else {
            sb.append(address.getAdminArea()).append(",");
        }
        sb.append(address.getPostalCode()).append("\n");
        sb.append(address.getCountryName());
      */
        tv_locationName.setText(sb.toString());
    }

    private void initToolbar() {
        header.setBackgroundColor(ContextCompat.getColor(this, R.color.greynew));

        imgHeaderBack.setImageResource(R.drawable.ic_back);
        DrawableCompat.setTint(imgHeaderBack.getDrawable(), ContextCompat.getColor(this, R.color.black));

        rightText.setVisibility(View.VISIBLE);
        rightText.setText(R.string.submit_ad);
        rightText.setTextColor(ContextCompat.getColor(AdDetaillsSellActivity.this, R.color.black));

        titleName.setText(R.string.ad_details);
        titleName.setTextColor(ContextCompat.getColor(this, R.color.black));
    }

    @OnClick(R.id.right_text)
    public void onSubmitAdClick() {
        String title = et_name.getText().toString().trim();
        String price = priceValue.getText().toString().trim();
        String desc = tvProductDescription.getText().toString().trim();
        String qunatity = et_quantity.getText().toString().trim();
if( SharedPreff.getSubSecCatName()!=null) {

        adDetailsSellPresenter.onSubmitAdClick(title, price.replaceAll("$",""), desc, productImages,
                tv_locationName.getText().toString().trim(),
                lat, longg, SharedPreff.getCatName().getId(), SharedPreff.getSubCatName().getId(),qunatity,
                SharedPreff.getSubSecCatName().getmId(),ProductType);
}
else {
    adDetailsSellPresenter.onSubmitAdClick(title, price.replaceAll("$", ""), desc, productImages,
            tv_locationName.getText().toString().trim(),
            lat, longg, SharedPreff.getCatName().getId(), SharedPreff.getSubCatName().getId(), qunatity,
            "",ProductType);
}
    }

    @OnClick(R.id.img_header_back)
    public void onBackClick() {
        finish();
    }

    @Override
    public void showProgressDialog() {
        DialogPopUps.showProgressDialog(this, getResources().getString(R.string.please_wait));
    }

    @Override
    public void showErrorDialog(String msg) {
        CommonUtils.showTopSnackBar(this, findViewById(android.R.id.content), msg);

    }

    @Override
    public void showMessageDialog(String msg) {
        msg = getString(R.string.cngrats_your_ad_posted);
        Constants.PRODUCT_FOR="";
        DialogPopUps.alertPopUp(this, msg, "",
                getResources().getString(R.string.ok)
                , new CommonListeners.AlertCallBackInterface() {
                    @Override
                    public void neutralClick() {
                        Intent splash = new Intent(AdDetaillsSellActivity.this, HomeActivity.class);
                        splash.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(splash);
                        finish();
                    }
                });

    }

    @Override
    public void hideProgressDialog() {
        DialogPopUps.hideDialog();
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
