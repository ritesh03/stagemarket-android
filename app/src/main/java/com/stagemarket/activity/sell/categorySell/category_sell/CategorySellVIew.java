package com.stagemarket.activity.sell.categorySell.category_sell;

import com.stagemarket.BasePresenter;
import com.stagemarket.pojo.categorySell.Datum;

import java.util.ArrayList;
import java.util.List;

public interface CategorySellVIew extends BasePresenter {
    void getCategoriesSellResponse(ArrayList<Datum> getCategoriesResponseData);
}
