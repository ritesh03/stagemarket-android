package com.stagemarket.activity.sell.categorySell.category_sell;

import com.stagemarket.CommonPresenter;

public interface CategorySellPresenter extends CommonPresenter {
    void fetchCategoriesSellList();

}
