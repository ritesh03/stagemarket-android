package com.stagemarket.activity.sell.categorySell.ad_details_sell;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.stagemarket.R;
import com.stagemarket.pojo.SaveLoginDetailsModel;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.retrofit.RestClient;
import com.stagemarket.utils.ApiGlobal;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.SharedPreff;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.disposables.CompositeDisposable;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.mime.TypedFile;

public class AdDetailsSellPresenterImp implements AdDetailsSellPresenter {
    private final CompositeDisposable disposables = new CompositeDisposable();
    private final SaveLoginDetailsModel saveLoginDetailsModel;
    NetWorkingService netWorkingService;
    AdDetailsSellView adDetailsSellView;

    public AdDetailsSellPresenterImp(AdDetailsSellView adDetailsSellView, NetWorkingService netWorkingService) {
        this.adDetailsSellView = adDetailsSellView;
        this.netWorkingService = netWorkingService;
        saveLoginDetailsModel = SharedPreff.getLoginDetailsModel();

    }

    @Override
    public void subscribe() {
    }

    @Override
    public void unSubscribe() {
        if (disposables != null) {
            disposables.clear();
        }
        adDetailsSellView = null;
        netWorkingService = null;
    }

    @Override
    public void onSubmitAdClick(String title, String price, String description, List<String> productImages,
                                String locName, String lat, String longg, String catID, String subCatID,String quantity,
                                String subSecCatID,int productType) {
        if (title.isEmpty()) {
            CommonUtils.showTopSnackBar((Context) adDetailsSellView, ((Activity) adDetailsSellView).findViewById(android.R.id.content)
                    , ((Activity) adDetailsSellView).getResources().getString(R.string.please_enter_title));
        }
        else if (quantity.isEmpty()) {
            CommonUtils.showTopSnackBar((Context) adDetailsSellView, ((Activity) adDetailsSellView).findViewById(android.R.id.content)
                    , ((Activity) adDetailsSellView).getResources().getString(R.string.please_enter_quantity));
        }
        else if (price.isEmpty()) {
            CommonUtils.showTopSnackBar((Context) adDetailsSellView, ((Activity) adDetailsSellView).findViewById(android.R.id.content)
                    , ((Activity) adDetailsSellView).getResources().getString(R.string.please_enter_price));
        } else if (description.isEmpty()) {
            CommonUtils.showTopSnackBar((Context) adDetailsSellView, ((Activity) adDetailsSellView).findViewById(android.R.id.content)
                    , ((Activity) adDetailsSellView).getResources().getString(R.string.please_enter_description));
        } else {
            if (CommonUtils.isConnectedToInternet(((Context) adDetailsSellView))) {
                adDetailsSellView.showProgressDialog();

                Map<String,TypedFile> files = new HashMap<>();
                for(int i=0;i<productImages.size();i++){
                    File file = new File(productImages.get(i));
                    files.put("image["+i+"]",new TypedFile("multipart/form-data",file));
                }
Log.e("params",files.toString()+" , "+saveLoginDetailsModel.getId()+" , "+catID+" , "+subCatID+" , "+title+" , "+description+" , "+price+" , "+title+" , "+locName+" , "+quantity+" , "+subSecCatID+" , "+productType);

                RestClient.get(60).addProduct(files,saveLoginDetailsModel.getId()
                        , catID,subCatID, title,
                       description, price, title,locName,"","",quantity,
                        subSecCatID,productType
                        , new Callback<ApiGlobal>() {
                            @Override
                            public void success(ApiGlobal apiGlobal, retrofit.client.Response response) {
                                if (adDetailsSellView != null) {
                                    adDetailsSellView.hideProgressDialog();
                                }

                                Log.e("response",apiGlobal.getResult());



                                    if (apiGlobal.getResult().equalsIgnoreCase(Constants.SUCCESS)) {
                                        adDetailsSellView.showMessageDialog(apiGlobal.getMessage());
                                    } else {
                                        adDetailsSellView.showErrorDialog(apiGlobal.getMessage());
                                    }


                            }

                            @Override
                            public void failure(RetrofitError error) {
                                if (adDetailsSellView != null) {
                                    adDetailsSellView.hideProgressDialog();
                                    adDetailsSellView.showErrorDialog(((Context) adDetailsSellView)
                                            .getResources().getString(R.string.something_went_wrong));
                                }
                            }
                        });
            } else {
                CommonUtils.check_your_network_available((Context) adDetailsSellView);
            }
        }
    }
}
