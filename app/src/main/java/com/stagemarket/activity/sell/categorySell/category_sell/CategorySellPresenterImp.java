package com.stagemarket.activity.sell.categorySell.category_sell;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;

import com.stagemarket.AppClass;
import com.stagemarket.R;
import com.stagemarket.pojo.SaveLoginDetailsModel;
import com.stagemarket.pojo.categorySell.GetCategoriesResponse;
import com.stagemarket.pojo.social_login.SocialLoginResponse;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.SharedPreff;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CategorySellPresenterImp implements CategorySellPresenter {

    private CategorySellVIew categorySellVIew = null;
    private NetWorkingService netWorkingService;
    private final CompositeDisposable disposables = new CompositeDisposable();

    public CategorySellPresenterImp(CategorySellVIew categorySellVIew, NetWorkingService netWorkingService) {
        this.categorySellVIew = categorySellVIew;
        this.netWorkingService = netWorkingService;
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        if (disposables != null) {
            disposables.clear();
        }
        categorySellVIew = null;
        netWorkingService = null;
    }


    @Override
    public void fetchCategoriesSellList() {
        if (CommonUtils.isConnectedToInternet(((Context) categorySellVIew))) {

            if (categorySellVIew != null) {
                categorySellVIew.showProgressDialog();
            }

            netWorkingService.getAPI().getCategoriesService().subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<GetCategoriesResponse>() {


                @Override
                public void onSubscribe(Disposable d) {
                    disposables.add(d);
                }

                @Override
                public void onNext(GetCategoriesResponse getCategoriesResponse) {

                    if (CommonUtils.isConnectedToInternet((Context) categorySellVIew)) {

                        if (categorySellVIew != null) {
                            categorySellVIew.hideProgressDialog();
                        }
                        if (getCategoriesResponse.getResult().equalsIgnoreCase(Constants.SUCCESS)) {

                            categorySellVIew.getCategoriesSellResponse(getCategoriesResponse.getData());

                        } else {
                            categorySellVIew.showErrorDialog(getCategoriesResponse.getMessage());
                        }
                    } else {
                        CommonUtils.check_your_network_available((Context) categorySellVIew);
                    }
                }

                @Override
                public void onError(Throwable e) {
                    if (categorySellVIew != null) {
                        categorySellVIew.hideProgressDialog();
                        categorySellVIew.showErrorDialog(((Context) categorySellVIew).getResources().getString(R.string.something_went_wrong));

                    }
                }

                @Override
                public void onComplete() {

                }
            });


        } else {
            CommonUtils.showTopSnackBar((Context) categorySellVIew,
                    ((Activity) categorySellVIew).findViewById(android.R.id.content), ((Context) categorySellVIew).getString(R.string.please_check_internet));
        }
    }
}
