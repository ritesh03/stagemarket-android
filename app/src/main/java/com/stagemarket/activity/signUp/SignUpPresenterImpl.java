package com.stagemarket.activity.signUp;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;

import com.stagemarket.AppClass;
import com.stagemarket.R;
import com.stagemarket.pojo.SaveLoginDetailsModel;
import com.stagemarket.pojo.signup.SignUpResponse;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.SharedPreff;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SignUpPresenterImpl implements SignUpPresenter {

    SignUpView signUpView = null;
    Context context;
    private NetWorkingService netWorkingService;
    private final CompositeDisposable disposables = new CompositeDisposable();

    public SignUpPresenterImpl(SignUpView signUpView) {
        this.signUpView = signUpView;
        context = AppClass.getInstance().getApplicationContext();
        netWorkingService = AppClass.getInstance().getNetworkService();
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        if (disposables != null) {
            disposables.clear();
        }
        signUpView = null;
        netWorkingService = null;
    }

    @Override
    public void onSignpUpClick(String fn, String ln, String emailId, String password, String phno,
                               String organizationType, String companyName, String companyAddress,
                               String companyWebsite,String companyEmail,String companyPwd,String companyPhno) {

        if (organizationType.equalsIgnoreCase("1")) {
            if (TextUtils.isEmpty(fn)) {
                signUpView.showErrorDialog(context.getString(R.string.please_enter_first_name));
            } else if (TextUtils.isEmpty(ln)) {
                signUpView.showErrorDialog(context.getString(R.string.please_enter_last_name));
            } else if (TextUtils.isEmpty(emailId)) {
                signUpView.showErrorDialog(context.getString(R.string.email_address_shouldnot_be_empty));
            } else if (!CommonUtils.validateEmail(emailId)) {
                signUpView.showErrorDialog(context.getString(R.string.please_enter_valid_emailid));
            } else if (TextUtils.isEmpty(password)) {
                signUpView.showErrorDialog(context.getString(R.string.password_shouldnot_be_empty));
            } else if (password.length() < 6) {
                signUpView.showErrorDialog(context.getString(R.string.password_shouldnot_be_less_than_6_characters));
            } else if (TextUtils.isEmpty(phno)) {
                signUpView.showErrorDialog(context.getString(R.string.please_enter_mobile_number));
            }/* else if (organizationType.equalsIgnoreCase("-1")) {
            signUpView.showErrorDialog(context.getString(R.string.please_select_organisation_type));
        } */ else {
                if (CommonUtils.isConnectedToInternet(context)) {
                    signUpView.showProgressDialog();
                    netWorkingService.getAPI().signUp(emailId, fn, ln, password, phno, SharedPreff.getNotificationToken(),
                            Constants.DEVICE_TYPE, organizationType, companyName, companyAddress, companyWebsite)
                            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Observer<SignUpResponse>() {
                                @Override
                                public void onSubscribe(Disposable d) {
                                    disposables.add(d);
                                }

                                @Override
                                public void onNext(SignUpResponse signUpResponse) {
                                    signUpView.hideProgressDialog();
                                    if (signUpView != null) {
                                        signUpView.hideProgressDialog();
                                        if (signUpResponse.getResult().equalsIgnoreCase(Constants.SUCCESS)) {

                                         /*   SaveLoginDetailsModel saveLoginDetailsModel = new SaveLoginDetailsModel();
                                            saveLoginDetailsModel.setFirst_name(signUpResponse.getData().getFirstName());

                                            saveLoginDetailsModel.setName(signUpResponse.getData().getFirstName());
                                            saveLoginDetailsModel.setLast_name(signUpResponse.getData().getLastName());
                                            saveLoginDetailsModel.setToken(signUpResponse.getData().getDeviceToken());
                                            saveLoginDetailsModel.setEmail(signUpResponse.getData().getEmail());
                                            saveLoginDetailsModel.setImage(signUpResponse.getData().getImageUrl());
                                            saveLoginDetailsModel.setId(signUpResponse.getData().getId() + "");
                                            saveLoginDetailsModel.setFromSocialLogin(false);
                                            saveLoginDetailsModel.setCompanyName(signUpResponse.getData().getCompanyName());
                                            saveLoginDetailsModel.setCompanyAddress(signUpResponse.getData().getCompanyAddress());
                                            saveLoginDetailsModel.setCompanyPhone(signUpResponse.getData().getCompanyPhone());
                                            saveLoginDetailsModel.setCompanyWebsite(signUpResponse.getData().getCompanyWebsite());

                                            SharedPreff.saveLoginDetails(saveLoginDetailsModel);*/

                                            signUpView.onSignUpResponse(signUpResponse);

                                        } else {
                                            signUpView.showErrorDialog(signUpResponse.getMessage());
                                        }
                                    }

                                }

                                @Override
                                public void onError(Throwable e) {
                                    signUpView.hideProgressDialog();
                                    signUpView.showErrorDialog(((Context) signUpView).getResources().getString(R.string.something_went_wrong));
                                }

                                @Override
                                public void onComplete() {

                                }
                            });
                } else {
                    CommonUtils.showTopSnackBar(context,
                            ((Activity) signUpView).findViewById(android.R.id.content), context.getString(R.string.please_check_internet));
                }
            }
        } else {
            if (TextUtils.isEmpty(companyName)) {
                signUpView.showErrorDialog(context.getString(R.string.please_enter_company_name));
            } else if (TextUtils.isEmpty(companyEmail)) {
                signUpView.showErrorDialog(context.getString(R.string.email_address_shouldnot_be_empty));
            } else if (!CommonUtils.validateEmail(companyEmail)) {
                signUpView.showErrorDialog(context.getString(R.string.please_enter_valid_emailid));
            } else if (TextUtils.isEmpty(companyPwd)) {
                signUpView.showErrorDialog(context.getString(R.string.password_shouldnot_be_empty));
            } else if (companyPwd.length() < 6) {
                signUpView.showErrorDialog(context.getString(R.string.password_shouldnot_be_less_than_6_characters));
            } else if (TextUtils.isEmpty(companyWebsite)) {
                signUpView.showErrorDialog(context.getString(R.string.please_enter_company_website));
            } else if (TextUtils.isEmpty(companyPhno)) {
                signUpView.showErrorDialog(context.getString(R.string.please_enter_mobile_number));
            } else if (TextUtils.isEmpty(companyAddress)) {
                signUpView.showErrorDialog(context.getString(R.string.please_enter_companyt_address));
            }
          /* else if (organizationType.equalsIgnoreCase("-1")) {
            signUpView.showErrorDialog(context.getString(R.string.please_select_organisation_type));
        } */
            else {
                if (CommonUtils.isConnectedToInternet(context)) {
                    signUpView.showProgressDialog();
                    netWorkingService.getAPI().signUp(companyEmail, fn, ln, companyPwd, companyPhno, SharedPreff.getNotificationToken(),
                            Constants.DEVICE_TYPE, organizationType, companyName, companyAddress, companyWebsite)
                            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Observer<SignUpResponse>() {
                                @Override
                                public void onSubscribe(Disposable d) {
                                    disposables.add(d);
                                }

                                @Override
                                public void onNext(SignUpResponse signUpResponse) {
                                    signUpView.hideProgressDialog();
                                    if (signUpView != null) {
                                        signUpView.hideProgressDialog();
                                        if (signUpResponse.getResult().equalsIgnoreCase(Constants.SUCCESS)) {
/*
                                            SaveLoginDetailsModel saveLoginDetailsModel = new SaveLoginDetailsModel();
                                            saveLoginDetailsModel.setFirst_name(signUpResponse.getData().getFirstName());

                                            saveLoginDetailsModel.setName(signUpResponse.getData().getFirstName());
                                            saveLoginDetailsModel.setLast_name(signUpResponse.getData().getLastName());
                                            saveLoginDetailsModel.setToken(signUpResponse.getData().getDeviceToken());
                                            saveLoginDetailsModel.setEmail(signUpResponse.getData().getEmail());
                                            saveLoginDetailsModel.setImage(signUpResponse.getData().getImageUrl());
                                            saveLoginDetailsModel.setId(signUpResponse.getData().getId() + "");
                                            saveLoginDetailsModel.setFromSocialLogin(false);
                                            saveLoginDetailsModel.setCompanyName(signUpResponse.getData().getCompanyName());
                                            saveLoginDetailsModel.setCompanyAddress(signUpResponse.getData().getCompanyAddress());
                                            saveLoginDetailsModel.setCompanyPhone(signUpResponse.getData().getCompanyPhone());
                                            saveLoginDetailsModel.setCompanyWebsite(signUpResponse.getData().getCompanyWebsite());

                                            SharedPreff.saveLoginDetails(saveLoginDetailsModel);*/

                                            signUpView.onSignUpResponse(signUpResponse);

                                        } else {
                                            signUpView.showErrorDialog(signUpResponse.getMessage());
                                        }
                                    }

                                }

                                @Override
                                public void onError(Throwable e) {
                                    signUpView.hideProgressDialog();
                                    signUpView.showErrorDialog(((Context) signUpView).getResources().getString(R.string.something_went_wrong));
                                }

                                @Override
                                public void onComplete() {

                                }
                            });
                } else {
                    CommonUtils.showTopSnackBar(context,
                            ((Activity) signUpView).findViewById(android.R.id.content), context.getString(R.string.please_check_internet));
                }
            }
        }

    }
}
