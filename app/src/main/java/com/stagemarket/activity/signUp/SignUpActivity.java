package com.stagemarket.activity.signUp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.stagemarket.R;
import com.stagemarket.pojo.signup.SignUpResponse;
import com.stagemarket.utils.CommonListeners;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.DialogPopUps;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SignUpActivity extends Activity implements SignUpView {

    @BindView(R.id.img_header_back)
    ImageView imgHeaderBack;
    @BindView(R.id.et_firstname)
    EditText etFirstname;
    @BindView(R.id.et_lastname)
    EditText etLastname;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.et_mobile)
    EditText etMobile;
    @BindView(R.id.et_email_id)
    EditText etEmailId;
    @BindView(R.id.tv_signUp)
    TextView tvSignUp;
    @BindView(R.id.tv_privacy_policy)
    TextView tvPrivacyPolicy;
    @BindView(R.id.tv_terms_of_service)
    TextView tvTermsOfService;
    @BindView(R.id.tv_by_sign)
    TextView tv_by_sign;
    @BindView(R.id.rb_company)
    RadioButton rb_company;
    @BindView(R.id.rb_individual)
    RadioButton rb_individual;
    SignUpPresenter mSignUpPresenter;
    @BindView(R.id.lyt_individual)
    LinearLayout lytIndividual;

    @BindView(R.id.et_email_company)
    EditText etEmailCompany;
    @BindView(R.id.et_password_company)
    EditText etPasswordCompany;
    @BindView(R.id.et_website)
    EditText etWebsite;
    @BindView(R.id.et_companyname)
    EditText etCompanyname;
    @BindView(R.id.et_address_company)
    EditText etAddressCompany;
    @BindView(R.id.et_phno_company)
    EditText etPhnoCompany;

    @BindView(R.id.lyt_company)
    LinearLayout lytCompany;
    private String organizationType = "1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);

        mSignUpPresenter = new SignUpPresenterImpl(this);
        imgHeaderBack.setImageResource(R.drawable.ic_back);

        initViews();
    }

    private void initViews() {
      lytIndividual.setVisibility(View.VISIBLE);


        rb_company.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    lytCompany.setVisibility(View.VISIBLE);
                    lytIndividual.setVisibility(View.GONE);
                    rb_individual.setChecked(false);
                    organizationType = "2";
                } else {
                    lytCompany.setVisibility(View.GONE);
                    lytIndividual.setVisibility(View.VISIBLE);
                    rb_individual.setChecked(true);
                    organizationType = "1";
                }
            }
        });

        rb_individual.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    lytCompany.setVisibility(View.GONE);
                    lytIndividual.setVisibility(View.VISIBLE);
                    rb_company.setChecked(false);
                    organizationType = "1";
                } else {
                    lytCompany.setVisibility(View.VISIBLE);
                    lytIndividual.setVisibility(View.GONE);
                    rb_company.setChecked(true);
                    organizationType = "2";
                }
            }
        });

    }

    @OnClick(R.id.img_header_back)
    public void onBackClick() {
        finish();
    }

    @OnClick(R.id.tv_privacy_policy)
    public void onPrivacyPolicyClick() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://206.189.23.147/StageMarket/public/term_and_condition"));
        startActivity(browserIntent);
    }

    @OnClick(R.id.tv_terms_of_service)
    public void onTermsServiceClick() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://206.189.23.147/StageMarket/public/term_and_condition"));
        startActivity(browserIntent);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        CommonUtils.hideSoftKeyboard(SignUpActivity.this);
        return super.dispatchTouchEvent(ev);

    }

    @OnClick(R.id.tv_signUp)
    public void onSignUpClick() {
        String fn = etFirstname.getText().toString().trim();
        String ln = etLastname.getText().toString().trim();
        String emailId = etEmailId.getText().toString().trim();
        String pwd = etPassword.getText().toString().trim();
        String mobileNo = etMobile.getText().toString().trim();
        String companyName = etCompanyname.getText().toString().trim();
        String companyEmail = etEmailCompany.getText().toString().trim();
        String companyAddress = etAddressCompany.getText().toString().trim();
        String website = etWebsite.getText().toString().trim();
        String companyPwd = etPasswordCompany.getText().toString().trim();
        String companyPhno = etPhnoCompany.getText().toString().trim();

        mSignUpPresenter.onSignpUpClick(fn, ln, emailId, pwd, mobileNo,
                organizationType,companyName,companyAddress,website,companyEmail,companyPwd,companyPhno);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void showProgressDialog() {
        DialogPopUps.showProgressDialog(SignUpActivity.this, getResources().getString(R.string.please_wait));
    }

    @Override
    public void showErrorDialog(String msg) {
        CommonUtils.showTopSnackBar(this, findViewById(android.R.id.content), msg);
    }

    @Override
    public void showMessageDialog(String msg) {
        DialogPopUps.alertPopUp(SignUpActivity.this, msg, getString(R.string.activation_required),
                getResources().getString(R.string.ok)
                , new CommonListeners.AlertCallBackInterface() {
                    @Override
                    public void neutralClick() {
                        finish();
                    }
                });
    }

    @Override
    public void hideProgressDialog() {
        DialogPopUps.hideDialog();
    }

    @Override
    public void onSignUpResponse(SignUpResponse signUpResponse) {
        //SharedPreff.saveSignUpResponse(signUpResponse);

        showMessageDialog(getString(R.string.signup_success_msg));
    }
}
