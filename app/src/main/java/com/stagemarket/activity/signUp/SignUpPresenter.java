package com.stagemarket.activity.signUp;

import com.stagemarket.CommonPresenter;

public interface SignUpPresenter extends CommonPresenter {
    void onSignpUpClick(String fn, String ln, String emailId, String password, String phno,
                        String organizationType,String companyName,String companyAddress, String companyWebsite,
                        String companyEmail,String companyPwd,String companyPhno);
}
