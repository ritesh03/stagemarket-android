package com.stagemarket.activity.signUp;

import com.stagemarket.BasePresenter;
import com.stagemarket.pojo.signup.SignUpResponse;

public interface SignUpView extends BasePresenter {
    void onSignUpResponse(SignUpResponse signUpResponse);
}
