package com.stagemarket.activity.orderSummary;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stagemarket.BaseActivity;
import com.stagemarket.R;
import com.stagemarket.activity.addAddress.AddAddressActivity;
import com.stagemarket.activity.viewProfile.ViewProfileActivity;
import com.stagemarket.pojo.discover.ProductData;
import com.stagemarket.pojo.viewOtherProfile.DataItem;
import com.stagemarket.utils.CommonListeners;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.DialogPopUps;
import com.stagemarket.utils.SharedPreff;

import java.lang.reflect.Type;
import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderSummary extends BaseActivity implements OrderSummaryView {
int maxQyt=1;
    @BindView(R.id.tvplus)
    TextView tvplus;
    @BindView(R.id.tvminus)
    TextView tvminus;
    @BindView(R.id.tvquantity)
    TextView tvquantity;
    @BindView(R.id.title_name)
    TextView titleName;
    @BindView(R.id.tvVat)
    TextView tvVat;
    @BindView(R.id.img_header_back)
    ImageView imgHeaderBack;

    @BindView(R.id.img_product)
    ImageView img_product;

    @BindView(R.id.lyt_image)
    LinearLayout lytImage;

    @BindView(R.id.tv_product_price)
    TextView tv_product_price;

    @BindView(R.id.tv_sold_by_username)
    TextView tv_sold_by_username;
    @BindView(R.id.view_profile)
    TextView viewProfile;

    @BindView(R.id.relyt_shipping_details)
    RelativeLayout relyt_shipping_details;

    @BindView(R.id.lyt_add_shipping_address)
    LinearLayout lytAddShipingAddress;

    @BindView(R.id.tv_big_total)
    TextView tv_big_total;

    @BindView(R.id.tv_total_price)
    TextView tv_total_price;


    @BindView(R.id.rg)
    RadioGroup rg;

    @BindView(R.id.tv_place_your_order)
    TextView tv_place_your_order;

    @BindView(R.id.header)
    View header;

    @BindView(R.id.tv_name)
    TextView tv_name;

    @BindView(R.id.tv_street_addresss)
    TextView tv_street_addresss;

    @BindView(R.id.tv_mobile_number)
    TextView tv_mobile_number;
    @BindView(R.id.rb_paybycard)
    RadioButton rb_paybycard;
    @BindView(R.id.tvProductName)
    TextView tvProductName;
    @BindView(R.id.rb_cash_on_delivery)
    RadioButton rb_cash_on_delivery;

    @BindView(R.id.tv_choose_a_different_address)
    TextView tv_choose_a_different_address;
    private String productDetail = "";
    private ProductData productData;
    OrderSummaryPresenter orderSummaryPresenter;
    private String billingName = "", billingAddress = "", billingCity = "";
    private String billingContact = "";
    private String viewProData = "";
    private DataItem viewData;
int quantity=1;
String type;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_summary);
        ButterKnife.bind(this);
        initToolbar();
        clickRadioButton();


        orderSummaryPresenter = new OrderSummaryPresenterImp(this);

        if (getIntent().getExtras() != null) {
            productDetail = getIntent().getStringExtra(Constants.PROD_DETAIL);
            viewProData = getIntent().getStringExtra(Constants.VIEW_PROD_DETAIL);
            if (productDetail != null) {
                type="product";
                Gson gson = new Gson();
                Type type = new TypeToken<ProductData>() {
                }.getType();
                productData = gson.fromJson(productDetail, type);
                initViews(productData);


                maxQyt=Integer.parseInt(productData.getQuantity());
            } else if (viewProData != null) {
                type="viewproduct";
                Gson gson = new Gson();
                Type type = new TypeToken<DataItem>() {
                }.getType();
                viewData = gson.fromJson(viewProData, type);
                 initViews(viewData);
                maxQyt=Integer.parseInt(viewData.getQuantity());
            }
        }


        tvplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Integer.parseInt(tvquantity.getText().toString())==maxQyt)
                {

                }
                else
                {
                    int q= Integer.parseInt(tvquantity.getText().toString() );
                    q=q+1;
                    Log.e("q",q+"");
                    tvquantity.setText(String.valueOf(q));


                    if(type.equalsIgnoreCase("product")) {
                        productCal(q);
                    }
                    else
                    {
                        viewProductCal(q);
                    }




                }


            }
        });
        tvminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Integer.parseInt(tvquantity.getText().toString())==1)
                {

                }
                else
                {
                    int q=Integer.parseInt(tvquantity.getText().toString());
                    q=q-1;

                    tvquantity.setText(String.valueOf(q));
if(type.equalsIgnoreCase("product")) {
    productCal(q);
}
else
{
    viewProductCal(q);
}
                }
            }
        });
    }
public void productCal(int q)
{

    float productPrice=Float.parseFloat(String.valueOf(productData.getCost()))*q;



    float priceval =  ((Integer.parseInt( productData.getCommision()) * productPrice )/100);
    float newPriceval = priceval+ productPrice;
    // tv_product_price.setText(Constants.DOLlAR + " " + new DecimalFormat("##.##").format(newPriceval));

        /*int priceval = (int) ((Integer.parseInt(productData.getCommision()) * Float.parseFloat(String.valueOf(productData.getCost()))) / 100);
        tv_product_price.setText(productData.getCurrency() + " " + priceval);*/

    // tv_product_price.setText(productData.getCurrency() + productData.getCost());
    tv_sold_by_username.setText(productData.getUsername());
    tv_big_total.setText(productData.getCurrency() + " " +new DecimalFormat("##.##").format(newPriceval) );
    tvProductName.setText(productData.getName());
    float vat = ((11 * newPriceval) / 100);
    tvVat.setText(Constants.DOLlAR +new DecimalFormat("##.##").format(vat)  + "");

    float total = newPriceval + vat;
    tv_total_price.setText(Constants.DOLlAR + new DecimalFormat("##.##").format(total) + "");

}
 public void viewProductCal(int q)
{

    float productPrice=Float.parseFloat(String.valueOf(viewData.getCost()))*q;



    float priceval =  ((Integer.parseInt( viewData.getCommision()) * productPrice )/100);
    float newPriceval = priceval+ productPrice;
    // tv_product_price.setText(Constants.DOLlAR + " " + new DecimalFormat("##.##").format(newPriceval));

        /*int priceval = (int) ((Integer.parseInt(productData.getCommision()) * Float.parseFloat(String.valueOf(productData.getCost()))) / 100);
        tv_product_price.setText(productData.getCurrency() + " " + priceval);*/

    // tv_product_price.setText(productData.getCurrency() + productData.getCost());
    tv_sold_by_username.setText(viewData.getUsername());
    tv_big_total.setText(viewData.getCurrency() + " " +new DecimalFormat("##.##").format(newPriceval) );
    tvProductName.setText(viewData.getName());
    float vat = ((11 * newPriceval) / 100);
    tvVat.setText(Constants.DOLlAR +new DecimalFormat("##.##").format(vat)  + "");

    float total = newPriceval + vat;
    tv_total_price.setText(Constants.DOLlAR + new DecimalFormat("##.##").format(total) + "");

}
    private void initViews(ProductData productData) {

        if (!SharedPreff.getLoginDetailsModel().getId().equalsIgnoreCase(productData.getUserId())) {
            viewProfile.setVisibility(View.GONE);
        } else {
            viewProfile.setVisibility(View.GONE);
        }
        if (productData.getmProductImages().size() != 0) {
            CommonUtils.setImage(this, img_product, productData.getmProductImages().get(0).getImage());
        } else {
            img_product.setImageResource(R.drawable.not_available);
        }

        float priceval =  ((Integer.parseInt( productData.getCommision()) * Float.parseFloat(String.valueOf(productData.getCost())))/100);
        float newPriceval = priceval+ Float.parseFloat(String.valueOf(productData.getCost()));
        tv_product_price.setText(Constants.DOLlAR + " " + new DecimalFormat("##.##").format(newPriceval));

        /*int priceval = (int) ((Integer.parseInt(productData.getCommision()) * Float.parseFloat(String.valueOf(productData.getCost()))) / 100);
        tv_product_price.setText(productData.getCurrency() + " " + priceval);*/

        // tv_product_price.setText(productData.getCurrency() + productData.getCost());
        tv_sold_by_username.setText(productData.getUsername());
        tv_big_total.setText(productData.getCurrency() + " " +new DecimalFormat("##.##").format(newPriceval) );
        tvProductName.setText(productData.getName());
        float vat = ((11 * newPriceval) / 100);
        tvVat.setText(Constants.DOLlAR +new DecimalFormat("##.##").format(vat)  + "");

        float total = newPriceval + vat;
        tv_total_price.setText(Constants.DOLlAR + new DecimalFormat("##.##").format(total) + "");
    }

    private void initViews(DataItem productData) {

        if (!SharedPreff.getLoginDetailsModel().getId().equalsIgnoreCase(String.valueOf(productData.getUserId()))) {
            viewProfile.setVisibility(View.GONE);
        } else {
            viewProfile.setVisibility(View.GONE);
        }
        if (productData.getProductImages().size() != 0) {
            CommonUtils.setImage(this, img_product, productData.getProductImages().get(0).getImage());
        } else {
            img_product.setImageResource(R.drawable.not_available);
        }

//        float priceval = productData.getCost();
//        tv_product_price.setText(productData.getCurrency() + " " + productData.getCost());

        float priceval =  ((Integer.parseInt( productData.getCommision()) * Float.parseFloat(String.valueOf(productData.getCost())))/100);
        float newPriceval = priceval+ Float.parseFloat(String.valueOf(productData.getCost()));
        tv_product_price.setText(Constants.DOLlAR + " " + new DecimalFormat("##.##").format(newPriceval));

        // tv_product_price.setText(productData.getCurrency() + productData.getCost());
        tv_sold_by_username.setText(productData.getUsername());
        tv_big_total.setText(Constants.DOLlAR + " " + new DecimalFormat("##.##").format(newPriceval));
        tvProductName.setText(productData.getName());
        float vat =    ((11 * newPriceval) / 100) ;
        tvVat.setText(Constants.DOLlAR + new DecimalFormat("##.##").format(vat) + "");

        float total = newPriceval + vat;
        tv_total_price.setText(Constants.DOLlAR + new DecimalFormat("##.##").format(total) + "");
    }


    private void initToolbar() {
        header.setBackgroundColor(ContextCompat.getColor(this, R.color.greynew));
        imgHeaderBack.setImageResource(R.drawable.ic_back);
        DrawableCompat.setTint(imgHeaderBack.getDrawable(), ContextCompat.getColor(this, R.color.black));
        titleName.setText(R.string.order_summary);
        titleName.setTextColor(ContextCompat.getColor(this, R.color.black));
    }

    private void clickRadioButton() {

        rb_cash_on_delivery.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    rb_paybycard.setChecked(false);
                } else {
                    rb_paybycard.setChecked(true);
                }
            }
        });

        rb_paybycard.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    rb_cash_on_delivery.setChecked(false);
                } else {
                    rb_cash_on_delivery.setChecked(true);
                }
            }
        });
      /*  rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                View radioButton = radioGroup.findViewById(checkedId);
                int idx = radioGroup.indexOfChild(radioButton);
                if (idx == 0) {
                    rb_paybycard.setChecked(true);
                    rb_cash_on_delivery.setChecked(false);

                } else {

                    rb_paybycard.setChecked(false);
                    rb_cash_on_delivery.setChecked(true);

                }
            }
        });*/
    }

    @OnClick(R.id.img_header_back)
    public void onBackClick() {
        finish();
    }

    @OnClick(R.id.tv_place_your_order)
    public void onPlaceOrderClick() {
        if (lytAddShipingAddress.getVisibility() == View.VISIBLE) {
            CommonUtils.showTopSnackBar(this, findViewById(android.R.id.content), getString(R.string.please_add_shipping_details));
        } else if (!rb_cash_on_delivery.isChecked() &&
                !rb_paybycard.isChecked()) {
            CommonUtils.showTopSnackBar(this, findViewById(android.R.id.content),
                    getString(R.string.please_check_payment_method));
        } else {
            String orderType = "";     //  1 card
            // 2 cod
            if (rb_cash_on_delivery.isChecked()) {
                orderType = Constants.COD;
            } else {
                orderType = Constants.CARD;
            }

          /*  @Field("subCategoryName") String subCategoryName,
            @Field("billingName") String billingName,
            @Field("billingAddress") String billingAddress,
            @Field("billingCity") String billingCity,
            @Field("billingContact") String billingContact,
            @Field("billingAmount") String billingAmount,
            @Field("commision") String commision,
            @Field("vat") String vat*/
if(productData!=null)
{
    Log.e("re",tv_big_total.getText().toString()
            .replace("$", "").trim());
            if (productData.getmProductImages().size() != 0) {
                orderSummaryPresenter.onPlaceOrderClick(productData.getId() + "", productData.getUserId(),
                        productData.getCost() + "", orderType, productData.getCatName(), productData.getSubCatName(),
                        billingName, billingAddress, billingCity, billingContact, tv_total_price.getText().toString()
                                .replace("$", "").trim()
                        , productData.getCommision(), tvVat.getText().toString()
                                .replace("$", "").trim() + "",
                        productData.getmProductImages().get(0).getImage(),productData.getUsername(),tvquantity.getText().toString().trim());
            } }

            if(viewData!=null)
            {
                orderSummaryPresenter.onPlaceOrderClick(viewData.getId() + "",String.valueOf(viewData.getUserId()),
                        viewData.getCost() + "", orderType, viewData.getCatName(), viewData.getSubCatName(),
                        billingName, billingAddress, billingCity, billingContact, tv_total_price.getText().toString()
                                .replace("$", "").trim()
                        , viewData.getCommision(), tvVat.getText().toString()
                                .replace("$", "").trim() + "",
                        Constants.NO_IMAGE,viewData.getUsername(),tvquantity.getText().toString().trim());
            }}}



    @OnClick(R.id.view_profile)
    public void onViewProfileClick() {

        if (!SharedPreff.getLoginDetailsModel().getId().equalsIgnoreCase(productData.getUserId())) {
            startActivity(new Intent(this, ViewProfileActivity.class)
                    .putExtra(Constants.OTHER_PROFILE_USER_ID, productData.getUserId())
                    .putExtra(Constants.OTHER_PROFILE_COMPANY_ID, productData.getCompanyId() + ""));
        }
    }

    @OnClick(R.id.lyt_add_shipping_address)
    public void OnClickAddShippingAddress() {
        Intent i = new Intent(this, AddAddressActivity.class);
        startActivityForResult(i, 1);
    }

    @OnClick(R.id.tv_choose_a_different_address)
    public void OnClickChooseADifferntAddress() {
        Intent i = new Intent(this, AddAddressActivity.class);
        startActivityForResult(i, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {

                String name = data.getStringExtra(Constants.NAME);
                String street_address = data.getStringExtra(Constants.STREET_ADDRESS);
                String city = data.getStringExtra(Constants.CITY);
                String mobile_number = data.getStringExtra(Constants.MOBILE_NUMBER);
                String type = data.getStringExtra("type");
                billingName = name;
                billingAddress = street_address + " " + city;
                billingCity = city;
                billingContact = mobile_number;
                if (!name.equalsIgnoreCase("") && !street_address.equalsIgnoreCase("")
                        && !city.equalsIgnoreCase("") && !mobile_number.equalsIgnoreCase("")) {

                    relyt_shipping_details.setVisibility(View.VISIBLE);
                    lytAddShipingAddress.setVisibility(View.GONE);

                    tv_name.setText(name);
                    tv_street_addresss.setText(street_address + ", " + city);
                    tv_mobile_number.setText(mobile_number);
                    tv_street_addresss.setVisibility(View.VISIBLE);
                    tv_mobile_number.setVisibility(View.VISIBLE);
                }
                if(type.equalsIgnoreCase("1"))
                {
                    relyt_shipping_details.setVisibility(View.VISIBLE);
                    lytAddShipingAddress.setVisibility(View.GONE);
                    tv_name.setText(name);
                    tv_street_addresss.setVisibility(View.GONE);
                    tv_mobile_number.setVisibility(View.GONE);
                }

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                // Toast.makeText(this, "No Result Found", Toast.LENGTH_SHORT).show();
            }
        }
    }//onActivityResult


    @Override
    public void showProgressDialog() {
        DialogPopUps.showProgressDialog(this, getResources().getString(R.string.please_wait));

    }

    @Override
    public void showErrorDialog(String msg) {
        CommonUtils.showTopSnackBar(this, findViewById(android.R.id.content), msg);

    }

    @Override
    public void showMessageDialog(String msg) {
        DialogPopUps.alertPopUp(this, msg, getString(R.string.alert),
                getResources().getString(R.string.ok)
                , new CommonListeners.AlertCallBackInterface() {
                    @Override
                    public void neutralClick() {
                        finish();
                    }
                });
    }

    @Override
    public void hideProgressDialog() {
        DialogPopUps.hideDialog();

    }
}

