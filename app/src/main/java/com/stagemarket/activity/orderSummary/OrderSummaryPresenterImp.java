package com.stagemarket.activity.orderSummary;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.gson.Gson;
import com.stagemarket.AppClass;
import com.stagemarket.R;
import com.stagemarket.activity.viewProfile.ViewProfileActivity;
import com.stagemarket.pojo.SaveLoginDetailsModel;
import com.stagemarket.pojo.placeOrder.PlaceOrderResponse;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.SharedPreff;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class OrderSummaryPresenterImp implements OrderSummaryPresenter {
    OrderSummaryView orderSummaryView;
    private NetWorkingService netWorkingService;
    private final CompositeDisposable disposables = new CompositeDisposable();
    Context mContext;
    SaveLoginDetailsModel saveLoginDetailsModel;

    public OrderSummaryPresenterImp(OrderSummaryView orderSummaryView) {
        this.orderSummaryView = orderSummaryView;
        mContext = AppClass.getInstance().getApplicationContext();
        saveLoginDetailsModel = SharedPreff.getLoginDetailsModel();
        netWorkingService = AppClass.getInstance().getNetworkService();

    }

    @Override
    public void subscribe() {
    }

    @Override
    public void unSubscribe() {
        if (disposables != null) {
            disposables.clear();
        }
        orderSummaryView = null;
        netWorkingService = null;
    }

    @Override
    public void onPlaceOrderClick(String productId, String sellerId, String price, String orderType,
                                  String catName, String subCatNAme, String billingName, String billingAddress,
                                  String billingCity, String billingContact, String billingAmount, String commission,
                                  String vat, final String image, final String soldBy,final String qty) {
        if (CommonUtils.isConnectedToInternet(mContext)) {
            orderSummaryView.showProgressDialog();

            netWorkingService.getAPI().orderProduct(
                    saveLoginDetailsModel.getId(),
                    productId,
                    sellerId,
                    price,
                    orderType, catName, subCatNAme, billingName, billingAddress, billingCity,
                    billingContact, billingAmount, commission, vat,qty
            )
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<PlaceOrderResponse>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            disposables.add(d);
                        }

                        @Override
                        public void onNext(PlaceOrderResponse placeOrderResponse) {
                            orderSummaryView.hideProgressDialog();

                            if (orderSummaryView != null) {
                                orderSummaryView.hideProgressDialog();

Gson  gson=new Gson();
                                Log.e("res",gson.toJson(placeOrderResponse)+"");
                                if (placeOrderResponse.getResult().equalsIgnoreCase(Constants.SUCCESS)) {
                                    if (placeOrderResponse.getData().getmData().getmOrderType().equalsIgnoreCase(Constants.COD)) {
                                        // orderSummaryView.showMessageDialog(placeOrderResponse.getMessage());
                                        mContext.startActivity(new Intent(mContext, ThankYouActivity.class)
                                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                                .putExtra(Constants.ORDER_COMPLETE, new Gson().toJson(placeOrderResponse))
                                                .putExtra("seller_name",soldBy )
                                                .putExtra(Constants.IMAGE, image));
                                       // ((Activity) orderSummaryView).finish();
                                    } else {
                                        mContext.startActivity(new Intent(mContext, PlaceOrderActivity.class)
                                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                                .putExtra(Constants.PAYMENT_URL, placeOrderResponse.getData()
                                                        .getPaymentUrl()).putExtra("seller_name",soldBy )


                                                .putExtra(Constants.ORDER_COMPLETE, new Gson().toJson(placeOrderResponse))
                                                .putExtra("seller_name",soldBy )
                                                .putExtra(Constants.IMAGE, image)
                                        );

                                      //  ((Activity) orderSummaryView).finish();
                                    }
                                } else {
                                    orderSummaryView.showErrorDialog(placeOrderResponse.getMessage());
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            orderSummaryView.hideProgressDialog();
                            orderSummaryView.showErrorDialog(mContext.getResources().getString(R.string.something_went_wrong));
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        } else {
            CommonUtils.showTopSnackBar(mContext,
                    ((OrderSummary) orderSummaryView).findViewById(android.R.id.content),
                    mContext.getString(R.string.please_check_internet));
        }
    }
}
