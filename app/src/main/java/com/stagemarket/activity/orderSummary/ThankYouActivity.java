package com.stagemarket.activity.orderSummary;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stagemarket.R;
import com.stagemarket.activity.home.HomeActivity;
import com.stagemarket.activity.welcome.WelcomeActivity;
import com.stagemarket.pojo.discover.ProductData;
import com.stagemarket.pojo.placeOrder.Data;
import com.stagemarket.pojo.placeOrder.PlaceOrderResponse;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;

import java.lang.reflect.Type;
import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ThankYouActivity extends AppCompatActivity {
    @BindView(R.id.header)
    View header;
    @BindView(R.id.tv_paymentMode)
    TextView tv_paymentMode;
    @BindView(R.id.tv_orderId)
    TextView tv_orderId;
    @BindView(R.id.title_name)
    TextView titleName;
    @BindView(R.id.img_header_back)
    ImageView imgHeaderBack;
    @BindView(R.id.tv_price)
    TextView tv_price;
    @BindView(R.id.tv_quantity)
    TextView tv_quantity;
    @BindView(R.id.tvVat)
    TextView tvVat;
    @BindView(R.id.tv_total_price)
    TextView tv_total_price;
    @BindView(R.id.img_product)
    ImageView img_product;
    @BindView(R.id.tvproductname)
    TextView tvproductname;
    @BindView(R.id.tv_deliveryDate)
    TextView tv_deliveryDate;
    private String orderDetail = "";
    private PlaceOrderResponse orderCompleteResponse;
    private String image = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thank_you);
        ButterKnife.bind(this);

        initToolbar();

        if (getIntent().getExtras() != null) {
            orderDetail = getIntent().getStringExtra(Constants.ORDER_COMPLETE);
            image = getIntent().getStringExtra(Constants.IMAGE);
            Gson gson = new Gson();
            Type type = new TypeToken<PlaceOrderResponse>() {
            }.getType();
            orderCompleteResponse = gson.fromJson(orderDetail, type);
            initViews(orderCompleteResponse);
        }

    }

    private void initViews(PlaceOrderResponse orderCompleteResponse) {

        if(orderCompleteResponse.getData().getOrderId()==null)
        {
            tv_orderId.setText("Order ID: " + "");
        }
        else
        {
            tv_orderId.setText("Order ID: " + orderCompleteResponse.getData().getOrderId());
        }

        if (orderCompleteResponse.getData().getmData().getmOrderType().equalsIgnoreCase(Constants.COD)) {
            tv_paymentMode.setText(Constants.CASH_ON_DELIVERY);
        } else {
            tv_paymentMode.setText(Constants.CREDIT_CARD);
        }

        float priceval =  ((Integer.parseInt( orderCompleteResponse.getData().getmData().getmCommision()) * Float.parseFloat(String.valueOf(orderCompleteResponse.getData().getmData().getmCost())))/100);
        float newPriceval = priceval+ Float.parseFloat(String.valueOf(orderCompleteResponse.getData().getmData().getmCost()));
      //  tv_product_price.setText(Constants.DOLlAR + " " + new DecimalFormat("##.##").format(newPriceval));
        tv_price.setText(Constants.DOLlAR + " " + new DecimalFormat("##.##").format(newPriceval));
        tv_total_price.setText(Constants.DOLlAR + ""+orderCompleteResponse.getData().getmData().getmBillingAmount());
        tvVat.setText(Constants.DOLlAR + ""+orderCompleteResponse.getData().getmData().getmVat());

        if (!image.equalsIgnoreCase(Constants.NO_IMAGE)) {
            CommonUtils.setImage(this, img_product, image);
        } else {
            img_product.setImageResource(R.drawable.not_available);
        }
        tvproductname.setText(orderCompleteResponse.getData().getmData().getmProductName());
        tv_quantity.setText(orderCompleteResponse.getData().getmData().getmQuantity());
        tv_deliveryDate.setText("Sold by: " + getIntent().getStringExtra("seller_name"));
    }

    @OnClick(R.id.tv_continuue_shopping)
    public void onContinueShopping() {
        startActivity(new Intent(ThankYouActivity.this, HomeActivity.class));
        finish();
    }

    private void initToolbar() {
        header.setBackgroundColor(ContextCompat.getColor(this, R.color.greynew));
        imgHeaderBack.setVisibility(View.GONE);
        titleName.setText(R.string.thankyou);
        titleName.setTextColor(ContextCompat.getColor(this, R.color.black));
    }
}
