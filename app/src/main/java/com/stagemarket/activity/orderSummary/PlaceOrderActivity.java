package com.stagemarket.activity.orderSummary;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.stagemarket.R;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.DialogPopUps;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PlaceOrderActivity extends AppCompatActivity {

    @BindView(R.id.header)
    View header;
    @BindView(R.id.title_name)
    TextView titleName;
    @BindView(R.id.img_header_back)
    ImageView imgHeaderBack;
    @BindView(R.id.webView)
    WebView webView;
    private WebView wv1;
    String paymentUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_order);
        ButterKnife.bind(this);
        if(getIntent().getExtras()!=null){
             paymentUrl = getIntent().getStringExtra(Constants.PAYMENT_URL);
           // paymentUrl = "https://testsecureacceptance.cybersource.com/checkout";
Log.e("backend url",""+getIntent().getStringExtra(Constants.PAYMENT_URL));
        }


         initToolbar();
          loadData();

        imgHeaderBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
             view.loadUrl(url);
            return false;
        }
    }
    private void loadData() {
        DialogPopUps.showProgressDialog(PlaceOrderActivity.this,
                getResources().getString(R.string.please_wait));
      webView.setWebViewClient(new myWebClient());
       // webView.getSettings().setJavaScriptEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }

        webView.clearHistory();
        webView.clearCache(true);
      webView.getSettings().setJavaScriptEnabled(true);
      //  webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
        webView.loadUrl(paymentUrl);
    }

    public class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
          //  super.onPageStarted(view, url, favicon);
            DialogPopUps.showProgressDialog(PlaceOrderActivity.this,
                    getResources().getString(R.string.please_wait));
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            view.loadUrl(url);
return true;        }

        @Override
        public void onFormResubmission(WebView view, Message dontResend, Message resend) {
            super.onFormResubmission(view, dontResend, resend);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub

             //   super.onPageFinished(view, url);
                Log.e("Result",url+","+view.getTitle());
if(url.equalsIgnoreCase("https://secureacceptance.cybersource.com/canceled"))
{
    Toast.makeText(PlaceOrderActivity.this, "Payment Failed", Toast.LENGTH_SHORT).show();

      finish();

}
else
    if(url.equalsIgnoreCase("https://secureacceptance.cybersource.com/declined"))
    {
        Toast.makeText(PlaceOrderActivity.this, "Payment Failed", Toast.LENGTH_SHORT).show();
        finish();
    }
    else   if(url.equalsIgnoreCase("http://206.189.23.147/StageMarket/public/api/paymentSuccess"))
       // http://206.189.23.147/StageMarket/public/api/paymentSuccess
    {
        startActivity(new Intent(PlaceOrderActivity.this, ThankYouActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .putExtra(Constants.ORDER_COMPLETE,  getIntent().getStringExtra(Constants.ORDER_COMPLETE))
                .putExtra("seller_name", getIntent().getStringExtra("seller_name") )
                .putExtra(Constants.IMAGE,  getIntent().getStringExtra(Constants.IMAGE)));
        finish();
    } /*else   if(url.contains("success"))

    {
        startActivity(new Intent(PlaceOrderActivity.this, ThankYouActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .putExtra(Constants.ORDER_COMPLETE,  getIntent().getStringExtra(Constants.ORDER_COMPLETE))
                .putExtra("seller_name", getIntent().getStringExtra("seller_name") )
                .putExtra(Constants.IMAGE,  getIntent().getStringExtra(Constants.IMAGE)));
        finish();
    }*/

            DialogPopUps.hideDialog();
        }
    }

    private void initToolbar() {
        header.setBackgroundColor(ContextCompat.getColor(this, R.color.greynew));
        imgHeaderBack.setImageResource(R.drawable.ic_back);
        DrawableCompat.setTint(imgHeaderBack.getDrawable(), ContextCompat.getColor(this, R.color.black));
        titleName.setText(R.string.payment_details);
        titleName.setTextColor(ContextCompat.getColor(this, R.color.black));
    }

}
