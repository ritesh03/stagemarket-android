package com.stagemarket.activity.orderSummary;

import com.stagemarket.CommonPresenter;

public interface OrderSummaryPresenter extends CommonPresenter {
    void onPlaceOrderClick(String productId, String sellerId, String price, String orderType,
                           String catName, String subCatNAme, String billingName, String billingAddress,
                           String billingCity, String billingContact, String billingAmount, String commission,
                           String vat, String image,String soldBy,String qty);
}
