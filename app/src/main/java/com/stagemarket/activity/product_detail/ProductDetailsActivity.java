package com.stagemarket.activity.product_detail;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stagemarket.AppClass;
import com.stagemarket.BaseActivity;
import com.stagemarket.R;
import com.stagemarket.activity.categories.CategoriesActivity;
import com.stagemarket.activity.home.HomeActivity;
import com.stagemarket.activity.login.LoginActivity;
import com.stagemarket.activity.myPostsDetails.ShowLocationActivity;
import com.stagemarket.activity.orderSummary.OrderSummary;
import com.stagemarket.activity.splash.Splash;
import com.stagemarket.activity.splash.SplashPresenterImp;
import com.stagemarket.activity.viewProfile.ViewProfileActivity;
import com.stagemarket.adapter.ViewPagerAdapter;
import com.stagemarket.pojo.categorySell.GetCategoriesResponse;
import com.stagemarket.pojo.discover.ProductData;
import com.stagemarket.pojo.myPosts.Datum;
import com.stagemarket.pojo.myPosts.ProductImage;
import com.stagemarket.pojo.viewOtherProfile.DataItem;
import com.stagemarket.pojo.viewOtherProfile.ProductImagesItem;
import com.stagemarket.pojo.wishlist.AddWishlistResponse;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.DialogPopUps;
import com.stagemarket.utils.SharedPreff;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.SharingHelper;
import io.branch.referral.util.ContentMetadata;
import io.branch.referral.util.LinkProperties;
import io.branch.referral.util.ShareSheetStyle;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProductDetailsActivity extends BaseActivity {

    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.left_nav)
    ImageView leftNav;
    @BindView(R.id.right_nav)
    ImageView rightNav;
    @BindView(R.id.loc_icon)
    ImageView loc_icon;
    @BindView(R.id.tv_photos_count)
    TextView tvPhotosCount;
    @BindView(R.id.lyt_main)
    FrameLayout lytMain;
    @BindView(R.id.tv_stage_market)
    TextView tvStageMarket;
    @BindView(R.id.tv_posted_timing)
    TextView tvPostedTiming;
    @BindView(R.id.tv_view_profile)
    TextView tvViewProfile;
    @BindView(R.id.product_title)
    TextView productTitle;
    @BindView(R.id.product_category)
    TextView productCategory;
    @BindView(R.id.product_price)
    TextView productPrice;
    @BindView(R.id.product_qty)
    TextView product_qty;
    @BindView(R.id.tv_product_description)
    TextView tvProductDescription;
    @BindView(R.id.product_location)
    TextView productLocation;
    @BindView(R.id.tv_checkout)
    TextView tvCheckout;
    @BindView(R.id.lytPhotosCount)
    LinearLayout lytPhotosCount;
    @BindView(R.id.lytLocation)
    LinearLayout lytLocation;
    @BindView(R.id.lyt_soldBy)
    LinearLayout lyt_soldBy;
    @BindView(R.id.share)
    ImageView share;

    ViewPagerAdapter viewPagerAdapter;
    List<ProductImage> productImages = new ArrayList<>();
    String productDetail;
    private ProductData productData;
    DataItem dataItem;
    private String viewProData;
    private NetWorkingService netWorkingService;
    private final CompositeDisposable disposables = new CompositeDisposable();
    String type;
    ImageView wishlist_btn;
    RelativeLayout wish_lay;
    String productId;
    String productUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        ButterKnife.bind(this);
        setUpViewPager();


// latest
        JSONObject sessionParams = Branch.getInstance().getLatestReferringParams();

// first
        JSONObject installParams = Branch.getInstance().getFirstReferringParams();

        wishlist_btn = findViewById(R.id.wishlist_btn);
        wish_lay = findViewById(R.id.wish_lay);
        netWorkingService = AppClass.getInstance().getNetworkService();


        if (getIntent().getExtras() != null) {


            if (getIntent().hasExtra("branch")) {
            } else {
                productDetail = getIntent().getStringExtra(Constants.PROD_DETAIL);
                viewProData = getIntent().getStringExtra(Constants.VIEW_PRO_DATA);
                if (productDetail != null) {
                    type = "product";
                    lyt_soldBy.setVisibility(View.VISIBLE);
                    tvViewProfile.setVisibility(View.VISIBLE);
                    Gson gson = new Gson();
                    Type type = new TypeToken<ProductData>() {
                    }.getType();
                    productData = gson.fromJson(productDetail, type);
                    initViews(productData);
                    productId = productData.getId();


                    if (productData.getProductType() == 1) {
                        productCategory.setText("Condition: New");
                    } else {
                        productCategory.setText("Condition: Used");
                    }

                    productUrl = productData.getmProductImages().get(0).getImage();

                    if (productData.getWishlistStatus().equalsIgnoreCase("1")) {
                        wishlist_btn.setBackgroundResource(R.drawable.wish_filled);
                    }

                } else if (viewProData != null) {
                    type = "viewproduct";
                    lyt_soldBy.setVisibility(View.INVISIBLE);
                    tvViewProfile.setVisibility(View.GONE);
                    Gson gson = new Gson();
                    Type type = new TypeToken<DataItem>() {
                    }.getType();
                    dataItem = gson.fromJson(viewProData, type);
                    initViews(dataItem);
                    productId = dataItem.getId();

                    if (dataItem.getProductType() == 1) {
                        productCategory.setText("Condition: New");
                    } else {
                        productCategory.setText("Condition: Used");
                    }
                    productUrl = dataItem.getProductImages().get(0).getImage();

                    if (dataItem.getWishlistStatus().equalsIgnoreCase("1")) {
                        wishlist_btn.setBackgroundResource(R.drawable.wish_filled);
                    }
                }
            }
        }


        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//Log.e("pro detail",getIntent().getStringExtra(Constants.PROD_DETAIL));
                BranchUniversalObject buo = null;
                if (getIntent().getExtras() != null) {
                    productDetail = getIntent().getStringExtra(Constants.PROD_DETAIL);
                    viewProData = getIntent().getStringExtra(Constants.VIEW_PRO_DATA);
Log.e("url",Constants.IMAGE_BASE_URL+productUrl);

                    if (productDetail != null) {

                        buo = new BranchUniversalObject()
                                .setCanonicalIdentifier("content/12345")
                                .setTitle(tvStageMarket.getText().toString())
                                .setContentDescription(tvProductDescription.getText().toString())
                                .setContentImageUrl(Constants.IMAGE_BASE_URL+productUrl)

                                .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                                .setLocalIndexMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                                .setContentMetadata(new ContentMetadata().addCustomMetadata("productDetail", getIntent().getStringExtra(Constants.PROD_DETAIL)).addCustomMetadata("viewProData", "1").addCustomMetadata("productId", productId));

                    } else {

                        buo = new BranchUniversalObject()
                                .setCanonicalIdentifier("content/12345")
                                .setTitle(tvStageMarket.getText().toString())
                                .setContentDescription(tvProductDescription.getText().toString())
                                .setContentImageUrl(Constants.IMAGE_BASE_URL+productUrl)
                                .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                                .setLocalIndexMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                                .setContentMetadata(new ContentMetadata().addCustomMetadata("viewProData", viewProData).addCustomMetadata("productDetail", "1").addCustomMetadata("productId", productId));


                    }
                }


                LinkProperties lp = new LinkProperties()
                        .setChannel("facebook")
                        .setFeature("sharing")
                        .setCampaign("content 123 launch")
                        .setStage("new user")

                        .addControlParameter("$desktop_url", "http://example.com/home")
                        .addControlParameter("custom", "data")
                        .addControlParameter("custom_random", Long.toString(Calendar.getInstance().getTimeInMillis()));
                final String[] final_url = {""};
                buo.generateShortUrl(ProductDetailsActivity.this, lp, new Branch.BranchLinkCreateListener() {
                    @Override
                    public void onLinkCreate(String url, BranchError error) {
                        if (error == null) {
                            Log.i("BRANCH SDK", "got my Branch link to share: " + url);
                            final_url[0] = url;
                        }
                    }
                });


                ShareSheetStyle ss = new ShareSheetStyle(ProductDetailsActivity.this, "Check this out!", tvStageMarket.getText().toString() + "" + final_url[0])
                        //.setCopyUrlStyle(ContextCompat.getDrawable(ProductDetailsActivity.this, android.R.drawable.ic_menu_send), "Copy", "Added to clipboard")

                        .setMoreOptionStyle(ContextCompat.getDrawable(ProductDetailsActivity.this, android.R.drawable.ic_menu_search), "Show more")
                        .addPreferredSharingOption(SharingHelper.SHARE_WITH.FACEBOOK)
                        .addPreferredSharingOption(SharingHelper.SHARE_WITH.EMAIL)
                        .addPreferredSharingOption(SharingHelper.SHARE_WITH.MESSAGE)
                        .addPreferredSharingOption(SharingHelper.SHARE_WITH.HANGOUT)
                        .setAsFullWidthStyle(true)
                        .setSharingTitle("Share With");
                buo.showShareSheet(ProductDetailsActivity.this, lp, ss, new Branch.BranchLinkShareListener() {
                    @Override
                    public void onShareLinkDialogLaunched() {
                    }

                    @Override
                    public void onShareLinkDialogDismissed() {
                    }

                    @Override
                    public void onLinkShareResponse(String sharedLink, String sharedChannel, BranchError error) {
                    }

                    @Override
                    public void onChannelSelected(String channelName) {
                    }
                });
            }
        });
        wish_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("click", "click");
                if (type.equalsIgnoreCase("product")) {
                    addRemoveWishlist(String.valueOf(productData.getId()), wishlist_btn);
                } else {
                    addRemoveWishlist(String.valueOf(dataItem.getId()), wishlist_btn);

                }
            }
        });
    }


    @Override
    public void onStart() {
        super.onStart();

        // Branch init
        Branch.getInstance().initSession(new Branch.BranchReferralInitListener() {
            @Override
            public void onInitFinished(JSONObject referringParams, BranchError error) {
                if (error == null) {
                    Log.i("BRANCH SDK", referringParams.toString());

                    try {
                        Log.e("return data", referringParams.getString("productId"));

                        if (SharedPreff.getLoginDetailsModel() != null) {

                            HttpGetRequest httpGetRequest = new HttpGetRequest();
                            try {
                                httpGetRequest.execute("http://www.stagemarket.store/StageMarket/public/api/getProductsDetail/" + referringParams.getString("productId"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                       /* // SharedPreff.clearPreferences();
                        if (this != null) {
                            showLoginView();
                        }*/
                            SharedPreff.clearSharedPrefs();
                            Intent splash = new Intent(ProductDetailsActivity.this, LoginActivity.class);
                            splash.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(splash);
                            finish();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.i("BRANCH SDK", error.getMessage());
                }
            }
        }, getIntent().getData(), ProductDetailsActivity.this);


    }

    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }


    class HttpGetRequest extends AsyncTask<String, Void, String> {
        public static final String REQUEST_METHOD = "GET";
        public static final int READ_TIMEOUT = 15000;
        public static final int CONNECTION_TIMEOUT = 15000;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            DialogPopUps.showProgressDialog(ProductDetailsActivity.this, getResources().getString(R.string.please_wait));

        }

        @Override
        protected String doInBackground(String... params) {
            String stringUrl = params[0];
            String result;
            String inputLine;
            try {
                //Create a URL object holding our url
                URL myUrl = new URL(stringUrl);
                //Create a connection
                HttpURLConnection connection = (HttpURLConnection)
                        myUrl.openConnection();
                //Set methods and timeouts
                connection.setRequestMethod(REQUEST_METHOD);
                connection.setReadTimeout(READ_TIMEOUT);
                connection.setConnectTimeout(CONNECTION_TIMEOUT);

                //Connect to our url
                connection.connect();
                //Create a new InputStreamReader
                InputStreamReader streamReader = new
                        InputStreamReader(connection.getInputStream());
                //Create a new buffered reader and String Builder
                BufferedReader reader = new BufferedReader(streamReader);
                StringBuilder stringBuilder = new StringBuilder();
                //Check if the line we are reading is not null
                while ((inputLine = reader.readLine()) != null) {
                    stringBuilder.append(inputLine);
                }
                //Close our InputStream and Buffered reader
                reader.close();
                streamReader.close();
                //Set our result equal to our stringBuilder
                result = stringBuilder.toString();
            } catch (IOException e) {
                e.printStackTrace();
                result = null;
            }
            return result;
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            DialogPopUps.hideDialog();
            Log.e("response", result);
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(result);
                productDetail = jsonObject.getString("data");
            } catch (JSONException e) {
                e.printStackTrace();
            }


            if (productDetail != null) {
                type = "product";
                lyt_soldBy.setVisibility(View.VISIBLE);
                tvViewProfile.setVisibility(View.VISIBLE);
                Gson gson = new Gson();
                Type type = new TypeToken<ProductData>() {
                }.getType();
                productData = gson.fromJson(productDetail, type);
                initViews(productData);
                productId = productData.getId();


                if (productData.getProductType() == 1) {
                    productCategory.setText("Condition: New");
                } else {
                    productCategory.setText("Condition: Used");
                }
                if (productData.getWishlistStatus().equalsIgnoreCase("1")) {
                    wishlist_btn.setBackgroundResource(R.drawable.wish_filled);
                }

            }

        }
    }

    private void initViews(DataItem dataItem) {

        if (dataItem.getProductImages().size() != 0) {
            viewpager.setVisibility(View.VISIBLE);
            for (int i = 0; i < dataItem.getProductImages().size(); i++) {
                ProductImagesItem dataItem1 = dataItem.getProductImages().get(i);
                ProductImage productImage = new ProductImage();
                productImage.setCreatedAt(dataItem1.getCreatedAt());
                productImage.setId((long) dataItem1.getId());
                productImage.setImage(dataItem1.getImage());
                productImage.setProductId((long) dataItem1.getProductId());
                productImage.setStatus((long) dataItem1.getStatus());
                productImage.setUpdatedAt(dataItem1.getUpdatedAt());
                productImages.add(productImage);
            }
            viewPagerAdapter.notifyDataSetChanged();
            if (productImages.size() == 1) {
                lytPhotosCount.setVisibility(View.GONE);
                leftNav.setVisibility(View.GONE);
                rightNav.setVisibility(View.GONE);
            } else {
                lytPhotosCount.setVisibility(View.VISIBLE);
            }
        } else {
            lytPhotosCount.setVisibility(View.GONE);
            leftNav.setVisibility(View.GONE);
            rightNav.setVisibility(View.GONE);
            viewpager.setVisibility(View.GONE);
            lytMain.setBackgroundResource(R.drawable.not_available);
        }

        String currentDate = CommonUtils.getCurrentDate();
        String current[] = currentDate.split(" ");

        String serverDate = CommonUtils.changeDateFormat(dataItem.getCreatedAt());
        String serverDateee = CommonUtils.changeeDateFormat(dataItem.getCreatedAt());
        Log.e("da", serverDateee);
        String server[] = serverDate.split(" ");
        if (current[0].equalsIgnoreCase(server[0])) {
            try {
                tvPostedTiming.setText("Ad posted at " + CommonUtils.convertUTCtoLocalTime(dataItem.getCreatedAt()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            String serever[] = serverDateee.split(" ");
            try {
                tvPostedTiming.setText("Ad posted on " + serever[0].replaceAll("\\-", " ") + " at " + CommonUtils.convertUTCtoLocalTime(dataItem.getCreatedAt()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        tvPhotosCount.setText(productImages.size() + getString(R.string.photos));

        tvStageMarket.setSelected(true);
        tvStageMarket.setText(dataItem.getName());
        float priceval = ((Integer.parseInt(dataItem.getCommision()) * Float.parseFloat(String.valueOf(dataItem.getCost()))) / 100);

        float newPriceval = priceval + Float.parseFloat(String.valueOf(dataItem.getCost()));
        //  myViewHolder.tv_price.setText(dataItem.getCurrency() + " " + String.format("%.2f",newPriceval));
        productPrice.setText(dataItem.getCurrency() + " " + String.format("%.2f", newPriceval));
        product_qty.setText("Quantity:" + " " + dataItem.getQuantity());
        productTitle.setText(dataItem.getUsername()/* + getString(R.string.for_sale)*/);
        //  productCategory.setText(getString(R.string.category_product_title) + dataItem.getCategoryName());
        tvProductDescription.setText(dataItem.getDescription());
        productLocation.setText(dataItem.getLocation());

        if (dataItem.getLocation().equalsIgnoreCase("")) {
            lytLocation.setVisibility(View.GONE);
        }
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if (position == 0) {
                    leftNav.setVisibility(View.GONE);
                    rightNav.setVisibility(View.VISIBLE);
                } else if (position >= 1 && position < productImages.size() - 1) {
                    leftNav.setVisibility(View.VISIBLE);
                    rightNav.setVisibility(View.VISIBLE);
                } else if (position == 1 && productImages.size() == 2) {
                    leftNav.setVisibility(View.VISIBLE);
                    rightNav.setVisibility(View.GONE);
                } else {
                    rightNav.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void initViews(ProductData productData) {


        if (!SharedPreff.getLoginDetailsModel().getId().equalsIgnoreCase(productData.getUserId())) {
            tvViewProfile.setVisibility(View.VISIBLE);
            tvCheckout.setVisibility(View.VISIBLE);
        } else {
            tvViewProfile.setVisibility(View.GONE);
            tvCheckout.setVisibility(View.GONE);
            wish_lay.setVisibility(View.GONE);
        }
        if (productData.getmProductImages().size() != 0) {
            tvPhotosCount.setVisibility(View.VISIBLE);
            viewpager.setVisibility(View.VISIBLE);
            productImages.addAll(productData.getmProductImages());
            viewPagerAdapter.notifyDataSetChanged();

            if (productImages.size() == 1) {
                lytPhotosCount.setVisibility(View.GONE);
                leftNav.setVisibility(View.GONE);
                rightNav.setVisibility(View.GONE);
            } else {
                lytPhotosCount.setVisibility(View.VISIBLE);
            }
        } else {
            lytPhotosCount.setVisibility(View.GONE);
            tvPhotosCount.setVisibility(View.INVISIBLE);
            leftNav.setVisibility(View.GONE);
            rightNav.setVisibility(View.GONE);
            viewpager.setVisibility(View.GONE);
            lytMain.setBackgroundResource(R.drawable.not_available);
        }

        String currentDate = CommonUtils.getCurrentDate();
        String current[] = currentDate.split(" ");

        String serverDate = CommonUtils.changeDateFormat(productData.getCreatedAt());
        String serverDaate = CommonUtils.changeeDateFormat(productData.getCreatedAt());
        String server[] = serverDate.split(" ");
        if (current[0].equalsIgnoreCase(server[0])) {
            try {
                tvPostedTiming.setText("Ad Posted at " + CommonUtils.convertUTCtoLocalTime(productData.getCreatedAt()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            String serever[] = serverDaate.split(" ");
            try {
                tvPostedTiming.setText("Ad posted on " + serever[0].replaceAll("\\-", " ") + " at " + CommonUtils.convertUTCtoLocalTime(productData.getCreatedAt()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        product_qty.setText("Quantity:" + " " + productData.getQuantity());
        // tvPostedTiming.setText("Ad Posted at "+CommonUtils.changeDateFormat(productData.getCreatedAt()));
        tvPhotosCount.setText(productImages.size() + getString(R.string.photos));
        tvStageMarket.setSelected(true);
        tvStageMarket.setText(productData.getName());

       /* int priceval = (int) ((Integer.parseInt(productData.getCommision()) * Float.parseFloat(
                        String.valueOf(productData.getCost()))) / 100);*/

        float priceval = ((Integer.parseInt(productData.getCommision())
                * Float.parseFloat(String.valueOf(productData.getCost()))) / 100);
        float newPriceval = priceval + Float.parseFloat(String.valueOf(productData.getCost()));
        productPrice.setText(productData.getCurrency() + " " + String.format("%.2f", newPriceval));

        // productPrice.setText(productData.getCurrency() + " " + priceval);

        //   productPrice.setText(Constants.DOLlAR + " " + productData.getCost());
        productTitle.setText(productData.getUsername()/* + getString(R.string.for_sale)*/);
        //  productCategory.setText(getString(R.string.category_product_title) + productData.getCatName());
        tvProductDescription.setText(productData.getDescription());
        productLocation.setText(productData.getLocation());
        if (productData.getLocation().equalsIgnoreCase("")) {
            lytLocation.setVisibility(View.GONE);
        }

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if (position == 0) {
                    leftNav.setVisibility(View.GONE);
                    rightNav.setVisibility(View.VISIBLE);
                } else if (position >= 1 && position < productImages.size() - 1) {
                    leftNav.setVisibility(View.VISIBLE);
                    rightNav.setVisibility(View.VISIBLE);
                } else if (position == 1 && productImages.size() == 2) {
                    leftNav.setVisibility(View.VISIBLE);
                    rightNav.setVisibility(View.GONE);
                } else {
                    rightNav.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @OnClick(R.id.tv_view_profile)
    public void onViewProfileClick() {
        if (!SharedPreff.getLoginDetailsModel().getId().equalsIgnoreCase(productData.getUserId())) {
            startActivity(new Intent(this, ViewProfileActivity.class)
                    .putExtra(Constants.OTHER_PROFILE_USER_ID, productData.getUserId())
                    .putExtra(Constants.OTHER_PROFILE_COMPANY_ID, productData.getCompanyId() + ""));
        }
    }

    @OnClick(R.id.tv_checkout)
    public void onCheckOutClick() {
        if (productDetail != null) {
            startActivity(new Intent(this, OrderSummary.class)
                    .putExtra(Constants.PROD_DETAIL, productDetail));
            finish();
        } else if (viewProData != null) {
            startActivity(new Intent(this, OrderSummary.class)
                    .putExtra(Constants.VIEW_PROD_DETAIL, viewProData));
            finish();
        }
    }

    @OnClick(R.id.lytLocation)
    public void onClickLocation() {
       /* startActivity(new Intent(this, ShowLocationActivity.class)
                .putExtra(Constants.LATITUDE, productData.getLatitude())
                .putExtra(Constants.LONGITUDE, productData.getLongitude()));*/
    }

    private void setUpViewPager() {
        leftNav.setVisibility(View.GONE);
        viewPagerAdapter = new ViewPagerAdapter(this, productImages, "");
        viewpager.setAdapter(viewPagerAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
      /*  Intent intent=new Intent(ProductDetailsActivity.this,HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);*/
        finish();
    }

    @OnClick(R.id.left_nav)
    public void OnLeftNavClick() {
        rightNav.setVisibility(View.VISIBLE);
        int tab = viewpager.getCurrentItem();
        if (tab == 1) {
            leftNav.setVisibility(View.GONE);
        }
        if (tab > 0) {
            tab--;
            viewpager.setCurrentItem(tab);
        } else if (tab == 0) {
            viewpager.setCurrentItem(tab);
        }
    }

    @OnClick(R.id.imgBack)
    public void OnImageBackClick() {

     /*   Intent intent=new Intent(ProductDetailsActivity.this,HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);*/
        finish();
    }

    @OnClick(R.id.right_nav)
    public void OnRightNavClick() {
        leftNav.setVisibility(View.VISIBLE);
        int tab = viewpager.getCurrentItem();
        tab++;
        viewpager.setCurrentItem(tab);

        if (tab == productImages.size() - 1) {
            rightNav.setVisibility(View.GONE);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void addRemoveWishlist(String productId, final ImageView wishlist_btn) {
        if (CommonUtils.isConnectedToInternet(ProductDetailsActivity.this)) {


            netWorkingService.getAPI().addToWishlist(SharedPreff.getLoginDetailsModel().getId(), productId, "1")
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<AddWishlistResponse>() {


                        @Override
                        public void onSubscribe(Disposable d) {
                            disposables.add(d);
                        }

                        @Override
                        public void onNext(AddWishlistResponse deleteProductResponse) {


                            if (deleteProductResponse.result.equalsIgnoreCase(Constants.SUCCESS)) {

                                if (deleteProductResponse.data.status == 1) {
                                    if (type.equalsIgnoreCase("product")) {
                                        productData.setWishlistStatus("1");
                                    } else {

                                        dataItem.setWishlistStatus("1");
                                    }
                                    wishlist_btn.setBackgroundResource(R.drawable.wish_filled);
                                    Toast.makeText(ProductDetailsActivity.this, "Item added in wishlist", Toast.LENGTH_SHORT).show();
                                } else {

                                    if (type.equalsIgnoreCase("product")) {
                                        productData.setWishlistStatus("0");
                                    } else {

                                        dataItem.setWishlistStatus("0");
                                    }
                                    wishlist_btn.setBackgroundResource(R.drawable.wish_blank);
                                    Toast.makeText(ProductDetailsActivity.this, "Item removed from wishlist", Toast.LENGTH_SHORT).show();
                                }

                                //  myPostsDetailsView.onDeleteProductResponse(deleteProductResponse);
                                //  myPostsDetailsView.showMessageDialog(deleteProductResponse.getMessage());

                            } else {
                                // myPostsDetailsView.showErrorDialog(deleteProductResponse.getMessage());
                            }


                        }

                        @Override
                        public void onError(Throwable e) {
                            //   myPostsDetailsView.hideProgressDialog();
                            //   myPostsDetailsView.showErrorDialog(mContext.getResources().getString(R.string.something_went_wrong));
                        }

                        @Override
                        public void onComplete() {

                        }
                    });


        } else {
            CommonUtils.showTopSnackBar(ProductDetailsActivity.this,
                    findViewById(android.R.id.content), getString(R.string.please_check_internet));

        }
    }
}
