package com.stagemarket.activity.changePassword;

import com.stagemarket.CommonPresenter;

public interface ChangePasswordPresenter extends CommonPresenter{

    void validateFields(String current_password, String new_password, String re_enter_password);
}
