package com.stagemarket.activity.changePassword;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.stagemarket.AppClass;
import com.stagemarket.R;
import com.stagemarket.activity.orderSummary.PlaceOrderActivity;
import com.stagemarket.activity.orderSummary.ThankYouActivity;
import com.stagemarket.activity.viewProfile.ViewProfileActivity;
import com.stagemarket.pojo.SaveLoginDetailsModel;
import com.stagemarket.pojo.changePassword.ChangePwdResponse;
import com.stagemarket.pojo.placeOrder.PlaceOrderResponse;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.SharedPreff;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ChangePresenterPresenterImp implements ChangePasswordPresenter {

    ChangePasswordView changePasswordView;
    Context context;
    private final CompositeDisposable disposables = new CompositeDisposable();
    private NetWorkingService netWorkingService;
    SaveLoginDetailsModel saveLoginDetailsModel;

    public ChangePresenterPresenterImp(ChangePasswordView changePasswordView) {
        this.changePasswordView = changePasswordView;
        context = AppClass.getInstance().getApplicationContext();
        saveLoginDetailsModel = SharedPreff.getLoginDetailsModel();
        netWorkingService = AppClass.getInstance().getNetworkService();

    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        if (disposables != null) {
            disposables.clear();
        }
        changePasswordView = null;
        netWorkingService = null;
    }

    @Override
    public void validateFields(String current_password, String new_password, String re_enter_password) {
        if (TextUtils.isEmpty(current_password) && TextUtils.isEmpty(new_password) && TextUtils.isEmpty(re_enter_password)) {
            if (changePasswordView != null) {
                changePasswordView.showErrorDialog(context.getString(R.string.please_fill_all_the_details));
            }
    }else if (TextUtils.isEmpty(current_password) || current_password.trim().equalsIgnoreCase("")) {
            changePasswordView.showErrorDialog(context.getString(R.string.current_password_should_not_be_empty));
        } else if (TextUtils.isEmpty(new_password) || new_password.trim().equalsIgnoreCase("")) {
            changePasswordView.showErrorDialog(context.getString(R.string.new_password_should_not_be_empty));
        } else if (TextUtils.isEmpty(re_enter_password) || re_enter_password.trim().equalsIgnoreCase("")) {
            changePasswordView.showErrorDialog(context.getString(R.string.re_enter_password_should_not_be_empty));
        } else if (!new_password.equalsIgnoreCase(re_enter_password)) {
            changePasswordView.showErrorDialog(context.getString(R.string.new_renter_password_should_be_matched));
        } else {
            if (changePasswordView != null) {
                if (CommonUtils.isConnectedToInternet((Context) changePasswordView)) {
                    changePasswordView.showProgressDialog();

                    netWorkingService.getAPI().updatePwd(
                            saveLoginDetailsModel.getId(),
                            new_password,
                            current_password
                    )
                            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Observer<ChangePwdResponse>() {
                                @Override
                                public void onSubscribe(Disposable d) {
                                    disposables.add(d);
                                }

                                @Override
                                public void onNext(ChangePwdResponse changePwdResponse) {
                                    changePasswordView.hideProgressDialog();

                                    if (changePasswordView != null) {
                                        changePasswordView.hideProgressDialog();
                                        if (changePwdResponse.getResult().equalsIgnoreCase(Constants.SUCCESS)) {
                                            changePasswordView.showMessageDialog(changePwdResponse.getMessage());
                                        } else {
                                            changePasswordView.showErrorDialog(changePwdResponse.getMessage());
                                        }
                                    }
                                }

                                @Override
                                public void onError(Throwable e) {
                                    changePasswordView.hideProgressDialog();
                                    changePasswordView.showErrorDialog(((Context)changePasswordView).getResources().getString(R.string.something_went_wrong));
                                }

                                @Override
                                public void onComplete() {
                                }
                            });
                } else {
                    CommonUtils.showTopSnackBar(((Context)changePasswordView),
                            ((ViewProfileActivity) changePasswordView).findViewById(android.R.id.content),
                            ((Context)changePasswordView).getString(R.string.please_check_internet));
                }
            }
        }
    }

    private void makeChangePasswordRequest() {

    }
}
