package com.stagemarket.activity.changePassword;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.stagemarket.BaseActivity;
import com.stagemarket.R;
import com.stagemarket.activity.viewProfile.ViewProfileActivity;
import com.stagemarket.utils.CommonListeners;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.DialogPopUps;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangePasswordActivity extends BaseActivity implements ChangePasswordView {

    @BindView(R.id.header)
    View header;
    @BindView(R.id.title_name)
    TextView titleName;
    @BindView(R.id.left_text)
    TextView leftText;
    @BindView(R.id.right_text)
    TextView rightText;
    @BindView(R.id.img_header_back)
    ImageView imgHeaderBack;

    ChangePasswordPresenter changePasswordPresenter;
    @BindView(R.id.et_current_password)
    EditText etCurrentPassword;
    @BindView(R.id.et_new_password)
    EditText etNewPassword;
    @BindView(R.id.et_re_enter_password)
    EditText etReEnterPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        changePasswordPresenter = new ChangePresenterPresenterImp(this);
        initToolbar();
    }


    private void initToolbar() {
        header.setBackgroundColor(ContextCompat.getColor(this, R.color.greynew));

        imgHeaderBack.setImageResource(R.drawable.ic_back);
        DrawableCompat.setTint(imgHeaderBack.getDrawable(), ContextCompat.getColor(this, R.color.black));
        imgHeaderBack.setVisibility(View.GONE);

        rightText.setVisibility(View.VISIBLE);
        rightText.setText(R.string.save);
        rightText.setTextColor(ContextCompat.getColor(ChangePasswordActivity.this, R.color.black));

        leftText.setVisibility(View.VISIBLE);
        leftText.setText(R.string.cancel);
        leftText.setTextColor(ContextCompat.getColor(ChangePasswordActivity.this, R.color.black));


        titleName.setText(R.string.change_password);
        titleName.setTextColor(ContextCompat.getColor(this, R.color.black));

    }


    @OnClick(R.id.right_text)
    public void OnClickSave() {
        if (CommonUtils.isConnectedToInternet(ChangePasswordActivity.this)) {
        changePasswordPresenter.validateFields(etCurrentPassword.getText().toString().trim(), etNewPassword.getText().toString().trim()
                , etReEnterPassword.getText().toString().trim());
        } else {
            CommonUtils.showTopSnackBar(ChangePasswordActivity.this,
                     findViewById(android.R.id.content), getString(R.string.please_check_internet));

        }
    }

    @OnClick(R.id.left_text)
    public void OnClickCancel() {
        finish();
    }


    @Override
    public void showProgressDialog() {
        DialogPopUps.showProgressDialog(this, getResources().getString(R.string.please_wait));

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        CommonUtils.hideSoftKeyboard(ChangePasswordActivity.this);
        return super.dispatchTouchEvent(ev);

    }

    @Override
    public void showErrorDialog(String msg) {
        CommonUtils.showTopSnackBar(this, findViewById(android.R.id.content), msg);

    }

    @Override
    public void showMessageDialog(String msg) {
        DialogPopUps.alertPopUp(this, msg, getString(R.string.alert),
                getResources().getString(R.string.ok)
                , new CommonListeners.AlertCallBackInterface() {
                    @Override
                    public void neutralClick() {
                        finish();
                    }
                });
    }

    @Override
    public void hideProgressDialog() {
        DialogPopUps.hideDialog();
    }
}
