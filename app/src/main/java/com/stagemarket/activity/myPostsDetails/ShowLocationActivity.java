package com.stagemarket.activity.myPostsDetails;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.stagemarket.BaseActivity;
import com.stagemarket.R;
import com.stagemarket.utils.Constants;

import butterknife.ButterKnife;

public class ShowLocationActivity extends BaseActivity implements OnMapReadyCallback {

    private GoogleMap mGoogleMap;
    String latitude = "", longitude = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_location);
        ButterKnife.bind(this);

        if (getIntent().hasExtra(Constants.LATITUDE)) {
            latitude = getIntent().getStringExtra(Constants.LATITUDE);
        }

        if (getIntent().hasExtra(Constants.LONGITUDE)) {
            longitude = getIntent().getStringExtra(Constants.LONGITUDE);
        }

        if (!latitude.equalsIgnoreCase("") &&
                !longitude.equalsIgnoreCase("")) {
            initGoogleMap();
        }
    }

    private void initGoogleMap() {
        SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFrag.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this
                , Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        //mGoogleMap.setOnInfoWindowClickListener(this);

        mGoogleMap.setMyLocationEnabled(true);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);

        initCamera();
    }

    private void initCamera() {
        if (mGoogleMap != null) {

            mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(latitude),
                    Double.parseDouble(longitude)))
                    // .title(saveCreateDealDetails.getDeal_name())
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(latitude),
                    Double.parseDouble(longitude)), 15.0f));
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(latitude),
                    Double.parseDouble(longitude)), 15.0f));

        }
    }

}
