package com.stagemarket.activity.myPostsDetails;

import com.stagemarket.CommonPresenter;

public interface MyPostsDetailsPresenter extends CommonPresenter {
    void onDeleteProductRequest(String id);
}
