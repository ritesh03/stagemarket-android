package com.stagemarket.activity.myPostsDetails;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stagemarket.BaseActivity;
import com.stagemarket.R;
import com.stagemarket.activity.editAdDetail.EditAdDetailActivity;
import com.stagemarket.activity.login.LoginActivity;
import com.stagemarket.adapter.ViewPagerAdapter;
import com.stagemarket.pojo.DeleteMyPostsPojo;
import com.stagemarket.pojo.myPosts.Datum;
import com.stagemarket.pojo.myPosts.ProductImage;
import com.stagemarket.utils.CommonListeners;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.DialogPopUps;
import com.stagemarket.utils.SharedPreff;

import org.greenrobot.eventbus.EventBus;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MyPostsDetailsActivity extends BaseActivity implements MyPostsVDetailsView {
    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.left_nav)
    ImageView leftNav;
    @BindView(R.id.right_nav)
    ImageView rightNav;
    @BindView(R.id.tv_photos_count)
    TextView tvPhotosCount;
    @BindView(R.id.lyt_main)
    FrameLayout lytMain;
    @BindView(R.id.product_title)
    TextView productTitle;
    @BindView(R.id.product_category)
    TextView productCategory;
    @BindView(R.id.product_price)
    TextView productPrice;
    @BindView(R.id.tv_product_description)
    TextView tvProductDescription;
    @BindView(R.id.product_location)
    TextView productLocation;
    @BindView(R.id.product_qty)
    TextView product_qty;
    @BindView(R.id.ad_status)
    TextView ad_status;
    @BindView(R.id.tv_delete_ad)
    TextView tvDeleteAd;
    @BindView(R.id.tv_edit_ad)
    TextView tvEditAd;
    @BindView(R.id.lytPhotosCount)
    LinearLayout lytPhotosCount;
    ViewPagerAdapter viewPagerAdapter;
    @BindView(R.id.lytLocation)
    LinearLayout lytLocation;
    private String productDetail = "";
    List<ProductImage> mArrayListProducts = new ArrayList<>();
    Datum prodDetail;
    private int position = 0;
    MyPostsDetailsPresenter myPostsDetailsPresenter;
    private int my_ads_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_posts_details);
        ButterKnife.bind(this);
        myPostsDetailsPresenter = new MyPostsDetailsPresenterImp(this);
        setUpViewPager();

        if (getIntent().getExtras() != null) {
            productDetail = getIntent().getStringExtra(Constants.PROD_DETAIL);
            Gson gson = new Gson();
            Type type = new TypeToken<Datum>() {
            }.getType();
            prodDetail = gson.fromJson(productDetail, type);
            initViews(prodDetail);
            product_qty.setText("Quantity: "+prodDetail.getQuantity());
        }

        if (getIntent().hasExtra(Constants.MY_POSTS_POSTION)) {
            position = getIntent().getIntExtra(Constants.MY_POSTS_POSTION, 0);
        }
        if (getIntent().hasExtra(Constants.MY_ADS_TYPE)) {
            my_ads_type = getIntent().getIntExtra(Constants.MY_ADS_TYPE, 0);
            if(my_ads_type==1){
                ad_status.setVisibility(View.GONE);
                tvDeleteAd.setVisibility(View.GONE);
                tvEditAd.setVisibility(View.GONE);
            }else{
                tvDeleteAd.setVisibility(View.VISIBLE);
                tvEditAd.setVisibility(View.VISIBLE);
            }
        }

    }

    @OnClick(R.id.tv_delete_ad)
    public void OnDeleteProduct() {


        DialogPopUps.showAlertWithButtons(MyPostsDetailsActivity.this, getString(R.string.alert), getString(R.string.are_you_sure_you_want_to_delete), getString(R.string.Yes), getString(R.string.no),
                "", false, false, new CommonListeners.AlertCallBackWithButtonsInterface() {
                    @Override
                    public void positiveClick() {
                        // Toast.makeText(getActivity(),"Yes",Toast.LENGTH_SHORT).show();
                        myPostsDetailsPresenter.onDeleteProductRequest(prodDetail.getId());
                    }

                    @Override
                    public void neutralClick() {

                    }

                    @Override
                    public void negativeClick() {
                        //     Toast.makeText(getActivity(),"No",Toast.LENGTH_SHORT).show();

                    }
                });

    }

    @OnClick(R.id.tv_edit_ad)
    public void onEditAdClick() {
        startActivity(new Intent(this, EditAdDetailActivity.class)
                .putExtra(Constants.PRODUCT_DATA, new Gson().toJson(prodDetail)));
    }

    private void initViews(Datum prodDetail) {



        if (prodDetail.getProductImages().size() != 0) {
            tvPhotosCount.setVisibility(View.VISIBLE);
            viewpager.setVisibility(View.VISIBLE);
            mArrayListProducts.addAll(prodDetail.getProductImages());
            viewPagerAdapter.notifyDataSetChanged();

            if (mArrayListProducts.size() == 1) {
                lytPhotosCount.setVisibility(View.GONE);
                leftNav.setVisibility(View.GONE);
                rightNav.setVisibility(View.GONE);
            } else {
                lytPhotosCount.setVisibility(View.VISIBLE);
                leftNav.setVisibility(View.GONE);
                rightNav.setVisibility(View.VISIBLE);
            }

        } else {
            lytPhotosCount.setVisibility(View.GONE);
            tvPhotosCount.setVisibility(View.INVISIBLE);
            viewpager.setVisibility(View.GONE);
            leftNav.setVisibility(View.GONE);
            rightNav.setVisibility(View.GONE);
            lytMain.setBackgroundResource(R.drawable.not_available);
        }
        tvPhotosCount.setText(mArrayListProducts.size() + getString(R.string.photos));
      //  Log.e("ddd",prodDetail.getName()+prodDetail.getCurrency()+prodDetail.getCost()+prodDetail.getTitle());
        productPrice.setText(prodDetail.getCurrency() + " " + prodDetail.getCost());
        productTitle.setText(prodDetail.getTitle() /*+ getString(R.string.for_sale)*/);
        productCategory.setText(getString(R.string.category_product_title) + prodDetail.getCategoryName());
        tvProductDescription.setText(prodDetail.getDescription());
        productLocation.setText(prodDetail.getLocation());
        if(prodDetail.getLocation().equalsIgnoreCase(""))
        {
            lytLocation.setVisibility(View.GONE);
        }


        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if (position == 0) {
                    leftNav.setVisibility(View.GONE);
                    rightNav.setVisibility(View.VISIBLE);
                } else if (position >= 1 && position < mArrayListProducts.size() - 1) {
                    leftNav.setVisibility(View.VISIBLE);
                    rightNav.setVisibility(View.VISIBLE);
                } else if (position == 1 && mArrayListProducts.size() == 2) {
                    leftNav.setVisibility(View.VISIBLE);
                    rightNav.setVisibility(View.GONE);
                } else {
                    rightNav.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private void setUpViewPager() {
        leftNav.setVisibility(View.GONE);
        viewPagerAdapter = new ViewPagerAdapter(this, mArrayListProducts,"");
        viewpager.setAdapter(viewPagerAdapter);
    }

    @OnClick(R.id.left_nav)
    public void OnLeftNavClick() {
        rightNav.setVisibility(View.VISIBLE);
        int tab = viewpager.getCurrentItem();

        if (tab == 1) {
            leftNav.setVisibility(View.GONE);
        }


        if (tab > 0) {
            tab--;
            viewpager.setCurrentItem(tab);
        } else if (tab == 0) {
            viewpager.setCurrentItem(tab);
        }
    }

    @OnClick(R.id.imgBack)
    public void OnImageBackClick() {
        finish();
    }


    @OnClick(R.id.lytLocation)
    public void onClickLocation() {
        startActivity(new Intent(this, ShowLocationActivity.class)
                .putExtra(Constants.LATITUDE, prodDetail.getLatitude())
                .putExtra(Constants.LONGITUDE, prodDetail.getLongitude()));
    }

    @OnClick(R.id.right_nav)
    public void OnRightNavClick() {
        leftNav.setVisibility(View.VISIBLE);
        int tab = viewpager.getCurrentItem();
        tab++;
        viewpager.setCurrentItem(tab);

        if (tab == mArrayListProducts.size() - 1) {
            rightNav.setVisibility(View.GONE);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    public void showProgressDialog() {
        DialogPopUps.showProgressDialog(this, getResources().getString(R.string.please_wait));
    }

    @Override
    public void showErrorDialog(String msg) {
        CommonUtils.showTopSnackBar(this, findViewById(android.R.id.content), msg);
    }

    @Override
    public void showMessageDialog(String msg) {

        DialogPopUps.alertPopUp(this, msg, getString(R.string.alert),
                getResources().getString(R.string.ok)
                , new CommonListeners.AlertCallBackInterface() {
                    @Override
                    public void neutralClick() {
                        EventBus.getDefault().post(new DeleteMyPostsPojo(position));
                        finish();
                    }
                });
    }

    @Override
    public void hideProgressDialog() {
        DialogPopUps.hideDialog();
    }
}
