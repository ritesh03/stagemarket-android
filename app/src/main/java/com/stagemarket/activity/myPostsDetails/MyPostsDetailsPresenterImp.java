package com.stagemarket.activity.myPostsDetails;

import android.content.Context;

import com.stagemarket.AppClass;
import com.stagemarket.R;
import com.stagemarket.activity.viewProfile.ViewProfileActivity;
import com.stagemarket.activity.viewProfile.ViewProfileView;
import com.stagemarket.pojo.SaveLoginDetailsModel;
import com.stagemarket.pojo.deleteProduct.DeleteProductResponse;
import com.stagemarket.pojo.viewOtherProfile.Response;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.SharedPreff;

import org.greenrobot.eventbus.EventBus;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MyPostsDetailsPresenterImp implements MyPostsDetailsPresenter {

    MyPostsVDetailsView myPostsDetailsView;
    SaveLoginDetailsModel saveLoginDetailsModel;
    Context context;
    private NetWorkingService netWorkingService;
    private final CompositeDisposable disposables = new CompositeDisposable();

    public MyPostsDetailsPresenterImp(MyPostsVDetailsView myPostsDetailsView) {
        this.myPostsDetailsView = myPostsDetailsView;
        saveLoginDetailsModel = SharedPreff.getLoginDetailsModel();
        context = AppClass.getInstance().getApplicationContext();
        netWorkingService = AppClass.getInstance().getNetworkService();
    }


    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        if (disposables != null) {
            disposables.clear();
        }
        myPostsDetailsView = null;
        netWorkingService = null;
    }


    @Override
    public void onDeleteProductRequest(String product_id) {
        if (CommonUtils.isConnectedToInternet(context)) {
            myPostsDetailsView.showProgressDialog();

            netWorkingService.getAPI().deleteProductService(
                    saveLoginDetailsModel.getId(), product_id)
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<DeleteProductResponse>() {


                        @Override
                        public void onSubscribe(Disposable d) {
                            disposables.add(d);
                        }

                        @Override
                        public void onNext(DeleteProductResponse deleteProductResponse) {
                            myPostsDetailsView.hideProgressDialog();

                            if (myPostsDetailsView != null) {
                                myPostsDetailsView.hideProgressDialog();
                                if (deleteProductResponse.getResult().equalsIgnoreCase(Constants.SUCCESS)) {
                                    //  myPostsDetailsView.onDeleteProductResponse(deleteProductResponse);
                                    myPostsDetailsView.showMessageDialog(deleteProductResponse.getMessage());

                                } else {
                                    myPostsDetailsView.showErrorDialog(deleteProductResponse.getMessage());
                                }
                            }

                        }

                        @Override
                        public void onError(Throwable e) {
                            myPostsDetailsView.hideProgressDialog();
                            myPostsDetailsView.showErrorDialog(context.getResources().getString(R.string.something_went_wrong));
                        }

                        @Override
                        public void onComplete() {

                        }
                    });


        } else {
            CommonUtils.showTopSnackBar(context,
                    ((ViewProfileActivity) context).findViewById(android.R.id.content), context.getString(R.string.please_check_internet));

        }
    }
}