package com.stagemarket.activity.welcome;

import android.net.Uri;

import com.stagemarket.CommonPresenter;

public interface WelcomePresenter extends CommonPresenter {
    void setBase64(Uri encodedImage);

    void uploadImageProcess();
}
