package com.stagemarket.activity.welcome;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mlsdev.rximagepicker.RxImagePicker;
import com.mlsdev.rximagepicker.Sources;
import com.squareup.picasso.Picasso;
import com.stagemarket.AppClass;
import com.stagemarket.BaseActivity;
import com.stagemarket.R;
import com.stagemarket.activity.home.HomeActivity;
import com.stagemarket.activity.signUp.SignUpActivity;
import com.stagemarket.pojo.SaveLoginDetailsModel;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.BorderCircleTransformation;
import com.stagemarket.utils.CommonListeners;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.DialogPopUps;
import com.stagemarket.utils.SharedPreff;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class WelcomeActivity extends BaseActivity implements WelcomeView {

    @BindView(R.id.img_header_back)
    ImageView imgHeaderBack;
    @BindView(R.id.right_text)
    TextView rightText;
    @BindView(R.id.imgPhoto)
    ImageView imgPhoto;
    @BindView(R.id.imgAdd)
    ImageView imgAdd;
    @BindView(R.id.tv_continue)
    TextView tvContinue;
    @BindView(R.id.tv_welcome_name)
    TextView tv_welcome_name;

    WelcomePresenter welcomePresenter;
    NetWorkingService netWorkingService;
    SaveLoginDetailsModel saveLoginDetailsModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        ButterKnife.bind(this);
        saveLoginDetailsModel = SharedPreff.getLoginDetailsModel();
        netWorkingService = ((AppClass) getApplication()).getNetworkService();
        welcomePresenter = new WelcomePresenterImpl(this, netWorkingService);

        setUpToolbar();
    }

    private void setUpToolbar() {
        tv_welcome_name.setText(getResources().getString(R.string.welcome)+" "+saveLoginDetailsModel.getName());
        imgHeaderBack.setVisibility(View.GONE);
        rightText.setVisibility(View.VISIBLE);
        rightText.setText(R.string.skip);
        rightText.setTextColor(ContextCompat.getColor(WelcomeActivity.this, R.color.black));
    }

    @OnClick(R.id.imgAdd)
    public void onAddPhotoClick() {
        openChoosePickerDialog();
    }

    private void openChoosePickerDialog() {
        DialogPopUps.openCamera(WelcomeActivity.this, new CommonListeners.AlertCallBackWithButtonsInterface() {
            @Override
            public void positiveClick() {
                // Gallery
                openGallery();
            }


            @Override
            public void neutralClick() {

            }

            @Override
            public void negativeClick() {
                //Camera
                openCamera();
            }
        });
    }

    private void openCamera() {
        RxImagePicker.with(WelcomeActivity.this).requestImage(Sources.CAMERA).subscribe(new Consumer<Uri>() {
            @Override
            public void accept(@NonNull Uri uri) throws Exception {

                UCrop.Options options = new UCrop.Options();
                options.setToolbarColor(ContextCompat.getColor(WelcomeActivity.this, R.color.colorPrimary));
                options.setStatusBarColor(ContextCompat.getColor(WelcomeActivity.this, R.color.colorPrimary));


                UCrop.of(uri, Uri.fromFile(new File(getCacheDir(), "temp_image" + System.currentTimeMillis() + ".jpeg")))
                        .withAspectRatio(1, 1)
                        .withMaxResultSize(700, 700)
                        .withOptions(options)
                        .start(WelcomeActivity.this);

            }
        });
    }


    private void openGallery() {
        RxImagePicker.with(WelcomeActivity.this).requestImage(Sources.GALLERY).subscribe(new Consumer<Uri>() {
            @Override
            public void accept(@NonNull Uri uri) throws Exception { //Get image by uri using one of image loading libraries. I use Glide in sample app.
                UCrop.Options options = new UCrop.Options();
                options.setToolbarColor(ContextCompat.getColor(WelcomeActivity.this, R.color.colorPrimary));
                options.setStatusBarColor(ContextCompat.getColor(WelcomeActivity.this, R.color.colorPrimary));
                UCrop.of(uri, Uri.fromFile(new File(getCacheDir(), "temp_image" + System.currentTimeMillis() + ".jpeg")))
                        .withAspectRatio(1, 1).withMaxResultSize(700, 700).withOptions(options).start((WelcomeActivity.this));
            }
        });

    }


    @OnClick(R.id.right_text)
    public void onRightClick() {
        startActivity(new Intent(this, HomeActivity.class));
    }


    @OnClick(R.id.tv_continue)
    public void onContinueClick() {
        // startActivity(new Intent(this, HomeActivity.class));

        welcomePresenter.uploadImageProcess();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK
                && requestCode == UCrop.REQUEST_CROP) {
            Uri resultUri = UCrop.getOutput(data);

            final InputStream imageStream;
            try {
                imageStream = getContentResolver().openInputStream(resultUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                String encodedImage = CommonUtils.encodeImage(selectedImage);

                welcomePresenter.setBase64(resultUri);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            if (resultUri != null) {

                Picasso.with(WelcomeActivity.this)
                        .load(resultUri).centerCrop().resize(200, 200)
                        .transform(new BorderCircleTransformation())
                        .into(imgPhoto);
                tvContinue.setVisibility(View.VISIBLE);

                //  createAccountViewPresnter.isProfilePicLoaded(true);
            }


        } else if (requestCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
            Log.d("uri", "error " + cropError);

        }
    }


    @Override
    public void showProgressDialog() {
        DialogPopUps.showProgressDialog(WelcomeActivity.this, getResources().getString(R.string.please_wait));
    }

    @Override
    public void showErrorDialog(String msg) {
        CommonUtils.showTopSnackBar(this, findViewById(android.R.id.content), msg);
    }

    @Override
    public void showMessageDialog(String msg) {
//        DialogPopUps.alertPopUp(WelcomeActivity.this, msg, getString(R.string.activation_required),
//                getResources().getString(R.string.ok)
//                , new CommonListeners.AlertCallBackInterface() {
//                    @Override
//                    public void neutralClick() {
//                        finish();
//                    }
//                });
    }

    @Override
    public void hideProgressDialog() {
        DialogPopUps.hideDialog();
    }


    @Override
    public void moveToHomeScreen() {
        startActivity(new Intent(WelcomeActivity.this, HomeActivity.class));
        finish();
    }
}
