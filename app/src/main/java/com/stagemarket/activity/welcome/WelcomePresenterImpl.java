package com.stagemarket.activity.welcome;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.stagemarket.AppClass;
import com.stagemarket.R;
import com.stagemarket.pojo.SaveLoginDetailsModel;
import com.stagemarket.pojo.updateProfile.UpdateProfileResponse;
import com.stagemarket.pojo.welcome_upload_image.UploadImageResponse;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.retrofit.RestClient;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.SharedPreff;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import io.reactivex.disposables.CompositeDisposable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit.RetrofitError;
import retrofit.mime.TypedFile;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WelcomePresenterImpl implements WelcomePresenter {

    WelcomeView view;
    NetWorkingService netWorkingService;
    private final CompositeDisposable disposables = new CompositeDisposable();
    Uri uri;
    SaveLoginDetailsModel saveLoginDetailsModel;
    private RequestBody description;
    TypedFile typedFile;

    public WelcomePresenterImpl(WelcomeView view, NetWorkingService netWorkingService) {
        this.view = view;
        this.netWorkingService = netWorkingService;
        saveLoginDetailsModel = SharedPreff.getLoginDetailsModel();

    }


    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        if (disposables != null) {
            disposables.clear();
        }
        view = null;
        netWorkingService = null;
    }

    MultipartBody.Part vFile = null;
    MultipartBody.Part mPartBody = null;

    @Override
    public void setBase64(Uri imageUri) {
        this.uri = imageUri;
        String pathToStoredVideo = CommonUtils.getPath((Context) view, uri);





        File file = null;

        if(uri!=null){
            if (CommonUtils.getPath((Context) view, uri) != null) {

                file = new File(pathToStoredVideo);
                typedFile = new TypedFile("multipart/form-data", file);
            } else {
                file = new File(uri.getPath());
                typedFile = new TypedFile("multipart/form-data", file);
            }
        }else{

            typedFile = new TypedFile("multipart/form-data",file);
        }








        mPartBody = prepareFilePart(Constants.FILE_PATH, imageUri);
        JSONObject mJsonObject = new JSONObject();
        try {
            mJsonObject.put(Constants.USERID, saveLoginDetailsModel.getId());

        } catch (JSONException e) {
            e.printStackTrace();
        }

       // String userid = "43";

        description = createPartFromString(mJsonObject.toString());

    }

    private RequestBody createPartFromString(String s) {

        return RequestBody.create(
                MultipartBody.FORM, s);
    }

    private MultipartBody.Part prepareFilePart(String partName, Uri fileUri) {
        File file = new File(fileUri.getPath());
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse("file"),
                        file
                );


        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

    @Override
    public void uploadImageProcess() {
        if (view != null) {
            makeUploadImageRequest(uri);
        }
    }

    private void makeUploadImageRequest(Uri uri) {
        if (CommonUtils.isConnectedToInternet(AppClass.getInstance().getApplicationContext())) {
            view.showProgressDialog();
            Log.e("params",description+","+mPartBody);


            RestClient.get(60).uploadFile(saveLoginDetailsModel.getId()

                     , typedFile, new retrofit.Callback<UploadImageResponse>() {
                        @Override
                        public void success(UploadImageResponse apiGlobal, retrofit.client.Response response) {
                            if (view != null) {
                                view.hideProgressDialog();
                                view.moveToHomeScreen();
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            if (view != null) {
                                view.hideProgressDialog();
                                view.showErrorDialog((AppClass.getInstance().getApplicationContext()).getResources().getString(R.string.something_went_wrong));

                            }
                        }
                    });
 /*           netWorkingService.getAPI().uploadFile(saveLoginDetailsModel.getId(), typedFile)
                    .enqueue(new Callback<UploadImageResponse>() {
                        @Override
                        public void onResponse(Call<UploadImageResponse> call, Response<UploadImageResponse> uploadImageResponse) {
                            if (view != null) {
                                view.hideProgressDialog();
                                if (uploadImageResponse.body().getResult().equalsIgnoreCase(Constants.SUCCESS)) {
                                    // view.showMessageDialog(uploadImageResponse.getMessage());
                                    view.moveToHomeScreen();
                                } else {
                                    view.showErrorDialog(uploadImageResponse.body().getMessage());
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<UploadImageResponse> call, Throwable t) {
                            view.hideProgressDialog();
                            view.showErrorDialog((AppClass.getInstance().getApplicationContext()).getResources().getString(R.string.something_went_wrong));
                        }
                    });*/
         /*   netWorkingService.getAPI().uploadFile(mPartBody, vFile
            ).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<UploadImageResponse>() {


                @Override
                public void onSubscribe(Disposable d) {
                    disposables.add(d);
                }

                @Override
                public void onNext(UploadImageResponse uploadImageResponse) {

                    if (view != null) {
                        view.hideProgressDialog();

                        if (uploadImageResponse.getResult().equalsIgnoreCase(Constants.SUCCESS)) {

                            // view.showMessageDialog(uploadImageResponse.getMessage());
                            view.moveToHomeScreen();


                        } else {

                            view.showErrorDialog(uploadImageResponse.getMessage());

                        }
                    }

                }

                @Override
                public void onError(Throwable e) {
                    view.hideProgressDialog();
                    view.showErrorDialog((AppClass.getInstance().getApplicationContext()).getResources().getString(R.string.something_went_wrong));
                }

                @Override
                public void onComplete() {

                }
            });*/
        } else {
           // CommonUtils.check_your_network_available(AppClass.getInstance().getApplicationContext());
            CommonUtils.showTopSnackBar((Context) view,
                    ((Activity) view).findViewById(android.R.id.content), ((Context) view).getString(R.string.please_check_internet));

        }
    }


}
