package com.stagemarket.activity.welcome;

import com.stagemarket.BasePresenter;

public interface WelcomeView extends BasePresenter {
    void moveToHomeScreen();
}
