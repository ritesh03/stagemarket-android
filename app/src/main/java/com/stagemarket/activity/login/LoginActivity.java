package com.stagemarket.activity.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.stagemarket.BaseActivity;
import com.stagemarket.R;
import com.stagemarket.activity.forgotPassword.ForgotPasswordActivity;
import com.stagemarket.activity.home.HomeActivity;
import com.stagemarket.activity.signUp.SignUpActivity;
import com.stagemarket.activity.splash.Splash;
import com.stagemarket.activity.welcome.WelcomeActivity;
import com.stagemarket.facebook.OnSocialLoginListener;
import com.stagemarket.facebook.SocialLogin;
import com.stagemarket.facebook.SocialLoginData;
import com.stagemarket.pojo.login.LoginResponse;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.DialogPopUps;
import com.stagemarket.utils.GPSTracker;
import com.stagemarket.utils.SharedPreff;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginActivity extends BaseActivity implements LoginView, OnSocialLoginListener {

    @BindView(R.id.img_slider)
    ImageView imgSlider;
    @BindView(R.id.img_header_back)
    ImageView imgHeaderBack;
    @BindView(R.id.et_email_id)
    EditText etEmailId;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.tv_forgotPassword)
    TextView tvForgotPassword;
    @BindView(R.id.tv_signIn)
    TextView tvSignIn;
    @BindView(R.id.lyt_facebook)
    LinearLayout lytFacebook;

    LoginPresenter mLoginPresenter;
    @BindView(R.id.title_name)
    TextView titleName;
    @BindView(R.id.tv_please_sign_in)
    TextView tvPleaseSignIn;
    @BindView(R.id.img_fb)
    ImageView imgFb;

    @BindView(R.id.tv_sign_upp)
    TextView tv_sign_upp;
    @BindView(R.id.tv_sign_up)
    TextView tvSignUp;
    @BindView(R.id.lyt_signUp)
    LinearLayout lytSignUp;

    private SocialLogin socialLogin;
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        mLoginPresenter = new LoginPresenterImp(this);
        initViews();
        initCallbackManager();


        tv_sign_upp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
            }
        });
    }

    GPSTracker gps;

    private void callLocation() {
        gps = new GPSTracker(LoginActivity.this);

        // check if GPS enabled
        if (gps.canGetLocation()) {
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();

            Log.e("MainActivity", "latitude -> " + latitude);
            Log.e("MainActivity", "longitude -> " + longitude);

            if (latitude != 0.0) {
                SharedPreff.saveLat(String.valueOf(latitude));
                SharedPreff.saveLng(String.valueOf(longitude));
            } else {

            }
            // LocationAddress locationAddress = new LocationAddress();
            /* locationAddress.getAddressFromLocation(latitude, longitude,
              getApplicationContext(), new GeocoderHandler());*/

        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            // gps.showSettingsAlert();
        }
    }


    /*fb initialization*/
    private void initCallbackManager() {
        socialLogin = new SocialLogin(LoginActivity.this);
        callbackManager = socialLogin.loginViaFacebook(R.id.img_fb);
    }


    private void initViews() {
        imgHeaderBack.setVisibility(View.INVISIBLE);
        //  ubHeaderToolbar = findViewById(R.id.ub_header_toolbar);
    }

    @OnClick(R.id.tv_signIn)
    public void onSignInClick() {
        mLoginPresenter.onSignInClick(etEmailId.getText().toString().trim()
                , etPassword.getText().toString().toLowerCase());
    }

    @OnClick(R.id.lyt_signUp)
    public void onSignUpClick() {
        startActivity(new Intent(this, SignUpActivity.class));
    }

    @OnClick(R.id.tv_forgotPassword)
    public void onForgotPasswordClick() {
        startActivity(new Intent(this, ForgotPasswordActivity.class));
    }


    @OnClick(R.id.lyt_facebook)
    public void onFacebookClick() {
        imgFb.performClick();

    }

    @Override
    public void showProgressDialog() {
        DialogPopUps.showProgressDialog(LoginActivity.this, getResources().getString(R.string.please_wait));
    }

    @Override
    public void showErrorDialog(String msg) {
        CommonUtils.showTopSnackBar(this, findViewById(android.R.id.content), msg);
    }

    @Override
    public void showMessageDialog(String msg) {

    }

    @Override
    public void hideProgressDialog() {
        DialogPopUps.hideDialog();

    }

    @Override
    public void onSocialLogin(int type, SocialLoginData loginData) {
        FacebookSdk.sdkInitialize(getApplicationContext());
        LoginManager.getInstance().logOut();

        Log.e("fb detail", new Gson().toJson(loginData));
        if (loginData.getEmail() == null || TextUtils.isEmpty(loginData.getEmail())) {
            // showError(getResources().getString(R.string.We_are_unable_to_get_your_email_id_from_login_Pleaseuse_Google_or_manual_Login_to_access_the_app));
            showErrorDialog(getResources().getString(R.string.We_are_unable_to_get_your_email_id_from_login_Please_use_Google_or_manual_Login_to_access_the_app));

        } else {

            SharedPreff.saveSocialUser(loginData);
            makeSocialLoginRequest();

            //  loginActivityPresenter.checkValidation(loginData.getEmail().toLowerCase(), loginData.getSocialUserID(),loginData.getUserType());
        }
    }

    private void makeSocialLoginRequest() {
        mLoginPresenter.makeSocialLoginRequest();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        socialLogin.releaseResources();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
//            Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
            callbackManager.onActivityResult(requestCode, resultCode, data);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        CommonUtils.hideSoftKeyboard(LoginActivity.this);
        return super.dispatchTouchEvent(ev);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        callLocation();
    }

    @Override
    public void onLoginResponse(LoginResponse loginResponse) {
        //SharedPreff.saveLoginResponse(loginResponse);
        if (!loginResponse.getData().getImageUrl().equalsIgnoreCase("")) {
            startActivity(new Intent(this, HomeActivity.class));
            finish();
        } else {
            startActivity(new Intent(this, WelcomeActivity.class));
            finish();
        }
    }

    @Override
    public void moveToWelcomeScreen() {
        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
        finish();
    }
}
