package com.stagemarket.activity.login;

import com.stagemarket.CommonPresenter;

public interface LoginPresenter extends CommonPresenter {
    void onSignInClick(String emailID, String password);

    void onForgotPasswordClick();

    void makeSocialLoginRequest();
}
