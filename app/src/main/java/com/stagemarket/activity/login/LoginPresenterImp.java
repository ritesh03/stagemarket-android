package com.stagemarket.activity.login;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import com.stagemarket.AppClass;
import com.stagemarket.R;
import com.stagemarket.pojo.SaveLoginDetailsModel;
import com.stagemarket.pojo.login.LoginResponse;
import com.stagemarket.pojo.social_login.SocialLoginResponse;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.SharedPreff;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class LoginPresenterImp implements LoginPresenter {

    Context context;
    private static final String TAG = LoginPresenterImp.class.getSimpleName();
    LoginView loginView = null;
    private NetWorkingService netWorkingService;
    private final CompositeDisposable disposables = new CompositeDisposable();

    public LoginPresenterImp(LoginView loginView) {
        this.loginView = loginView;
        netWorkingService = AppClass.getInstance().getNetworkService();
        this.context = AppClass.getInstance().getApplicationContext();
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        if (disposables != null) {
            disposables.clear();
        }
        loginView = null;
        netWorkingService = null;
    }


    @Override
    public void onSignInClick(String emailID, String password) {
        if (TextUtils.isEmpty(emailID)) {
            loginView.showErrorDialog(context.getString(R.string.email_address_shouldnot_be_empty));
        } else if (!CommonUtils.validateEmail(emailID)) {
            loginView.showErrorDialog(context.getString(R.string.please_enter_valid_emailid));
        } else if (TextUtils.isEmpty(password)) {
            loginView.showErrorDialog(context.getString(R.string.password_shouldnot_be_empty));
        } else if (password.length() < 6) {
            loginView.showErrorDialog(context.getString(R.string.password_shouldnot_be_less_than_6_characters));
        } else {
            if (CommonUtils.isConnectedToInternet(context)) {
                loginView.showProgressDialog();
                netWorkingService.getAPI().login(emailID, password,
                        Constants.DEVICE_TYPE,SharedPreff.getNotificationToken())
                        .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<LoginResponse>() {


                            @Override
                            public void onSubscribe(Disposable d) {
                                disposables.add(d);
                            }

                            @Override
                            public void onNext(LoginResponse loginResponse) {
                                loginView.hideProgressDialog();
                                if (loginView != null) {
                                    loginView.hideProgressDialog();
                                    if (loginResponse.getResult().equalsIgnoreCase(Constants.SUCCESS)) {

                                        SaveLoginDetailsModel saveLoginDetailsModel = new SaveLoginDetailsModel();
                                        saveLoginDetailsModel.setFirst_name(loginResponse.getData().getFirstName());

                                        saveLoginDetailsModel.setName(loginResponse.getData().getFirstName());
                                        saveLoginDetailsModel.setLast_name(loginResponse.getData().getLastName());
                                        saveLoginDetailsModel.setToken(loginResponse.getData().getDeviceToken());
                                        saveLoginDetailsModel.setEmail(loginResponse.getData().getEmail());
                                        saveLoginDetailsModel.setEmail(loginResponse.getData().getEmail());
                                       // Toast.makeText(context, ""+loginResponse.getData().getOrganizationType(), Toast.LENGTH_SHORT).show();
                                        saveLoginDetailsModel.setOrganizationType(loginResponse.getData().getOrganizationType());
                                        saveLoginDetailsModel.setId(loginResponse.getData().getId());
                                        saveLoginDetailsModel.setFromSocialLogin(false);

                                        SharedPreff.saveLoginDetails(saveLoginDetailsModel);


                                        loginView.onLoginResponse(loginResponse);

                                    } else {
                                        loginView.showErrorDialog(loginResponse.getMessage());
                                    }
                                }

                            }

                            @Override
                            public void onError(Throwable e) {
                                loginView.hideProgressDialog();
                                loginView.showErrorDialog(context.getResources().getString(R.string.something_went_wrong));
                            }

                            @Override
                            public void onComplete() {

                            }
                        });
            } else {
                CommonUtils.showTopSnackBar(context,
                        ((Activity) loginView).findViewById(android.R.id.content), context.getString(R.string.please_check_internet));
            }
        }
    }

    @Override
    public void onForgotPasswordClick() {

    }

    @Override
    public void makeSocialLoginRequest() {

        if (loginView != null) {
            loginView.showProgressDialog();
        }

        netWorkingService.getAPI().socialLoginService(SharedPreff.getSocialUser().getSocialUserID(),
                SharedPreff.getSocialUser().getEmail(),SharedPreff.getSocialUser().getFirst_name(),SharedPreff.getSocialUser().getLast_name(), Constants.DEVICE_TYPE, SharedPreff.getNotificationToken()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<SocialLoginResponse>() {


            @Override
            public void onSubscribe(Disposable d) {

                disposables.add(d);
            }

            @Override
            public void onNext(SocialLoginResponse socialLoginResponse) {

                if (CommonUtils.isConnectedToInternet((Context) loginView)) {

                    if (loginView != null) {
                        loginView.hideProgressDialog();
                    }
                    if (socialLoginResponse.getResult().equalsIgnoreCase(Constants.SUCCESS)) {

                        SaveLoginDetailsModel saveLoginDetailsModel = new SaveLoginDetailsModel();
                        saveLoginDetailsModel.setFirst_name(socialLoginResponse.getData().getFirstName());

                        saveLoginDetailsModel.setName(socialLoginResponse.getData().getFirstName());
                        saveLoginDetailsModel.setLast_name(socialLoginResponse.getData().getLastName());
                        saveLoginDetailsModel.setToken(socialLoginResponse.getData().getDeviceToken());
                        saveLoginDetailsModel.setEmail(socialLoginResponse.getData().getEmail());
                        saveLoginDetailsModel.setImage(socialLoginResponse.getData().getImageUrl());
                        saveLoginDetailsModel.setId(socialLoginResponse.getData().getId());
                        saveLoginDetailsModel.setOrganizationType(socialLoginResponse.getData().getOrganizationType());
                        saveLoginDetailsModel.setFromSocialLogin(true);

                        SharedPreff.saveLoginDetails(saveLoginDetailsModel);


                        loginView.moveToWelcomeScreen();
                    } else {
                        loginView.showErrorDialog(socialLoginResponse.getMessage());
                    }
                } else {
                    CommonUtils.check_your_network_available((Context) loginView);
                }
            }

            @Override
            public void onError(Throwable e) {
                if (loginView != null) {
                    loginView.hideProgressDialog();
                    loginView.showErrorDialog(((Context) loginView).getResources().getString(R.string.something_went_wrong));

                }
            }

            @Override
            public void onComplete() {

            }
        });


    }


}
