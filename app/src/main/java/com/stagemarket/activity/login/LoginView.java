package com.stagemarket.activity.login;

import com.stagemarket.BasePresenter;
import com.stagemarket.pojo.login.LoginResponse;

public interface LoginView extends BasePresenter {
    void onLoginResponse(LoginResponse loginResponse);

    void moveToWelcomeScreen();
}
