package com.stagemarket.activity.home;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;

import com.stagemarket.BaseActivity;
import com.stagemarket.R;
import com.stagemarket.activity.myOrders.MyOrdersActivity;
import com.stagemarket.activity.sell.categorySell.camera.CameraFragmentMainActivity;
import com.stagemarket.activity.sell.categorySell.category_sell.CategorySellActivity;
import com.stagemarket.activity.viewProfile.ViewProfileActivity;
import com.stagemarket.activity.yourAccount.YourAccountActivity;
import com.stagemarket.fragment.discover.Discover;
import com.stagemarket.fragment.myposts.MyPosts;
import com.stagemarket.fragment.myprofile.MyProfile;
import com.stagemarket.fragment.notifications.Notifications;
import com.stagemarket.utils.BottomNavigationViewEx;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HomeActivity extends BaseActivity {


    @BindView(R.id.bottomnav)
    BottomNavigationViewEx bottomnavLeft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        setUpViews();
isStoragePermissionGranted();
        replaceFragment(new Discover());
    }
    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("dd","Permission is granted");
                return true;
            } else {

                Log.v("dd","Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v("dd","Permission is granted");
            return true;
        }
    }
    private void replaceFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_frame, fragment)
                .commit();
    }

    @OnClick(R.id.imgSell)
    public void onSellClick() {
        startActivity(new Intent(this, CameraFragmentMainActivity.class));
        // startActivity(new Intent(this, MyOrdersActivity.class));
        //  startActivity(new Intent(this, YourAccountActivity.class));
        //  startActivity(new Intent(this, CategorySellActivity.class));
    }

    @SuppressLint("RestrictedApi")
    private void setUpViews() {
        bottomnavLeft.enableShiftingMode(false);
        bottomnavLeft.setTextVisibility(true);
        bottomnavLeft.enableAnimation(false);
        BottomNavigationMenuView bottomNavigationMenuView = (BottomNavigationMenuView) bottomnavLeft.getChildAt(0);
        for (int i = 0; i < bottomNavigationMenuView.getChildCount(); i++) {
            BottomNavigationItemView bottomNavigationItemView = (BottomNavigationItemView) bottomNavigationMenuView.getChildAt(i);
            bottomNavigationItemView.setShiftingMode(false);
            bottomNavigationItemView.setChecked(false);
        }

        bottomnavLeft.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_discover:
                        if (!item.isChecked()) {
                            replaceFragment(new Discover());
                        }

                        break;
                    case R.id.menu_mypost:
                        if (!item.isChecked()) {
                            replaceFragment(new MyPosts());
                        }

                        break;
                    case R.id.menu_notifications:
                        if (!item.isChecked()) {
                            replaceFragment(new Notifications());
                        }
                        break;
                    case R.id.menu_myprofile:
                        if (!item.isChecked()) {
                            replaceFragment(new MyProfile());
                        }
                        break;
                }
                return true;
            }

        });


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
