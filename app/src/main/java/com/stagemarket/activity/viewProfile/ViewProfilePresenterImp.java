package com.stagemarket.activity.viewProfile;


import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.stagemarket.AppClass;
import com.stagemarket.R;
import com.stagemarket.pojo.SaveLoginDetailsModel;
import com.stagemarket.pojo.viewOtherProfile.Response;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.SharedPreff;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ViewProfilePresenterImp implements ViewProfilePresenter {

    ViewProfileView viewProfileView;
    SaveLoginDetailsModel saveLoginDetailsModel;
    private NetWorkingService netWorkingService;
    private final CompositeDisposable disposables = new CompositeDisposable();
    Context mContext;

    public ViewProfilePresenterImp(ViewProfileView viewProfileView) {
        this.viewProfileView = viewProfileView;
        mContext = AppClass.getInstance().getApplicationContext();
        saveLoginDetailsModel = SharedPreff.getLoginDetailsModel();
        netWorkingService = AppClass.getInstance().getNetworkService();

    }


    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        if (disposables != null) {
            disposables.clear();
        }
        viewProfileView = null;
        netWorkingService = null;
    }


    @Override
    public void getOtherProfileRequest(String other_profile_id, String other_profile_company_id) {
        if (CommonUtils.isConnectedToInternet(mContext)) {
            viewProfileView.showProgressDialog();
            Log.e("other params","other="+other_profile_id+",own="+SharedPreff.getLoginDetailsModel().getId()+",company id="+other_profile_company_id);
            netWorkingService.getAPI().getOtherProfileService(
                    other_profile_id,SharedPreff.getLoginDetailsModel().getId(),other_profile_company_id)
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response>() {


                        @Override
                        public void onSubscribe(Disposable d) {
                            disposables.add(d);
                        }

                        @Override
                        public void onNext(Response viewOtherProfileResponse) {
                            viewProfileView.hideProgressDialog();

                            Gson gson=new Gson();
Log.e("other response",gson.toJson(viewOtherProfileResponse));
                            if (viewProfileView != null) {
                                viewProfileView.hideProgressDialog();
                                if (viewOtherProfileResponse.getResult().equalsIgnoreCase(Constants.SUCCESS)) {

                                    viewProfileView.onOtherProfileResponse(viewOtherProfileResponse);

                                } else {
                                    viewProfileView.showErrorDialog(viewOtherProfileResponse.getMessage());
                                }
                            }

                        }

                        @Override
                        public void onError(Throwable e) {
                            viewProfileView.hideProgressDialog();
                            viewProfileView.showErrorDialog(mContext.getResources().getString(R.string.something_went_wrong));
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        } else {
            CommonUtils.showTopSnackBar(mContext,
                    ((ViewProfileActivity) viewProfileView).findViewById(android.R.id.content), mContext.getString(R.string.please_check_internet));
        }
    }



}
