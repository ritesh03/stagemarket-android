package com.stagemarket.activity.viewProfile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.stagemarket.BaseActivity;
import com.stagemarket.R;
import com.stagemarket.adapter.MYProfileSellingAdapter;
import com.stagemarket.pojo.viewOtherProfile.DataItem;
import com.stagemarket.pojo.viewOtherProfile.Response;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewProfileActivity extends BaseActivity implements ViewProfileView {

    @BindView(R.id.title_name)
    TextView titleName;
    @BindView(R.id.img_header_back)
    ImageView imgHeaderBack;
    @BindView(R.id.header)
    View header;
    @BindView(R.id.img_profile)
    ImageView imgProfile;
    @BindView(R.id.tv_profile_name)
    TextView tvProfileName;
    @BindView(R.id.tv_profile_address)
    TextView tvProfileAddress;
    @BindView(R.id.recyclerViewSelling)
    RecyclerView recyclerViewSelling;
    @BindView(R.id.lytMain)
    LinearLayout lytMain;
    MYProfileSellingAdapter mMyProfileSellingAdapter;
    // ArrayList<ViewProfileSelling> mArrayListViewProfileSelling = new ArrayList<>();
    @BindView(R.id.imgDefault)
    ImageView imgDefault;
    @BindView(R.id.img_slider)
    ImageView imgSlider;
    @BindView(R.id.scrollview)
    NestedScrollView scrollview;
    ArrayList<Response> mArrayList_OtherProfileView = new ArrayList<>();
    ViewProfilePresenter viewProfilePresenter;
    ArrayList<DataItem> mArrayList_DataItem = new ArrayList<>();
    @BindView(R.id.swipeRefreshView)
    SwipeRefreshLayout swipeRefreshView;
    String other_profile_id = "";
    private String other_profile_company_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_profile);
        ButterKnife.bind(this);
        viewProfilePresenter = new ViewProfilePresenterImp(this);

        if (getIntent().hasExtra(Constants.OTHER_PROFILE_USER_ID)) {
            other_profile_id = getIntent().getStringExtra(Constants.OTHER_PROFILE_USER_ID);

        }
        if (getIntent().hasExtra(Constants.OTHER_PROFILE_COMPANY_ID)) {
            other_profile_company_id = getIntent().getStringExtra(Constants.OTHER_PROFILE_COMPANY_ID);
        }
        scrollview.scrollTo(0, 0);
        initToolbar();
        initRecyclerView();

         swipeRefreshView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshView.setRefreshing(false);
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        mArrayList_DataItem.clear();
        viewProfilePresenter.getOtherProfileRequest(other_profile_id,other_profile_company_id);


    }

    private void initToolbar() {
        header.setBackgroundColor(ContextCompat.getColor(this, R.color.greynew));

        imgHeaderBack.setImageResource(R.drawable.ic_back);
        DrawableCompat.setTint(imgHeaderBack.getDrawable(), ContextCompat.getColor(this, R.color.black));

        // titleName.setText(R.string.view_profile);
        //  titleName.setTextColor(ContextCompat.getColor(this, R.color.black));


    }

    @OnClick(R.id.img_header_back)
    public void onBackClick() {
        ViewProfileActivity.super.onBackPressed();
    }


    private void initRecyclerView() {
        //  initData();

        mMyProfileSellingAdapter = new MYProfileSellingAdapter(this, mArrayList_DataItem);
        recyclerViewSelling.setLayoutManager(new GridLayoutManager(ViewProfileActivity.this, 2));
        recyclerViewSelling.setAdapter(mMyProfileSellingAdapter);
    }


//    private void initData() {
//        mArrayListViewProfileSelling.add(new ViewProfileSelling("1", "$89.90", "Akg and shure Microphone"));
//        mArrayListViewProfileSelling.add(new ViewProfileSelling("2", "$759.99", "Electric Guitar Model Microphone"));
//        mArrayListViewProfileSelling.add(new ViewProfileSelling("3", "$459.90", "Victrola Vintage 3 Spe.."));
//        mArrayListViewProfileSelling.add(new ViewProfileSelling("4", "$289.99", "Herculus DJ Instinct"));
//        mArrayListViewProfileSelling.add(new ViewProfileSelling("5", "$89.90", "Akg and shure Microphone"));
//        mArrayListViewProfileSelling.add(new ViewProfileSelling("6", "$759.99", "Electric Guitar Model Microphone"));
//        mArrayListViewProfileSelling.add(new ViewProfileSelling("7", "$459.90", "Victrola Vintage 3 Spe.."));
//        mArrayListViewProfileSelling.add(new ViewProfileSelling("8", "$289.99", "Herculus DJ Instinct"));
//
//
//    }


    @Override
    public void showProgressDialog() {
        swipeRefreshView.setRefreshing(true);
    }

    @Override
    public void showErrorDialog(String msg) {

    }

    @Override
    public void showMessageDialog(String msg) {

    }

    @Override
    public void hideProgressDialog() {
        swipeRefreshView.setRefreshing(false);
    }


    @Override
    public void onOtherProfileResponse(Response viewOtherProfileResponse) {
        if (viewOtherProfileResponse != null) {
            if (viewOtherProfileResponse.getUserProfile() != null) {
                lytMain.setVisibility(View.VISIBLE);

                tvProfileName.setText(viewOtherProfileResponse.getUserProfile().getFirstName() + " "
                        + viewOtherProfileResponse.getUserProfile().getLastName());
                if(!viewOtherProfileResponse.getUserProfile().getImageUrl().equalsIgnoreCase("")){

                    imgDefault.setVisibility(View.GONE);
                    CommonUtils.setImage(ViewProfileActivity.this, imgProfile,
                            Constants.IMAGE_BASE_URL + viewOtherProfileResponse.getUserProfile().getImageUrl(),
                            200, 200);
                }else{
                    imgDefault.setVisibility(View.VISIBLE);
                }

            } else {
                imgDefault.setVisibility(View.VISIBLE);
            }

            mArrayList_DataItem.addAll(viewOtherProfileResponse.getData());
            mMyProfileSellingAdapter.notifyDataSetChanged();

        }
    }
}
