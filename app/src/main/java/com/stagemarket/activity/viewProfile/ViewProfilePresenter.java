package com.stagemarket.activity.viewProfile;

import com.stagemarket.CommonPresenter;

public interface ViewProfilePresenter extends CommonPresenter {
    void getOtherProfileRequest(String other_profile_id, String other_profile_company_id);

}
