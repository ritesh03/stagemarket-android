package com.stagemarket.activity.viewProfile;

import com.stagemarket.BasePresenter;
import com.stagemarket.pojo.viewOtherProfile.Response;

public interface ViewProfileView extends BasePresenter{
    void onOtherProfileResponse(Response viewOtherProfileResponse);
}
