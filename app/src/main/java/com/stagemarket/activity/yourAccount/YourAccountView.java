package com.stagemarket.activity.yourAccount;

import com.stagemarket.BasePresenter;

public interface YourAccountView extends BasePresenter {
    void goToEditAddressScreen();
}
