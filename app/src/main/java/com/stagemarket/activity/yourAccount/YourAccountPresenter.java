package com.stagemarket.activity.yourAccount;

import android.net.Uri;

import com.stagemarket.CommonPresenter;

public interface YourAccountPresenter extends CommonPresenter {
    void setBase64(Uri encodedImage);

    void onSaveEditProfileClick(String name, String email_address, String mobile, String dob, String gender,String lastNAme,String companyName,String companyAddress,String companyWebsite);
}
