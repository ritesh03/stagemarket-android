package com.stagemarket.activity.yourAccount;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;

import com.github.florent37.camerafragment.internal.utils.Utils;
import com.stagemarket.AppClass;
import com.stagemarket.R;
import com.stagemarket.pojo.SaveLoginDetailsModel;
import com.stagemarket.pojo.signup.SignUpResponse;
import com.stagemarket.pojo.updateProfile.UpdateProfileResponse;
import com.stagemarket.pojo.welcome_upload_image.UploadImageResponse;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.retrofit.RestClient;
import com.stagemarket.utils.ApiGlobal;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.SharedPreff;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit.RetrofitError;
import retrofit.mime.TypedFile;
import retrofit2.Call;
import retrofit.Callback;
import retrofit2.Response;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public class YourAccountPresenterImp implements YourAccountPresenter {

    YourAccountView yourAccountView;
    private NetWorkingService netWorkingService;
    private final CompositeDisposable disposables = new CompositeDisposable();
    Context context;
    SaveLoginDetailsModel saveLoginDetailsModel;

    Uri uri;

    public YourAccountPresenterImp(YourAccountView yourAccountView) {
        this.yourAccountView = yourAccountView;
        netWorkingService = AppClass.getInstance().getNetworkService();
        saveLoginDetailsModel = SharedPreff.getLoginDetailsModel();
        context = AppClass.getInstance().getApplicationContext();
    }

    @Override
    public void subscribe() {
    }

    @Override
    public void unSubscribe() {
        if (disposables != null) {
            disposables.clear();
        }
        yourAccountView = null;
        netWorkingService = null;
    }

    MultipartBody.Part mPartBody = null;
    private RequestBody description;

    @Override
    public void setBase64(Uri imageUri) {
        this.uri = imageUri;

        String pathToStoredVideo = CommonUtils.getPath((Context) yourAccountView, uri);
        mPartBody = prepareFilePart(Constants.FILE_PATH, imageUri);

    }

    private MultipartBody.Part prepareFilePart(String partName, Uri fileUri) {
        File file = new File(fileUri.getPath());
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse("file"),
                        file
                );
        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }


    private RequestBody createPartFromString(String s) {

        return RequestBody.create(
                MultipartBody.FORM, s);
    }

    @Override
    public void onSaveEditProfileClick(String name, String email_address, String mobile, String dob, String gender,
                                       String lastName,String companyName,String companyAddress,String companyWebsite) {


        if(SharedPreff.getLoginDetailsModel().getOrganizationType().equalsIgnoreCase("1")) {
            if (TextUtils.isEmpty(name) || name.trim().equalsIgnoreCase("")) {
                yourAccountView.showErrorDialog(context.getString(R.string.please_enter_name));
            } else if (TextUtils.isEmpty(lastName) || lastName.trim().equalsIgnoreCase("")) {
                yourAccountView.showErrorDialog(context.getString(R.string.please_enter_lastname));
            } /*else if (TextUtils.isEmpty(email_address) || email_address.trim().equalsIgnoreCase("")) {
            yourAccountView.showErrorDialog(context.getString(R.string.email_address_shouldnot_be_empty));
        } else if (!CommonUtils.validateEmail(email_address)) {
            yourAccountView.showErrorDialog(context.getString(R.string.please_enter_valid_emailid));
        }*/ else if (TextUtils.isEmpty(mobile) || mobile.trim().equalsIgnoreCase("")) {
                yourAccountView.showErrorDialog(context.getString(R.string.plase_enter_mobile_number));
            }
            else
            {
                if (CommonUtils.isConnectedToInternet( context)) {

                    if (yourAccountView != null) {
                    makeUpdateProfileRequest(name+" "+lastName, email_address, mobile, gender,companyName,companyAddress,companyWebsite);
                }}
                else {
                    CommonUtils.showTopSnackBar(context,
                            ((Activity) context).findViewById(android.R.id.content), context.getString(R.string.please_check_internet));
                }
            }
        }
        else
        {  if (TextUtils.isEmpty(companyName) || companyName.trim().equalsIgnoreCase("")) {
            yourAccountView.showErrorDialog(context.getString(R.string.please_enter_company_name));
        } else if (TextUtils.isEmpty(companyWebsite) || companyWebsite.trim().equalsIgnoreCase("")) {
            yourAccountView.showErrorDialog(context.getString(R.string.please_enter_company_website));
        }
        else if (TextUtils.isEmpty(companyAddress) || companyAddress.trim().equalsIgnoreCase("")) {
            yourAccountView.showErrorDialog(context.getString(R.string.please_enter_companyt_address));
        }/*else if (TextUtils.isEmpty(email_address) || email_address.trim().equalsIgnoreCase("")) {
            yourAccountView.showErrorDialog(context.getString(R.string.email_address_shouldnot_be_empty));
        } else if (!CommonUtils.validateEmail(email_address)) {
            yourAccountView.showErrorDialog(context.getString(R.string.please_enter_valid_emailid));
        }*/ else if (TextUtils.isEmpty(mobile) || mobile.trim().equalsIgnoreCase("")) {
            yourAccountView.showErrorDialog(context.getString(R.string.plase_enter_mobile_number));
        }
        else
        { if (CommonUtils.isConnectedToInternet( YourAccountActivity.context)) {
            if (yourAccountView != null) {
                makeUpdateProfileRequest(name, email_address, mobile, gender,companyName,companyAddress,companyWebsite);
            }}
        else {
            CommonUtils.showTopSnackBar(YourAccountActivity.context,
                    ((Activity) YourAccountActivity.context).findViewById(android.R.id.content), YourAccountActivity.context.getString(R.string.please_check_internet));
        }
        }

        }



            JSONObject mJsonObject = new JSONObject();
            try {
                String first_name = "", last_name = "";
                if (name.trim().contains(" ")) {
                    String namee[] = name.split(" ");

                    first_name = namee[0];
                    last_name = namee[1];
                } else {
                    first_name = name;
                }

                mJsonObject.put(Constants.USERID, saveLoginDetailsModel.getId());
                mJsonObject.put(Constants.FIRSTNAME, first_name);
                mJsonObject.put(Constants.LASTNAME, last_name);
                mJsonObject.put(Constants.EMAIL, email_address);
                mJsonObject.put(Constants.DOB, dob);
                mJsonObject.put(Constants.MOBILE, mobile);
                mJsonObject.put("companyName", companyName);
                mJsonObject.put("companyAddress", companyAddress);
                mJsonObject.put("companyWebsite", companyWebsite);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            // String userid = "43";

            description = createPartFromString(mJsonObject.toString());

          /*  if (yourAccountView != null) {
                makeUpdateProfileRequest(name, email_address, mobile, gender);
            }*/

    }

    private void makeUpdateProfileRequest(String name, String email_address, String mobile, String gender,String companyName,String companyAddress,String companyWebsite) {

        String first_name = "", last_name = "";
        if (name.trim().contains(" ")) {
            String namee[] = name.split(" ");

            first_name = namee[0];
            last_name = namee[1];
        } else {
            first_name = name;
        }

        if (yourAccountView != null) {
            yourAccountView.showProgressDialog();
        }
        File file = null;
        TypedFile typedFile = null;
        if(uri!=null){
            if (CommonUtils.getPath((Context) yourAccountView, uri) != null) {
                String pathToStoredVideo = CommonUtils.getPath((Context) yourAccountView, uri);
                file = new File(pathToStoredVideo);
                typedFile = new TypedFile("multipart/form-data", file);
            } else {
                file = new File(uri.getPath());
                typedFile = new TypedFile("multipart/form-data", file);
            }




            RestClient.get(60).updateProfileService(saveLoginDetailsModel.getId()
                    , first_name, email_address, mobile,
                    gender, last_name,companyName,companyAddress,companyWebsite, typedFile, new Callback<UpdateProfileResponse>() {
                        @Override
                        public void success(UpdateProfileResponse apiGlobal, retrofit.client.Response response) {
                            if (yourAccountView != null) {
                                yourAccountView.hideProgressDialog();
                            }


                            if (apiGlobal.getResult().equalsIgnoreCase(Constants.SUCCESS)) {
                                yourAccountView.showMessageDialog(apiGlobal.getMessage());
                            } else {
                                yourAccountView.showErrorDialog(apiGlobal.getMessage());
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            if (yourAccountView != null) {
                                yourAccountView.hideProgressDialog();
                                yourAccountView.showErrorDialog(((Context) yourAccountView)
                                        .getResources().getString(R.string.something_went_wrong));

                            }
                        }
                    });
        }else{


            netWorkingService.getAPI().editProfile(saveLoginDetailsModel.getId()
                    , first_name, email_address, mobile,
                    gender, last_name,companyName,companyAddress,companyWebsite)
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<UpdateProfileResponse>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            disposables.add(d);
                        }

                        @Override
                        public void onNext(UpdateProfileResponse signUpResponse) {
                            if (yourAccountView != null) {
                                yourAccountView.hideProgressDialog();
                            }


                            if (signUpResponse.getResult().equalsIgnoreCase(Constants.SUCCESS)) {
                                yourAccountView.showMessageDialog(signUpResponse.getMessage());
                            } else {
                                yourAccountView.showErrorDialog(signUpResponse.getMessage());
                            }

                        }

                        @Override
                        public void onError(Throwable e) {
                            if (yourAccountView != null) {
                                yourAccountView.hideProgressDialog();
                                yourAccountView.showErrorDialog(((Context) yourAccountView)
                                        .getResources().getString(R.string.something_went_wrong));

                            } }

                        @Override
                        public void onComplete() {

                        }
                    });

         /*   RestClient.get(60).updateProfileService(saveLoginDetailsModel.getId()
                    , first_name, email_address, mobile,
                    gender, last_name,companyName,companyAddress,companyWebsite, new Callback<UpdateProfileResponse>() {
                        @Override
                        public void success(UpdateProfileResponse apiGlobal, retrofit.client.Response response) {
                            if (yourAccountView != null) {
                                yourAccountView.hideProgressDialog();
                            }


                            if (apiGlobal.getResult().equalsIgnoreCase(Constants.SUCCESS)) {
                                yourAccountView.showMessageDialog(apiGlobal.getMessage());
                            } else {
                                yourAccountView.showErrorDialog(apiGlobal.getMessage());
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            if (yourAccountView != null) {
                                yourAccountView.hideProgressDialog();
                                yourAccountView.showErrorDialog(((Context) yourAccountView)
                                        .getResources().getString(R.string.something_went_wrong));

                            }
                        }
                    });*/
        }

       /* RestClient.get(60).updateProfileService(saveLoginDetailsModel.getId()
                , first_name, email_address, mobile,
                gender, last_name,companyName,companyAddress,companyWebsite, new Callback<UpdateProfileResponse>() {
                    @Override
                    public void success(UpdateProfileResponse apiGlobal, retrofit.client.Response response) {
                        if (yourAccountView != null) {
                            yourAccountView.hideProgressDialog();
                        }


                        if (apiGlobal.getResult().equalsIgnoreCase(Constants.SUCCESS)) {
                            yourAccountView.showMessageDialog(apiGlobal.getMessage());
                        } else {
                            yourAccountView.showErrorDialog(apiGlobal.getMessage());
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if (yourAccountView != null) {
                            yourAccountView.hideProgressDialog();
                            yourAccountView.showErrorDialog(((Context) yourAccountView)
                                    .getResources().getString(R.string.something_went_wrong));

                        }
                    }
                });*/
    }


}
