package com.stagemarket.activity.yourAccount;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.joooonho.SelectableRoundedImageView;
import com.mlsdev.rximagepicker.RxImagePicker;
import com.mlsdev.rximagepicker.Sources;
import com.squareup.picasso.Picasso;
import com.stagemarket.BaseActivity;
import com.stagemarket.R;
import com.stagemarket.activity.addAddress.AddAddressActivity;
import com.stagemarket.activity.changePassword.ChangePasswordActivity;
import com.stagemarket.activity.editAdDetail.EditAdDetailActivity;
import com.stagemarket.activity.editAddress.EditAddressActivity;
import com.stagemarket.activity.signUp.SignUpActivity;
import com.stagemarket.adapter.AddressesAdapter;
import com.stagemarket.pojo.addressDetails.AddressesDetails;
import com.stagemarket.pojo.myProfile.MyProfileResponse;
import com.stagemarket.utils.CommonListeners;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.DialogPopUps;
import com.stagemarket.utils.SharedPreff;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;

public class YourAccountActivity extends BaseActivity implements YourAccountView, DatePickerDialog.OnDateSetListener {
    @BindView(R.id.et_website)
    EditText etWebsite;
    @BindView(R.id.et_companyname)
    EditText etCompanyname;
    @BindView(R.id.et_address_company)
    EditText etAddressCompany;
    @BindView(R.id.header)
    View header;
    @BindView(R.id.title_name)
    TextView titleName;
    @BindView(R.id.left_text)
    TextView leftText;
    @BindView(R.id.right_text)
    TextView rightText;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.img_header_back)
    ImageView imgHeaderBack;
    @BindView(R.id.img_profile_pic)
    ImageView imgProfilePic;
    @BindView(R.id.tv_change_password)
    TextView tvChangePassword;
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_lastname)
    EditText et_lastname;
    @BindView(R.id.et_email_id)
    EditText etEmailId;
    @BindView(R.id.f_name_lay)
    TextInputLayout f_name_lay;
    @BindView(R.id.l_name_lay)
    TextInputLayout l_name_lay;
    @BindView(R.id.web_lay)
    TextInputLayout web_lay;
    @BindView(R.id.et_mobile)
    EditText etMobile;
    @BindView(R.id.et_dob)
    EditText etDob;
    @BindView(R.id.lyt_profile)
    LinearLayout lytProfile;
    @BindView(R.id.tv_no_address_available)
    TextView tvNoAddressAvailable;
    @BindView(R.id.recyclerViewAddressLines)
    RecyclerView recyclerViewAddressLines;
    @BindView(R.id.gender)
    MaterialBetterSpinner gender;

    YourAccountPresenter yourAccountPresenter;
    DatePickerDialog datePickerDialog;
    AddressesAdapter mAddressesAdapter;
    ArrayList<AddressesDetails> mArrayListAddressesDetails = new ArrayList<>();
    String str_gender_type = Constants.MALE;
    private String myprofileData;
    String[] genderList;
    private int noimage = 1;
public static Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_account);
        context=YourAccountActivity.this;
        ButterKnife.bind(this);
//Toast.makeText(YourAccountActivity.this,""+ SharedPreff.getLoginDetailsModel().getOrganizationType(),Toast.LENGTH_SHORT).show();
        genderList = getResources().getStringArray(R.array.gender_category);
        myprofileData = getIntent().getStringExtra(Constants.MYPROFILE_DATA);
        yourAccountPresenter = new YourAccountPresenterImp(this);

        initToolbar();
        initViews();
        setReadioButtonsClick();
    }

    private void initViews() {
        if (myprofileData != null || !myprofileData.equalsIgnoreCase("")) {
            Gson gson = new Gson();
            Type type = new TypeToken<MyProfileResponse>() {
            }.getType();

            Log.e("profile response",myprofileData);
            MyProfileResponse myProfileResponse = gson.fromJson(myprofileData, type);
            if (myProfileResponse != null) {


                if(SharedPreff.getLoginDetailsModel().organizationType.equalsIgnoreCase("2"))
                {
                    et_lastname.setVisibility(View.GONE);
                    etName.setVisibility(View.GONE);
                    l_name_lay.setVisibility(View.GONE);
                    f_name_lay.setVisibility(View.GONE);
                    etCompanyname.setText(CommonUtils.capWordCase(myProfileResponse.getData().getCompanyName()));
                    etAddressCompany.setText(CommonUtils.capWordCase(myProfileResponse.getData().getCompanyAddress()));
                    etWebsite.setText(CommonUtils.capWordCase(myProfileResponse.getData().getCompanyWebsite()));
                }
                else
                {

                    etCompanyname.setVisibility(View.GONE);
                    etAddressCompany.setVisibility(View.GONE);
                    etWebsite.setVisibility(View.GONE);
                    web_lay.setVisibility(View.GONE);

                    etName.setText(CommonUtils.capWordCase(myProfileResponse.getData().getFirstName()));
                    et_lastname.setText(CommonUtils.capWordCase(myProfileResponse.getData().getLastName()));



                }




                etMobile.setText(myProfileResponse.getData().getMobile());
                etEmailId.setText(myProfileResponse.getData().getEmail());
                gender.setText(CommonUtils.capWordCase(myProfileResponse.getData().getGender()));
                etDob.setText(myProfileResponse.getData().getDob());
                etName.setSelection(etName.getText().toString().trim().length());
                etMobile.setSelection(etMobile.getText().toString().trim().length());
                etEmailId.setSelection(etEmailId.getText().toString().trim().length());
                gender.setSelection(gender.getText().toString().trim().length());
                etDob.setSelection(etDob.getText().toString().trim().length());

                if (!myProfileResponse.getData().getImageUrl().equalsIgnoreCase("")) {
                    noimage = 0;
                  //  CommonUtils.setImageSec(this, imgProfilePic, myProfileResponse.getData().getImageUrl());

                    Glide.with(this).load(Constants.IMAGE_BASE_URL + myProfileResponse.getData().getImageUrl())
                             .placeholder(R.drawable.dummy_profile)
                            .diskCacheStrategy(DiskCacheStrategy.ALL).into(imgProfilePic);



                  //  new GetBitmap(myProfileResponse.getData().getImageUrl()).execute();
                } else {
                    noimage = 1;
                  /*  Glide.with(this).load(Constants.IMAGE_BASE_URL + myProfileResponse.getData().getImageUrl())
                            .placeholder(R.drawable.dummy_profile)
                            .diskCacheStrategy(DiskCacheStrategy.ALL).into(imgProfilePic);
*/
                    imgProfilePic.setImageResource(R.drawable.dummy_profile);
                }
            }
        }
    }

    public class GetBitmap extends AsyncTask<Void, Void, Void> {
        String imageUrl;
        public GetBitmap(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Bitmap b = getBitmapFromURL(Constants.IMAGE_BASE_URL
                    + imageUrl);
if(b!=null) {

    // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
    Uri tempUri = getImageUri(getApplicationContext(), b);
    yourAccountPresenter.setBase64(tempUri);

}
            return null;
        }
    }

    public class GetBitmapDummy extends AsyncTask<Void, Void, Void> {
        String imageUrl;
        public GetBitmapDummy(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Bitmap b = getBitmapFromURL(imageUrl);
if(b!=null) {
    // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
    Uri tempUri = getImageUri(getApplicationContext(), b);
    yourAccountPresenter.setBase64(tempUri);
}return null;
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        String path = null;
      try {
            path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
      }
      catch (Exception e)
      {}
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @OnClick(R.id.img_profile_pic)
    public void OnClickEditPhoto() {
        openChoosePickerDialog();
    }

    private void openChoosePickerDialog() {
        DialogPopUps.openCamera(this, new CommonListeners.AlertCallBackWithButtonsInterface() {
            @Override
            public void positiveClick() {
                // Gallery
                openGallery();
            }


            @Override
            public void neutralClick() {

            }

            @Override
            public void negativeClick() {
                //Camera
                openCamera();
            }
        });
    }

    private void openCamera() {
        RxImagePicker.with(this).requestImage(Sources.CAMERA).subscribe(new Consumer<Uri>() {
            @Override
            public void accept(@NonNull Uri uri) throws Exception {

                UCrop.Options options = new UCrop.Options();
                options.setToolbarColor(ContextCompat.getColor(YourAccountActivity.this, R.color.colorPrimary));
                options.setStatusBarColor(ContextCompat.getColor(YourAccountActivity.this, R.color.colorPrimary));


                UCrop.of(uri, Uri.fromFile(new File(getCacheDir(), "temp_image" + System.currentTimeMillis() + ".jpeg")))
                        .withAspectRatio(1, 1)
                        .withMaxResultSize(700, 700)
                        .withOptions(options)
                        .start(YourAccountActivity.this);

            }
        });
    }


    private void openGallery() {
        RxImagePicker.with(YourAccountActivity.this).requestImage(Sources.GALLERY).subscribe(new Consumer<Uri>() {
            @Override
            public void accept(@NonNull Uri uri) throws Exception { //Get image by uri using one of image loading libraries. I use Glide in sample app.
                UCrop.Options options = new UCrop.Options();
                options.setToolbarColor(ContextCompat.getColor(YourAccountActivity.this, R.color.colorPrimary));
                options.setStatusBarColor(ContextCompat.getColor(YourAccountActivity.this, R.color.colorPrimary));
                UCrop.of(uri, Uri.fromFile(new File(getCacheDir(), "temp_image" + System.currentTimeMillis() + ".jpeg")))
                        .withAspectRatio(1, 1).withMaxResultSize(700, 700).withOptions(options).start((YourAccountActivity.this));
            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK
                && requestCode == UCrop.REQUEST_CROP) {
            Uri resultUri = UCrop.getOutput(data);

            final InputStream imageStream;
            try {
                imageStream = getContentResolver().openInputStream(resultUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                String encodedImage = CommonUtils.encodeImage(selectedImage);
                noimage = 0;
                yourAccountPresenter.setBase64(resultUri);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            if (resultUri != null) {
            /*    Picasso.with(YourAccountActivity.this)
                        .load(resultUri).centerCrop().resize(200, 200)
                        .into(imgProfilePic);*/
                Glide.with(YourAccountActivity.this)
                        .load(resultUri).diskCacheStrategy(DiskCacheStrategy.ALL).into(imgProfilePic);


                // createAccountViewPresnter.isProfilePicLoaded(true);
            }


        } else if (requestCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
            Log.d("uri", "error " + cropError);

        }
    }

    private void settingRecyclerView() {
        addData();
        mAddressesAdapter = new AddressesAdapter(this, mArrayListAddressesDetails);

        recyclerViewAddressLines.setLayoutManager(new LinearLayoutManager(this
                , LinearLayoutManager.VERTICAL, false));
        recyclerViewAddressLines.setItemAnimator(new DefaultItemAnimator());
        recyclerViewAddressLines.setAdapter(mAddressesAdapter);
    }

    private void addData() {
        mArrayListAddressesDetails.add(new AddressesDetails("Address 1", "Micheal",
                "Address Line 1", "Address Line 2", "Contact Details"));

        mArrayListAddressesDetails.add(new AddressesDetails("Address 2", "Micheal",
                "Address Line 1", "Address Line 2", "Contact Details"));


    }


    private void setReadioButtonsClick() {
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                View radioButton = radioGroup.findViewById(checkedId);
                int idx = radioGroup.indexOfChild(radioButton);
                rightText.setTextColor(ContextCompat.getColor(YourAccountActivity.this, R.color.black));
                if (idx == 0) {
                    rightText.setText(R.string.save);
                    lytProfile.setVisibility(View.VISIBLE);
                    recyclerViewAddressLines.setVisibility(View.GONE);
                    tvNoAddressAvailable.setVisibility(View.GONE);
                } else {
                    //  tvNoAddressAvailable.setVisibility(View.VISIBLE);
                    mArrayListAddressesDetails.clear();
                    lytProfile.setVisibility(View.GONE);
                    recyclerViewAddressLines.setVisibility(View.VISIBLE);
                    settingRecyclerView();

                    rightText.setText(R.string.add);
                }
            }
        });
    }

    private void initToolbar() {
        header.setBackgroundColor(ContextCompat.getColor(this, R.color.greynew));

        imgHeaderBack.setImageResource(R.drawable.ic_back);
        DrawableCompat.setTint(imgHeaderBack.getDrawable(), ContextCompat.getColor(this, R.color.black));
        imgHeaderBack.setVisibility(View.GONE);

        rightText.setVisibility(View.VISIBLE);
        rightText.setText(R.string.save);
        rightText.setTextColor(ContextCompat.getColor(YourAccountActivity.this, R.color.black));


        leftText.setVisibility(View.VISIBLE);
        leftText.setText(R.string.cancel);
        leftText.setTextColor(ContextCompat.getColor(YourAccountActivity.this, R.color.black));


        titleName.setText(R.string.your_account);
        titleName.setTextColor(ContextCompat.getColor(this, R.color.black));


        lytProfile.setVisibility(View.VISIBLE);
        recyclerViewAddressLines.setVisibility(View.GONE);
        tvNoAddressAvailable.setVisibility(View.GONE);


        // DatePicker
        datePickerDialog = new DatePickerDialog(YourAccountActivity.this, 0, YourAccountActivity.this, 0, 0, 0);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

        ArrayAdapter loanAdapter = new ArrayAdapter(this, R.layout.spinner, genderList);
        gender.setAdapter(loanAdapter);


        gender.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                str_gender_type = parent.getItemAtPosition(position).toString();

            }
        });


    }


    @OnClick(R.id.tv_change_password)
    public void OnClickChangePassword() {
        startActivity(new Intent(YourAccountActivity.this, ChangePasswordActivity.class));
    }


    @OnClick(R.id.et_dob)
    public void OnClickDateOfBirth() {
        datePickerDialog.show();

    }

    @OnClick(R.id.right_text)
    public void OnClickSave() {
        if (rightText.getText().toString().trim().equalsIgnoreCase(Constants.ADD)) {
            // Add
            startActivity(new Intent(YourAccountActivity.this, AddAddressActivity.class));
        } else {
            // Save
            /*if (noimage == 1) {
                CommonUtils.showTopSnackBar(this, findViewById(android.R.id.content), getString(R.string.upload_image));
            } else {*/
                yourAccountPresenter.onSaveEditProfileClick(etName.getText().toString().trim(), etEmailId.getText().toString().trim(),
                        etMobile.getText().toString().trim(), etDob.getText().toString().trim(), gender.getText().toString().trim(),
                        et_lastname.getText().toString().trim(),etCompanyname.getText().toString().trim(),etAddressCompany.getText().toString().trim(),etWebsite.getText().toString().trim() );
            /*}*/
        }
    }

    @OnClick(R.id.left_text)
    public void OnClickCancel() {
        finish();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        CommonUtils.hideSoftKeyboard(YourAccountActivity.this);
        return super.dispatchTouchEvent(ev);

    }

    @Override
    public void showProgressDialog() {
        DialogPopUps.showProgressDialog(this, getResources().getString(R.string.please_wait));

    }

    @Override
    public void showErrorDialog(String msg) {
        CommonUtils.showTopSnackBar(this, findViewById(android.R.id.content), msg);

    }

    @Override
    public void showMessageDialog(String msg) {
        DialogPopUps.alertPopUp(this, msg, getString(R.string.activation_required),
                getResources().getString(R.string.ok)
                , new CommonListeners.AlertCallBackInterface() {
                    @Override
                    public void neutralClick() {
                        finish();
                    }
                });

    }

    @Override
    public void hideProgressDialog() {
        DialogPopUps.hideDialog();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
        datePickerDialog.updateDate(year, (monthOfYear), dayOfMonth);
        etDob.setText(year + "-" + CommonUtils.getMonth(monthOfYear + 1) + "-" + (CommonUtils.getDate(dayOfMonth)));

    }


    @Override
    public void goToEditAddressScreen() {
        startActivity(new Intent(this, EditAddressActivity.class));
    }
}
