package com.stagemarket.activity.forgotPassword;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.stagemarket.R;
import com.stagemarket.activity.login.LoginActivity;
import com.stagemarket.utils.CommonListeners;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.DialogPopUps;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ForgotPasswordActivity extends AppCompatActivity implements ForgotPasswordView {

    @BindView(R.id.et_email_id)
    EditText etEmailId;

    ForgotPasswordPresenter mForgotPasswordPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);

        mForgotPasswordPresenter = new ForgotPasswordImpl(this);
    }

    @OnClick(R.id.img_header_back)
    public void onBackClick() {
        finish();
    }

    @OnClick(R.id.tv_sendResetLink)
    public void onResetClick() {
        mForgotPasswordPresenter.onResetClick(etEmailId.getText().toString().trim());
    }

    @Override
    public void showProgressDialog() {
        DialogPopUps.showProgressDialog(ForgotPasswordActivity.this, getResources().getString(R.string.please_wait));
    }


    @Override
    public void showErrorDialog(String msg) {
        CommonUtils.showTopSnackBar(this, findViewById(android.R.id.content), msg);
    }

    @Override
    public void showMessageDialog(String msg) {
        DialogPopUps.alertPopUp(ForgotPasswordActivity.this, msg,getString(R.string.alert),
                getResources().getString(R.string.ok)
                , new CommonListeners.AlertCallBackInterface() {
                    @Override
                    public void neutralClick() {
                        finish();
                    }
                });
    }

    @Override
    public void hideProgressDialog() {
        DialogPopUps.hideDialog();

    }



    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        CommonUtils.hideSoftKeyboard(ForgotPasswordActivity.this);
        return super.dispatchTouchEvent(ev);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
