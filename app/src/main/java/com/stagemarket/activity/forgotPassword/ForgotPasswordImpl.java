package com.stagemarket.activity.forgotPassword;

import android.content.Context;
import android.text.TextUtils;

import com.stagemarket.AppClass;
import com.stagemarket.R;
import com.stagemarket.pojo.SaveLoginDetailsModel;
import com.stagemarket.pojo.forgotPassword.ForgotPasswordResponse;
import com.stagemarket.pojo.social_login.SocialLoginResponse;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.DialogPopUps;
import com.stagemarket.utils.SharedPreff;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ForgotPasswordImpl implements ForgotPasswordPresenter {
    ForgotPasswordView forgotPasswordView;
    Context context;
    private NetWorkingService netWorkingService;
    private final CompositeDisposable disposables = new CompositeDisposable();

    public ForgotPasswordImpl(ForgotPasswordView forgotPasswordView) {
        this.forgotPasswordView = forgotPasswordView;
        netWorkingService = AppClass.getInstance().getNetworkService();
        context = AppClass.getInstance().getApplicationContext();
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        if (disposables != null) {
            disposables.clear();
        }
        forgotPasswordView = null;
        netWorkingService = null;
    }

    @Override
    public void onResetClick(String emailId) {
        if (TextUtils.isEmpty(emailId)) {
            forgotPasswordView.showErrorDialog(context.getString(R.string.email_address_shouldnot_be_empty));
        } else if (!CommonUtils.validateEmail(emailId)) {
            forgotPasswordView.showErrorDialog(context.getString(R.string.please_enter_valid_emailid));
        }else{
         // forgotPasswordView.showMessageDialog(context.getString(R.string.pwd_has_been_sent_to_your_mail));
            makeForgotApiRequest(emailId);
        }
    }

    private void makeForgotApiRequest(String email_id) {
        if (forgotPasswordView != null) {
            forgotPasswordView.showProgressDialog();
        }

        netWorkingService.getAPI().forgotPasswordService(email_id).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<ForgotPasswordResponse>() {


            @Override
            public void onSubscribe(Disposable d) {

                disposables.add(d);
            }

            @Override
            public void onNext(ForgotPasswordResponse forgotPasswordResponse) {


                if (CommonUtils.isConnectedToInternet((Context)forgotPasswordView )) {

                    if (forgotPasswordView != null) {
                        forgotPasswordView.hideProgressDialog();
                    }
                    if (forgotPasswordResponse.getResult().equalsIgnoreCase(Constants.SUCCESS)) {

                        forgotPasswordView.showMessageDialog(forgotPasswordResponse.getMessage());


                    } else {
                        forgotPasswordView.showErrorDialog(forgotPasswordResponse.getMessage());
                    }
                } else {
                    CommonUtils.check_your_network_available((Context) forgotPasswordView);
                }
            }

            @Override
            public void onError(Throwable e) {
                if (forgotPasswordView != null) {
                    forgotPasswordView.hideProgressDialog();
                    forgotPasswordView.showErrorDialog(((Context) forgotPasswordView).getResources().getString(R.string.something_went_wrong));

                }
            }

            @Override
            public void onComplete() {

            }
        });
    }
}
