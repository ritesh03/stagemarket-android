package com.stagemarket.activity.forgotPassword;

import com.stagemarket.CommonPresenter;

public interface ForgotPasswordPresenter  extends CommonPresenter {
    void onResetClick(String emailId);
}
