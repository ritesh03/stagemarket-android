package com.stagemarket.activity.editAddress;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.stagemarket.BaseActivity;
import com.stagemarket.R;
import com.stagemarket.activity.login.LoginActivity;
import com.stagemarket.activity.yourAccount.YourAccountActivity;
import com.stagemarket.utils.CommonUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditAddressActivity extends BaseActivity implements EditAddressView {


    @BindView(R.id.title_name)
    TextView titleName;
    @BindView(R.id.img_header_back)
    ImageView imgHeaderBack;
    @BindView(R.id.left_text)
    TextView leftText;
    @BindView(R.id.right_text)
    TextView rightText;
    @BindView(R.id.header)
    View header;
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_street_address)
    EditText etStreetAddress;
    @BindView(R.id.et_city)
    EditText etCity;
    @BindView(R.id.et_mobile)
    EditText etMobile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_address);
        ButterKnife.bind(this);
        initToolbar();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        CommonUtils.hideSoftKeyboard(EditAddressActivity.this);
        return super.dispatchTouchEvent(ev);

    }

    private void initToolbar() {
        header.setBackgroundColor(ContextCompat.getColor(this, R.color.greynew));

        imgHeaderBack.setImageResource(R.drawable.ic_back);
        DrawableCompat.setTint(imgHeaderBack.getDrawable(), ContextCompat.getColor(this, R.color.black));
        imgHeaderBack.setVisibility(View.GONE);

        rightText.setVisibility(View.VISIBLE);
        rightText.setText(R.string.save);
        rightText.setTextColor(ContextCompat.getColor(this, R.color.black));


        leftText.setVisibility(View.VISIBLE);
        leftText.setText(R.string.cancel);
        leftText.setTextColor(ContextCompat.getColor(this, R.color.black));


        titleName.setText(R.string.edit_address);
        titleName.setTextColor(ContextCompat.getColor(this, R.color.black));

    }

    @OnClick(R.id.right_text)
    public void OnClickSave() {

    }


    @OnClick(R.id.left_text)
    public void OnClickCancel() {
        EditAddressActivity.this.finish();

    }


    @Override
    public void showProgressDialog() {

    }

    @Override
    public void showErrorDialog(String msg) {

    }

    @Override
    public void showMessageDialog(String msg) {

    }

    @Override
    public void hideProgressDialog() {

    }
}
