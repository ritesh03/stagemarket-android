package com.stagemarket.activity;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bogdwellers.pinchtozoom.ImageMatrixTouchHandler;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stagemarket.R;
import com.stagemarket.adapter.TouchImageAdapter;
import com.stagemarket.adapter.ViewPagerAdapter;
import com.stagemarket.pojo.discover.ProductData;
import com.stagemarket.pojo.myPosts.ProductImage;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FullViewActivity extends AppCompatActivity {

    @BindView(R.id.view_pager)
    ViewPager extendedViewPager;
    private List<ProductImage> productsList = new ArrayList<>();
    private String productDetail;
    private int pos;
    TouchImageAdapter touchImageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_view);
        ButterKnife.bind(this);

        initViews();

        if (getIntent().getExtras() != null) {
            pos = getIntent().getIntExtra(Constants.POS,-1);
            productDetail = getIntent().getStringExtra(Constants.LIST_IMAGES);
            if (productDetail != null) {
                Gson gson = new Gson();
                Type type = new TypeToken<List<ProductImage>>() {
                }.getType();
                productsList.addAll((Collection<? extends ProductImage>) gson.fromJson(productDetail, type));
               touchImageAdapter.notifyDataSetChanged();
            }
        }


    }

    private void initViews() {
         touchImageAdapter = new TouchImageAdapter(this,productsList,getIntent().getStringExtra("type"));
        extendedViewPager.setAdapter(touchImageAdapter);
    }


}
