package com.stagemarket.activity.subCategories;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stagemarket.BaseActivity;
import com.stagemarket.R;
import com.stagemarket.activity.categories.CategoriesActivity;
import com.stagemarket.adapter.SubCategoriesAdapter;
import com.stagemarket.adapter.SubCategoriesDataAdapter;
import com.stagemarket.pojo.SubCatPojo;
import com.stagemarket.pojo.category.CategoriesPojo;
import com.stagemarket.pojo.category.SubCatData;
import com.stagemarket.pojo.categorySell.Datum;
import com.stagemarket.pojo.categorySell.SubCategory;
import com.stagemarket.utils.Constants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SubCategoriesActivity extends BaseActivity implements SubCategoriesView {

    @BindView(R.id.header)
    View header;
    @BindView(R.id.title_name)
    TextView titleName;
    @BindView(R.id.img_header_back)
    ImageView imgHeaderBack;
    @BindView(R.id.img_slider)
    ImageView imgSlider;
    @BindView(R.id.recyclerViewSubCategories)
    RecyclerView recyclerViewSubCategories;
    @BindView(R.id.recyclerViewSubCatValue)
    RecyclerView recyclerViewSubCatValue;
    @BindView(R.id.tv_catName)
    TextView tv_catName;
    @BindView(R.id.tv_noRecordFound)
    TextView tv_noRecordFound;
    ArrayList<SubCatData> data = new ArrayList<>();
    SubCategoriesAdapter mSubCategoriesAdapter;
    SubCategoriesDataAdapter subCategoriesDataAdapter;

    ArrayList<CategoriesPojo> mArrayListSubCat = new ArrayList<>();
    private String catid = "";
    private String subCatValues = "";
    private ArrayList<SubCategory> mArrayListCategory = new ArrayList<>();
    ArrayList<Datum> mArrayListCatPojo = new ArrayList<>();
    private String catName = "";
    private String catValues = "";
    List<Datum> categories = new ArrayList<>();
    private int pos = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_categories);
        ButterKnife.bind(this);

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        catid = getIntent().getStringExtra(Constants.CAT_POS);
        subCatValues = getIntent().getStringExtra(Constants.SUB_CAT_VALUES);
        catName = getIntent().getStringExtra(Constants.CAT_NAME);
        pos = getIntent().getIntExtra(Constants.POS, -1);


        Gson gson = new Gson();
        Type type = new TypeToken<List<SubCategory>>() {
        }.getType();
        List<SubCategory> subCategories = gson.fromJson(subCatValues, type);
        mArrayListCategory.addAll(subCategories);


        catValues = getIntent().getStringExtra(Constants.CAT_VALUES);
        Gson gson1 = new Gson();
        Type type1 = new TypeToken<List<Datum>>() {
        }.getType();
        categories = gson1.fromJson(catValues, type1);
        mArrayListCatPojo.addAll(categories);


        initSubCatData();
        //  initListData();
        initToolbar();
        initRecyclerView();
        imgHeaderBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SubCategoriesActivity.this, CategoriesActivity.class));
                finish();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onSubCAtSelect(SubCatPojo subCategory) {

        mArrayListCategory.clear();
        SubCategory subCategory1 = new SubCategory();
        subCategory1.setId("0");
        subCategory1.setSelected(false);
        subCategory1.setSubCatName("All");
        mArrayListCategory.add(0,subCategory1);
        mArrayListCategory.addAll(categories.get(subCategory.getPosition()).getSubCategories());
        if(subCategoriesDataAdapter!=null) {
            subCategoriesDataAdapter.notifyDataSetChanged();
        }
        if(mArrayListCategory.size()!=0){
            tv_noRecordFound.setVisibility(View.GONE);
            recyclerViewSubCatValue.setVisibility(View.VISIBLE);
        }else{
            tv_noRecordFound.setVisibility(View.VISIBLE);
            recyclerViewSubCatValue.setVisibility(View.GONE);
        }
    }

    private void initListData() {
        data.add(new SubCatData("Guitars", false));
        data.add(new SubCatData("Pedals", false));
        data.add(new SubCatData("Amplifiers", false));
        data.add(new SubCatData("Keyboards", false));
        data.add(new SubCatData("Trumpet", false));
        data.add(new SubCatData("Violin", false));
        data.add(new SubCatData("Drums", false));

    }


    @OnClick(R.id.img_header_back)
    public void onBackClick() {
        finish();
    }

    private void initRecyclerView() {

        mSubCategoriesAdapter = new SubCategoriesAdapter(this, mArrayListCatPojo);
        recyclerViewSubCategories.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL, false));
        mSubCategoriesAdapter.setSelectedCategory(Integer.parseInt(catid));
        recyclerViewSubCategories.setAdapter(mSubCategoriesAdapter);
        if (pos != -1) {
            recyclerViewSubCategories.scrollToPosition(pos);

            EventBus.getDefault().post(new SubCatPojo(pos,
                    categories.get(pos).getName(),categories.get(pos).getId()));
        }

        subCategoriesDataAdapter = new SubCategoriesDataAdapter(mArrayListCategory, this);
        recyclerViewSubCatValue.setLayoutManager(new LinearLayoutManager(this
                , LinearLayoutManager.VERTICAL, false));
        recyclerViewSubCatValue.setAdapter(subCategoriesDataAdapter);

        if(mArrayListCategory.size()!=0){
            tv_noRecordFound.setVisibility(View.GONE);
            recyclerViewSubCatValue.setVisibility(View.VISIBLE);
        }else{
            tv_noRecordFound.setVisibility(View.VISIBLE);
            recyclerViewSubCatValue.setVisibility(View.GONE);
        }

    }

    private void initSubCatData() {
        mArrayListSubCat.add(new CategoriesPojo("Musical Instruments", 3, R.drawable.subcat_one));
        mArrayListSubCat.add(new CategoriesPojo("Studio Equipments", 2, R.drawable.subcat_two));
        mArrayListSubCat.add(new CategoriesPojo("PA Systems", 1, R.drawable.subcat_three));
        mArrayListSubCat.add(new CategoriesPojo("Home Sound", 4, R.drawable.subcat_four));

    }

    private void initToolbar() {
       /* for (int i = 0; i < mArrayListSubCat.size(); i++) {
            if (mArrayListSubCat.get(i).getId() == catid) {
                EventBus.getDefault().post(mArrayListSubCat.get(i));
                tv_catName.setText(mArrayListSubCat.get(i).getName());
            }
        }*/

        EventBus.getDefault().post(new CategoriesPojo(catName, 1, 1));
        tv_catName.setText(catName);

        header.setBackgroundColor(ContextCompat.getColor(this, R.color.greynew));
        imgHeaderBack.setImageResource(R.drawable.ic_back);
        DrawableCompat.setTint(imgHeaderBack.getDrawable(), ContextCompat.getColor(this, R.color.black));

        titleName.setText(R.string.select_sub_cat);
        titleName.setTextColor(ContextCompat.getColor(this, R.color.black));
    }

    @Override
    public void showProgressDialog() {

    }

    @Override
    public void showErrorDialog(String msg) {

    }

    @Override
    public void showMessageDialog(String msg) {

    }

    @Override
    public void hideProgressDialog() {

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void setCateText(String val) {
        tv_catName.setText(val);
    }
}
