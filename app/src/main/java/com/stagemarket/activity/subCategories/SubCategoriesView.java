package com.stagemarket.activity.subCategories;

import com.stagemarket.BasePresenter;

public interface SubCategoriesView extends BasePresenter {
    void setCateText(String val);
}
