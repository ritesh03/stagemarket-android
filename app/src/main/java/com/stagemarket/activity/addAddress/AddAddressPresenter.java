package com.stagemarket.activity.addAddress;

import com.stagemarket.CommonPresenter;

public interface AddAddressPresenter extends CommonPresenter{
    void checkValidations(String name, String stree_address, String city, String mobile_number);
}
