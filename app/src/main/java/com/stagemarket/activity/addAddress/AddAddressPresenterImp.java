package com.stagemarket.activity.addAddress;

import android.content.Context;
import android.text.TextUtils;

import com.stagemarket.AppClass;
import com.stagemarket.R;
import com.stagemarket.pojo.SaveLoginDetailsModel;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.SharedPreff;

import io.reactivex.disposables.CompositeDisposable;

public class AddAddressPresenterImp implements AddAddressPresenter {
    AddAddressView addAddressView;
    SaveLoginDetailsModel saveLoginDetailsModel;
    Context context;
    private NetWorkingService netWorkingService;
    private final CompositeDisposable disposables = new CompositeDisposable();

    public AddAddressPresenterImp(AddAddressView addAddressView) {
        this.addAddressView = addAddressView;
        saveLoginDetailsModel = SharedPreff.getLoginDetailsModel();
        context = AppClass.getInstance().getApplicationContext();
        netWorkingService = AppClass.getInstance().getNetworkService();
    }


    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        if (disposables != null) {
            disposables.clear();
        }
        addAddressView = null;
        netWorkingService = null;
    }


    @Override
    public void checkValidations(String name, String stree_address, String city, String mobile_number) {
        if (TextUtils.isEmpty(name)) {
            addAddressView.showErrorDialog(context.getString(R.string.name_shouldnot_be_empty));
        } else if (TextUtils.isEmpty(stree_address)) {
            addAddressView.showErrorDialog(context.getString(R.string.street_address_shouldnot_be_empty));
        } else if (TextUtils.isEmpty(city)) {
            addAddressView.showErrorDialog(context.getString(R.string.city_shouldnot_be_empty));
        } else if (TextUtils.isEmpty(mobile_number)) {
            addAddressView.showErrorDialog(context.getString(R.string.mobile_number_shouldnot_be_empty));
        } /*else if (mobile_number.length() < 10) {
            addAddressView.showErrorDialog(context.getString(R.string.mobile_number_shouldnot_be_less_than_10));
        } */
        else {
            addAddressView.setResult(name, stree_address, city, mobile_number);
        }
    }
}
