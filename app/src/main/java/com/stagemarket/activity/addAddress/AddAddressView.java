package com.stagemarket.activity.addAddress;

import com.stagemarket.BasePresenter;

public interface AddAddressView extends BasePresenter {
    void setResult(String name, String street_address, String city, String mobile_number);
}
