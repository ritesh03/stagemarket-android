package com.stagemarket.activity.addAddress;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.stagemarket.AppClass;
import com.stagemarket.BaseActivity;
import com.stagemarket.R;
import com.stagemarket.pojo.addressDetails.AddAddressResponse;
import com.stagemarket.pojo.addressDetails.GetAddressResponse;
import com.stagemarket.pojo.myProfile.MyProfileResponse;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.DialogPopUps;
import com.stagemarket.utils.SharedPreff;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class AddAddressActivity extends BaseActivity implements AddAddressView {

    private NetWorkingService netWorkingService;
    private final CompositeDisposable disposables = new CompositeDisposable();
    @BindView(R.id.title_name)
    TextView titleName;
    @BindView(R.id.img_header_back)
    ImageView imgHeaderBack;
    @BindView(R.id.left_text)
    TextView leftText;
    @BindView(R.id.address_placeholder)
    TextView address_placeholder;
    @BindView(R.id.right_text)
    TextView rightText;
    @BindView(R.id.edit_btn)
    TextView edit_btn;
    @BindView(R.id.saved_address)
    TextView saved_address;
    @BindView(R.id.header)
    View header;
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_street_address)
    EditText etStreetAddress;
    @BindView(R.id.et_city)
    EditText etCity;
    @BindView(R.id.et_mobile)
    EditText etMobile;
    @BindView(R.id.cancel_btn)
    Button cancel_btn;
    @BindView(R.id.save_btn)
    Button save_btn;
    @BindView(R.id.bottom_lay)
    LinearLayout bottom_lay;
    @BindView(R.id.form_lay)
    LinearLayout form_lay;

    AddAddressPresenter mAddAddressPresenter;
String mName,mAddress,mCity,mMobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);
        ButterKnife.bind(this);
        mAddAddressPresenter = new AddAddressPresenterImp(this);
        initToolbar();
        netWorkingService = AppClass.getInstance().getNetworkService();
        getAddressApi();
        saved_address=findViewById(R.id.saved_address);
        edit_btn=findViewById(R.id.edit_btn);
        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addAddressApi();
            }
        });
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottom_lay.setVisibility(View.GONE);
                rightText.setVisibility(View.VISIBLE);
                etName.setText("");
                etCity.setText("");
                etMobile.setText("");
                etStreetAddress.setText("");
            }
        });
        edit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottom_lay.setVisibility(View.VISIBLE);
                rightText.setVisibility(View.GONE);


                etName.setText(mName);
                etCity.setText(mCity);
                etMobile.setText(mMobile);
                etStreetAddress.setText(mAddress);

            }
        });
        saved_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent returnIntent = new Intent();
                if (returnIntent != null) {

                    returnIntent.putExtra(Constants.NAME, mName);
                    returnIntent.putExtra("type", " ");
                    returnIntent.putExtra(Constants.STREET_ADDRESS,mAddress);
                    returnIntent.putExtra(Constants.CITY, mCity);
                    returnIntent.putExtra(Constants.MOBILE_NUMBER, mMobile);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                } else {
                    setResult(Activity.RESULT_CANCELED, returnIntent);
                    finish();
                }
            }
        });
    }


    private void initToolbar() {
        header.setBackgroundColor(ContextCompat.getColor(this, R.color.greynew));

        imgHeaderBack.setImageResource(R.drawable.ic_back);
        DrawableCompat.setTint(imgHeaderBack.getDrawable(), ContextCompat.getColor(this, R.color.black));
        imgHeaderBack.setVisibility(View.GONE);

        rightText.setVisibility(View.VISIBLE);
        rightText.setText(R.string.add);
        rightText.setTextColor(ContextCompat.getColor(this, R.color.black));


        leftText.setVisibility(View.VISIBLE);
        leftText.setText(R.string.cancel);
        leftText.setTextColor(ContextCompat.getColor(this, R.color.black));


        titleName.setText("Select Address");
        titleName.setTextColor(ContextCompat.getColor(this, R.color.black));

    }


    @OnClick(R.id.right_text)
    public void OnClickAdd() {
       /*  mAddAddressPresenter.checkValidations(etName.getText().toString().trim(), etStreetAddress.getText().toString().trim()
                , etCity.getText().toString().trim(), etMobile.getText().toString().trim());*/

if(rightText.getText().toString().equalsIgnoreCase("Edit"))
{
    bottom_lay.setVisibility(View.VISIBLE);
   // rightText.setVisibility(View.GONE);
form_lay.setVisibility(View.VISIBLE);

    etName.setText(mName);
    etCity.setText(mCity);
    etMobile.setText(mMobile);
    etStreetAddress.setText(mAddress);
    rightText.setText("Save");
}
else {
    addAddressApi();
}   }




    @OnClick(R.id.left_text)
    public void OnClickCancel() {
        finish();
    }


    @Override
    public void showProgressDialog() {
        DialogPopUps.showProgressDialog(this, getResources().getString(R.string.please_wait));

    }

    @Override
    public void showErrorDialog(String msg) {
        CommonUtils.showTopSnackBar(this, findViewById(android.R.id.content), msg);

    }

    @Override
    public void showMessageDialog(String msg) {

    }

    @Override
    public void hideProgressDialog() {
        DialogPopUps.hideDialog();

    }

    @Override
    public void setResult(String name, String street_address, String city, String mobile_number) {
        Intent returnIntent = new Intent();
        if (returnIntent != null) {

            returnIntent.putExtra(Constants.NAME, name);
            returnIntent.putExtra(Constants.STREET_ADDRESS, street_address);
            returnIntent.putExtra(Constants.CITY, city);
            returnIntent.putExtra(Constants.MOBILE_NUMBER, mobile_number);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        } else {
            setResult(Activity.RESULT_CANCELED, returnIntent);
            finish();
        }

    }

    public void getAddressApi()
    {
        if (CommonUtils.isConnectedToInternet(AddAddressActivity.this)) {

         //   myProfileView.showProgressDialog();


            netWorkingService.getAPI().getAddress(
                    SharedPreff.getLoginDetailsModel().getId())
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<GetAddressResponse>() {


                        @Override
                        public void onSubscribe(Disposable d) {
                            disposables.add(d);
                        }

                        @Override
                        public void onNext(GetAddressResponse myProfileResponse) {
                           // myProfileView.hideProgressDialog();


                                if (myProfileResponse.result.equalsIgnoreCase(Constants.SUCCESS)) {
                                    saved_address.setVisibility(View.VISIBLE);
                                    edit_btn.setVisibility(View.GONE);
mName=myProfileResponse.data.get(0).billingName;
mCity=myProfileResponse.data.get(0).billingCity;
mMobile=myProfileResponse.data.get(0).billingContact;
mAddress=myProfileResponse.data.get(0).billingddress;
saved_address.setText(myProfileResponse.data.get(0).billingName+"\n"+myProfileResponse.data.get(0).billingddress+", "+myProfileResponse.data.get(0).billingCity+"\n"+myProfileResponse.data.get(0).billingContact);


                                    form_lay.setVisibility(View.GONE);
                                    address_placeholder.setVisibility(View.GONE);
                                    rightText.setText("Edit");
                                } else {

                                    saved_address.setVisibility(View.GONE);
                                    edit_btn.setVisibility(View.GONE);
                                    bottom_lay.setVisibility(View.GONE);
form_lay.setVisibility(View.VISIBLE);
                                    address_placeholder.setVisibility(View.VISIBLE);

                                     //showErrorDialog(myProfileResponse.message);
                                }


                        }

                        @Override
                        public void onError(Throwable e) {
                           // hideProgressDialog();
                            showErrorDialog(AddAddressActivity.this.getResources().getString(R.string.something_went_wrong));
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        } else {
            CommonUtils.showTopSnackBar(AddAddressActivity.this,
                    findViewById(android.R.id.content),  getString(R.string.please_check_internet));
        }
    }
    public void addAddressApi()
    {
        if (CommonUtils.isConnectedToInternet(AddAddressActivity.this)) {
            if (TextUtils.isEmpty(etName.getText().toString().trim())) {
                showErrorDialog( getString(R.string.name_shouldnot_be_empty));
            } else if (TextUtils.isEmpty( etStreetAddress.getText().toString().trim())) {
                 showErrorDialog( getString(R.string.street_address_shouldnot_be_empty));
            } else if (TextUtils.isEmpty(etCity.getText().toString().trim())) {
                 showErrorDialog( getString(R.string.city_shouldnot_be_empty));
            } else if (TextUtils.isEmpty(etMobile.getText().toString().trim())) {
                 showErrorDialog( getString(R.string.mobile_number_shouldnot_be_empty));
            } /*else if (mobile_number.length() < 10) {
            addAddressView.showErrorDialog(context.getString(R.string.mobile_number_shouldnot_be_less_than_10));
        } */
            else {
         //   myProfileView.showProgressDialog();
showProgressDialog();

            netWorkingService.getAPI().addAddress(
                    SharedPreff.getLoginDetailsModel().getId(),etName.getText().toString().trim(),etMobile.getText().toString().trim(), etStreetAddress.getText().toString().trim()
                    , etCity.getText().toString().trim())
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<AddAddressResponse>() {


                        @Override
                        public void onSubscribe(Disposable d) {
                            disposables.add(d);
                        }

                        @Override
                        public void onNext(AddAddressResponse myProfileResponse) {
                          hideProgressDialog();


                                if (myProfileResponse.result.equalsIgnoreCase(Constants.SUCCESS)) {
                                    saved_address.setVisibility(View.VISIBLE);
                                    edit_btn.setVisibility(View.GONE);
                                    bottom_lay.setVisibility(View.VISIBLE);


                                    Intent returnIntent = new Intent();
                                    if (returnIntent != null) {

                                        returnIntent.putExtra(Constants.NAME, myProfileResponse.data .billingName);
                                        returnIntent.putExtra("type", "");
                                        returnIntent.putExtra(Constants.STREET_ADDRESS, myProfileResponse.data .billingddress);
                                        returnIntent.putExtra(Constants.CITY, myProfileResponse.data .billingCity);
                                        returnIntent.putExtra(Constants.MOBILE_NUMBER, myProfileResponse.data .billingContact);
                                        setResult(Activity.RESULT_OK, returnIntent);
                                        finish();
                                    } else {
                                        setResult(Activity.RESULT_CANCELED, returnIntent);
                                        finish();
                                    }
//saved_address.setText(myProfileResponse.data.get(0).billingName+"\n"+myProfileResponse.data.get(0).billingddress+", "+myProfileResponse.data.get(0).billingCity);
                                } else {

                                    saved_address.setVisibility(View.GONE);
                                    edit_btn.setVisibility(View.GONE);
                                    bottom_lay.setVisibility(View.GONE);

                                     //showErrorDialog(myProfileResponse.message);
                                }


                        }

                        @Override
                        public void onError(Throwable e) {
                             hideProgressDialog();
                            showErrorDialog(AddAddressActivity.this.getResources().getString(R.string.something_went_wrong));
                        }

                        @Override
                        public void onComplete() {

                        }
                    });}
        } else {
            CommonUtils.showTopSnackBar(AddAddressActivity.this,
                    findViewById(android.R.id.content),  getString(R.string.please_check_internet));
        }
    }

}
