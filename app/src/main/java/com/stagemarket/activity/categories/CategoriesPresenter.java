package com.stagemarket.activity.categories;

import com.stagemarket.CommonPresenter;

public interface CategoriesPresenter extends CommonPresenter {
    void fetchCategoriesList();

}
