package com.stagemarket.activity.categories;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.location.Address;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.stagemarket.AppClass;
import com.stagemarket.BaseActivity;
import com.stagemarket.R;
import com.stagemarket.activity.sell.categorySell.category_sell.CategorySellActivity;
import com.stagemarket.adapter.CategoriesAdapter;
import com.stagemarket.pojo.category.CategoriesPojo;
import com.stagemarket.pojo.categorySell.Datum;
import com.stagemarket.pojo.discover.LatLongBean;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.DialogPopUps;
import com.stagemarket.utils.SharedPreff;
import com.stagemarket.utils.SpacesItemDecoration;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CategoriesActivity extends BaseActivity implements CategoriesView {

    @BindView(R.id.title_name)
    TextView titleName;
    @BindView(R.id.img_header_back)
    ImageView imgHeaderBack;
    @BindView(R.id.rl_header)
    RelativeLayout rlHeader;
    @BindView(R.id.tv_locationName)
    TextView tvLocationName;
    @BindView(R.id.tv_change)
    TextView tvChange;
    @BindView(R.id.tv_locationNameSec)
    TextView tvLocationNameSec;
    @BindView(R.id.imgLocation)
    ImageView imgLocation;
    @BindView(R.id.recyclerViewCategories)
    RecyclerView recyclerViewCategories;
    @BindView(R.id.header)
    View header;
    public static int DASHBOARD_RESULT = 50;
    NetWorkingService netWorkingService;

    ArrayList<CategoriesPojo> mArrayListCatPojo = new ArrayList<>();
    CategoriesAdapter categoriesAdapter;
    CategoriesPresenter categoriesPresenter;
    ArrayList<Datum> getCategoriesResponseData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        ButterKnife.bind(this);
        netWorkingService = AppClass.getInstance().getNetworkService();
        categoriesPresenter = new CategoriesPresenterImpl(this, netWorkingService);
        categoriesPresenter.fetchCategoriesList();
        initToolbar();
        initViews();
        initRecyclerView();
    }

    private void initToolbar() {
        header.setBackgroundColor(ContextCompat.getColor(this, R.color.greynew));
        imgHeaderBack.setImageResource(R.drawable.ic_back);
        DrawableCompat.setTint(imgHeaderBack.getDrawable(), ContextCompat.getColor(this, R.color.black));
        titleName.setText(R.string.categories);
        titleName.setTextColor(ContextCompat.getColor(this, R.color.black));
    }


    int PLACE_PICKER_REQUEST = 1;

    @OnClick(R.id.tv_change)
    public void onChangeLocationClick() {
        try {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);
                getLocationAddress(place.getLatLng().latitude, place.getLatLng().longitude, false);
                /*SharedPreff.saveLng(String.valueOf(place.getLatLng().longitude));
                SharedPreff.saveLat(String.valueOf(place.getLatLng().latitude));
*/
                SharedPreff.saveTempLat(String.valueOf(place.getLatLng().latitude));
                SharedPreff.saveTempLng(String.valueOf(place.getLatLng().longitude));
                EventBus.getDefault().post(new LatLongBean(place.getLatLng().latitude, place.getLatLng().longitude));

                finish();
                //  String toastMsg = String.format("Place: %s", place.getName());
                // Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
               /* if (place != null) {
                    gettingAddressFromLatLong(true, "" + place.getLatLng().latitude, "" + place.getLatLng().longitude);
                }*/
            }
        } else if (requestCode == DASHBOARD_RESULT) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    finish();
                }
            }
        }
    }

    @OnClick(R.id.imgLocation)
    public void onCurrentLocation() {

      /*  double lat = Double.parseDouble(SharedPreff.getLat());
        double lng = Double.parseDouble(SharedPreff.getLng());
        SharedPreff.saveTempLat(String.valueOf(lat));
        SharedPreff.saveTempLng(String.valueOf(lng));
        EventBus.getDefault().post(new LatLongBean(lat, lng));

        Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                finish();
            }
        };
        handler.postDelayed(runnable,500);
        getLocationAddress(lat, lng, false);*/
    }

    @OnClick(R.id.tv_locationNameSec)
    public void onCurrentLocationLabel() {

      /*  double lat = Double.parseDouble(SharedPreff.getLat());
        double lng = Double.parseDouble(SharedPreff.getLng());
        SharedPreff.saveTempLat(String.valueOf(lat));
        SharedPreff.saveTempLng(String.valueOf(lng));
        EventBus.getDefault().post(new LatLongBean(lat, lng));
        Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                finish();
            }
        };
        handler.postDelayed(runnable,500);
        getLocationAddress(lat, lng, false);*/
    }

    @OnClick(R.id.img_header_back)
    public void onBackClick() {
        finish();
    }

    private void initViews() {
       /* double lat = Double.parseDouble(SharedPreff.getLat());
        double lng = Double.parseDouble(SharedPreff.getLng());

        getLocationAddress(lat, lng, true);
        if (!SharedPreff.getLatTemp().equalsIgnoreCase("")) {
            double latTemp = Double.parseDouble(SharedPreff.getLatTemp());
            double lngTemp = Double.parseDouble(SharedPreff.getLngTemp());
            getLocationAddress(latTemp, lngTemp, false);
        }*/

    }

    private void getLocationAddress(double lat, double lng, boolean isCurrent) {
        Address address = CommonUtils.getAddressObjectFromLocation(lat, lng, this);
      /*  StringBuilder sb = new StringBuilder();
        sb.append(address.getThoroughfare()).append("\n");
        sb.append(address.getSubAdminArea()).append(",");
        sb.append(address.getCountryName());
        String result = sb.toString();
      */
        StringBuilder sb = new StringBuilder();
        if (address.getAddressLine(0) != null)
            sb.append(address.getAddressLine(0)).append("\n");
      /*  if (address.getSubLocality() != null) {
            sb.append(address.getSubLocality()).append(",");
        } else {
            sb.append(address.getLocality()).append(",");
        }
        if (address.getSubAdminArea() != null) {
            sb.append(address.getSubAdminArea()).append("\n");
        } else {
            sb.append(address.getAdminArea()).append(",");
        }
        sb.append(address.getPostalCode()).append("\n");
        sb.append(address.getCountryName());
      */
        tvLocationName.setText(sb.toString());
        if (isCurrent) {
            tvLocationNameSec.setText(sb.toString());
        }


    }

    private void initRecyclerView() {
        //initListData();
        categoriesAdapter = new CategoriesAdapter(this, getCategoriesResponseData);
        recyclerViewCategories.setLayoutManager(new GridLayoutManager(this, 2));
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen._1sdp);

        recyclerViewCategories.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
        recyclerViewCategories.setAdapter(categoriesAdapter);
    }

    private void initListData() {
        mArrayListCatPojo.add(new CategoriesPojo("All Categories", 1, R.drawable.cat_one));
        mArrayListCatPojo.add(new CategoriesPojo("Studio Equipments", 2, R.drawable.cat_two));
        mArrayListCatPojo.add(new CategoriesPojo("Musical Instruments", 3, R.drawable.cat_three));

        mArrayListCatPojo.add(new CategoriesPojo("Home Sound", 4, R.drawable.cat_four));
    }


    @Override
    public void showProgressDialog() {
        DialogPopUps.showProgressDialog(CategoriesActivity.this, getResources().getString(R.string.please_wait));

    }

    @Override
    public void showErrorDialog(String msg) {
        CommonUtils.showTopSnackBar(CategoriesActivity.this, findViewById(android.R.id.content), msg);

    }

    @Override
    public void showMessageDialog(String msg) {

    }

    @Override
    public void hideProgressDialog() {
        DialogPopUps.hideDialog();
    }


    @Override
    public void getCategoriesResponse(ArrayList<Datum> getCategoriesResponseData) {

        Datum datum = new Datum();
        datum.setName("All Categories");
        datum.setId("-1");
        getCategoriesResponseData.add(0, datum);
        this.getCategoriesResponseData = getCategoriesResponseData;
        categoriesAdapter.addingCategoriesData(getCategoriesResponseData);

    }

}
