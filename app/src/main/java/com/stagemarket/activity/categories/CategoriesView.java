package com.stagemarket.activity.categories;

import android.widget.BaseAdapter;

import com.stagemarket.BasePresenter;
import com.stagemarket.pojo.categorySell.Datum;

import java.util.ArrayList;

public interface CategoriesView extends BasePresenter {
    void getCategoriesResponse(ArrayList<Datum> data);
}
