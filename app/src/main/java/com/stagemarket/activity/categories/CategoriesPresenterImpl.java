package com.stagemarket.activity.categories;

import android.app.Activity;
import android.content.Context;

import com.stagemarket.AppClass;
import com.stagemarket.R;
import com.stagemarket.activity.home.HomeActivity;
import com.stagemarket.pojo.categorySell.GetCategoriesResponse;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CategoriesPresenterImpl implements CategoriesPresenter {
    
    CategoriesView categoriesView = null;
    private NetWorkingService netWorkingService;
    private final CompositeDisposable disposables = new CompositeDisposable();

    public CategoriesPresenterImpl(CategoriesView categoriesView,NetWorkingService netWorkingService) {
        this.categoriesView = categoriesView;
        this.netWorkingService = netWorkingService;
    }

    @Override
    public void fetchCategoriesList() {
        if (CommonUtils.isConnectedToInternet(((Context) categoriesView))) {

            if (categoriesView != null) {
                categoriesView.showProgressDialog();
            }

            netWorkingService.getAPI().getCategoriesService().subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<GetCategoriesResponse>() {


                @Override
                public void onSubscribe(Disposable d) {
                    disposables.add(d);
                }

                @Override
                public void onNext(GetCategoriesResponse getCategoriesResponse) {

                    if (CommonUtils.isConnectedToInternet((Context) categoriesView)) {

                        if (categoriesView != null) {
                            categoriesView.hideProgressDialog();
                        }
                        if (getCategoriesResponse.getResult().equalsIgnoreCase(Constants.SUCCESS)) {

                            categoriesView.getCategoriesResponse(getCategoriesResponse.getData());

                        } else {
                            categoriesView.showErrorDialog(getCategoriesResponse.getMessage());
                        }
                    } else {
                        CommonUtils.check_your_network_available((Context) categoriesView);
                    }
                }

                @Override
                public void onError(Throwable e) {
                    if (categoriesView != null) {
                        categoriesView.hideProgressDialog();
                        categoriesView.showErrorDialog(((Context) categoriesView).getResources().getString(R.string.something_went_wrong));

                    }
                }

                @Override
                public void onComplete() {

                }
            });


        } else {
          /*  CommonUtils.showTopSnackBar((Context) categoriesView,
                    ((Activity) AppClass.getInstance().getApplicationContext()).findViewById(android.R.id.content), ((Context) categoriesView).getString(R.string.please_check_internet));
*/

            CommonUtils.showTopSnackBar(((CategoriesActivity) categoriesView),
                    ((CategoriesActivity) categoriesView).findViewById(android.R.id.content),
                    ((CategoriesActivity) categoriesView).getString(R.string.please_check_internet));
        }
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        if (disposables != null) {
            disposables.clear();
        }
        categoriesView = null;
        netWorkingService = null;
    }
}
