package com.stagemarket.adapter;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.stagemarket.AppClass;
import com.stagemarket.R;
import com.stagemarket.activity.myPostsDetails.MyPostsDetailsActivity;
import com.stagemarket.fragment.myposts.MyPosts;
import com.stagemarket.fragment.myposts.MyPostsView;
import com.stagemarket.pojo.myPosts.Datum;
import com.stagemarket.pojo.myPosts.MyPostsPojo;
import com.stagemarket.pojo.myProfile.ViewProfileSelling;
import com.stagemarket.utils.CommonListeners;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class MyPostsAdapter extends RecyclerView.Adapter {

    private MyPostsView myPostsView;
    ArrayList<Datum> mArrayListMyPostsPojo;
    RecyclerView recyclerView;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private int visibleThreshold = 1;
    private boolean isLoading;
    private int lastVisibleItem, totalItemCount;
    private CommonListeners.OnLoadMoreListener onLoadMoreListener;
    CallBackLoadMore callBackLoadMore;
    Context mContext;
    FragmentActivity activity;
    int type;
    public MyPostsAdapter(MyPostsView myPostsView, RecyclerView recyclerView
            , ArrayList<Datum> mArrayListMyPostsPojo, FragmentActivity activity,
                          CallBackLoadMore callBackLoadMore, int type) {
        mContext = AppClass.getInstance().getApplicationContext();
        this.myPostsView = myPostsView;
        this.mArrayListMyPostsPojo = mArrayListMyPostsPojo;
        this.recyclerView = recyclerView;
        this.activity = activity;
        this.callBackLoadMore = callBackLoadMore;
        this.type = type;
    }


    public void addingMyPostsData(ArrayList<Datum> my_posts_list) {
        this.mArrayListMyPostsPojo = my_posts_list;
        notifyDataSetChanged();
    }

    public void addingByPagination(ArrayList<Datum> my_posts_list) {
        this.mArrayListMyPostsPojo.addAll(my_posts_list);
        notifyDataSetChanged();
    }


    public void setOnLoadMoreListener(CommonListeners.OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_myposts, parent, false);
//        return new MyViewHolder(view);

        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_item_myposts, parent, false);
            return new MyViewHolder(view);
        } /*else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }*/
        return null;


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MyViewHolder) {
            bindingListValues(holder, position);
        }

        if (position == mArrayListMyPostsPojo.size() - 1) {
            callBackLoadMore.onBottomReach();
        }
    }


    private void bindingListValues(RecyclerView.ViewHolder myViewHolder, final int position) {
        MyPostsAdapter.MyViewHolder myViewHolder1 = (MyPostsAdapter.MyViewHolder) myViewHolder;

        final Datum myPostsList = mArrayListMyPostsPojo.get(position);

        myViewHolder1.tv_name.setText(myPostsList.getName());
        myViewHolder1.tv_price.setText(Constants.DOLlAR + " " + myPostsList.getCost());
     /*   float priceval =   15 * Float.parseFloat(String.valueOf(myPostsList.getCost())) /100 ;
        float newPriceval = priceval+ Float.parseFloat(String.valueOf(myPostsList.getCost()));
        myViewHolder1.tv_price.setText(Constants.DOLlAR + " " + new DecimalFormat("##.##").format(newPriceval));*/
        if (myPostsList.getProductImages().size() > 0) {
            CommonUtils.setImage(mContext, myViewHolder1.imgProduct,
                    myPostsList.getProductImages().get(0).getImage());
        }else{
            // make sure Glide doesn't load anything into this view until told otherwise
            Glide.clear(myViewHolder1.imgProduct);
            // remove the placeholder (optional); read comments below
            myViewHolder1.imgProduct.setImageResource(R.drawable.not_available);
        }


        myViewHolder1.lytMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // if (myPostsList.getProductImages().size() != 0) {

                Log.e("data",myPostsList.getName());

                    mContext.startActivity(new Intent(mContext, MyPostsDetailsActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            .putExtra(Constants.PROD_DETAIL, new Gson().toJson(myPostsList).toString()
                            ).putExtra(Constants.MY_POSTS_POSTION, position)
                    .putExtra(Constants.MY_ADS_TYPE,type));
              /*  } else {
                    CommonUtils.showTopSnackBar(mContext, (activity).findViewById(android.R.id.content)
                            , mContext.getString(R.string.no_product_found));
                }*/
            }
        });
    }


    @Override
    public int getItemCount() {
        return mArrayListMyPostsPojo == null ? 0 : mArrayListMyPostsPojo.size();
    }

    public void setType(int type) {
        this.type = type;
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_name, tv_price;
        LinearLayout lytMain;
        ImageView imgProduct;
        ProgressBar pb_myposts;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_price = itemView.findViewById(R.id.tv_price);
            lytMain = itemView.findViewById(R.id.lytMain);
            imgProduct = itemView.findViewById(R.id.imgProduct);
            pb_myposts = itemView.findViewById(R.id.pb_myposts);
        }


    }

    @Override
    public int getItemViewType(int position) {
        return mArrayListMyPostsPojo.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    public void setLoaded() {
        isLoading = false;
    }

    public class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
        }
    }


    public interface CallBackLoadMore {
        void onBottomReach();
    }
}
