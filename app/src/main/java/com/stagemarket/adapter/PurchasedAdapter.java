package com.stagemarket.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.stagemarket.AppClass;
import com.stagemarket.R;
import com.stagemarket.activity.login.LoginActivity;
import com.stagemarket.activity.myOrders.MyOrdersActivity;
import com.stagemarket.activity.myOrders.MyOrdersView;
import com.stagemarket.activity.myOrders.purchased_order_details.PurchasedOrderDetailsActivity;
import com.stagemarket.activity.product_detail.ProductDetailsActivity;
import com.stagemarket.activity.viewProfile.ViewProfileActivity;
import com.stagemarket.pojo.discover.ProductData;
import com.stagemarket.pojo.myOrders.purchased.Datum;
import com.stagemarket.pojo.wishlist.AddWishlistResponse;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.CommonListeners;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.DialogPopUps;
import com.stagemarket.utils.SharedPreff;

import java.text.DecimalFormat;
import java.util.ArrayList;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class PurchasedAdapter extends RecyclerView.Adapter {

    private MyOrdersView myOrdersView = null;
    ArrayList<Datum> mArrayList_myOrders = new ArrayList<>();
    ArrayList<ProductData> mArrayList_myWishlist = new ArrayList<>();
    CallBackBottomReach callBackBottomReach;
    String type;
    private final CompositeDisposable disposables = new CompositeDisposable();
    ProductData productDataModel;
    Context mContext;
    TextView tv_no_orders;
    private NetWorkingService netWorkingService;
    public PurchasedAdapter(Context context,MyOrdersView myOrdersView, ArrayList<Datum> mArrayList_myOrders,
                            CallBackBottomReach callBackBottomReach) {
        this.myOrdersView = myOrdersView;
        this.callBackBottomReach = callBackBottomReach;
        this.mArrayList_myOrders = mArrayList_myOrders;
        type="orders";
        mContext=context;

        netWorkingService = AppClass.getInstance().getNetworkService();
    }
    public PurchasedAdapter(Context context,MyOrdersView myOrdersView, ArrayList<ProductData> mArrayList_myOrders,
                            CallBackBottomReach callBackBottomReach, TextView tv_no_orders) {
        this.myOrdersView = myOrdersView;
        this.callBackBottomReach = callBackBottomReach;
        this.mArrayList_myWishlist = mArrayList_myOrders;
        type="wishlist";
        mContext=context;
        this.tv_no_orders=tv_no_orders;
        netWorkingService = AppClass.getInstance().getNetworkService();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_purchased_my_orders, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        MyViewHolder myViewHolder = (MyViewHolder) holder;


        if(type.equalsIgnoreCase("orders")) {
            Datum myOrdersPojo = mArrayList_myOrders.get(position);

            myViewHolder.tv_order_id.setText("Order ID " + mArrayList_myOrders.get(position).getId());
            myViewHolder.tv_purchased_product_name.setText(mArrayList_myOrders.get(position).getName());

/*
            float vvv = Float.parseFloat(String.valueOf(mArrayList_myOrders.get(position).getCost())) * 15;
            float vv = vvv / 100;
            float v = Float.parseFloat(String.valueOf(mArrayList_myOrders.get(position).getCost())) + vv;

            float total = Float.parseFloat(String.valueOf(mArrayList_myOrders.get(position).getVat())) + v;
       */


            float pCost=Float.parseFloat(String.valueOf(mArrayList_myOrders.get(position).getCost()))*Integer.parseInt(mArrayList_myOrders.get(position).getQuantity());
            float vvv=pCost*15;
            float vv=vvv/100;
            float v= pCost+ vv;

            float total=Float.parseFloat(String.valueOf(mArrayList_myOrders.get(position).getVat()))+v;
          //  myViewHolder.tv_purchased_product_price.setText(Constants.DOLlAR + String.valueOf(new DecimalFormat(".##").format(total)));
            myViewHolder.tv_purchased_product_price.setText(Constants.DOLlAR +  String.valueOf(mArrayList_myOrders.get(position).getBillingAmount()));

           // myViewHolder.tv_purchased_product_price.setText(Constants.DOLlAR + String.valueOf(new DecimalFormat(".##").format(total)));
            //  myViewHolder.tv_purchased_delivered_status.setText(CommonUtils.DateTimeFormatChange(mArrayList_myOrders.get(position).getDeliveredDate()));
            try {
                myViewHolder.tv_purchased_delivered_status.setText(CommonUtils.getMyDateFormat(CommonUtils.convertUTCtoLocalTime(mArrayList_myOrders.get(position).getDeliveredDate()), "yyyy-MM-dd hh:mm:ss", "hh:mm a"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            //  myViewHolder.tv_purchased_product_timings.setText(mArrayList_myOrders.get(position).getProduct_timimgs());

            //   myViewHolder.img_purchased_product.setImageResource(R.drawable.guitar);
            if (mArrayList_myOrders.get(position)
                    .getProductImages().size() != 0) {
                CommonUtils.setImage((Context) myOrdersView, myViewHolder.img_purchased_product,
                        mArrayList_myOrders.get(position)
                                .getProductImages().get(0).getImage());

            } else {
                // make sure Glide doesn't load anything into this view until told otherwise
                Glide.clear(myViewHolder.img_purchased_product);
                // remove the placeholder (optional); read comments below
                myViewHolder.img_purchased_product.setImageResource(R.drawable.noimage);
            }

            if (position == mArrayList_myOrders.size() - 1) {
                callBackBottomReach.onBottomReach();
            }
        }

        else
        {
            myViewHolder.wishlist_btn.setVisibility(View.VISIBLE);

              productDataModel = mArrayList_myWishlist.get(position);
//            myViewHolder.tv_order_id.setText("Order ID " + mArrayList_myOrders.get(position).getId());
            myViewHolder.tv_order_id.setVisibility(View.GONE);
            myViewHolder.tv_purchased_product_name.setText(mArrayList_myWishlist.get(position).getName());


            float vvv = Float.parseFloat(String.valueOf(mArrayList_myWishlist.get(position).getCost())) * 15;
            float vv = vvv / 100;
            float v = Float.parseFloat(String.valueOf(mArrayList_myWishlist.get(position).getCost())) + vv;
            float c_vvv = v * 11;
            float c_vv = c_vvv / 100;
            float total = v + c_vv;

          //  float total = Float.parseFloat(String.valueOf(mArrayList_myWishlist.get(position).getVat())) + v;
            myViewHolder.tv_purchased_product_price.setText(Constants.DOLlAR + String.valueOf(new DecimalFormat(".##").format(v)));
            //  myViewHolder.tv_purchased_delivered_status.setText(CommonUtils.DateTimeFormatChange(mArrayList_myOrders.get(position).getDeliveredDate()));
            try {
               // myViewHolder.tv_purchased_delivered_status.setText(CommonUtils.getMyDateFormat(CommonUtils.convertUTCtoLocalTime(mArrayList_myOrders.get(position).getDeliveredDate()), "yyyy-MM-dd hh:mm:ss", "hh:mm a"));
                myViewHolder.tv_purchased_delivered_status.setVisibility(View.GONE);
            } catch (Exception e) {
                e.printStackTrace();
            }
            //  myViewHolder.tv_purchased_product_timings.setText(mArrayList_myOrders.get(position).getProduct_timimgs());

            //   myViewHolder.img_purchased_product.setImageResource(R.drawable.guitar);
            if (mArrayList_myWishlist.get(position)
                    .getmProductImages().size() != 0) {
                CommonUtils.setImage((Context) myOrdersView, myViewHolder.img_purchased_product,
                        mArrayList_myWishlist.get(position)
                                .getmProductImages().get(0).getImage());

            } else {
                // make sure Glide doesn't load anything into this view until told otherwise
                Glide.clear(myViewHolder.img_purchased_product);
                // remove the placeholder (optional); read comments below
                myViewHolder.img_purchased_product.setImageResource(R.drawable.noimage);
            }

            if (position == mArrayList_myWishlist.size() - 1) {
                callBackBottomReach.onBottomReach();
            }
        }

        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(type.equalsIgnoreCase("orders")) {
                    ((Context) myOrdersView).startActivity
                            (new Intent((Context) myOrdersView, PurchasedOrderDetailsActivity.class)
                                    .putExtra(Constants.ORDER_DETAIL, new Gson().toJson(mArrayList_myOrders.get(position))));
                }
                else
                {
                    ((Context) myOrdersView).startActivity(new Intent(((Context) myOrdersView), ProductDetailsActivity.class)
                            .putExtra(Constants.PROD_DETAIL, new Gson().toJson(mArrayList_myWishlist.get(position)).toString()));
                }

            }
        });
        myViewHolder.wishlist_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                DialogPopUps.showAlertWithButtons(mContext, "Alert", "Are you sure you want to remove this product?", "Yes", "No",
                        "", false, false, new CommonListeners.AlertCallBackWithButtonsInterface() {
                            @Override
                            public void positiveClick() {
                                // Toast.makeText(getActivity(),"Yes",Toast.LENGTH_SHORT).show();
                                addRemoveWishlist(String.valueOf(mArrayList_myWishlist.get(position).getId()),position);
                            }

                            @Override
                            public void neutralClick() {

                            }

                            @Override
                            public void negativeClick() {
                                //     Toast.makeText(getActivity(),"No",Toast.LENGTH_SHORT).show();

                            }
                        });

            }
        });
    }

    public interface CallBackBottomReach{
        void onBottomReach();
    }

    @Override
    public int getItemCount() {

        if(type.equalsIgnoreCase("orders"))
        {
        return mArrayList_myOrders.size();}
        else
        {
            return mArrayList_myWishlist.size();
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_order_id, tv_purchased_product_name, tv_purchased_product_price,
                tv_purchased_delivered_status, tv_purchased_product_timings,wishlist_btn;
        ImageView img_purchased_product;

        public MyViewHolder(View itemView) {
            super(itemView);

            tv_order_id = itemView.findViewById(R.id.tv_order_id);
            tv_purchased_product_name = itemView.findViewById(R.id.tv_purchased_product_name);
            tv_purchased_product_price = itemView.findViewById(R.id.tv_purchased_product_price);
            tv_purchased_delivered_status = itemView.findViewById(R.id.tv_purchased_delivered_status);

            img_purchased_product = itemView.findViewById(R.id.img_purchased_product);
            wishlist_btn = itemView.findViewById(R.id.wishlist_btn);



        }
    }
    public void addRemoveWishlist(String productId, final int pos)
    {
        if (CommonUtils.isConnectedToInternet(mContext)) {


            netWorkingService.getAPI().addToWishlist(SharedPreff.getLoginDetailsModel().getId(),productId,"2")
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<AddWishlistResponse>() {


                        @Override
                        public void onSubscribe(Disposable d) {
                            disposables.add(d);
                        }

                        @Override
                        public void onNext(AddWishlistResponse deleteProductResponse) {




                            if (deleteProductResponse.result.equalsIgnoreCase(Constants.SUCCESS)) {

                                if(deleteProductResponse.data.status==1)
                                {
                                   // wishlist_btn.setBackgroundResource(R.drawable.wish_filled);
                                }
                                else
                                {
                                  //  wishlist_btn.setBackgroundResource(R.drawable.wish_blank);
                                    mArrayList_myWishlist.remove(pos);
                                    if (mArrayList_myWishlist.size()==0)
                                    {tv_no_orders.setVisibility(View.VISIBLE);
                                        tv_no_orders.setText("No Wishlist");
                                    }
                                    notifyDataSetChanged();
                                }

                                //  myPostsDetailsView.onDeleteProductResponse(deleteProductResponse);
                                //  myPostsDetailsView.showMessageDialog(deleteProductResponse.getMessage());

                            } else {
                                // myPostsDetailsView.showErrorDialog(deleteProductResponse.getMessage());
                            }


                        }

                        @Override
                        public void onError(Throwable e) {
                            //   myPostsDetailsView.hideProgressDialog();
                            //   myPostsDetailsView.showErrorDialog(mContext.getResources().getString(R.string.something_went_wrong));
                        }

                        @Override
                        public void onComplete() {

                        }
                    });


        } else {
            CommonUtils.showTopSnackBar((Context) myOrdersView,
                    ((MyOrdersActivity) mContext).findViewById(android.R.id.content), mContext.getString(R.string.please_check_internet));

        }
    }

}
