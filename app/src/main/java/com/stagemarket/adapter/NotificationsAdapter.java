package com.stagemarket.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.stagemarket.R;
import com.stagemarket.fragment.notifications.Notifications;
import com.stagemarket.fragment.notifications.NotificationsView;
import com.stagemarket.pojo.categorySell.Datum;
import com.stagemarket.pojo.categorySell.GetCategoriesResponse;
import com.stagemarket.pojo.discover.DiscoverBean;
import com.stagemarket.pojo.notifications.GetNotificationResponse;
import com.stagemarket.pojo.notifications.NotificationData;
import com.stagemarket.pojo.notifications.NotificationsPojo;

import java.util.ArrayList;


public class NotificationsAdapter extends RecyclerView.Adapter {

    private NotificationsView notificationsView;
    private ArrayList<NotificationData> mArrayListNotificationsPojo = new ArrayList<>();

    public NotificationsAdapter(NotificationsView notificationsView, ArrayList<NotificationData> mArrayListNotificationsPojo) {
        this.notificationsView = notificationsView;
        this.mArrayListNotificationsPojo = mArrayListNotificationsPojo;
    }

    public void addingNotificationData(ArrayList<NotificationData> notificationDataArrayList) {
        this.mArrayListNotificationsPojo.addAll(notificationDataArrayList);
        notifyDataSetChanged();
    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_notifications, parent, false);
        return new MyViewHolder(view);    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        NotificationsAdapter.MyViewHolder myViewHolder = (NotificationsAdapter.MyViewHolder) holder;
        NotificationData notificationData = mArrayListNotificationsPojo.get(position);

        if(position == 0){
            myViewHolder.empty_view.setVisibility(View.GONE);
        }else {
            myViewHolder.empty_view.setVisibility(View.VISIBLE);
        }

        myViewHolder.notification_title.setText(notificationData.getMessage());
    }

    @Override
    public int getItemCount() {
        return mArrayListNotificationsPojo.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView notification_title;
        View empty_view;

        public MyViewHolder(View itemView) {
            super(itemView);
            notification_title = itemView.findViewById(R.id.notification_title);
            empty_view = itemView.findViewById(R.id.empty_view);
        }
    }
}
