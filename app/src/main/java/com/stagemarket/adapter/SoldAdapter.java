package com.stagemarket.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.stagemarket.R;
import com.stagemarket.activity.myOrders.MyOrdersView;
import com.stagemarket.pojo.myOrders.purchased.Datum;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;

import java.util.ArrayList;

public class SoldAdapter extends RecyclerView.Adapter {


    private MyOrdersView myOrdersView = null;
    ArrayList<Datum> mArrayList_myOrders = new ArrayList<>();
    CallBackBottomReach callBackBottomReach;
    public SoldAdapter(MyOrdersView myOrdersView, ArrayList<Datum> mArrayList_myOrders,
                       CallBackBottomReach callBackBottomReach) {
        this.myOrdersView = myOrdersView;
        this.callBackBottomReach = callBackBottomReach;
        this.mArrayList_myOrders = mArrayList_myOrders;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_sold_my_orders, parent, false);
        return new MyViewHolder(view);    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MyViewHolder myViewHolder = (MyViewHolder) holder;
        Datum myOrdersPojo = mArrayList_myOrders.get(position);

        myViewHolder.tv_order_id.setText("Order ID "+ mArrayList_myOrders.get(position).getId());
        myViewHolder.tv_sold_product_name.setText(mArrayList_myOrders.get(position).getName());
        myViewHolder.tv_sold_product_price.setText(Constants.DOLlAR+mArrayList_myOrders.get(position).getCost());
      //  myViewHolder.tv_sold_delivered_status.setText(CommonUtils.DateTimeFormatChange(mArrayList_myOrders.get(position).getDeliveredDate()));
        try {
            myViewHolder.tv_sold_product_price.setText(CommonUtils.getMyDateFormat(CommonUtils.convertUTCtoLocalTime(mArrayList_myOrders.get(position).getDeliveredDate()),"yyyy-MM-dd hh:mm:ss","hh:mm a"));
        } catch (Exception e) {
            e.printStackTrace();
        } //  myViewHolder.tv_sold_product_timings.setText(mArrayList_myOrders.get(position).getProduct_timimgs());

        if(  mArrayList_myOrders.get(position)
                .getProductImages().size()!=0){
            CommonUtils.setImage((Context) myOrdersView, myViewHolder.img_sold_product,
                    mArrayList_myOrders.get(position)
                            .getProductImages().get(0).getImage());

        }else{
            // make sure Glide doesn't load anything into this view until told otherwise
            Glide.clear(myViewHolder.img_sold_product);
            // remove the placeholder (optional); read comments below
            myViewHolder.img_sold_product.setImageResource(R.drawable.noimage);
        }
        if(position == mArrayList_myOrders.size()-1){
            callBackBottomReach.onBottomReachSold();
        }
    }

    public interface CallBackBottomReach{
        void onBottomReachSold();
    }

    @Override
    public int getItemCount() {
        return mArrayList_myOrders.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_order_id, tv_sold_product_name,tv_sold_product_price,
                tv_sold_delivered_status,tv_sold_product_timings;
        ImageView img_sold_product;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_order_id = itemView.findViewById(R.id.tv_order_id);
            tv_sold_product_name = itemView.findViewById(R.id.tv_sold_product_name);
            tv_sold_product_price = itemView.findViewById(R.id.tv_sold_product_price);
            tv_sold_delivered_status = itemView.findViewById(R.id.tv_sold_delivered_status);
            tv_sold_product_timings = itemView.findViewById(R.id.tv_sold_product_timings);

            img_sold_product = itemView.findViewById(R.id.img_sold_product);

        }
    }
    
    
}
