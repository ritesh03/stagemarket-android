package com.stagemarket.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.stagemarket.AppClass;
import com.stagemarket.R;
import com.stagemarket.activity.home.HomeActivity;
import com.stagemarket.activity.myPostsDetails.MyPostsDetailsActivity;
import com.stagemarket.activity.product_detail.ProductDetailsActivity;
import com.stagemarket.activity.viewProfile.ViewProfileActivity;
import com.stagemarket.pojo.deleteProduct.DeleteProductResponse;
import com.stagemarket.pojo.discover.DiscoverBean;
import com.stagemarket.pojo.discover.ProductData;
import com.stagemarket.pojo.myPosts.Datum;
import com.stagemarket.pojo.wishlist.AddWishlistResponse;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.CommonListeners;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.SharedPreff;

import java.util.ArrayList;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DiscoverAdapter extends RecyclerView.Adapter {

    ArrayList<DiscoverBean> mArrayListDiscover;
    Context mContext;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private int visibleThreshold = 1;
    private boolean isLoading;
    private int lastVisibleItem, totalItemCount;
    private CommonListeners.OnLoadMoreListener onLoadMoreListener;

    RecyclerView recyclerView;
    ArrayList<ProductData> mArrayListProductData;
    private FragmentActivity activity;
    private NetWorkingService netWorkingService;
    private final CompositeDisposable disposables = new CompositeDisposable();
    CallBackLoadMore callBackLoadMore;

    public DiscoverAdapter(Context mContext, RecyclerView recyclerView, ArrayList<ProductData> mArrayListProductData,
                           FragmentActivity activity, CallBackLoadMore callBackLoadMore) {
        this.mArrayListProductData = mArrayListProductData;
        this.mContext = mContext;
        this.recyclerView = recyclerView;
        this.activity = activity;
        this.callBackLoadMore = callBackLoadMore;
        netWorkingService = AppClass.getInstance().getNetworkService();
        final GridLayoutManager gridLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
             /*   totalItemCount = gridLayoutManager.getItemCount();
                lastVisibleItem = gridLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }*/
            }
        });


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_home, parent, false);
//        return new MyViewHolder(view);

        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_item_home, parent, false);
            return new MyViewHolder(view);
        } /*else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }*/
        return null;

    }

    public void addingMyPostsData(ArrayList<ProductData> my_product_list) {
        this.mArrayListProductData.addAll(my_product_list);
        notifyDataSetChanged();
    }

    public void addingByPagination(ArrayList<ProductData> my_product_list) {
        this.mArrayListProductData.addAll(my_product_list);
        notifyDataSetChanged();
    }


    public void setOnLoadMoreListener(CommonListeners.OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof MyViewHolder) {
            bindingListValues(holder, position);

        }/* else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }*/

        if (position == mArrayListProductData.size() - 1) {
           // Toast.makeText(mContext, "reach", Toast.LENGTH_SHORT).show();
            callBackLoadMore.onBottomReach();
        }


    }

    private void bindingListValues(RecyclerView.ViewHolder holder, final int position) {
        final MyViewHolder myViewHolder = (MyViewHolder) holder;
        final ProductData productDataModel = mArrayListProductData.get(position);
        myViewHolder.tv_name.setText(productDataModel.getName());

        if(productDataModel.getProductType()==1) {
            myViewHolder.product_type.setText("New");
        }
        else
        {
            myViewHolder.product_type.setText("Used");
        }

        float priceval =  ((Integer.parseInt( productDataModel.getCommision()) * Float.parseFloat(String.valueOf(productDataModel.getCost())))/100);
        float newPriceval = priceval+ Float.parseFloat(String.valueOf(productDataModel.getCost()));
        myViewHolder.tv_price.setText(productDataModel.getCurrency() + " " + String.format("%.2f",newPriceval));

//        if (productDataModel.getmProductImages().size() > 0) {
//            CommonUtils.setImage(mContext, myViewHolder.imgProduct,
//                    productDataModel.getmProductImages().get(0).getImage());
//        }
if(productDataModel.getWishlistStatus().equalsIgnoreCase("1"))
{
    myViewHolder.wishlist_btn.setBackgroundResource(R.drawable.wish_filled);
}
else
{
    myViewHolder.wishlist_btn.setBackgroundResource(R.drawable.wish_blank);

}
        if (productDataModel.getmProductImages().size() != 0) {
            CommonUtils.setImage(mContext, myViewHolder.imgProduct,
                    productDataModel.getmProductImages().get(0).getImage());
        }else{
            // make sure Glide doesn't load anything into this view until told otherwise
            Glide.clear(myViewHolder.imgProduct);
            // remove the placeholder (optional); read comments below
            myViewHolder.imgProduct.setImageResource(R.drawable.not_available);
        }
myViewHolder.wishlist_btn.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Log.e("click","click");
addRemoveWishlist(String.valueOf(productDataModel.getId()),myViewHolder.wishlist_btn,position);
    }
});
        myViewHolder.lytMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  if (productDataModel.getmProductImages().size() != 0) {
                    mContext.startActivity(new Intent(mContext, ProductDetailsActivity.class)
                            .putExtra(Constants.PROD_DETAIL, new Gson().toJson(productDataModel).toString()));

               /* } else {
                    CommonUtils.showTopSnackBar(mContext, (activity).findViewById(android.R.id.content)
                            , mContext.getString(R.string.no_product_found));
                }*/
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayListProductData == null ? 0 : mArrayListProductData.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mArrayListProductData.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    public void setLoaded() {
        isLoading = false;
    }

    public class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
        }
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_name, tv_price,product_type;
        LinearLayout lytMain;
        ImageView imgProduct,wishlist_btn;

        public MyViewHolder(View itemView) {
            super(itemView);
            product_type = itemView.findViewById(R.id.product_type);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_price = itemView.findViewById(R.id.tv_price);
            lytMain = itemView.findViewById(R.id.lytMain);
            imgProduct = itemView.findViewById(R.id.imgProduct);
            wishlist_btn = itemView.findViewById(R.id.wishlist_btn);
        }
    }
public void addRemoveWishlist(String productId, final ImageView wishlist_btn, final int pos)
{
    if (CommonUtils.isConnectedToInternet(mContext)) {


        netWorkingService.getAPI().addToWishlist(SharedPreff.getLoginDetailsModel().getId(),productId,"1")
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<AddWishlistResponse>() {


                    @Override
                    public void onSubscribe(Disposable d) {
                        disposables.add(d);
                    }

                    @Override
                    public void onNext(AddWishlistResponse deleteProductResponse) {




                        if (deleteProductResponse.result.equalsIgnoreCase(Constants.SUCCESS)) {

if(deleteProductResponse.data.status==1)
{
    wishlist_btn.setBackgroundResource(R.drawable.wish_filled);
    mArrayListProductData.get(pos).setWishlistStatus("1");
    Toast.makeText(mContext, "Item added in wishlist", Toast.LENGTH_SHORT).show();
}
else
{
    mArrayListProductData.get(pos).setWishlistStatus("0");
    wishlist_btn.setBackgroundResource(R.drawable.wish_blank);
    Toast.makeText(mContext, "Item removed from wishlist", Toast.LENGTH_SHORT).show();
}

                            //  myPostsDetailsView.onDeleteProductResponse(deleteProductResponse);
                            //  myPostsDetailsView.showMessageDialog(deleteProductResponse.getMessage());

                        } else {
                            // myPostsDetailsView.showErrorDialog(deleteProductResponse.getMessage());
                        }


                    }

                    @Override
                    public void onError(Throwable e) {
                        //   myPostsDetailsView.hideProgressDialog();
                        //   myPostsDetailsView.showErrorDialog(mContext.getResources().getString(R.string.something_went_wrong));
                    }

                    @Override
                    public void onComplete() {

                    }
                });


    } else {
        CommonUtils.showTopSnackBar(mContext,
                ((HomeActivity) mContext).findViewById(android.R.id.content), mContext.getString(R.string.please_check_internet));

    }
}
    public interface CallBackLoadMore {
        void onBottomReach();
    }
}
