package com.stagemarket.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.stagemarket.R;
import com.stagemarket.activity.sell.categorySell.category_sell.CategorySellVIew;
import com.stagemarket.activity.sell.categorySell.select_category_sell.SelectCategorySellActivity;
import com.stagemarket.pojo.categorySell.Datum;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.SharedPreff;

import java.util.ArrayList;

public class CategorySellAdapter extends RecyclerView.Adapter {

    CategorySellVIew categorySellVIew = null;
    ArrayList<Datum> mArraListCategorySellPojos = new ArrayList<>();


    public CategorySellAdapter(CategorySellVIew categorySellVIew, ArrayList<Datum> mArrayListCategorySell) {
        this.categorySellVIew = categorySellVIew;
        this.mArraListCategorySellPojos = mArrayListCategorySell;
    }


    public void addingCategoriesSellData(ArrayList<Datum> getCategoriesResponseData) {
        this.mArraListCategorySellPojos = getCategoriesResponseData;
        notifyDataSetChanged();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_categories, parent
                , false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MyViewHolder myViewHolder = (MyViewHolder) holder;
        myViewHolder.tv_categoryName.setText(mArraListCategorySellPojos.get(position).getName());
        //  myViewHolder.tv_categoryName.setPaintFlags(myViewHolder.tv_categoryName.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        //  myViewHolder.img_category.setImageResource(mArraListCategorySellPojos.get(position).getImage());

        //myViewHolder.img_category.

        if (mArraListCategorySellPojos.get(position).getId().equalsIgnoreCase("-1")) {
            myViewHolder.img_category.setImageResource(R.drawable.allcat);
        } else {
            CommonUtils.setImage((Context) categorySellVIew, myViewHolder.img_category,
                    mArraListCategorySellPojos.get(position).getImage());
        }

    }

    @Override
    public int getItemCount() {
        return mArraListCategorySellPojos.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_categoryName;
        ImageView img_category;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_categoryName = itemView.findViewById(R.id.tv_categoryName);
            img_category = itemView.findViewById(R.id.img_category);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

               if(mArraListCategorySellPojos.get(getAdapterPosition()).getId().equalsIgnoreCase("-1")){

               }else{
                   SharedPreff.saveCatName(mArraListCategorySellPojos.get(getAdapterPosition()));
                   Intent intent = new Intent(((Context) categorySellVIew), SelectCategorySellActivity.class);
                   intent.putExtra(Constants.CAT_NAME, mArraListCategorySellPojos.get(getAdapterPosition()).getName());
                   intent.putExtra(Constants.SUB_CAT_VALUES,new Gson().toJson(mArraListCategorySellPojos.get(getAdapterPosition())
                           .getSubCategories()));
                   ((Context) categorySellVIew).startActivity(intent);
               }

                }
            });
        }
    }


}
