package com.stagemarket.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.stagemarket.R;
import com.stagemarket.activity.subCategories.SubCategoriesActivity;
import com.stagemarket.fragment.discover.Discover;
import com.stagemarket.pojo.SubCatDataPojo;
import com.stagemarket.pojo.SubCatPojo;
import com.stagemarket.pojo.category.CategoriesPojo;
import com.stagemarket.pojo.category.SubCatData;
import com.stagemarket.pojo.categorySell.Datum;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.SharedPreff;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

public class CategoriesAdapter extends RecyclerView.Adapter {

    Context context;
    ArrayList<Datum> mArrayListCatPojo;

    public CategoriesAdapter(Context context, ArrayList<Datum> mArrayListCatPojo) {
        this.context = context;
        this.mArrayListCatPojo = mArrayListCatPojo;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_categories, parent
                , false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MyViewHolder myViewHolder = (MyViewHolder) holder;
        myViewHolder.tv_categoryName.setText(mArrayListCatPojo.get(position).getName());
        //    myViewHolder.tv_categoryName.setPaintFlags(myViewHolder.tv_categoryName.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        if (mArrayListCatPojo.get(position).getId().equalsIgnoreCase("-1")) {
            myViewHolder.img_category.setImageResource(R.drawable.allcat);
        } else {
            CommonUtils.setImage(context, myViewHolder.img_category,
                    mArrayListCatPojo.get(position).getImage());
        }
    }

    @Override
    public int getItemCount() {
        return mArrayListCatPojo.size();
    }

    public void addingCategoriesData(ArrayList<Datum> getCategoriesResponseData) {
        this.mArrayListCatPojo = getCategoriesResponseData;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_categoryName;
        ImageView img_category;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_categoryName = itemView.findViewById(R.id.tv_categoryName);
            img_category = itemView.findViewById(R.id.img_category);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mArrayListCatPojo.get(getAdapterPosition()).getId().equalsIgnoreCase("-1")) {
                        EventBus.getDefault().post(new SubCatPojo(-1,Constants.ALL,Constants.ALL));

                        ((Activity) context).finish();
                    } else {
                        SharedPreff.saveCatName(mArrayListCatPojo.get(getAdapterPosition()));
                        Intent intent = new Intent(context, SubCategoriesActivity.class);
                        intent.putExtra(Constants.POS, getAdapterPosition());

                        intent.putExtra(Constants.CAT_POS, mArrayListCatPojo.get(getAdapterPosition()).getId());
                        intent.putExtra(Constants.CAT_NAME, mArrayListCatPojo.get(getAdapterPosition()).getName());
                        intent.putExtra(Constants.CAT_VALUES, new Gson().toJson(mArrayListCatPojo));
                        intent.putExtra(Constants.SUB_CAT_VALUES, new Gson().toJson(mArrayListCatPojo.get(getAdapterPosition()).getSubCategories()));
//                        ((Activity) context).startActivityForResult(intent, 50);
                        ((Activity) context).startActivity(intent);
                       ((Activity) context).finish();
                    }
                }
            });
        }
    }
}
