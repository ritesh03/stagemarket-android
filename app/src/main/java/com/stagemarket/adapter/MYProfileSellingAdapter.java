package com.stagemarket.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.stagemarket.AppClass;
import com.stagemarket.R;
import com.stagemarket.activity.home.HomeActivity;
import com.stagemarket.activity.product_detail.ProductDetailsActivity;
import com.stagemarket.activity.viewProfile.ViewProfileActivity;
import com.stagemarket.activity.viewProfile.ViewProfileView;
import com.stagemarket.pojo.ImageCamera;
import com.stagemarket.pojo.myProfile.ViewProfileSelling;
import com.stagemarket.pojo.viewOtherProfile.DataItem;
import com.stagemarket.pojo.wishlist.AddWishlistResponse;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.SharedPreff;

import java.util.ArrayList;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MYProfileSellingAdapter extends RecyclerView.Adapter {
    ViewProfileView viewProfileView;
    ArrayList<DataItem> mArrayListViewProfileSelling = new ArrayList<>();
    private NetWorkingService netWorkingService;
    private final CompositeDisposable disposables = new CompositeDisposable();
    public MYProfileSellingAdapter(ViewProfileView viewProfileView, ArrayList<DataItem> mArrayListViewProfileSelling) {
        this.viewProfileView = viewProfileView;
        this.mArrayListViewProfileSelling = mArrayListViewProfileSelling;
        netWorkingService = AppClass.getInstance().getNetworkService();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_home, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final MYProfileSellingAdapter.MyViewHolder myViewHolder = (MYProfileSellingAdapter.MyViewHolder) holder;
        final DataItem dataItem = mArrayListViewProfileSelling.get(position);
        myViewHolder.tv_name.setText(dataItem.getName());

        if (dataItem.getProductType()==1) {
            myViewHolder.product_type.setText("New");
        }
        else
        {
            myViewHolder.product_type.setText("Used");
        }
        float priceval =  ((Integer.parseInt( dataItem.getCommision()) * Float.parseFloat(String.valueOf(dataItem.getCost())))/100);
        float newPriceval = priceval+ Float.parseFloat(String.valueOf(dataItem.getCost()));
        myViewHolder.tv_price.setText(dataItem.getCurrency() + " " + String.format("%.2f",newPriceval));
        if(dataItem.getWishlistStatus().equalsIgnoreCase("1"))
        {
            myViewHolder.wishlist_btn.setBackgroundResource(R.drawable.wish_filled);
        }
        else
        {
            myViewHolder.wishlist_btn.setBackgroundResource(R.drawable.wish_blank);

        }
        myViewHolder.wishlist_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("click","click");
                addRemoveWishlist(String.valueOf(dataItem.getId()),myViewHolder.wishlist_btn,position);
            }
        });
       // myViewHolder.tv_price.setText(dataItem.getCurrency() + " " + dataItem.getCost());
        if (dataItem.getProductImages().size() > 0) {
            CommonUtils.setImage((Context) viewProfileView, myViewHolder.imgProduct,
                    Constants.IMAGE_BASE_URL + dataItem.getProductImages().get(0).getImage(),
                    200, 200);
        }
        myViewHolder.lytMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Context) viewProfileView).startActivity(new Intent((Context) viewProfileView,
                        ProductDetailsActivity.class).putExtra(Constants.VIEW_PRO_DATA,new Gson().toJson(dataItem)));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayListViewProfileSelling.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_name, tv_price,product_type;
        LinearLayout lytMain;
        ImageView imgProduct,wishlist_btn;

        public MyViewHolder(View itemView) {
            super(itemView);
            product_type = itemView.findViewById(R.id.product_type);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_price = itemView.findViewById(R.id.tv_price);
            lytMain = itemView.findViewById(R.id.lytMain);
            imgProduct = itemView.findViewById(R.id.imgProduct);
            wishlist_btn = itemView.findViewById(R.id.wishlist_btn);
        }
    }

    public void addRemoveWishlist(String productId, final ImageView wishlist_btn, final int pos)
    {
        if (CommonUtils.isConnectedToInternet(((ViewProfileActivity) (Context) viewProfileView))) {


            netWorkingService.getAPI().addToWishlist(SharedPreff.getLoginDetailsModel().getId(),productId,"1")
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<AddWishlistResponse>() {


                        @Override
                        public void onSubscribe(Disposable d) {
                            disposables.add(d);
                        }

                        @Override
                        public void onNext(AddWishlistResponse deleteProductResponse) {




                            if (deleteProductResponse.result.equalsIgnoreCase(Constants.SUCCESS)) {

                                if(deleteProductResponse.data.status==1)
                                {
                                    wishlist_btn.setBackgroundResource(R.drawable.wish_filled);
                                    Toast.makeText(((ViewProfileActivity) (Context) viewProfileView), "Item added in wishlist", Toast.LENGTH_SHORT).show();
                                    mArrayListViewProfileSelling.get(pos).setWishlistStatus("1");
                                }
                                else
                                {
                                    mArrayListViewProfileSelling.get(pos).setWishlistStatus("0");
                                    wishlist_btn.setBackgroundResource(R.drawable.wish_blank);
                                    Toast.makeText(((ViewProfileActivity) (Context) viewProfileView), "Item removed from wishlist", Toast.LENGTH_SHORT).show();
                                }


                                //  myPostsDetailsView.onDeleteProductResponse(deleteProductResponse);
                                //  myPostsDetailsView.showMessageDialog(deleteProductResponse.getMessage());

                            } else {
                                // myPostsDetailsView.showErrorDialog(deleteProductResponse.getMessage());
                            }


                        }

                        @Override
                        public void onError(Throwable e) {
                            //   myPostsDetailsView.hideProgressDialog();
                            //   myPostsDetailsView.showErrorDialog(mContext.getResources().getString(R.string.something_went_wrong));
                        }

                        @Override
                        public void onComplete() {

                        }
                    });


        } else {
            CommonUtils.showTopSnackBar((Context) viewProfileView,
                    ((ViewProfileActivity) (Context) viewProfileView).findViewById(android.R.id.content),   ((ViewProfileActivity) (Context) viewProfileView).getString(R.string.please_check_internet));

        }
    }
}