package com.stagemarket.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.stagemarket.R;
import com.stagemarket.activity.home.HomeActivity;
import com.stagemarket.activity.sell.categorySell.ad_details_sell.AdDetaillsSellActivity;
import com.stagemarket.pojo.AdDetailCategoryPojo;
import com.stagemarket.pojo.SubCatDataPojo;
import com.stagemarket.pojo.SubSecCatDataPojo;
import com.stagemarket.pojo.myOrders.SubSecondCategory;
import com.stagemarket.utils.SharedPreff;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

public class SubSecCategoryAdapter extends RecyclerView.Adapter {

    ArrayList<SubSecondCategory> data;
    Context context;
    //  MyCallBack callback;
    int posSelected = -1;    private int selectedPos = -1;

    SubSecondCategory subCatDataSelected = null;
    String from;
//    public SubCategoriesDataAdapter(ArrayList<SubCatData> data, Context context,MyCallBack callback) {
//        this.data = data;
//        this.context = context;
//       //this.callback = callback;
//    }

    public SubSecCategoryAdapter(ArrayList<SubSecondCategory> data, Context context, String from) {
        this.data = data;
        this.context = context;
        this.from = from;
    }


    public int getPosSelectedVal(){
        return selectedPos;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_subcate_data, parent
                , false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final MyViewHolder myViewHolder = (MyViewHolder) holder;
        myViewHolder.tvData.setText(data.get(position).getSubSecondCatName());
        if (data.get(position).isSelected()) {

            posSelected = position;
            subCatDataSelected = data.get(position);
            myViewHolder.lytSubCat.setBackgroundColor(ContextCompat.getColor(context, R.color.yellow));
        } else {


            myViewHolder.lytSubCat.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
        }

        myViewHolder.lytSubCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPos = position;

                SubSecondCategory subCatData = data.get(position);
                EventBus.getDefault().post(new SubSecCatDataPojo(data.get(position).getCatId()));

                if (subCatData.isSelected()) {

                } else {

                    if (posSelected < 0) {

                        SubSecondCategory subCatData1 = data.get(position);
                        subCatData1.setSelected(true);
                        data.set(position, subCatData1);
                        notifyDataSetChanged();
                        subCatDataSelected = data.get(position);

                        myViewHolder.lytSubCat.setBackgroundColor(ContextCompat.getColor(context, R.color.yellow));

                        // holder.line.setBackgroundColor(ContextCompat.getColor(((Fragment) paymentsScreenView).getActivity(), R.color.search_screen_header));
                        // paymentsScreenView.valueSelected("", "", position);

                    } else {

                        SubSecondCategory subCatDatalist1 = data.get(posSelected);
                        subCatDatalist1.setSelected(false);


                        SubSecondCategory subCatData1 = data.get(position);
                        subCatData1.setSelected(true);

                        data.set(position, subCatData1);
                        data.set(posSelected, subCatDatalist1);

                        subCatDataSelected = data.get(position);

                        //  holder.line.setBackgroundColor(ContextCompat.getColor(((Fragment) paymentsScreenView).getActivity(), R.color.line_color));
                        // notifyDataSetChanged();
                        myViewHolder.lytSubCat.setBackgroundColor(ContextCompat.getColor(context, R.color.white));

                        notifyDataSetChanged();
                    }


                }

                if (from.equalsIgnoreCase("1")) {

                    EventBus.getDefault().post(new SubSecCatDataPojo(data.get(position).getCatId()));

                    SharedPreff.saveSubSecCatName(data.get(position));
                    EventBus.getDefault().post(new AdDetailCategoryPojo(data.get(position).getSubSecondCatName()));
//                Intent returnIntent = new Intent();
//                ((Activity) context).setResult(RESULT_OK, returnIntent);
//
//                ((Activity) context).finish();

                 /*   Intent splash = new Intent(context, HomeActivity.class);
                    splash.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    ((Activity) context).startActivity(splash);*/
                    ((Activity) context).finish();
                } else {

                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvData;
        LinearLayout lytSubCat;

        public MyViewHolder(View itemView) {
            super(itemView);
            lytSubCat = itemView.findViewById(R.id.lytSubCat);
            tvData = itemView.findViewById(R.id.tvData);
        }
    }

}
