package com.stagemarket.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.stagemarket.R;
import com.stagemarket.activity.yourAccount.YourAccountView;
import com.stagemarket.pojo.addressDetails.AddressesDetails;

import java.util.ArrayList;

public class AddressesAdapter extends RecyclerView.Adapter {

    YourAccountView yourAccountView;
    ArrayList<AddressesDetails> mArrayListAddressesDetails = new ArrayList<>();

    public AddressesAdapter(YourAccountView yourAccountView, ArrayList<AddressesDetails> mArrayListAddressesDetails) {
        this.yourAccountView = yourAccountView;
        this.mArrayListAddressesDetails = mArrayListAddressesDetails;
    }

//    public void addingList(QualificationExperience qualificationExperience) {
//        dataList.add(qualificationExperience);
//        notifyDataSetChanged();
//    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_addresses_details, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MyViewHolder myViewHolder = (MyViewHolder) holder;
        AddressesDetails addressesDetails = mArrayListAddressesDetails.get(position);


        myViewHolder.tv_address_count.setText(addressesDetails.getAddressCount());
        myViewHolder.tv_address_username.setText(addressesDetails.getAddressUsername());
        myViewHolder.tv_addressLine1.setText(addressesDetails.getAddressLine1());
        myViewHolder.tv_addressLine2.setText(addressesDetails.getAddressLine2());
        myViewHolder.tv_contact_details.setText(addressesDetails.getAddressContactDetails());


        myViewHolder.tv_address_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                yourAccountView.goToEditAddressScreen();
            }
        });


    }

    @Override
    public int getItemCount() {
        return mArrayListAddressesDetails.size();
    }

//    public void deletingItem(int pos) {
//        dataList.remove(pos);
//        notifyDataSetChanged();
//    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_address_count, tv_address_username, tv_address_edit, tv_addressLine1, tv_addressLine2, tv_contact_details;
        View empty_view;
        public MyViewHolder(View itemView) {
            super(itemView);
            tv_address_count = itemView.findViewById(R.id.tv_address_count);
            tv_address_username = itemView.findViewById(R.id.tv_address_username);
            tv_address_edit = itemView.findViewById(R.id.tv_address_edit);
            tv_addressLine1 = itemView.findViewById(R.id.tv_addressLine1);
            tv_addressLine2 = itemView.findViewById(R.id.tv_addressLine2);
            tv_contact_details = itemView.findViewById(R.id.tv_contact_details);

            empty_view = itemView.findViewById(R.id.empty_view);


        }
    }


}
