package com.stagemarket.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.stagemarket.R;
import com.stagemarket.activity.sell.categorySell.ad_details_sell.AdDetaillsSellActivity;
import com.stagemarket.activity.sell.categorySell.category_sell.CategorySellVIew;
import com.stagemarket.activity.sell.categorySell.select_category_sell.SelectCategorySellActivity;
import com.stagemarket.activity.sell.categorySell.select_category_sell.SelectCategorySellView;
import com.stagemarket.pojo.category.SubCatData;
import com.stagemarket.pojo.categorySell.CategorySellPojo;
import com.stagemarket.pojo.categorySell.SubCategory;
import com.stagemarket.utils.SharedPreff;

import java.util.ArrayList;

public class SelectCategorySellAdapter extends RecyclerView.Adapter {


    SelectCategorySellView selectCategorySellView;
    ArrayList<SubCategory> mArraListSelectCategorySellPojo = new ArrayList<>();
    int posSelected = -1;
    SubCategory subCatDataSelected = null;
    private int selectedPos = -1;

    public SelectCategorySellAdapter(SelectCategorySellView selectCategorySellView, ArrayList<SubCategory> mArraListSelectCategorySellPojo) {
        this.selectCategorySellView = selectCategorySellView;
        this.mArraListSelectCategorySellPojo = mArraListSelectCategorySellPojo;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_subcate_data, parent
                , false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final MyViewHolder myViewHolder = (MyViewHolder) holder;
        myViewHolder.tvData.setText(mArraListSelectCategorySellPojo.get(position).getSubCatName());
        if (mArraListSelectCategorySellPojo.get(position).isSelected()) {

            posSelected = position;
            subCatDataSelected = mArraListSelectCategorySellPojo.get(position);
            myViewHolder.lytSubCat.setBackgroundColor(ContextCompat.getColor((Context) selectCategorySellView, R.color.yellow));
        } else {


            myViewHolder.lytSubCat.setBackgroundColor(ContextCompat.getColor((Context) selectCategorySellView, R.color.white));
        }

        myViewHolder.lytSubCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPos = position;

                SubCategory subCatData = mArraListSelectCategorySellPojo.get(position);

                if (subCatData.isSelected()) {

                } else {

                    if (posSelected < 0) {

                        SubCategory subCatData1 = mArraListSelectCategorySellPojo.get(position);
                        subCatData1.setSelected(true);
                        mArraListSelectCategorySellPojo.set(position, subCatData1);
                        notifyDataSetChanged();
                        subCatDataSelected = mArraListSelectCategorySellPojo.get(position);

                        myViewHolder.lytSubCat.setBackgroundColor(ContextCompat.getColor((Context) selectCategorySellView, R.color.yellow));

                        // holder.line.setBackgroundColor(ContextCompat.getColor(((Fragment) paymentsScreenView).getActivity(), R.color.search_screen_header));
                        // paymentsScreenView.valueSelected("", "", position);

                    } else {

                        SubCategory subCatDatalist1 = mArraListSelectCategorySellPojo.get(posSelected);
                        subCatDatalist1.setSelected(false);


                        SubCategory subCatData1 = mArraListSelectCategorySellPojo.get(position);
                        subCatData1.setSelected(true);

                        mArraListSelectCategorySellPojo.set(position, subCatData1);
                        mArraListSelectCategorySellPojo.set(posSelected, subCatDatalist1);

                        subCatDataSelected = mArraListSelectCategorySellPojo.get(position);

                        //  holder.line.setBackgroundColor(ContextCompat.getColor(((Fragment) paymentsScreenView).getActivity(), R.color.line_color));
                        // notifyDataSetChanged();
                        myViewHolder.lytSubCat.setBackgroundColor(ContextCompat.getColor((Context) selectCategorySellView, R.color.white));

                        notifyDataSetChanged();
                    }


                }


            }
        });


    }


    public int getPosSelectedVal(){
        return selectedPos;
    }

    @Override
    public int getItemCount() {
        return mArraListSelectCategorySellPojo.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvData;
        LinearLayout lytSubCat;

        public MyViewHolder(View itemView) {
            super(itemView);
            lytSubCat = itemView.findViewById(R.id.lytSubCat);
            tvData = itemView.findViewById(R.id.tvData);


        }

        @Override
        public void onClick(View view) {

        }
    }

}
