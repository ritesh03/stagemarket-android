package com.stagemarket.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.stagemarket.R;
import com.stagemarket.activity.home.HomeActivity;
import com.stagemarket.activity.subSubCategories.SubSecCategory;
import com.stagemarket.pojo.AdDetailCategoryPojo;
import com.stagemarket.pojo.SubCatDataPojo;
import com.stagemarket.pojo.categorySell.SubCategory;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.SharedPreff;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

public class SubCategoriesDataAdapter extends RecyclerView.Adapter {

    ArrayList<SubCategory> data;
    Context context;
    //  MyCallBack callback;
    int posSelected = -1;
    SubCategory subCatDataSelected = null;

//    public SubCategoriesDataAdapter(ArrayList<SubCatData> data, Context context,MyCallBack callback) {
//        this.data = data;
//        this.context = context;
//       //this.callback = callback;
//    }

    public SubCategoriesDataAdapter(ArrayList<SubCategory> data, Context context) {
        this.data = data;
        this.context = context;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_subcate_data, parent
                , false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final MyViewHolder myViewHolder = (MyViewHolder) holder;
        myViewHolder.tvData.setText(data.get(position).getSubCatName());
        if (data.get(position).isSelected()) {

            posSelected = position;
            subCatDataSelected = data.get(position);
            myViewHolder.lytSubCat.setBackgroundColor(ContextCompat.getColor(context, R.color.yellow));
        } else {


            myViewHolder.lytSubCat.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
        }

        myViewHolder.lytSubCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SubCategory subCatData = data.get(position);
                if (subCatData.isSelected()) {

                } else {

                    if (posSelected < 0) {

                        SubCategory subCatData1 = data.get(position);
                        subCatData1.setSelected(true);
                        data.set(position, subCatData1);
                        notifyDataSetChanged();
                        subCatDataSelected = data.get(position);

                        myViewHolder.lytSubCat.setBackgroundColor(ContextCompat.getColor(context, R.color.yellow));

                        // holder.line.setBackgroundColor(ContextCompat.getColor(((Fragment) paymentsScreenView).getActivity(), R.color.search_screen_header));
                        // paymentsScreenView.valueSelected("", "", position);

                    } else {

                        SubCategory subCatDatalist1 = data.get(posSelected);
                        subCatDatalist1.setSelected(false);


                        SubCategory subCatData1 = data.get(position);
                        subCatData1.setSelected(true);

                        data.set(position, subCatData1);
                        data.set(posSelected, subCatDatalist1);

                        subCatDataSelected = data.get(position);

                        //  holder.line.setBackgroundColor(ContextCompat.getColor(((Fragment) paymentsScreenView).getActivity(), R.color.line_color));
                        // notifyDataSetChanged();
                        myViewHolder.lytSubCat.setBackgroundColor(ContextCompat.getColor(context, R.color.white));

                        notifyDataSetChanged();
                    }


                }
                SharedPreff.saveSubCatName(data.get(position));
                EventBus.getDefault().post(new SubCatDataPojo(data.get(position).getId()));


                EventBus.getDefault().post(new AdDetailCategoryPojo(data.get(position).getSubCatName()));
                Log.e("data",""+data.get(position).getmArrayListSubSecCat());
                if(data.get(position).getmArrayListSubSecCat()!=null&&data.get(position).getmArrayListSubSecCat().size()>0) {
                    Intent returnIntent = new Intent(context, SubSecCategory.class);
                    returnIntent.putExtra(Constants.FROM, "1");
                    returnIntent.putExtra(Constants.CAT_NAME, data.get(position).getSubCatName());
                    returnIntent.putExtra(Constants.SUB_SEC_DATA, new Gson().toJson(data.get(position).getmArrayListSubSecCat()));
                    ((Activity) context).startActivity(returnIntent);
                    ((Activity) context).finish();
                }
                else
                {  /* Intent splash = new Intent(context, HomeActivity.class);
                    splash.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    ((Activity) context).startActivity(splash);*/
                    ((Activity) context).finish();

                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvData;
        LinearLayout lytSubCat;

        public MyViewHolder(View itemView) {
            super(itemView);
            lytSubCat = itemView.findViewById(R.id.lytSubCat);
            tvData = itemView.findViewById(R.id.tvData);
        }
    }

//    public interface MyCallBack{
//        void onViewClick(int pos, LinearLayout lytSubCat, int posSelected, SubCatData subCatDataSelected);
//    }
}
