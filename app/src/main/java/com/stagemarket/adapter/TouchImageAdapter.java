package com.stagemarket.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.stagemarket.R;
import com.stagemarket.activity.FullViewActivity;
import com.stagemarket.activity.TouchImageView;
import com.stagemarket.pojo.myPosts.ProductImage;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;

import java.util.List;

public class TouchImageAdapter extends PagerAdapter {

    private Context mContext;
    List<ProductImage> productImages;
String type="";
    public TouchImageAdapter(Context context, List<ProductImage> productImages,String type) {
        this.mContext = context;
        this.type = type;
        this.productImages = productImages;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, final int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.row_item_full, collection, false);
        collection.addView(layout);
        TouchImageView imageView = layout.findViewById(R.id.touchImg);
        if(type.equalsIgnoreCase("edit"))
        {
            Glide.with(mContext).load(  productImages.get(position).getImage())
                    // .error(R.drawable.noimage).placeholder(R.drawable.noimage)
                    .diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);
        }
        else {
            CommonUtils.setImageSec(mContext, imageView, productImages.get(position).getImage());
        }
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return productImages.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

}
