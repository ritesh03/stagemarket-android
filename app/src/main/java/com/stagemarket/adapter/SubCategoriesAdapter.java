package com.stagemarket.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.stagemarket.R;
import com.stagemarket.activity.subCategories.SubCategoriesView;
import com.stagemarket.fragment.discover.Discover;
import com.stagemarket.pojo.AdDetailCategoryPojo;
import com.stagemarket.pojo.SubCatDataPojo;
import com.stagemarket.pojo.SubCatPojo;
import com.stagemarket.pojo.categorySell.Datum;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.SharedPreff;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

public class SubCategoriesAdapter extends RecyclerView.Adapter {

    Context mContext;
    ArrayList<Datum> mArrayListSubCat;
    private SubCategoriesView subCategoriesView;
    int posSelected = -1;
    Datum categoriesSelected = null;


    public SubCategoriesAdapter(SubCategoriesView subCategoriesView, ArrayList<Datum> mArrayListSubCat) {
        this.mArrayListSubCat = mArrayListSubCat;
        this.subCategoriesView = subCategoriesView;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_subcategories, parent
                , false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final MyViewHolder myViewHolder = (MyViewHolder) holder;
        if (mArrayListSubCat.get(position).isSelected()) {

            posSelected = position;
            categoriesSelected = mArrayListSubCat.get(position);
            myViewHolder.lyt_categoriesVal.setBackgroundColor(ContextCompat.getColor((Context) subCategoriesView, R.color.black));
            myViewHolder.tv_Categories.setTextColor(ContextCompat.getColor((Context) subCategoriesView, R.color.white));
        } else {
            myViewHolder.lyt_categoriesVal.setBackgroundColor(ContextCompat.getColor((Context) subCategoriesView, R.color.white));
            myViewHolder.tv_Categories.setTextColor(ContextCompat.getColor((Context) subCategoriesView, R.color.black));
        }

        if (mArrayListSubCat.get(position).getId().equalsIgnoreCase("-1")) {
            myViewHolder.img_category.setImageResource(R.drawable.allcat);
        } else {
            CommonUtils.setImage((Context) subCategoriesView, myViewHolder.img_category,
                    mArrayListSubCat.get(position).getImage());
        }

        myViewHolder.tv_Categories.setText(mArrayListSubCat.get(position).getName());
        // myViewHolder.img_category.setImageResource(mArrayListSubCat.get(position).getImage());

        myViewHolder.lyt_subcat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent returnIntent = new Intent();
//                ((Activity) subCategoriesView).setResult(RESULT_OK, returnIntent);
//                EventBus.getDefault().post(mArrayListSubCat.get(position));
//                ((Activity) subCategoriesView).finish();
            /*    Discover discover=new Discover();
                discover.onSubCategorySelect(new SubCatDataPojo(
                        mArrayListSubCat.get(position).getId()));*/

                EventBus.getDefault().post(new SubCatPojo(position, mArrayListSubCat.get(position).getName(),
                        mArrayListSubCat.get(position).getId()));

                //EventBus.getDefault().post(mArrayListSubCat.get(position));
                Datum categoriesModel = mArrayListSubCat.get(position);
                subCategoriesView.setCateText(categoriesModel.getName());
                if (categoriesModel.isSelected()) {

                } else {

                    if (posSelected < 0) {

                        Datum categoriesModel1 = mArrayListSubCat.get(position);
                        categoriesModel1.setSelected(true);
                        mArrayListSubCat.set(position, categoriesModel1);

                        categoriesSelected = mArrayListSubCat.get(position);
                        notifyDataSetChanged();
                    } else {

                        Datum categoriesPojo = mArrayListSubCat.get(posSelected);
                        categoriesPojo.setSelected(false);

                        Datum categoriesPojo1 = mArrayListSubCat.get(position);
                        categoriesPojo1.setSelected(true);

                        mArrayListSubCat.set(position, categoriesPojo1);
                        mArrayListSubCat.set(posSelected, categoriesPojo);

                        categoriesSelected = mArrayListSubCat.get(position);
                        notifyDataSetChanged();
                    }

                }

                if(categoriesSelected.getId().equalsIgnoreCase("-1")){
                    EventBus.getDefault().post(new SubCatDataPojo("0"));

                   // SharedPreff.saveSubCatName(data.get(position));
                    EventBus.getDefault().post(new AdDetailCategoryPojo("All"));
                    Intent returnIntent = new Intent();
                    ((Activity) subCategoriesView).setResult(RESULT_OK, returnIntent);

                    ((Activity) subCategoriesView).finish();
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return mArrayListSubCat.size();
    }

    public void setSelectedCategory(int catid) {
        for (int i = 0; i < mArrayListSubCat.size(); i++) {
            if (Integer.parseInt(mArrayListSubCat.get(i).getId()) == catid) {
                Datum categoriesModel1 = mArrayListSubCat.get(i);
                categoriesModel1.setSelected(true);
                mArrayListSubCat.set(i, categoriesModel1);
                categoriesSelected = mArrayListSubCat.get(i);
                notifyDataSetChanged();
            }
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView img_category;
        TextView tv_Categories;
        LinearLayout lyt_subcat, lyt_categoriesVal;

        public MyViewHolder(View itemView) {
            super(itemView);
            lyt_subcat = itemView.findViewById(R.id.lyt_subcat);
            img_category = itemView.findViewById(R.id.img_category);
            tv_Categories = itemView.findViewById(R.id.tv_Categories);
            lyt_categoriesVal = itemView.findViewById(R.id.lyt_categoriesVal);
            tv_Categories = itemView.findViewById(R.id.tv_Categories);
        }
    }
}
