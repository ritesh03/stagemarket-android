package com.stagemarket.fcm;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.stagemarket.AppClass;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.SharedPreff;


/**
 * Created by Bhvesh on 03-Nov-16.
 */
public class TokenService extends FirebaseInstanceIdService {
    public NetWorkingService netWorkingService;

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e("notificationToken", refreshedToken);
        SharedPreff.setNotificationToken(refreshedToken);


        // sendRegistrationToServer(refreshedToken);

        netWorkingService = ((AppClass) getApplication()).getNetworkService();

    }

    private void sendRegistrationToServer(String refreshedToken) {

    }


}
