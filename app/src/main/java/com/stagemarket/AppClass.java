package com.stagemarket;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.retrofit.RestClient;

import io.branch.referral.Branch;
import io.paperdb.Paper;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class AppClass extends MultiDexApplication {
    private NetWorkingService netWorkingService;
    private static AppClass instance;

    @Override
    public void onCreate() {
        super.onCreate();
        Branch.enableLogging();

        // Branch object initialization
        Branch.getAutoInstance(this);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/OpenSans-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        
        netWorkingService = new NetWorkingService(this);
        Paper.init(this);
        instance = this;

        new RestClient(this,60);
    }


    public static AppClass getInstance() {
        return instance;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);

    }

    public NetWorkingService getNetworkService() {
        return netWorkingService;
    }


}
