package com.stagemarket.retrofit;

import com.stagemarket.pojo.addressDetails.AddAddressResponse;
import com.stagemarket.pojo.addressDetails.GetAddressResponse;
import com.stagemarket.pojo.categorySell.GetCategoriesResponse;
import com.stagemarket.pojo.changePassword.ChangePwdResponse;
import com.stagemarket.pojo.deleteProduct.DeleteProductResponse;
import com.stagemarket.pojo.discover.GetProductsResponse;
import com.stagemarket.pojo.forgotPassword.ForgotPasswordResponse;
import com.stagemarket.pojo.login.LoginResponse;
import com.stagemarket.pojo.myOrders.OrderDetalResponse;
import com.stagemarket.pojo.myOrders.purchased.PurchasedResponse;
import com.stagemarket.pojo.myPosts.GetMyPostsResponse;
import com.stagemarket.pojo.myProfile.MyProfileResponse;
import com.stagemarket.pojo.notifications.GetNotificationResponse;
import com.stagemarket.pojo.placeOrder.PlaceOrderResponse;
import com.stagemarket.pojo.placeOrder.ProdData;
import com.stagemarket.pojo.signup.SignUpResponse;
import com.stagemarket.pojo.social_login.SocialLoginResponse;
import com.stagemarket.pojo.updateProfile.UpdateProfileResponse;
import com.stagemarket.pojo.viewOtherProfile.Response;
import com.stagemarket.pojo.welcome_upload_image.UploadImageResponse;
import com.stagemarket.pojo.wishlist.AddWishlistResponse;
import com.stagemarket.pojo.wishlist.GetWishlistProductsResponse;
import com.stagemarket.utils.Constants;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit.mime.TypedFile;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by Bhvesh on 02-May-17.
 */

public interface ApiService {
    @FormUrlEncoded
    @POST("signup")
    public Observable<SignUpResponse> signUp(@Field("email") String email
            , @Field("firstName") String firstName
            , @Field("lastName") String lastName
            , @Field("password") String password
            , @Field("mobile") String mobile
            , @Field("deviceToken") String deviceToken
            , @Field("deviceType") String deviceType
            , @Field("organizationType") String organizationType
            , @Field("companyName") String companyName
            , @Field("companyAddress") String companyAddress
            , @Field("companyWebsite") String companyWebsite);

    @FormUrlEncoded
    @POST("login")
    public Observable<LoginResponse> login(@Field("email") String email
            , @Field("password") String password
            , @Field("deviceType") String deviceType, @Field("deviceToken") String deviceToken);

    @FormUrlEncoded
    @POST("fbLogin")
    public Observable<SocialLoginResponse> socialLoginService(@Field("fbId") String fbId, @Field("email") String email, @Field("firstName") String firstName, @Field("lastName") String lastName
            , @Field("deviceType") String deviceType, @Field("deviceToken") String deviceToken);

    @FormUrlEncoded
    @retrofit2.http.POST("editProfile")
    public Observable<UpdateProfileResponse> editProfile(

            @Field(Constants.USERID) String userid
            , @Field(Constants.FIRSTNAME) String firstname
            , @Field(Constants.EMAIL) String email
            , @Field(Constants.MOBILE) String mobile
            , @Field(Constants.GENDER) String gender
            , @Field(Constants.LASTNAME) String lastname
            , @Field("companyName") String companyname
            , @Field("companyAddress") String companyaddress
            , @Field("companyWebsite") String companywebsite
    );

    @FormUrlEncoded
    @POST("uploadProfileImage")
    public Observable<UploadImageResponse> uploadProfileImageService(@Field("userId") String userId, @Field("image") String image
    );

    @FormUrlEncoded
    @POST("getProfile")
    public Observable<MyProfileResponse> myProfile(@Field("userId") String userId);

    @FormUrlEncoded
    @POST("getAddress")
    public Observable<GetAddressResponse> getAddress(@Field("userId") String userId);

    @GET("getCategories")
    public Observable<GetCategoriesResponse> getCategoriesService();

    @GET("getCategories")
    public Observable<ProdData> getProductDetail();


    @FormUrlEncoded
    @POST("forgotPassword")
    public Observable<ForgotPasswordResponse> forgotPasswordService(@Field("email") String email);
/*
    @FormUrlEncoded
    @POST("updateProfile")
    public Observable<UpdateProfileResponse> updateProfileService(@Field("userId") String userId,
                                                                  @Field("firstName") String firstName,
                                                                  @Field("email") String email,
                                                                  @Field("mobile") String mobile,
                                                                  @Field("gender") String gender,
                                                                  @Field("image") String image,
                                                                  @Field("lastName") String lastName);*/


    @FormUrlEncoded
    @POST("getProducts")
    public Observable<GetProductsResponse> getProductsService(@Field("userId") String userId,
                                                              @Field("current_page") int current_page,
                                                              @Field("latitude") String latitude,
                                                              @Field("longitude") String longitude,
                                                              @Field("categoryId") String categoryId,
                                                              @Field("subcategoryId") String subcategoryId,
                                                              @Field("subsecondcatId") String subsecondcatId);

    @FormUrlEncoded
    @POST("addAddress")
    public Observable<AddAddressResponse> addAddress(@Field("userId") String userId,
                                                     @Field("billingName") String billingName,
                                                     @Field("billingContact") String billingContact,
                                                     @Field("billingddress") String billingddress,
                                                     @Field("billingCity") String billingCity);

    @FormUrlEncoded
    @POST("getNotification")
    public Observable<GetNotificationResponse> getNotificationService(@Field("userId") String userId);


    @FormUrlEncoded
    @POST("getMyPosts")
    public Observable<GetMyPostsResponse> getMyPostsService(@Field("userId") String userId,
                                                            @Field("current_page") int current_page,
                                                            @Field("type") int type); @FormUrlEncoded

    @POST("Wishlist")
    public Observable<AddWishlistResponse> addToWishlist(@Field("userId") String userId,
                                                         @Field("productId") String productId,
                                                         @Field("type") String type);



    @FormUrlEncoded
    @POST("getOtherProfile")
    public Observable<Response> getOtherProfileService(@Field("userId") String userId,@Field("ownUserId") String ownUserId,
                                                       @Field("companyId") String companyId);

    @FormUrlEncoded
    @POST("deleteProduct")
    public Observable<DeleteProductResponse> deleteProductService(@Field("userId") String userId,
                                                                  @Field("productId") String productId);

    @FormUrlEncoded
    @POST("orderProduct")
    public Observable<PlaceOrderResponse> orderProduct(@Field("userId") String userId,
                                                       @Field("productId") String productId,
                                                       @Field("sellerId") String sellerId,
                                                       @Field("price") String price,
                                                       @Field("orderType") String orderType,
                                                       @Field("categoryName") String categoryName,
                                                       @Field("subCategoryName") String subCategoryName,
                                                       @Field("billingName") String billingName,
                                                       @Field("billingAddress") String billingAddress,
                                                       @Field("billingCity") String billingCity,
                                                       @Field("billingContact") String billingContact,
                                                       @Field("billingAmount") String billingAmount,
                                                       @Field("commision") String commision,
                                                       @Field("vat") String vat,
                                                       @Field("quantity") String quantity
    );

    @FormUrlEncoded
    @POST("getMyOrders")
    public Observable<PurchasedResponse> getMyOrders(@Field("userId") String userId,
                                                     @Field("type") String type,
                                                     @Field("current_page") int current_page);
    @FormUrlEncoded
    @POST("getWishlist")
    public Observable<GetWishlistProductsResponse> getWishlistProdcuts(@Field("userId") String userId,
                                                                                        @Field("current_page") int current_page);


    @FormUrlEncoded
    @POST("getMyOrdersDetail")
    public Observable<OrderDetalResponse> getMyOrdersdetail(@Field("orderId") String orderId,
                                                            @Field("userId") String userId)
            ;
    @FormUrlEncoded
    @POST("searchProducts")
    public Observable<GetProductsResponse> searchProductsService(@Field("userId") String userId, @Field("current_page") int current_page,
                                                                 @Field("name") String name, @Field("latitude") String latitude,
                                                                 @Field("longitude") String longitude);

    @FormUrlEncoded
    @POST("updatePassword")
    public Observable<ChangePwdResponse> updatePwd(@Field("userId") String userId,
                                                   @Field("newPwd") String newPwd,
                                                   @Field("oldPwd") String oldPwd);
}
