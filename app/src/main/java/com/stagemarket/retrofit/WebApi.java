package com.stagemarket.retrofit;

import com.google.gson.JsonObject;
import com.stagemarket.pojo.signup.SignUpResponse;
import com.stagemarket.pojo.updateProfile.UpdateProfileResponse;
import com.stagemarket.pojo.welcome_upload_image.UploadImageResponse;
import com.stagemarket.utils.ApiGlobal;
import com.stagemarket.utils.Constants;

import java.util.Map;

import io.reactivex.Observable;
import retrofit.Callback;
import retrofit.http.PartMap;
import retrofit.mime.TypedFile;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Query;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;

public interface WebApi {


    @Multipart
    @POST("/uploadProfileImage")
    void uploadFile(
            @Query(Constants.USERID) String userid,
            @Part(Constants.IMAGE) TypedFile image_file, Callback<UploadImageResponse> callback);




    @Multipart
    @POST("/editProfile")
    void updateProfileService(
            @Query(Constants.USERID) String userid,
            @Query(Constants.FIRSTNAME) String firstname,
            @Query(Constants.EMAIL) String email,
            @Query(Constants.MOBILE) String mobile,
            @Query(Constants.GENDER) String gender,
            @Query(Constants.LASTNAME) String lastname,
            @Query("companyName") String companyname,
            @Query("companyAddress") String companyaddress,
            @Query("companyWebsite") String companywebsite,
            @Part(Constants.IMAGE) TypedFile image_file,
            Callback<UpdateProfileResponse> postResponseCallback);

    @Multipart
    @POST("/editProfile")
    void updateProfileService(
            @Query(Constants.USERID) String userid,
            @Query(Constants.FIRSTNAME) String firstname,
            @Query(Constants.EMAIL) String email,
            @Query(Constants.MOBILE) String mobile,
            @Query(Constants.GENDER) String gender,
            @Query(Constants.LASTNAME) String lastname,
            @Query("companyName") String companyname,
            @Query("companyAddress") String companyaddress,
            @Query("companyWebsite") String companywebsite,

            Callback<UpdateProfileResponse> postResponseCallback);

    @Multipart
    @POST("/addProduct")
    void addProduct(
            @PartMap Map<String,TypedFile> files,
            @Query(Constants.USERID) String userid,
            @Query(Constants.CATID) String catId,
            @Query(Constants.SUBCATID) String subcatId,
            @Query(Constants.NAME) String name,
            @Query(Constants.DESCRIPTION) String description,
            @Query(Constants.COST) String cost,
            @Query(Constants.TITLE) String title,
            @Query(Constants.LOCATION) String location,
            @Query(Constants.LATITUDE) String latitude,
            @Query(Constants.LONGITUDE) String longitude,
            @Query(Constants.QUANTITY) String quantity,
            @Query(Constants.SUBSECONDCATID) String subsecondcatid,
            @Query("productType") int productType,
            Callback<ApiGlobal> postResponseCallback);

    @Multipart
    @POST("/updateProduct")
    void updateProduct(
            @PartMap Map<String,TypedFile> files,
            @Query(Constants.USERID) String userid,
            @Query(Constants.DESCRIPTION) String description,
            @Query(Constants.COST) String cost,
            @Query(Constants.TITLE) String title,
            @Query("quantity") String qty,
            @Query(Constants.PRODUCTID) String productId,
            @Query("productType") int productType,
            Callback<ApiGlobal> postResponseCallback);
}
