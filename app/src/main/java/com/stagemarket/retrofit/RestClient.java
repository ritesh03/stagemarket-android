package com.stagemarket.retrofit;

import android.content.Context;

import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;


public class RestClient {
    private static WebApi REST_CLIENT;
    static Context context;
    private static String baseUrl = "http://206.189.23.147/StageMarket/public/api/";

    public RestClient(Context context, int timeout) {
        this.context = context;
        setUpRestClient(timeout);
    }


    public static WebApi get(int timeOut) {
        if (REST_CLIENT != null) {
            return REST_CLIENT;
        } else {
            setUpRestClient(timeOut);
            return REST_CLIENT;
        }
    }

    public static void setUpRestClient(int timeOut) {
        OkHttpClient ok = new OkHttpClient();
        ok.setReadTimeout(timeOut, TimeUnit.SECONDS);
        ok.setWriteTimeout(timeOut, TimeUnit.SECONDS);
        ok.setConnectTimeout(timeOut, TimeUnit.SECONDS);
        ok.setRetryOnConnectionFailure(true);
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(baseUrl)
                .setClient(new OkClient(ok))
                .setLogLevel(RestAdapter.LogLevel.FULL);

        RestAdapter restAdapter = builder.build();
        REST_CLIENT = restAdapter.create(WebApi.class);
    }

}
