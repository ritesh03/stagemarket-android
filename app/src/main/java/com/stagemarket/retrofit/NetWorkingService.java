package com.stagemarket.retrofit;


import android.app.Application;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetWorkingService {
    private static String baseUrl = "http://206.189.23.147/StageMarket/public/api/";
    //private static String baseUrl = "http://xperge.com/StageMarket/public/api/";
    public static final String GEO_BASE_URL = "https://maps.googleapis.com/";
    private ApiService networkAPI, networkAPI_2;
    private static ApiService networkAPI_3;

    public NetWorkingService(Application app) {
        OkHttpClient.Builder client = new OkHttpClient.Builder()
                .connectTimeout(400, TimeUnit.SECONDS).readTimeout(400, TimeUnit.SECONDS);
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        client.addInterceptor(loggingInterceptor);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client.build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        Retrofit retrofit2 = new Retrofit.Builder()
                .baseUrl(GEO_BASE_URL)
                .client(client.build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();


        networkAPI = retrofit.create(ApiService.class);
        networkAPI_2 = retrofit2.create(ApiService.class);
    }

    public NetWorkingService(String url) {
        OkHttpClient.Builder client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS);
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        client.addInterceptor(loggingInterceptor);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .client(client.build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        networkAPI_3 = retrofit.create(ApiService.class);
    }

    public ApiService getAPI() {
        return networkAPI;
    }

    public ApiService getAPI2() {
        return networkAPI_2;
    }

    public ApiService getAPI3() {
        return networkAPI_3;
    }


}
