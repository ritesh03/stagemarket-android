package com.stagemarket.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.CalendarContract;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Telephony;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidadvance.topsnackbar.TSnackbar;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.stagemarket.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.yalantis.ucrop.util.FileUtils.getDataColumn;


/**
 * Created by Bhvesh on 01-May-17.
 */

public class CommonUtils {

    public static Typeface light, sanslight, reguler, bold, semibold; // fonts
    static String month = "", date = "", hours = "", minutes = "";

    /**
     * Checks for internet connectivity
     *
     * @param act
     * @return
     */
    public static boolean isConnectedToInternet(Context act) {
        boolean net;
        try {
            ConnectivityManager ConMgr = (ConnectivityManager) act
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            net = ConMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED
                    || ConMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
                    .getState() == NetworkInfo.State.CONNECTED;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return net;
    }



    /**
     * Change case of input string to capWords
     *
     * @param input
     * @return
     */
    public static String capWordCase(String input) {
        String output = "";
        if (input != null && !input.isEmpty()) {
            int count = 0;
            for (char c : input.toCharArray()) {
                if (count == 0) {
                    output += Character.toUpperCase(c);
                } else {
                    output += Character.toLowerCase(c);
                }
                count++;
            }
        }
        return output;
    }
    public static String convertUTCtoLocalTime(  String p_UTCDateTime) throws Exception{

        String lv_dateFormateInLocalTimeZone="";//Will hold the final converted date

        SimpleDateFormat lv_formatter;
        SimpleDateFormat lv_parser;

//Temp for testing(mapping of cities and timezones will eventually be in a properties file


//create a new Date object using the UTC timezone
        lv_parser = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        lv_parser.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date lv_localDate = lv_parser.parse(p_UTCDateTime);

//Set output format - // prints "2007/10/25  18:35:07 EDT(-0400)"
        lv_formatter = new SimpleDateFormat("hh:mm a");
        System.out.println("convertUTCtoLocalTime "+""+": "+ "The Date in the UTC time zone(UTC) " + lv_formatter.format(lv_localDate));

//Convert the UTC date to Local timezone
        lv_formatter.setTimeZone(TimeZone.getDefault());
        lv_dateFormateInLocalTimeZone = lv_formatter.format(lv_localDate);
        System.out.println("convertUTCtoLocalTime: "+""+": "+"The Date in the LocalTime Zone time zone " + lv_formatter.format(lv_localDate));
        System.out.println("convertUTCtoLocalTimeZONE: "+""+": "+"- " +TimeZone.getDefault()  );

        return lv_dateFormateInLocalTimeZone;
    }    public static String OrderDetailsconvertUTCtoLocalTime(  String p_UTCDateTime) throws Exception{

        String lv_dateFormateInLocalTimeZone="";//Will hold the final converted date

        SimpleDateFormat lv_formatter;
        SimpleDateFormat lv_parser;

//Temp for testing(mapping of cities and timezones will eventually be in a properties file


//create a new Date object using the UTC timezone
        lv_parser = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        lv_parser.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date lv_localDate = lv_parser.parse(p_UTCDateTime);

//Set output format - // prints "2007/10/25  18:35:07 EDT(-0400)"
        lv_formatter = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm a");
        System.out.println("convertUTCtoLocalTime "+""+": "+ "The Date in the UTC time zone(UTC) " + lv_formatter.format(lv_localDate));

//Convert the UTC date to Local timezone
        lv_formatter.setTimeZone(TimeZone.getDefault());
        lv_dateFormateInLocalTimeZone = lv_formatter.format(lv_localDate);
        System.out.println("convertUTCtoLocalTime: "+""+": "+"The Date in the LocalTime Zone time zone " + lv_formatter.format(lv_localDate));
        System.out.println("convertUTCtoLocalTimeZONE: "+""+": "+"- " +TimeZone.getDefault()  );

        return lv_dateFormateInLocalTimeZone;
    }
   /* public static boolean checkPlayServices(Activity context) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(context, resultCode, Constants.PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i("", "This device is not supported.");
            }
            return false;
        }
        return true;
    }*/

    public static void textScrollable(TextView view) {
        view.setMovementMethod(new ScrollingMovementMethod());
    }


    public static void hideKeyboard(View v, Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getApplicationContext().getSystemService(activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param activity
     */
    public static void closingKeyboard(Activity activity) {
        try {
            activity.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showKeyboard(Context context) {
        ((InputMethodManager) (context).getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public static String getLastTwoDigits(String phoneNumber) {
        try {
            return phoneNumber.substring(8, 10);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String changeDateFormat(String dateString) {
        String result = "";
        try {
            //2018-08-04 06:59:08
            DateFormat srcDf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            // parse the date string into Date object
            Date date = srcDf.parse(dateString);

            DateFormat destDf = new SimpleDateFormat("yyyy-MM-dd hh:mm a");

            // format the date into another format
            dateString = destDf.format(date);

            System.out.println("Converted date is : " + dateString);
            result = dateString;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String changeeDateFormat(String dateString) {
        String result = "";
        try {
            //2018-08-04 06:59:08
            DateFormat srcDf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            // parse the date string into Date object
            Date date = srcDf.parse(dateString);

            DateFormat destDf = new SimpleDateFormat("dd-MMM-yyyy hh:mma");


            // format the date into another format
            dateString = destDf.format(date);

            System.out.println("Converted date is : " + dateString);
            result = dateString;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getConvertedPhoneNum(String phoneNumber) {
        StringBuilder sb = new StringBuilder();
        int phone_length = 0;

        try {
            phone_length = phoneNumber.length();

            if (phone_length > 2) {
                for (int g = 0; g < (phone_length - 2); g++) {
                    sb.append("x");
                }
                sb.append(phoneNumber.substring(phone_length - 2));


            }
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return phoneNumber;
        }

    }

    public static Double roundTwoDecimal(double d) {
        try {
            DecimalFormat twoDForm = new DecimalFormat("#.#");
            String.format("%.2f", d);
            Log.e("normal" + d, "" + String.format("%.2f", d));
            // Log.e("double"+d,""+Double.valueOf(String.format("%.2f", d)));


            return Double.valueOf(twoDForm.format(d));


        } catch (NumberFormatException e) {
            e.printStackTrace();
            return d;
        }
        // return double.format("%.2f", d);
    }

    /**
     * @param context
     * @param url
     */
    public static void openLink(final Activity context, final String url) {

        try {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            context.startActivity(i);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Please check your Web Address or may be your device can not perform this action.", Toast.LENGTH_SHORT).show();
        }
    }

    public static void sendEmail(final Activity activity, String data) {
        try {

            /* Create the Intent */
            final Intent emailIntent = new Intent(Intent.ACTION_SEND);

            /* Fill it with Data */
            emailIntent.setType("text/html");
            emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{});
            // emailIntent.putExtra(Intent.EXTRA_SUBJECT, "211 Resource");
            //emailIntent.putExtra(Intent.EXTRA_TEXT, data);
            emailIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(data));
            /* Send it off to the Activity-Chooser */
            activity.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(activity, "Can not perform this action on this device", Toast.LENGTH_SHORT).show();
        }
    }

    public static void sendEmailTo(final Activity activity, String data, String email) {
        try {
            /* Create the Intent */

            final Intent emailIntent = new Intent(Intent.ACTION_SEND);

            /* Fill it with Data */
            emailIntent.setType("text/plain");
            emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "You have been referred to Kathryn’s App!");
            emailIntent.putExtra(Intent.EXTRA_TEXT, data);
            //emailIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(data));
            /* Send it off to the Activity-Chooser */
            activity.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(activity, "Can not perform this action on this device", Toast.LENGTH_SHORT).show();
        }
    }


    public static void sendViaSms(Context mContext, String data) {
        try {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) //At least KitKat
            {
                String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(mContext); //Need to change the build to API 19

                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");
                sendIntent.putExtra(Intent.EXTRA_TEXT, data);

                if (defaultSmsPackageName != null)//Can be null in case that there is no default, then the user would be able to choose any app that support this intent.
                {
                    sendIntent.setPackage(defaultSmsPackageName);
                }
                mContext.startActivity(sendIntent);

            } else //For early versions, do what worked for you before.
            {
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setData(Uri.parse("sms:"));
                sendIntent.putExtra("sms_body", data);
                mContext.startActivity(sendIntent);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(mContext, "Can not perform this action on this device", Toast.LENGTH_SHORT).show();
        }
    }


    public static void sendViaSms(Context mContext, String data, String smsNumber) {
//        try {
//
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) //At least KitKat
//            {
//                String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(mContext); //Need to change the build to API 19
//
//                Intent sendIntent = new Intent(Intent.ACTION_SEND);
//                sendIntent.setType("text/plain");
//                sendIntent.putExtra(Intent.EXTRA_TEXT, data);
//
//                if (defaultSmsPackageName != null)//Can be null in case that there is no default, then the user would be able to choose any app that support this intent.
//                {
//                    sendIntent.setPackage(defaultSmsPackageName);
//                }
//                mContext.startActivity(sendIntent);
//
//            } else //For early versions, do what worked for you before.
//            {
//                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
//                sendIntent.setData(Uri.parse("sms:"));
//                sendIntent.putExtra("sms_body", data);
//                mContext.startActivity(sendIntent);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            Toast.makeText(mContext, "Can not perform this action on this device", Toast.LENGTH_SHORT).show();
//        }

        try {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) //At least KitKat
            {

                String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(mContext); //Need to change the build to API 19

                Uri sms_uri = Uri.parse("smsto:" + smsNumber);
                Intent sms_intent = new Intent(Intent.ACTION_SENDTO, sms_uri);
                //   sms_intent.setType("text/plain");
                sms_intent.putExtra(Intent.EXTRA_TEXT, data);

             /*   if (defaultSmsPackageName != null)//Can be null in case that there is no default, then the user would be able to choose any app that support this intent.
                {
                    sms_intent.setPackage(defaultSmsPackageName);
                }*/
                mContext.startActivity(sms_intent);

            } else //For early versions, do what worked for you before.
            {
                Uri sms_uri = Uri.parse("smsto:" + smsNumber);
                Intent sendIntent = new Intent(Intent.ACTION_SENDTO, sms_uri);
                sendIntent.setData(Uri.parse("sms:"));
                sendIntent.putExtra("sms_body", data);
                mContext.startActivity(sendIntent);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(mContext, "Can not perform this action on this device", Toast.LENGTH_SHORT).show();
        }
    }


    public static Boolean validateEmail(final String email) {
        final String EMAIL_PATTERN = Patterns.EMAIL_ADDRESS.toString();
        //"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @SuppressLint("MissingPermission")
    public static void call(Activity activity, String phoone) {
        try {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + phoone));
            activity.startActivity(callIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static String[] getArrayFromList(List<String> list) {
        String array[] = new String[list.size()];
        for (int j = 0; j < list.size(); j++) {
            array[j] = list.get(j);
        }
        return array;
    }

    public static String[] getArrayFromList(ArrayList<String> list) {
        String array[] = new String[list.size()];
        for (int j = 0; j < list.size(); j++) {
            array[j] = list.get(j);
        }
        return array;
    }

    public static String getCommaSepratedString(ArrayList<Integer> list) {
        if (list.size() > 0) {
            StringBuilder commaSepValueBuilder = new StringBuilder();
            for (int i = 0; i < list.size(); i++) {
                //append the value into the builder
                commaSepValueBuilder.append(list.get(i));
                //if the value is not the last element of the list
                //then append the comma(,) as well
                if (i != list.size() - 1) {
                    commaSepValueBuilder.append(", ");
                }
            }

            return commaSepValueBuilder.toString();
        } else {
            return null;
        }

    }

    public static String getCommaSepratedStrings(ArrayList<String> list) {
        if (list.size() > 0) {
            StringBuilder commaSepValueBuilder = new StringBuilder();
            for (int i = 0; i < list.size(); i++) {
                //append the value into the builder
                commaSepValueBuilder.append(list.get(i));
                //if the value is not the last element of the list
                //then append the comma(,) as well
                if (i != list.size() - 1) {
                    commaSepValueBuilder.append(",");
                }
            }

            return commaSepValueBuilder.toString();
        } else {
            return null;
        }

    }

    public static String getMonth(String dateMonth) {
        String dateM = "";
        if (dateMonth.equalsIgnoreCase("1")) {
            dateM = "January";
        } else if (dateMonth.equalsIgnoreCase("2")) {
            dateM = "February";
        } else if (dateMonth.equalsIgnoreCase("3")) {
            dateM = "March";
        } else if (dateMonth.equalsIgnoreCase("4")) {
            dateM = "April";
        } else if (dateMonth.equalsIgnoreCase("5")) {
            dateM = "May";
        } else if (dateMonth.equalsIgnoreCase("6")) {
            dateM = "June";
        } else if (dateMonth.equalsIgnoreCase("7")) {
            dateM = "July";
        } else if (dateMonth.equalsIgnoreCase("8")) {
            dateM = "August";
        } else if (dateMonth.equalsIgnoreCase("9")) {
            dateM = "September";
        } else if (dateMonth.equalsIgnoreCase("10")) {
            dateM = "October";
        } else if (dateMonth.equalsIgnoreCase("11")) {
            dateM = "November";
        } else if (dateMonth.equalsIgnoreCase("12")) {
            dateM = "December";
        }

        return dateM;
    }


    public static String getMonthInteger(String dateMonth) {
        String dateM = "";
        if (dateMonth.equalsIgnoreCase("January")) {
            dateM = "1";
        } else if (dateMonth.equalsIgnoreCase("February")) {
            dateM = "2";
        } else if (dateMonth.equalsIgnoreCase("March")) {
            dateM = "3";
        } else if (dateMonth.equalsIgnoreCase("April")) {
            dateM = "4";
        } else if (dateMonth.equalsIgnoreCase("May")) {
            dateM = "5";
        } else if (dateMonth.equalsIgnoreCase("June")) {
            dateM = "6";
        } else if (dateMonth.equalsIgnoreCase("July")) {
            dateM = "7";
        } else if (dateMonth.equalsIgnoreCase("August")) {
            dateM = "8";
        } else if (dateMonth.equalsIgnoreCase("September")) {
            dateM = "9";
        } else if (dateMonth.equalsIgnoreCase("October")) {
            dateM = "10";
        } else if (dateMonth.equalsIgnoreCase("November")) {
            dateM = "11";
        } else if (dateMonth.equalsIgnoreCase("December")) {
            dateM = "12";
        }

        return dateM;
    }

    public static Typeface opensansLightFont(Context appContext) { // accessing
        // fonts
        // functions
        if (sanslight == null) {
            sanslight = Typeface.createFromAsset(appContext.getAssets(),
                    "fonts/OpenSans-Light.ttf");
        }
        return sanslight;
    }

    /**
     * @param dp
     * @param context
     * @return
     */
    public static int dpToPx(int dp, Context context) {
        DisplayMetrics displayMetrics = context.getApplicationContext().getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    /**
     * @param px
     * @param context
     * @return
     */
    public static int pxToDp(int px, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int dp = Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return dp;
    }


    public static String getLocalTime(String s) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.S'Z'");

        // Log.i("dateConverSion", "" + date);
        // dateString = "03/26/2012 11:49:00 AM";
        String result = "";
        //SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date convertedDate = new Date();
        try {
            convertedDate = formatter.parse(s);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SimpleDateFormat day = new SimpleDateFormat("EEEE");
        SimpleDateFormat month = new SimpleDateFormat("MMMM");
        SimpleDateFormat date_ = new SimpleDateFormat("d");
        SimpleDateFormat hour = new SimpleDateFormat("hh");
        SimpleDateFormat mint = new SimpleDateFormat("mm");
        SimpleDateFormat am_pm = new SimpleDateFormat("aa");
        SimpleDateFormat year = new SimpleDateFormat("yyyy");


        String dayOfTheWeek = day.format(convertedDate);
        // time,month_,date_shown_converted
        String time = hour.format(convertedDate) + ":" + mint.format(convertedDate) + " " + am_pm.format(convertedDate);
        String month_ = month.format(convertedDate);
        String date_shown_converted = date_.format(convertedDate);
        String year_shown = year.format(convertedDate);

        //  result = dayOfTheWeek+", "+month_ + " " + date_shown_converted;
        result = date_shown_converted + " " + month_ + " " + year_shown;
        return time;
    }

    public static String DateTimeFormatChange(String val) {

        SimpleDateFormat src = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        SimpleDateFormat dest = new SimpleDateFormat("dd-MMM hh:mm a");
        Date date = null;
        try {
            date = src.parse(val);
        } catch (ParseException e) {
            //handle exception
        }
        String result = dest.format(date);


        return result;
    }

    /**
     * @param latitude
     * @param longitude
     * @param context
     * @return
     */
    public static Address getAddressObjectFromLocation(double latitude, double longitude, Activity context) {
       /* Geocoder geo = new Geocoder(context);
        List<Address> gotAddresses = null;
        try {

            gotAddresses = geo.getFromLocation(latitude, longitude, 5);
            Log.e("gotAddresses", "" + new Gson().toJson(gotAddresses));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (gotAddresses != null && gotAddresses.size() > 0) {
            Address address = (Address) gotAddresses.get(0);
            return address;
        }*/
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());

        List<Address> addressList = null;
        try {
            addressList = geocoder.getFromLocation(
                    latitude, longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addressList != null && addressList.size() > 0) {
            Address address = addressList.get(0);

            return address;
        }


        return null;
    }


    public static long getDateConverted(String data) {
        long mili = 0;
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
        try {
            Date date1 = formatter.parse(data);
            mili = date1.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return mili;
    }

    public static String getStringValue(Map<String, String> data, String key) {

        if (data.containsKey(key)) {
            return data.get(key);
        }

        return "";
    }

    public static int getIntValue(Map<String, String> data, String key) {
        if (data.containsKey(key)) {
            return Integer.parseInt(data.get(key));
        }
        return -1;
    }

    public static boolean getBoolValue(Map<String, String> data, String key) {
        if (data.containsKey(key)) {
            return Boolean.parseBoolean(data.get(key));
        }
        return false;
    }


    public static String getMyDateFormat(String date, String currentFormat, String format) {
        String finalDate;
        SimpleDateFormat curFormater = new SimpleDateFormat(currentFormat, Locale.ENGLISH);
        Date dateObj = null;
        try {
            dateObj = curFormater.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat postFormater;
        postFormater = new SimpleDateFormat(format,Locale.ENGLISH);
        if (dateObj != null)
            finalDate = postFormater.format(dateObj);
        else
            finalDate = "";
        return finalDate;
    }
    public static String phoneFormatter(String value) {
        String result = value;
        int lenghtTotal = value.length();
        if (lenghtTotal > 0) {
            ArrayList<String> list = new ArrayList<>();
            for (int k = 0; k < lenghtTotal; k++) {
                String s = String.valueOf(value.charAt(k));
                list.add(s);

            }

            list.add(3, "-");
            StringBuilder sb = new StringBuilder();
            for (String str : list) {
                sb.append(str).append(""); //separating contents using semi colon
            }

            result = sb.toString();

        }

        return result;

    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    public static boolean isFormatSupported(String file) {
        boolean supported = false;
        if (file.toString().endsWith(".jpg")) {
            supported = true;
        } else if (file.toString().endsWith(".3gp")) {
            supported = true;
        } else if (file.toString().endsWith(".mp4")) {
            supported = true;
        } else if (file.toString().endsWith(".mkv")) {
            supported = true;
        } else if (file.toString().endsWith(".png")) {
            supported = true;
        } else if (file.toString().endsWith(".webm")) {
            supported = true;
        } else if (file.toString().endsWith(".pdf")) {
            supported = true;
        } else if (file.toString().endsWith(".jpeg")) {
            supported = true;
        }


        return supported;
    }


    public static boolean isImage(String file) {
        boolean supported = false;
        if (file.toString().endsWith(".jpg")) {
            supported = true;
        } else if (file.toString().endsWith(".png")) {
            supported = true;
        } else if (file.toString().endsWith(".jpeg")) {
            supported = true;
        }


        return supported;
    }

    public static boolean isMedia(String file) {
        boolean supported = false;
        if (file.toString().endsWith(".3gp")) {
            supported = true;
        } else if (file.toString().endsWith(".mp4")) {
            supported = true;
        } else if (file.toString().endsWith(".mkv")) {
            supported = true;
        } else if (file.toString().endsWith(".webm")) {
            supported = true;
        }

        return supported;
    }


   /* public static String getChatSubString(String file){
        String supported=file;

        if(file.contains(":-")){
            int index=file.indexOf(":-");
            supported=file.substring(index+2);
        }

        return supported.trim();
    }*/


    public static String getCurrentDate() {

        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat mdformat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss",Locale.US);
        String strDate = mdformat.format(calendar.getTime());

        return strDate;
    }


    /**
     * @param activity
     * @param requiredWidth
     * @return
     */
    public static int getWindowDimension(Activity activity, boolean requiredWidth) {
        int measuredWidth = 0;
        int measuredHeight = 0;
        Point size = new Point();
        WindowManager w = activity.getWindowManager();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            w.getDefaultDisplay().getSize(size);
            measuredWidth = size.x;
            measuredHeight = size.y;
        } else {
            Display d = w.getDefaultDisplay();
            measuredWidth = d.getWidth();
            measuredHeight = d.getHeight();
        }

        Log.v("WindowDimension", "Width = " + measuredWidth + ", Height = " + measuredHeight);
        if (requiredWidth) {
            return measuredWidth;
        } else {
            return measuredHeight;
        }
    }


    public static String getFirstNameAndLastName(String name, boolean isFirstName) {
        String result = name;
        String[] splited = name.split("\\s+");
        if (splited.length == 1) {
            if (isFirstName) {
                result = splited[0];
            } else {
                result = "";
            }
        } else if (splited.length == 2) {
            if (isFirstName) {
                result = splited[0];
            } else {
                result = splited[1];
            }
        } else {
            if (isFirstName) {
                result = splited[0];
            } else {
                result = splited[1];
            }
        }

        return result;
    }


    public static void check_your_network_available(Context context) {

        DialogPopUps.alertPopUp((context), (context).getResources().getString(R.string.check_your_network),
                context.getString(R.string.alert),
                (context).getResources().getString(R.string.ok)
                , new CommonListeners.AlertCallBackInterface() {
                    @Override
                    public void neutralClick() {

                    }
                });
    }


    public static void hideSoftKeyboard(Context mContext) {
        try {
            InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(((Activity) mContext).getWindow().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void showSnackBar(Context context, String message) {
        final Snackbar snackbar = Snackbar.make(((Activity) context).findViewById(android.R.id.content), message, Snackbar.LENGTH_INDEFINITE);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.GREEN);
        snackbar.setAction((context).getResources().getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3000);
                    snackbar.dismiss();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

//
//    public static void setImage(Context context, ImageView imageView, String value) {
//        Picasso.with(context)
//                .load(value)
//                .resize(200, 200).placeholder(R.drawable.profile_grey)
//                .into(imageView);
//    }
//
//    public static void setImage1(Context context, ImageView imageView, String value) {
//        Picasso.with(context)
//                .load(value)
//                .resize(200, 200).placeholder(R.drawable.noimage)
//                .into(imageView);
//    }
//


    public static void setImage(Context context, ImageView imageView, String value, int height, int width) {
      /*  Picasso.with(context)
                .load(value)
                .resize(width, height).placeholder(R.drawable.noimage)
                .into(imageView);*/
        if (context != null) {

            Glide.with(context)
                    .load(value)
                    // .override(width,height)
                    //  .error(R.drawable.noimage).placeholder(R.drawable.noimage)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageView);
        }

    }


    //---SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static String utcToLocal(String dateTime) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = df.parse(dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        df.setTimeZone(TimeZone.getDefault());
        String formattedDate = df.format(date);
        return formattedDate;
    }

    public static String getDateMonthFromPickDeal(String s, boolean date_in_next_line) {
        String result = "";
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date convertedDate = new Date();
        try {
            convertedDate = df.parse(s);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        SimpleDateFormat month = new SimpleDateFormat("MMM");
        SimpleDateFormat date_ = new SimpleDateFormat("dd");


        String month_ = month.format(convertedDate);
        String date_shown_converted = date_.format(convertedDate);

        if (date_in_next_line) {
            result = date_shown_converted + "\n" + month_;
        } else {
            result = date_shown_converted + " " + month_;
        }

        return result;
    }

    public static String getDateFromPickDeal(String s) {
        String result = "";
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date convertedDate = new Date();
        try {
            convertedDate = df.parse(s);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        SimpleDateFormat date_ = new SimpleDateFormat("dd");
        String date_shown_converted = date_.format(convertedDate);
        result = date_shown_converted;
        return result;
    }


    public static String getTimeFromPickDeal(String s) {
        String result = "";
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date convertedDate = new Date();
        try {
            convertedDate = df.parse(s);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        SimpleDateFormat hour = new SimpleDateFormat("HH");
        SimpleDateFormat mint = new SimpleDateFormat("mm");

        String hour_s = hour.format(convertedDate);
        String mint_s = mint.format(convertedDate);
        result = hour_s + ":" + mint_s;
        return result + " " + Constants.HRS;
    }


    public static String getCustomDateString(String date) {
        SimpleDateFormat tmp = new SimpleDateFormat("d MMM");


        Date date1 = null;
        DateFormat inputFormat = new SimpleDateFormat("dd-MM-yyyy");
        try {
            date1 = inputFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String str = tmp.format(date1);
        str = str.substring(0, 1).toUpperCase() + str.substring(1);

        if (date1.getDate() > 10 && date1.getDate() < 14)
            str = str /*+ "th, "*/;
        else {
            if (str.endsWith("1")) str = str /*+ "st, "*/;
            else if (str.endsWith("2")) str = str /*+ "nd, "*/;
            else if (str.endsWith("3")) str = str /*+ "rd, "*/;
            else str = str /*+ "th, "*/;
        }

        // tmp = new SimpleDateFormat("yyyy");
        // str = str + tmp.format(date1);

        return str;
    }


    public static boolean isGpsEnabled(Application application) {
        final LocationManager lm = (LocationManager) application.getSystemService(Context.LOCATION_SERVICE);
        boolean gpsProviderEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);

        String s;
        if (gpsProviderEnabled) {
            s = "GPS : enable";
            return true;
        } else {
            s = "GPS : disable";
            return false;
        }
    }


    public static String getDate(int dayOfMonth) {
        if ((dayOfMonth) <= 9) {
            date = "0" + (dayOfMonth);
        } else {
            date = "" + (dayOfMonth);
        }
        return date;
    }

    public static String getMonth(int monthOfYear) {
        if (monthOfYear <= 9) {
            month = "0" + monthOfYear;
        } else {
            month = "" + monthOfYear;
        }
        return month;
    }

    public static void showTopSnackBar(Context context, View view, String msg) {
        TSnackbar snackbar = TSnackbar.make(view, msg, TSnackbar.LENGTH_LONG);
        // snackbar.setActionTextColor(Color.WHITE);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(ContextCompat.getColor(context, R.color.red));
        TextView textView = (TextView) snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }


//    public static Class getFlagClass(PushModel pushModel) {
//
//        if (pushModel.getUser_type().equalsIgnoreCase(Constants.HOST)) {
//
//            switch (pushModel.getStatus()) {
//
//                case Constants.PUBLISHED:
//                    SharedPreff.saveClassFromPush(pushModel);
//                    return HomeActivity.class;
//
//
//                default:
//                    return Splash.class;
//            }
//        }
//        return Splash.class;
//    }


    public static void setImage(Context context, ImageView imageView, String value) {
        Glide.with(context)
                .load(Constants.IMAGE_BASE_URL + value)

                // .error(R.drawable.noimage).placeholder(R.drawable.noimage)
                //  .override(200,200)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);
     /*   Picasso.with(context)
                .load(Constants.IMAGE_BASE_URL + value)
                .resize(200, 200).error(R.drawable.noimage).placeholder(R.drawable.noimageplaceholder)
                .into(imageView);*/
    }

    public static void setImageSec(Context context, ImageView imageView, String value) {
     /*   Picasso.with(context)
                .load(Constants.IMAGE_BASE_URL + value)
                .error(R.drawable.noimage).placeholder(R.drawable.noimageplaceholder)
                .into(imageView);*/
        Glide.with(context).load(Constants.IMAGE_BASE_URL + value)
                // .error(R.drawable.noimage).placeholder(R.drawable.noimage)
                .diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);
    }

    public static String getHours(int hourOfDay) {
        if (hourOfDay <= 9) {
            hours = "0" + hourOfDay;
        } else {
            hours = "" + hourOfDay;
        }
        return hours;
    }

    public static String getMinutes(int minute) {
        if (minute <= 9) {
            minutes = "0" + minute;
        } else {
            minutes = "" + minute;
        }
        return minutes;
    }

    public static String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encImage;
    }

    @SuppressLint("NewApi")
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }
}