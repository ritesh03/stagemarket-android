package com.stagemarket.utils;

import android.app.Dialog;
import android.os.Bundle;
import android.widget.EditText;


/**
 * Created by Muddassir on 6/25/15.
 * These listeners that are commonly used by activities and fragments
 */
public class CommonListeners {
   /* Server API Key help
    AIzaSyDylfVpqJq94RZvkM2Y0Yc9d4lS-q77gSA
    Sender ID help
    789182794435*/

    public interface AlertCallBackWithButtonsInterface {
        void positiveClick();

        void neutralClick();

        void negativeClick();
    }

    public interface AlertCallBackWithOptionalInterface {
        void positiveClick();

        void neutralClick();

        void negativeClick();

        void optional();
    }


    public interface AlertCallBackWithButtonsInterface2 {
        void positiveClick(int index);

        void neutralClick();

        void negativeClick();
    }


    public interface AlertCallBackWithReturnButtonsInterface {
        void positiveClick(Dialog dialog);

        void neutralClick();

        void negativeClick(Dialog dialog);
    }


    public interface AlertCallBackInterface {
        void neutralClick();
    }

    public interface AlertInputCallBackInterface {
        void positiveClick(EditText input);

        void negativeClick();
    }


    public interface AlertInputInterface {

        void positiveClick(String input);


    }

    public interface CloseDrawerListener {
        void closeDrawer();
    }


    public interface ChangeFragmentListener {
        void initiateFragment(int position, String title, Bundle args);
    }

    public interface ToggleClicked {
        void toggleClicked(boolean value);
    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }



}
