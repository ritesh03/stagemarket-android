package com.stagemarket.utils;

/**
 * Created by Muddassir on 6/22/15.
 */

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.stagemarket.R;


public class DialogPopUps {
    static Dialog dialog;

    public static void showAlertWithButtons(final Context activity, String titleText, String message
            , String positive, String negative, String neutral,
                                            boolean isConfirm, boolean hideCancelIcon, final CommonListeners.AlertCallBackWithButtonsInterface alertCallBackWithButtonsInterface) {
        try {
            hideDialog();
            final Dialog dialog = new Dialog(activity,
                    R.style.Theme_AppCompat_Translucent);

            // Configure dialog box
            dialog.setContentView(R.layout.alert_new_popup);
            WindowManager.LayoutParams layoutParams = dialog.getWindow()
                    .getAttributes();
            layoutParams.dimAmount = 0.6f;
            dialog.getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);

            // set the message
            TextView title = (TextView) dialog.findViewById(R.id.title);
            TextView dialog_message = (TextView) dialog.findViewById(R.id.dialog_message);
            dialog_message.setText(message);

            TextView action_ok = (TextView) dialog.findViewById(R.id.action_ok);
            TextView action_no = (TextView) dialog.findViewById(R.id.action_no);
            ImageView img_cancel = (ImageView) dialog.findViewById(R.id.img_cancel);
            // ImageView img_alert=(ImageView) dialog.findViewById(R.id.img_alert);


            action_ok.setVisibility(View.VISIBLE);
            action_no.setVisibility(View.VISIBLE);

            if (!TextUtils.isEmpty(titleText)) {
                title.setText(titleText);
            }
            if (!TextUtils.isEmpty(positive)) {
                action_ok.setText(positive);
            }
            if (!TextUtils.isEmpty(negative)) {
                action_no.setText(negative);
            }

            if (isConfirm) {
                // img_alert.setImageResource(R.drawable.confirmation_icon);
                img_cancel.setVisibility(View.GONE);
            } else {
                if (hideCancelIcon) {
                    img_cancel.setVisibility(View.GONE);
                }
            }


            action_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertCallBackWithButtonsInterface.positiveClick();
                    dialog.dismiss();
                }
            });

            action_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertCallBackWithButtonsInterface.negativeClick();
                    dialog.dismiss();
                }
            });

            img_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertCallBackWithButtonsInterface.negativeClick();
                    dialog.dismiss();
                }
            });

            try {
                dialog.show();
            } catch (Exception e) {
                Log.v("CPS_Dialog_crash", e.getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // hide the progress dialog
    public static void hideDialog() {
        try {
            if (dialog != null) {
                if (dialog.isShowing()) {
                    dialog.dismiss();

                }
                dialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Show the circular progress bar as loading with the message
     *
     * @param activity on which it is to be displayed
     * @param message  that is to be shown
     */
    public static void showProgressDialog(final Context activity, String message) {
        try {
            hideDialog();
            dialog = new Dialog(activity,
                    R.style.Theme_AppCompat_Translucent);

            // Configure dialog box
            dialog.setContentView(R.layout.progress_layout);
            WindowManager.LayoutParams layoutParams = dialog.getWindow()
                    .getAttributes();
            layoutParams.dimAmount = 0.6f;
            dialog.getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);

            // set progress drawable
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                ProgressBar progressBar = (ProgressBar) dialog.findViewById(R.id.circular_progress);
                progressBar.setIndeterminateDrawable(activity.getResources().getDrawable(R.drawable.progress));
            }

            // set the message
            if (!message.isEmpty()) {
                final TextView dialogMsg = (TextView) dialog.findViewById(R.id.progress_msg);
                dialogMsg.setText(message);
            }

            try {
                dialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void alertPopUp(final Context activity, String message) {
        try {
            hideDialog();
            dialog = new Dialog(activity,
                    R.style.Theme_AppCompat_Translucent);

            // Configure dialog box
            dialog.setContentView(R.layout.custom_dialog_neutral_layout);
            WindowManager.LayoutParams layoutParams = dialog.getWindow()
                    .getAttributes();
            layoutParams.dimAmount = 0.6f;
            dialog.getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);

            // set the message
            TextView dialogMessage = (TextView) dialog.findViewById(R.id.dialog_message);
            dialogMessage.setText(message);

            TextView actionNeutral = (TextView) dialog.findViewById(R.id.action_neutral);
            actionNeutral.setText(R.string.ok);
            actionNeutral.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            try {
                dialog.show();
            } catch (Exception e) {
                Log.v("CPS_Dialog_crash", e.getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void alertPopUp(final Context activity, String message, String titleval,
                                  String neutral
            , final CommonListeners.AlertCallBackInterface alertCallBackInterface) {
        try {
            hideDialog();
            dialog = new Dialog(activity,
                    R.style.Theme_AppCompat_Translucent);

            // Configure dialog box
            dialog.setContentView(R.layout.custom_dialog_neutral_layout);
            WindowManager.LayoutParams layoutParams = dialog.getWindow()
                    .getAttributes();

            layoutParams.dimAmount = 0.6f;
            dialog.getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);

            // set the message
            TextView dialogMessage = (TextView) dialog.findViewById(R.id.dialog_message);
            dialogMessage.setText(message);

            TextView title = (TextView) dialog.findViewById(R.id.title);
            title.setText(titleval);
            TextView actionNeutral = (TextView) dialog.findViewById(R.id.action_neutral);
            actionNeutral.setText(neutral);

            actionNeutral.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertCallBackInterface.neutralClick();
                    dialog.dismiss();
                }
            });

            try {
                dialog.show();
            } catch (Exception e) {
                Log.v("CPS_Dialog_crash", e.getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("CPS_Dialog_crash", e.getMessage());
        }
    }

    public static void openCamera(final Context activity
            , final CommonListeners.AlertCallBackWithButtonsInterface alertCallBackInterface) {
        try {
            hideDialog();

            dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.setContentView(R.layout.camera_gallery_popup);
            // Configure dialog box
            // Animation animation = AnimationUtils.loadAnimation(context, R.anim.bottom_up_anim);
            dialog.show();

            TextView camera_text = (TextView) dialog.findViewById(R.id.camera_text);

            // RelativeLayout dialog_rel = (RelativeLayout) dialogBox.findViewById(R.id.dialog_rel);

            TextView open_gallery_text = (TextView) dialog.findViewById(R.id.open_gallery_text);
            //remove_photo_text= (TextView) dialogBox.findViewById(R.id.remove_photo_text);
            TextView cancel_text = (TextView) dialog.findViewById(R.id.cancel_text);

            cancel_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    dialog.dismiss();
                    alertCallBackInterface.neutralClick();

                }
            });

            //remove_photo_text.setVisibility(View.GONE);
            camera_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    alertCallBackInterface.negativeClick();

                }
            });

            open_gallery_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    alertCallBackInterface.positiveClick();


                }
            });
            try {
                dialog.show();
            } catch (Exception e) {
                Log.v("CPS_Dialog_crash", e.getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.v("CPS_Dialog_crash", e.getMessage());
        }
    }


    public static void confirmationPopUp(final Context activity, String title,
                                         String message
            , final CommonListeners.AlertCallBackInterface alertCallBackInterface) {
        try {
            hideDialog();
            final Dialog dialog = new Dialog(activity,
                    R.style.Theme_AppCompat_Translucent);

            // Configure dialog box
            dialog.setContentView(R.layout.confirmation_popup);
            WindowManager.LayoutParams layoutParams = dialog.getWindow()
                    .getAttributes();

            layoutParams.dimAmount = 0.6f;
            dialog.getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);

            // set the message
            TextView dialogMessage = (TextView) dialog.findViewById(R.id.dialog_message);
            TextView titleText = (TextView) dialog.findViewById(R.id.title);
            TextView actionNeutral = (TextView) dialog.findViewById(R.id.action_neutral);
            // ImageView img=(ImageView) dialog.findViewById(R.id.img);

            if (!TextUtils.isEmpty(message)) {
                dialogMessage.setText(message);

            }
            if (!TextUtils.isEmpty(title)) {
                titleText.setText(title);
                if (title.equalsIgnoreCase(activity.getResources().getString(R.string.alert))) {
                    // img.setImageResource(R.drawable.alert_icon);
                }

            }

            actionNeutral.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertCallBackInterface.neutralClick();
                    dialog.dismiss();
                }
            });

            try {
                dialog.show();
            } catch (Exception e) {
                Log.v("CPS_Dialog_crash", e.getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.v("CPS_Dialog_crash", e.getMessage());
        }
    }
}

