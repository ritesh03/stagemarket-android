package com.stagemarket.utils;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import fr.quentinklein.slt.LocationTracker;
import fr.quentinklein.slt.TrackerSettings;



public class LocationTrackerClass {
    public LocationTracker myTracker;
    public Location location_;

    public LocationTrackerClass(Context context){
        TrackerSettings settings =
                new TrackerSettings()
                        .setUseGPS(true)
                        .setUseNetwork(true)
                        .setUsePassive(false)
                        .setTimeBetweenUpdates(30 * 60 * 1000)
                        .setMetersBetweenUpdates(100);
        myTracker = new LocationTracker(context, settings) {

            @Override
            public void onLocationFound(Location location) {
                location_=location;
                // Do some stuff when a new location has been found.
                if(location !=null){
                    SharedPreff.saveLat(""+location.getLatitude());
                    SharedPreff.saveLng(""+location.getLongitude());

                    Log.e("LAT",""+location.getLatitude());
                    Log.e("LONG",""+location.getLongitude());


                }
            }

            @Override
            public void onTimeout() {
                startLocationTracking();
            }
        };

    }


    public void startLocationTracking(){
        if(myTracker != null) {
            myTracker.startListening();
        }
    }

    public void stopLocationTracking(){
        if(myTracker != null) {
            myTracker.stopListening();
        }
    }


    public Location getLocation(){
        return location_;
    }

}
