package com.stagemarket.utils;


import com.stagemarket.facebook.SocialLoginData;
import com.stagemarket.pojo.SaveLoginDetailsModel;
import com.stagemarket.pojo.categorySell.Datum;
import com.stagemarket.pojo.categorySell.SubCategory;
import com.stagemarket.pojo.myOrders.SubSecondCategory;

import java.util.ArrayList;

import io.paperdb.Paper;


public class SharedPreff {


    private static final String StageMarket = "StageMarket App";
    private static final String LOGIN_DETAILS = "login_details";
    private static final String FCM_TOKEN = "fcm_token";
    public static final String NOTIFICATION_TOKEN = "notification_token";
    public static final String ACCOUNT_DETAILS = "account_details";
    public final static String OTP_CODE = "otp_code";
    public final static String PICK_DEAL_OBJECT = "pick_deal_object";
    public final static String SELECTED_FIELDS = "selected_fields";
    private static final String COMPANY_CATEGORIES = "company_categories";
    private static final String FILTER_TAG = "filter_tag";
    private static final String SAVE_CLASS_FROM_PUSH = "saveClassFromPush";
    private static final String PUSH = "push";
    private static final String CAT_NAME = "Category Name";
    private static final String SUBCAT_NAME = "SubCategory Name";
    private static final String SELL_PHOTOS = "Sell photos list";
    private static final String SELL_PHOTOS_BEAN = "Sell photos_bean";
    private static final String SEC_SUBCAT_NAME = "SEC_SUBCAT_NAME";


    public static void saveLat(String lat) {
        Paper.book().write(Constants.LAT, lat);
    }

    public static void saveLng(String lng) {
        Paper.book().write(Constants.LNG, lng);
    }

    public static void saveTempLat(String lat) {
        Paper.book().write(Constants.LAT_TEMP, lat);
    }

    public static void saveTempLng(String lng) {
        Paper.book().write(Constants.LNG_TEMP, lng);
    }


    public static void saveSocialUser(SocialLoginData socialLogin) {
        Paper.book().write(Constants.SOCIAL_USER, socialLogin);
    }

    public static SocialLoginData getSocialUser() {
        return Paper.book().read(Constants.SOCIAL_USER, null);
    }


//    public static void saveDeviceToken(String token) {
//        Paper.book().write(Constants.DEVICE_TOKEN, token);
//    }
//
//    public static String getDeviceToken() {
//        return Paper.book().read(Constants.DEVICE_TOKEN, null);
//    }
//

    //
//
//    public static void saveSocialUser(SocialLoginData socialLogin) {
//        Paper.book(HOST).write(Constants.SOCIAL_USER, socialLogin);
//    }
//
//    public static SocialLoginData getSocialUser() {
//        return Paper.book(HOST).read(Constants.SOCIAL_USER, null);
//    }
//
//
//
//
    public static String getLat() {
        return Paper.book().read(Constants.LAT, "");
    }

    public static String getLng() {
        return Paper.book().read(Constants.LNG, "");
    }

    public static String getLatTemp() {
        return Paper.book().read(Constants.LAT_TEMP, "");
    }

    public static String getLngTemp() {
        return Paper.book().read(Constants.LNG_TEMP, "");
    }


//    public static void saveSignUpResponse(SignUpResponse signUpResponse) {
//        Paper.book().write(Constants.SIGNUP_RESPONSE, signUpResponse);
//    }
//
//
//    public static SignUpResponse getSignUpResponse() {
//        return Paper.book().read(Constants.SIGNUP_RESPONSE, null);
//    }


//    public static void saveLoginResponse(LoginResponse loginResponse) {
//        Paper.book().write(Constants.LOGIN_RESPONSE, loginResponse);
//    }
//
//    public static LoginResponse getLoginResponse() {
//        return Paper.book().read(Constants.LOGIN_RESPONSE, null);
//    }


    // PushNotification Token
    public static void setNotificationToken(String refreshedToken) {
        Paper.book(FCM_TOKEN).write(NOTIFICATION_TOKEN, refreshedToken);
    }


    public static String getNotificationToken() {
        return Paper.book(FCM_TOKEN).read(NOTIFICATION_TOKEN, "");
    }


    public static void saveLoginDetails(SaveLoginDetailsModel saveLoginDetailsModel) {
        Paper.book().write(LOGIN_DETAILS, saveLoginDetailsModel);
    }

    public static SaveLoginDetailsModel getLoginDetailsModel() {
        return Paper.book().read(LOGIN_DETAILS, null);
    }

    public static void clearSharedPrefs() {
        Paper.book().destroy();
    }

    public static void saveCatName(Datum name) {
        Paper.book().write(CAT_NAME, name);
    }

    public static Datum getCatName() {
        return Paper.book().read(CAT_NAME, null);
    }

    public static void saveSubCatName(SubCategory name) {
        Paper.book().write(SUBCAT_NAME, name);
    }

    public static SubCategory getSubCatName() {
        return Paper.book().read(SUBCAT_NAME, null);
    }


    public static void saveSubSecCatName(SubSecondCategory name) {
        Paper.book().write(SEC_SUBCAT_NAME, name);
    }

    public static SubSecondCategory getSubSecCatName() {
        return Paper.book().read(SEC_SUBCAT_NAME, null);
    }

    public static void saveSellPhotos(ArrayList<String> filePathList) {
        Paper.book(SELL_PHOTOS_BEAN).write(SELL_PHOTOS, filePathList);
    }

    public static ArrayList<String> getSellPhotos() {
        return Paper.book(SELL_PHOTOS_BEAN).read(SELL_PHOTOS, new ArrayList<String>());
    }

    public static void clearSellPhotos() {
        Paper.book(SELL_PHOTOS_BEAN).destroy();
    }

//
//
//    //Login save details
//    public static void saveLoginDetails(SaveLoginDetailsModel saveLoginDetailsModel) {
//        Paper.book(HOST).write(LOGIN_DETAILS, saveLoginDetailsModel);
//    }
//
//    public static SaveLoginDetailsModel getLoginDetailsModel() {
//        return Paper.book(HOST).read(LOGIN_DETAILS, null);
//    }
//
//
//    // PushNotification Token
//    public static void setNotificationToken(String refreshedToken) {
//        Paper.book(FCM_TOKEN).write(NOTIFICATION_TOKEN, refreshedToken);
//    }
//
//
//    public static String getNotificationToken() {
//        return Paper.book(FCM_TOKEN).read(NOTIFICATION_TOKEN, "");
//    }
//
//
//    // Social Login Case---> Use CreateAccountPresenterImp
//    public static void setSaveCreateAccountDataToOtpScreen(SaveCreateAccountDataToOTPScreen saveCreateAccountDataToOTPScreen) {
//        Paper.book(HOST).write(ACCOUNT_DETAILS, saveCreateAccountDataToOTPScreen);
//
//    }
//
//    public static SaveCreateAccountDataToOTPScreen saveCreateAccountDataToOTPScreen() {
//        return Paper.book(HOST).read(ACCOUNT_DETAILS, null);
//    }
//
//
//    public static void savePickDealObject(PickDealObject pickDealObject) {
//        Paper.book(HOST).write(PICK_DEAL_OBJECT, pickDealObject);
//    }
//
//    public static PickDealObject getPickDealObject() {
//        return Paper.book(HOST).read(PICK_DEAL_OBJECT, null);
//    }
//
//    public static void saveOTP(String token) {
//        Paper.book(HOST).write(OTP_CODE, token);
//    }
//
//    public static String getOTP() {
//        return Paper.book(HOST).read(OTP_CODE, null);
//    }
//
//
//    public static void setCompanyCategoryList(ArrayList<Category> categoryArrayList) {
//        Paper.book(HOST).write(COMPANY_CATEGORIES, categoryArrayList);
//    }
//
//    public static ArrayList<Category> getCompanyCategoryList() {
//        return Paper.book(HOST).read(COMPANY_CATEGORIES, new ArrayList<Category>());
//    }
//
//
//    public static void clearPreferences() {
//        try {
//            Paper.book(HOST).destroy();
//        } catch (Exception ex) {
//
//        }
//    }
//
//
//
//    public static void saveCommonSelectedFields(ArrayList<String> fields) {
//        Paper.book(FILTER_TAG).write(SELECTED_FIELDS, fields);
//    }
//
//    public static ArrayList<String> getCommonSelectedFields() {
//        return Paper.book(FILTER_TAG).read(SELECTED_FIELDS, null);
//    }
//
//    public static void clearFilterTags() {
//        try {
//            Paper.book(FILTER_TAG).destroy();
//        } catch (Exception ex) {
//
//        }
//    }
//
//
//
//
//    public static void saveStripeCUstomerID(String stripeId) {
//        Paper.book(HOST).write(Constants.STRIPE_ID, stripeId);
//    }
//
//    public static String getStripeCUstomerID() {
//        return Paper.book(HOST).read(Constants.STRIPE_ID, "");
//    }
//
//
//    public static void saveClassFromPush(PushModel pushModel) {
//        Paper.book(PUSH).write(SAVE_CLASS_FROM_PUSH, pushModel);
//    }
//
//    public static PushModel getClassFromPush() {
//        return Paper.book(PUSH).read(SAVE_CLASS_FROM_PUSH, null);
//    }
//
//    public static void clearPushPreferences() {
//        try {
//            Paper.book(PUSH).destroy();
//        } catch (Exception e) {
//
//        }
//    }


}
