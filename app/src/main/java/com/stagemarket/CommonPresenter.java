package com.stagemarket;

/**
 * Created by Jarnail Singh on 12/7/2017.
 */

public interface CommonPresenter {
    public void subscribe();
    public void unSubscribe();

}
