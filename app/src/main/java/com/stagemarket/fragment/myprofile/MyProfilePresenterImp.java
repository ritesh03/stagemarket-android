package com.stagemarket.fragment.myprofile;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentActivity;

import com.stagemarket.AppClass;
import com.stagemarket.R;
import com.stagemarket.pojo.SaveLoginDetailsModel;
import com.stagemarket.pojo.myProfile.MyProfileResponse;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.SharedPreff;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MyProfilePresenterImp implements  MyProfilePresenter {

    SaveLoginDetailsModel saveLoginDetailsModel;
    MyProfileView myProfileView;
    Context context;
    private static final String TAG = MyProfilePresenterImp.class.getSimpleName();
    private NetWorkingService netWorkingService;
    private final CompositeDisposable disposables = new CompositeDisposable();
    FragmentActivity activity;


    public MyProfilePresenterImp(MyProfileView myProfileView, FragmentActivity activity) {
        this.myProfileView = myProfileView;
        this.activity = activity;
        netWorkingService = AppClass.getInstance().getNetworkService();
        this.context = AppClass.getInstance().getApplicationContext();
        saveLoginDetailsModel = SharedPreff.getLoginDetailsModel();
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        if (disposables != null) {
            disposables.clear();
        }
        myProfileView = null;
        netWorkingService = null;
    }

    @Override
    public void makeMyProfileRequest() {
        if (CommonUtils.isConnectedToInternet(context)) {

            myProfileView.showProgressDialog();


            netWorkingService.getAPI().myProfile(
                    saveLoginDetailsModel.getId())
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<MyProfileResponse>() {


                        @Override
                        public void onSubscribe(Disposable d) {
                            disposables.add(d);
                        }

                        @Override
                        public void onNext(MyProfileResponse myProfileResponse) {
                            myProfileView.hideProgressDialog();
                            if (myProfileView != null) {
                                myProfileView.hideProgressDialog();
                                if (myProfileResponse.getResult().equalsIgnoreCase(Constants.SUCCESS)) {
                                    myProfileView.onMyProfileResponse(myProfileResponse);

                                } else {
                                    myProfileView.showErrorDialog(myProfileResponse.getMessage());
                                }
                            }

                        }

                        @Override
                        public void onError(Throwable e) {
                            myProfileView.hideProgressDialog();
                            myProfileView.showErrorDialog(context.getResources().getString(R.string.something_went_wrong));
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        } else {
            CommonUtils.showTopSnackBar(context,
                    (activity).findViewById(android.R.id.content), context.getString(R.string.please_check_internet));
        }
    }
}
