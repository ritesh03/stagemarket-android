package com.stagemarket.fragment.myprofile;

import com.stagemarket.CommonPresenter;

public interface MyProfilePresenter extends CommonPresenter {
    void makeMyProfileRequest();
}
