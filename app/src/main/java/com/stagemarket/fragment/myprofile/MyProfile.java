package com.stagemarket.fragment.myprofile;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.stagemarket.R;
import com.stagemarket.activity.login.LoginActivity;
import com.stagemarket.activity.myOrders.MyOrdersActivity;
import com.stagemarket.activity.signUp.SignUpActivity;
import com.stagemarket.activity.viewProfile.ViewProfileActivity;
import com.stagemarket.activity.yourAccount.YourAccountActivity;
import com.stagemarket.fragment.BaseFragment;
import com.stagemarket.pojo.SaveLoginDetailsModel;
import com.stagemarket.pojo.myProfile.Data;
import com.stagemarket.pojo.myProfile.MyProfileResponse;
import com.stagemarket.utils.CommonListeners;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.DialogPopUps;
import com.stagemarket.utils.SharedPreff;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MyProfile extends BaseFragment implements MyProfileView {

    @BindView(R.id.tv_logout)
    TextView tvLogout;
    @BindView(R.id.img_profile)
    ImageView imgProfile;
    @BindView(R.id.tv_profile_name)
    TextView tvProfileName;
    @BindView(R.id.tv_myorders)
    TextView tvMyorders;
    @BindView(R.id.tv_editProfile)
    TextView tvEditProfile;
    @BindView(R.id.tv_wishlist)
    TextView tv_wishlist;
    @BindView(R.id.tv_share)
    TextView tvShare;
    @BindView(R.id.tv_rateUs)
    TextView tvRateUs;
    @BindView(R.id.tv_support)
    TextView tvSupport;
    @BindView(R.id.tv_helpSupport)
    TextView tvHelpSupport;
    @BindView(R.id.tv_terms_and_conditions)
    TextView tvTermsAndConditions;
    Unbinder unbinder;
    @BindView(R.id.swipeMyProfile)
    SwipeRefreshLayout swipeMyProfile;
    MyProfilePresenter myProfilePresenter;
    MyProfileResponse myProfileResponseTag;
    SaveLoginDetailsModel saveLoginDetailsModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View screen = inflater.inflate(R.layout.profile_view, container, false);
        myProfilePresenter = new MyProfilePresenterImp(this, getActivity());

        unbinder = ButterKnife.bind(this, screen);
        saveLoginDetailsModel = SharedPreff.getLoginDetailsModel();
        tv_wishlist=screen.findViewById(R.id.tv_wishlist);
        setUpViews();

        return screen;
    }

    private void setUpViews() {
        tv_wishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MyOrdersActivity.class).putExtra("type","wishlist"));
            }
        });
        tvProfileName.setText(CommonUtils.capWordCase(saveLoginDetailsModel.getFirst_name()) + " " +
                CommonUtils.capWordCase(saveLoginDetailsModel.getLast_name()));

        if (!saveLoginDetailsModel.getImage().equalsIgnoreCase("")) {
            CommonUtils.setImage(getActivity(), imgProfile, Constants.IMAGE_BASE_URL
                    + saveLoginDetailsModel.getImage(), 200, 200);
        }else{
            imgProfile.setImageResource(R.drawable.dummy_profile);
        }

        swipeMyProfile.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeMyProfile.setRefreshing(false);
            }
        });
    }

    @OnClick(R.id.tv_myorders)
    public void onMyOrdersClick() {
        startActivity(new Intent(getActivity(), MyOrdersActivity.class).putExtra("type","orders"));
    }





    @OnClick(R.id.tv_editProfile)
    public void onEditProfileClick() {
        startActivity(new Intent(getActivity(), YourAccountActivity.class)
                .putExtra(Constants.MYPROFILE_DATA, new Gson().toJson(myProfileResponseTag)));
    }

    @OnClick(R.id.tv_logout)
    public void OnClickLogout() {

        DialogPopUps.showAlertWithButtons(getActivity(), getString(R.string.alert), getString(R.string.are_you_sure_you_want_to_logout), getString(R.string.Yes), getString(R.string.no),
                "", false, false, new CommonListeners.AlertCallBackWithButtonsInterface() {
                    @Override
                    public void positiveClick() {
                        // Toast.makeText(getActivity(),"Yes",Toast.LENGTH_SHORT).show();
                        SharedPreff.clearSharedPrefs();
                        Intent splash = new Intent(getActivity(), LoginActivity.class);
                        splash.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(splash);
                        getActivity().finish();
                    }

                    @Override
                    public void neutralClick() {

                    }

                    @Override
                    public void negativeClick() {
                        //     Toast.makeText(getActivity(),"No",Toast.LENGTH_SHORT).show();

                    }
                });
    }

    @OnClick(R.id.tv_myorders)
    public void OnClickMyOrders() {

    }


    @OnClick(R.id.tv_share)
    public void OnClickShare() {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");// Plain format text
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Hi, I thought you might be interested in selling and buying products near you. Download the free StageMarket app available in iOS and Android.\nAndroid: https://play.google.com/store/apps/details?id=com.stagemarket\niOS: https://itunes.apple.com/us/app/stagemarket/id1432038520?ls=1&mt=8");
        startActivity(Intent.createChooser(sharingIntent, "Share Text Using"));
    }

    @OnClick(R.id.tv_rateUs)
    public void OnClickRateUs() {

    }

    @OnClick(R.id.tv_helpSupport)
    public void OnClickHelpSupport() {
    }

    @OnClick(R.id.tv_terms_and_conditions)
    public void OnClickTermsAndConditions() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://206.189.23.147/StageMarket/public/term_and_condition"));
        startActivity(browserIntent);
    }

    @Override
    public void showProgressDialog() {
        swipeMyProfile.setRefreshing(true);
        // DialogPopUps.showProgressDialog(getActivity(), getResources().getString(R.string.please_wait));
    }

    @Override
    public void showErrorDialog(String msg) {
        CommonUtils.showTopSnackBar(getActivity(), getActivity().findViewById(android.R.id.content), msg);
    }

    @Override
    public void showMessageDialog(String msg) {

    }

    @Override
    public void hideProgressDialog() {
        //  DialogPopUps.hideDialog();
        if(swipeMyProfile!=null){
            if(swipeMyProfile.isRefreshing()){
                swipeMyProfile.setRefreshing(false);
            }
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onMyProfileResponse(MyProfileResponse myProfileResponse) {
//        StringBuilder sb = new StringBuilder(myProfileResponse.getData().getFirstName());
//        sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
//        tvProfileName.setText(getString(R.string.welcome) + " +" + sb.toString()
//                + " " + myProfileResponse.getData().getLastName());
        myProfileResponseTag = myProfileResponse;
        if(tvProfileName!=null){

            if(myProfileResponse!=null){
                if(myProfileResponse.getData().getFirstName()!=null){

                    tvProfileName.setText(CommonUtils.capWordCase(myProfileResponse.getData().getFirstName()) + " " +
                            CommonUtils.capWordCase(myProfileResponse.getData().getLastName()));
                }
            }
        }

        if(!myProfileResponseTag.getData().getImageUrl().equalsIgnoreCase("")){

            CommonUtils.setImage(getActivity(), imgProfile, Constants.IMAGE_BASE_URL
                    + myProfileResponseTag.getData().getImageUrl(), 200, 200);
        }else{
            imgProfile.setImageResource(R.drawable.dummy_profile);
        }

        updateLoginDetail(myProfileResponse.getData());

    }

    private void updateLoginDetail(Data data) {
        SaveLoginDetailsModel saveLoginDetailsModel = new SaveLoginDetailsModel();
        saveLoginDetailsModel.setName(data.getFirstName());
        saveLoginDetailsModel.setFirst_name(data.getFirstName());

        saveLoginDetailsModel.setLast_name(data.getLastName());
        saveLoginDetailsModel.setToken(data.getDeviceToken());
        saveLoginDetailsModel.setEmail(data.getEmail());
        saveLoginDetailsModel.setImage(data.getImageUrl());
        saveLoginDetailsModel.setId(data.getId());
        saveLoginDetailsModel.setOrganizationType(data.getOrganizationType());
        saveLoginDetailsModel.setFromSocialLogin(false);

        SharedPreff.saveLoginDetails(saveLoginDetailsModel);

    }

    @Override
    public void onResume() {
        super.onResume();
        myProfilePresenter.makeMyProfileRequest();
    }
}
