package com.stagemarket.fragment.myprofile;

import com.stagemarket.BasePresenter;
import com.stagemarket.pojo.myProfile.MyProfileResponse;

public interface MyProfileView extends BasePresenter{
    void onMyProfileResponse(MyProfileResponse myProfileResponse);
}
