package com.stagemarket.fragment.notifications;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.stagemarket.AppClass;
import com.stagemarket.R;
import com.stagemarket.fragment.discover.DiscoverView;
import com.stagemarket.pojo.SaveLoginDetailsModel;
import com.stagemarket.pojo.myProfile.MyProfileResponse;
import com.stagemarket.pojo.notifications.GetNotificationResponse;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.SharedPreff;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class NotificationsPresenterImp implements NotificationsPresenter {

    NetWorkingService netWorkingService;
    NotificationsView notificationsView;
    SaveLoginDetailsModel saveLoginDetailsModel;
    private final CompositeDisposable disposables = new CompositeDisposable();
    Context context;
    FragmentActivity activity;


    public NotificationsPresenterImp(NotificationsView notificationsView, NetWorkingService netWorkingService, FragmentActivity activity) {
        this.notificationsView = notificationsView;
        this.netWorkingService = netWorkingService;
        this.activity = activity;
        saveLoginDetailsModel = SharedPreff.getLoginDetailsModel();
        context = AppClass.getInstance().getApplicationContext();
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        if (disposables != null) {
            disposables.clear();
        }
        notificationsView = null;
        netWorkingService = null;
    }


    @Override
    public void fetchingAllNotificationsData() {
        if (CommonUtils.isConnectedToInternet(((Fragment) notificationsView).getActivity())) {

            if (notificationsView != null) {
                notificationsView.showProgressDialog();
            }


            netWorkingService.getAPI().getNotificationService(
                    saveLoginDetailsModel.getId())
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<GetNotificationResponse>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            disposables.add(d);
                        }
                        @Override
                        public void onNext(GetNotificationResponse getNotificationResponse) {

                            if (notificationsView != null) {
                                notificationsView.hideProgressDialog();

                                if (getNotificationResponse.getResult().equalsIgnoreCase(Constants.SUCCESS)) {

                                    notificationsView.onNotificationResponse(getNotificationResponse.getData());

                                } else {
                                    notificationsView.showErrorDialog(getNotificationResponse.getMessage());
                                }
                            }

                        }

                        @Override
                        public void onError(Throwable e) {
                            notificationsView.hideProgressDialog();
                            notificationsView.showErrorDialog(context.getResources().getString(R.string.something_went_wrong));
                        }

                        @Override
                        public void onComplete() {

                        }
                    });

        } else {
            CommonUtils.showTopSnackBar(context,
                    (activity).findViewById(android.R.id.content), context.getString(R.string.please_check_internet));
        }
    }
}
