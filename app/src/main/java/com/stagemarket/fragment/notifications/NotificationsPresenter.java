package com.stagemarket.fragment.notifications;

import com.stagemarket.CommonPresenter;

public interface NotificationsPresenter extends CommonPresenter{
    void fetchingAllNotificationsData();
}
