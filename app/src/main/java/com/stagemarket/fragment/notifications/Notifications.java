package com.stagemarket.fragment.notifications;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.stagemarket.AppClass;
import com.stagemarket.R;
import com.stagemarket.adapter.NotificationsAdapter;
import com.stagemarket.fragment.BaseFragment;
import com.stagemarket.pojo.notifications.NotificationData;
import com.stagemarket.pojo.notifications.NotificationsPojo;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.DialogPopUps;
import com.stagemarket.utils.SpacesItemDecoration;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class Notifications extends BaseFragment implements NotificationsView {

    @BindView(R.id.title_name)
    TextView titleName;
    @BindView(R.id.right_text)
    TextView rightText;
    @BindView(R.id.recyclerViewNotifications)
    RecyclerView recyclerViewNotifications;
    Unbinder unbinder;
    @BindView(R.id.header)
    View header;
    NotificationsAdapter mNotificationsAdapter;
    ArrayList<NotificationsPojo> mArrayListNotificationsPojo = new ArrayList<>();

    @BindView(R.id.img_header_back)
    ImageView imgHeaderBack;
    @BindView(R.id.swipeNotifications)
    SwipeRefreshLayout swipeNotifications;

    NotificationsPresenter notificationsPresenter;
    NetWorkingService netWorkingService;
    ArrayList<NotificationData> getNotificationResponseData = new ArrayList<>();
    @BindView(R.id.tv_noNotificationFound)
    TextView tvNoNotificationFound;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View screen = inflater.inflate(R.layout.notifications_view, container, false);
        unbinder = ButterKnife.bind(this, screen);
        initToolbar();
        return screen;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        netWorkingService = ((AppClass) getActivity().getApplication()).getNetworkService();
        super.onActivityCreated(savedInstanceState);
        notificationsPresenter = new NotificationsPresenterImp(this, netWorkingService, getActivity());
        initRecyclerView();

        notificationsPresenter.fetchingAllNotificationsData();

        swipeNotifications.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                /* notificationsPresenter.fetchingAllNotificationsData();*/
                swipeNotifications.setRefreshing(false);
            }
        });
    }

    private void initToolbar() {
        header.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.greynew));

        imgHeaderBack.setImageResource(R.drawable.ic_back);
        DrawableCompat.setTint(imgHeaderBack.getDrawable(), ContextCompat.getColor(getActivity(), R.color.black));
        imgHeaderBack.setVisibility(View.GONE);

        titleName.setText(R.string.menu_notifications);
        titleName.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
    }


    private void initRecyclerView() {
        //initListData();

        mNotificationsAdapter = new NotificationsAdapter(this, getNotificationResponseData);
        recyclerViewNotifications.setLayoutManager(new LinearLayoutManager(getActivity()
                , LinearLayoutManager.VERTICAL, false));
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen._1sdp);

        recyclerViewNotifications.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
        recyclerViewNotifications.setAdapter(mNotificationsAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        notificationsPresenter.unSubscribe();
    }

    @Override
    public void showProgressDialog() {
        //  DialogPopUps.showProgressDialog(getActivity(), getResources().getString(R.string.please_wait));
        swipeNotifications.setRefreshing(true);
    }

    @Override
    public void showErrorDialog(String msg) {
        //CommonUtils.showTopSnackBar(getActivity(), getActivity().findViewById(android.R.id.content), msg);
        tvNoNotificationFound.setVisibility(View.VISIBLE);
    }

    @Override
    public void showMessageDialog(String msg) {
    }

    @Override
    public void hideProgressDialog() {
        // DialogPopUps.hideDialog();
        swipeNotifications.setRefreshing(false);
    }


    @Override
    public void onNotificationResponse(ArrayList<NotificationData> getNotificationResponseData) {
        //   this.getNotificationResponseData = getNotificationResponseData;

        if (getNotificationResponseData.size() > 0) {
            mNotificationsAdapter.addingNotificationData(getNotificationResponseData);
            tvNoNotificationFound.setVisibility(View.GONE);
            recyclerViewNotifications.setVisibility(View.VISIBLE);
        } else {
            tvNoNotificationFound.setVisibility(View.VISIBLE);
            recyclerViewNotifications.setVisibility(View.GONE);
        }


    }
}
