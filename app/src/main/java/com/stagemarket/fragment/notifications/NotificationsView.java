package com.stagemarket.fragment.notifications;

import com.stagemarket.BasePresenter;
import com.stagemarket.pojo.notifications.NotificationData;

import java.util.ArrayList;
import java.util.List;

public interface NotificationsView extends BasePresenter{
    void onNotificationResponse(ArrayList<NotificationData> getNotificationResponseData);
}
