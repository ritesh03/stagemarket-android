package com.stagemarket.fragment.discover;

import com.stagemarket.CommonPresenter;

public interface DiscoverPresenter extends CommonPresenter{
    void fetchingAllProductsData(int page_count, boolean value,String lat,String lng,String catId,String subCatId,String subsecondcatId);

    void pullToRefreshCalled(int page, String latTemp, String lngTemp);
    void addToWishlist(String userId, String productId, String type);
    void searchProducts(int page_count, String search_str, String lat, String lng);
}
