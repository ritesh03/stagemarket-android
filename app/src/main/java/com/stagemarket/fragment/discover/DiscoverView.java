package com.stagemarket.fragment.discover;

import com.stagemarket.BasePresenter;
import com.stagemarket.pojo.discover.Data;
import com.stagemarket.pojo.discover.ProductData;

import java.util.ArrayList;

public interface DiscoverView extends BasePresenter{
    void setListData(ArrayList<ProductData> productData, int total_size, boolean isLoadMore,String type);
    void showNoProductError(boolean isLoadMore);

    void setFromPullToRefresh(ArrayList<ProductData> productData, int total_size);
}
