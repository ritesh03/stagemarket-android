package com.stagemarket.fragment.discover;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.stagemarket.AppClass;
import com.stagemarket.R;
import com.stagemarket.activity.categories.CategoriesActivity;
import com.stagemarket.adapter.DiscoverAdapter;
import com.stagemarket.fragment.BaseFragment;
import com.stagemarket.pojo.SubCatDataPojo;
import com.stagemarket.pojo.SubCatPojo;
import com.stagemarket.pojo.SubSecCatDataPojo;
import com.stagemarket.pojo.categorySell.Datum;
import com.stagemarket.pojo.discover.DiscoverBean;
import com.stagemarket.pojo.discover.LatLongBean;
import com.stagemarket.pojo.discover.ProductData;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.CommonListeners;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.DialogPopUps;
import com.stagemarket.utils.GPSTracker;
import com.stagemarket.utils.SharedPreff;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class Discover extends BaseFragment implements DiscoverView, DiscoverAdapter.CallBackLoadMore {

    @BindView(R.id.recyclerViewHome)
    RecyclerView recyclerViewHome;
    Unbinder unbinder;
    DiscoverAdapter discoverAdapter;
    @BindView(R.id.tv_categories)
    TextView tvCategories;
    @BindView(R.id.tv_locationName)
    TextView tvLocationName;
    @BindView(R.id.right_text)
    TextView rightText;
    @BindView(R.id.arrow)
    ImageView arrow;

    ArrayList<DiscoverBean> mArrayListDiscover = new ArrayList<>();
    @BindView(R.id.img_search)
    ImageView imgSearch;
    DiscoverPresenter discoverPresenter;
    NetWorkingService netWorkingService;
    @BindView(R.id.swipe_view)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.tv_noProductsFound)
    TextView tvNoProductsFound;
    private int PAGE_COUNT = 1;
    ArrayList<ProductData> productData_list1 = new ArrayList<>();
    private static int DEAL_LIST_SIZE = 5;

    @BindView(R.id.header)
    View header;

    @BindView(R.id.search_layout)
    RelativeLayout search_layout;

    @BindView(R.id.text_search)
    EditText text_search;

    boolean change = false;
    private String search_str = "";
    final Handler mHandler = new Handler();
    private boolean hasMore = false;
    private String catId = Constants.ALL;
    private String subCatId = Constants.ALL;
    private String subSecCatId = "";
    private String type = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View screen = inflater.inflate(R.layout.discover_view, container, false);
        unbinder = ButterKnife.bind(this, screen);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        callSearchRecords();
arrow.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        startActivity(new Intent(getActivity(), CategoriesActivity.class));
    }
});
        return screen;
    }

    @Override
    public void onResume() {
        super.onResume();
        Constants.PRODUCT_FOR="";
        PAGE_COUNT = 1;
        initRecyclerView();

        swipeRefreshLayout.setRefreshing(true);
        discoverPresenter.fetchingAllProductsData(PAGE_COUNT, false, SharedPreff.getLat(), SharedPreff.getLng(),
                catId, subCatId,subSecCatId);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //Define your task here
                PAGE_COUNT = 1;
               /* if (change) {
                    discoverPresenter.pullToRefreshCalled(PAGE_COUNT, SharedPreff.getLatTemp(), SharedPreff.getLngTemp());
                } else {
                    discoverPresenter.pullToRefreshCalled(PAGE_COUNT, SharedPreff.getLat(), SharedPreff.getLng());
                }*/

                if (change) {
                  //  Toast.makeText(getActivity(), ""+"in", Toast.LENGTH_SHORT).show();
                    discoverPresenter.fetchingAllProductsData(PAGE_COUNT, true, SharedPreff.getLatTemp(),
                            SharedPreff.getLngTemp(), catId, subCatId,subSecCatId);

                } else {
                    if(type.equalsIgnoreCase("")) {
                        productData_list1.clear();
                        discoverPresenter.fetchingAllProductsData(PAGE_COUNT, true, SharedPreff.getLat(),
                                SharedPreff.getLng(), catId, subCatId, subSecCatId);
                    }
                    else
                    {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                    //discoverPresenter.pullToRefreshCalled(PAGE_COUNT, SharedPreff.getLat(), SharedPreff.getLng());
                }

            }
        });

       // discoverAdapter.notifyDataSetChanged();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        netWorkingService = ((AppClass) getActivity().getApplication()).getNetworkService();
        super.onActivityCreated(savedInstanceState);

        header.setVisibility(View.VISIBLE);
        search_layout.setVisibility(View.GONE);

        discoverPresenter = new DiscoverPresenterImp(this, netWorkingService);





        setUpViews();
    }

    private void callSearchRecords() {
        text_search.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {

                Log.i("Search_text_value", cs.toString());

                search_str = cs.toString();
                mHandler.removeCallbacksAndMessages(null);
                mHandler.postDelayed(userStoppedTyping, 500);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
            }

            @Override
            public void afterTextChanged(Editable cs) {

            }
        });


    }


    Runnable userStoppedTyping = new Runnable() {
        @Override
        public void run() {
            // user didn't typed for 2 seconds, do whatever you want
            PAGE_COUNT = 1;

            if (search_str.trim().length() > 0) {
                if(swipeRefreshLayout!=null)
                {
                swipeRefreshLayout.setRefreshing(true);}
                if (change) {
                    discoverPresenter.searchProducts(PAGE_COUNT, search_str, SharedPreff.getLatTemp(),
                            SharedPreff.getLngTemp());

                } else {
                    discoverPresenter.searchProducts(PAGE_COUNT, search_str, SharedPreff.getLat(), SharedPreff.getLng());

                }

            } else {
                productData_list1.clear();
                if(swipeRefreshLayout!=null)
                {
                swipeRefreshLayout.setRefreshing(true);}
                if (change) {
                    discoverPresenter.fetchingAllProductsData(PAGE_COUNT, false, SharedPreff.getLatTemp(),
                            SharedPreff.getLngTemp(), catId, subCatId,subSecCatId);
                } else {
                    discoverPresenter.fetchingAllProductsData(PAGE_COUNT, false, SharedPreff.getLat(),
                            SharedPreff.getLng(), catId, subCatId,subSecCatId);
                }

                discoverAdapter.notifyDataSetChanged();

            }


        }
    };

    @OnClick(R.id.cross_icon)
    public void onCrossClick() {
        swipeRefreshLayout.setRefreshing(true);
        header.setVisibility(View.VISIBLE);
        search_layout.setVisibility(View.GONE);
        text_search.setText("");
        CommonUtils.hideKeyboard(text_search, getActivity());
        productData_list1.clear();

        if (change) {
            discoverPresenter.fetchingAllProductsData(PAGE_COUNT, true, SharedPreff.getLatTemp(),
                    SharedPreff.getLngTemp(), catId, subCatId,subSecCatId);

        } else {
            discoverPresenter.fetchingAllProductsData(PAGE_COUNT, true, SharedPreff.getLat(),
                    SharedPreff.getLng(), catId, subCatId,subSecCatId);
        }
        discoverAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.lytTop)
    public void onAddressClick() {
        Constants.PRODUCT_FOR="1";
        startActivity(new Intent(getActivity(), CategoriesActivity.class));
    }

    @OnClick(R.id.img_search)
    public void OnClickSearch() {
        header.setVisibility(View.GONE);
        search_layout.setVisibility(View.VISIBLE);
        InputMethodManager inputMethodManager =
                (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInputFromWindow(
                text_search.getApplicationWindowToken(),
                InputMethodManager.SHOW_FORCED, 0);
    }


    private void setUpViews() {
        if (!SharedPreff.getLat().equalsIgnoreCase("")) {
            double lat = Double.parseDouble(SharedPreff.getLat());
            double lng = Double.parseDouble(SharedPreff.getLng());
            Address address = CommonUtils.getAddressObjectFromLocation(lat, lng, getActivity());
            StringBuilder sb = new StringBuilder();
            if (address != null) {
                if (address.getSubLocality() != null) {
                    sb.append(address.getSubLocality()).append("\n");
                } else {
                    sb.append(address.getLocality()).append("\n");
                }
                if (address.getSubAdminArea() != null) {

                    sb.append(address.getSubAdminArea()).append(",");
                }

                sb.append(address.getCountryName());
                String result = sb.toString();
                tvLocationName.setText(result);

            }

        } else {
            callLocation();
        }

    }

    GPSTracker gps;

    private void callLocation() {

        Log.e("keshav", "callLocation Called ... ");

        gps = new GPSTracker(getActivity());

        // check if GPS enabled
        if (gps.canGetLocation()) {

            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();

            Log.e("MainActivity", "latitude -> " + latitude);
            Log.e("MainActivity", "longitude -> " + longitude);

            if (latitude != 0.0) {
                SharedPreff.saveLat(String.valueOf(latitude));
                SharedPreff.saveLng(String.valueOf(longitude));
                setUpViews();
            } else {

            }

        } else {
        }
    }

    private void initRecyclerView() {
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerViewHome.setLayoutManager(mLayoutManager);
        discoverAdapter = new DiscoverAdapter(getActivity(), recyclerViewHome, productData_list1,
                getActivity(), this);
        recyclerViewHome.setAdapter(discoverAdapter);
        // recyclerViewHome.setHasFixedSize(false);
        //recyclerViewHome.setItemAnimator(new DefaultItemAnimator());
        recyclerViewHome.setVisibility(View.VISIBLE);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        discoverPresenter.unSubscribe();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void showProgressDialog() {
        DialogPopUps.showProgressDialog(getActivity(), getResources().getString(R.string.please_wait));
    }

    @Override
    public void showErrorDialog(String msg) {

        CommonUtils.showTopSnackBar(getActivity(), getActivity().findViewById(android.R.id.content), msg);
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void showMessageDialog(String msg) {

    }

    @Override
    public void hideProgressDialog() {
        DialogPopUps.hideDialog();
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        discoverPresenter.unSubscribe();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onCategorySelect(SubCatPojo categoriesPojo) {
        productData_list1.clear();
        if (categoriesPojo.getCatName().equalsIgnoreCase(Constants.ALL)) {
         //   Toast.makeText(getActivity(), "all", Toast.LENGTH_SHORT).show();
            tvCategories.setText(Constants.ALL_CATEGORIES);
            catId = Constants.ALL;
            subCatId = Constants.ALL;
             PAGE_COUNT=1;

            swipeRefreshLayout.setRefreshing(true);
            discoverPresenter.fetchingAllProductsData(PAGE_COUNT, false, SharedPreff.getLat(), SharedPreff.getLng(),
                    catId, subCatId,subSecCatId);
      /*      if (change) {
                discoverPresenter.fetchingAllProductsData(PAGE_COUNT, true, SharedPreff.getLatTemp(),
                        SharedPreff.getLngTemp(), catId, subCatId,subSecCatId);

            } else {
                discoverPresenter.fetchingAllProductsData(PAGE_COUNT, true, SharedPreff.getLat(),
                        SharedPreff.getLng(), catId, subCatId,subSecCatId);
            }*/
        } else {
            catId = categoriesPojo.getSubCatId();
            tvCategories.setText(categoriesPojo.getCatName());



          //  Toast.makeText(getContext(), ""+categoriesPojo.getCatName(), Toast.LENGTH_SHORT).show();
            if (change) {
                discoverPresenter.fetchingAllProductsData(PAGE_COUNT, false, SharedPreff.getLatTemp(),
                        SharedPreff.getLngTemp(), catId, subCatId,subSecCatId);

            } else {
                discoverPresenter.fetchingAllProductsData(PAGE_COUNT, false, SharedPreff.getLat(),
                        SharedPreff.getLng(), catId, subCatId,subSecCatId);
            }
        }
        discoverAdapter.notifyDataSetChanged();
    }


    @Subscribe
    public void onSubCategorySelect(SubCatDataPojo categoriesPojo) {
        productData_list1.clear();
      //  Toast.makeText(getContext(), catId+","+categoriesPojo.getSubId(), Toast.LENGTH_SHORT).show();
        subCatId = categoriesPojo.getSubId();
        PAGE_COUNT=1;
        if (change) {
            discoverPresenter.fetchingAllProductsData(PAGE_COUNT, false, SharedPreff.getLatTemp(),
                    SharedPreff.getLngTemp(), catId, subCatId,subSecCatId);

        } else {
            discoverPresenter.fetchingAllProductsData(PAGE_COUNT, false, SharedPreff.getLat(),
                    SharedPreff.getLng(), catId, subCatId,subSecCatId);
        }
        discoverAdapter.notifyDataSetChanged();
    }

    @Subscribe
    public void onSubSecCategorySelect(SubSecCatDataPojo categoriesPojo) {
        productData_list1.clear();
      //  Toast.makeText(getContext(), subCatId+catId+","+categoriesPojo.getSubId(), Toast.LENGTH_SHORT).show();
        subSecCatId = categoriesPojo.getSubId();
        PAGE_COUNT=1;
        if (change) {
            discoverPresenter.fetchingAllProductsData(PAGE_COUNT, false, SharedPreff.getLatTemp(),
                    SharedPreff.getLngTemp(), catId, subCatId,subSecCatId);

        } else {
            discoverPresenter.fetchingAllProductsData(PAGE_COUNT, false, SharedPreff.getLat(),
                    SharedPreff.getLng(), catId, subCatId,subSecCatId);
        }
        discoverAdapter.notifyDataSetChanged();
    }

    @Subscribe
    public void onLocationChanged(LatLongBean latLng) {
        change = true;
        Address address = CommonUtils.getAddressObjectFromLocation(latLng.getLat(), latLng.getLng(), getActivity());
        StringBuilder sb = new StringBuilder();
        if (address.getSubLocality() != null) {
            sb.append(address.getSubLocality()).append("\n");
        } else {
            sb.append(address.getLocality()).append("\n");
        }

        if (address.getSubAdminArea() != null) {
            sb.append(address.getSubAdminArea()).append(",");
        } else {
            sb.append(address.getThoroughfare()).append(",");
        }

        sb.append(address.getCountryName());
        String result = sb.toString();
        tvLocationName.setText(result);

        PAGE_COUNT = 1;
        discoverPresenter.fetchingAllProductsData(PAGE_COUNT, false, String.valueOf(latLng.getLat()),
                String.valueOf(latLng.getLng()), catId, subCatId,subSecCatId);
    }

    @Override
    public void setListData(ArrayList<ProductData> productData_list, int total_size, boolean isLoadMore,String type) {
        DEAL_LIST_SIZE = total_size;
        swipeRefreshLayout.setRefreshing(false);
     //   Toast.makeText(getActivity(), "page count="+PAGE_COUNT+"size"+productData_list.size(), Toast.LENGTH_SHORT).show();
      /*  if (productData_list.size() < 10) {
            hasMore = true;
          //  productData_list1.clear();
        }*/
      //  Toast.makeText(getActivity(), ""+total_size, Toast.LENGTH_SHORT).show();
        if (PAGE_COUNT == 1) {
            productData_list1.clear();
        }
if(total_size<10)
{
    hasMore = true;
    productData_list1.clear();
}
else
{
    if(PAGE_COUNT==1)
    {
        productData_list1.clear();
        discoverAdapter.notifyDataSetChanged();
    }
    hasMore = false;
}
        if (productData_list.size() > 0) {
            PAGE_COUNT = PAGE_COUNT + 1;

            productData_list1.addAll(productData_list);
            discoverAdapter.notifyDataSetChanged();

            //  discoverAdapter.addingMyPostsData(productData_list1);
            tvNoProductsFound.setVisibility(View.GONE);
        } else {
            if (PAGE_COUNT == 1) {
                productData_list.clear();
                productData_list1.clear();
                discoverAdapter.notifyDataSetChanged();
                tvNoProductsFound.setVisibility(View.VISIBLE);
            }
        }
if(type.equalsIgnoreCase("search")) {
            this.type="search";
    swipeRefreshLayout.setRefreshing(false);
}

    }

    @Override
    public void showNoProductError(boolean isLoadMore) {
        swipeRefreshLayout.setRefreshing(false);
        if (!isLoadMore) {
            discoverAdapter.setLoaded();
            productData_list1.clear();
            discoverAdapter.notifyDataSetChanged();
            tvNoProductsFound.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void setFromPullToRefresh(ArrayList<ProductData> productData_list, int total_size) {
    //    Toast.makeText(getActivity(), "dd="+total_size, Toast.LENGTH_SHORT).show();
        swipeRefreshLayout.setRefreshing(false);
        DEAL_LIST_SIZE = total_size;
        hasMore = false;
        if (productData_list.size() > 0) {
            PAGE_COUNT = PAGE_COUNT + 1;
            productData_list1.addAll(productData_list);
            discoverAdapter.notifyDataSetChanged();
            //  discoverAdapter.addingMyPostsData(productData_list);
            recyclerViewHome.setVisibility(View.VISIBLE);
            tvNoProductsFound.setVisibility(View.GONE);
        } else {
            if (PAGE_COUNT == 1) {
                tvNoProductsFound.setVisibility(View.VISIBLE);
                recyclerViewHome.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onBottomReach() {
        if (!hasMore) {
             swipeRefreshLayout.setRefreshing(true);
            if (change) {
                discoverPresenter.fetchingAllProductsData(PAGE_COUNT, true, SharedPreff.getLatTemp(),
                        SharedPreff.getLngTemp(), catId, subCatId,subSecCatId);

            } else {
                discoverPresenter.fetchingAllProductsData(PAGE_COUNT, true, SharedPreff.getLat(),
                        SharedPreff.getLng(), catId, subCatId,subSecCatId);
            }
        }

    }
}