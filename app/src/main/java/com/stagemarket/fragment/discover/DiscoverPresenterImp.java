package com.stagemarket.fragment.discover;

import android.support.v4.app.Fragment;
import android.util.Log;

import com.google.gson.Gson;
import com.stagemarket.R;
import com.stagemarket.pojo.SaveLoginDetailsModel;
import com.stagemarket.pojo.discover.GetProductsResponse;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.SharedPreff;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DiscoverPresenterImp implements DiscoverPresenter {


 public static   DiscoverView discoverView;
    SaveLoginDetailsModel saveLoginDetailsModel;
    private final CompositeDisposable disposables = new CompositeDisposable();
   public static NetWorkingService netWorkingService;

    public DiscoverPresenterImp(DiscoverView discoverView, NetWorkingService netWorkingService) {
        this.discoverView = discoverView;
        this.netWorkingService = netWorkingService;
        saveLoginDetailsModel = SharedPreff.getLoginDetailsModel();
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        if (disposables != null) {
            disposables.clear();
        }
        discoverView = null;
        netWorkingService = null;
    }


    @Override
    public void searchProducts(int page_count, String search_str, String lat, String lng) {
        if (CommonUtils.isConnectedToInternet(((Fragment) discoverView).getActivity())) {

            netWorkingService.getAPI().searchProductsService(saveLoginDetailsModel.getId(), page_count, search_str,
                    lat, lng).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<GetProductsResponse>() {
                @Override
                public void onSubscribe(Disposable d) {
                    disposables.add(d);
                }

                @Override
                public void onNext(GetProductsResponse getProductsResponse) {

                    if (discoverView != null) {

                        if (getProductsResponse.getResult().equalsIgnoreCase(Constants.SUCCESS)) {
                            discoverView.setListData(getProductsResponse.getData().getProductData(),
                                    getProductsResponse.getData().getmTotal(), false,"search");
                        } else {
                            discoverView.showNoProductError(false);
                        }
                    }
                }

                @Override
                public void onError(Throwable e) {
                    discoverView.hideProgressDialog();
                    discoverView.showErrorDialog((((Fragment) discoverView).getActivity()).getResources().getString(R.string.something_went_wrong));
                }

                @Override
                public void onComplete() {

                }
            });
        } else {
            CommonUtils.check_your_network_available((((Fragment) discoverView).getActivity()));
        }
    }


    @Override
    public void fetchingAllProductsData(int page_count, final boolean isLoadMore,String lat,String lng,
                                        String catId,String subCatId,String subsecondcatId) {
        if (CommonUtils.isConnectedToInternet(((Fragment) discoverView).getActivity())) {

               // discoverView.showProgressDialog();
//SharedPreff.getLat(),SharedPreff.getLng()
            netWorkingService.getAPI().getProductsService(saveLoginDetailsModel.getId(), page_count,
                   lat, lng,catId,subCatId,subsecondcatId).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<GetProductsResponse>() {
                @Override
                public void onSubscribe(Disposable d) {
                    disposables.add(d);
                }

                @Override
                public void onNext(GetProductsResponse getProductsResponse) {

                    if (discoverView != null) {
                       // if (!isLoadMore)
                           // discoverView.hideProgressDialog();

                        if (getProductsResponse.getResult().equalsIgnoreCase(Constants.SUCCESS)) {

                            Gson  gson=new Gson();
                            Log.e("posts res",gson.toJson(getProductsResponse));

                            discoverView.setListData(getProductsResponse.getData().getProductData(),
                                    getProductsResponse.getData().getmTotal(), isLoadMore,"");
                        } else {
                            discoverView.showNoProductError(isLoadMore);
                        }
                    }

                }

                @Override
                public void onError(Throwable e) {
                    discoverView.hideProgressDialog();
                    discoverView.showErrorDialog((((Fragment) discoverView).getActivity()).getResources().getString(R.string.something_went_wrong));
                }

                @Override
                public void onComplete() {

                }
            });
        } else {
            discoverView.hideProgressDialog();
            CommonUtils.check_your_network_available((((Fragment) discoverView).getActivity()));
        }
    }


    @Override
    public void pullToRefreshCalled(int page, String latTemp, String lngTemp) {

        if (CommonUtils.isConnectedToInternet(((Fragment) discoverView).getActivity())) {

         //SharedPreff.getLat(),SharedPreff.getLng()
            netWorkingService.getAPI().getProductsService(saveLoginDetailsModel.getId(),
                    page,latTemp,lngTemp,"","","")
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<GetProductsResponse>() {

                @Override
                public void onSubscribe(Disposable d) {
                    disposables.add(d);
                }

                @Override
                public void onNext(GetProductsResponse getProductsResponse) {

                    if (discoverView != null) {
                        if (getProductsResponse.getResult().equalsIgnoreCase(Constants.SUCCESS)) {

                            discoverView.setFromPullToRefresh(getProductsResponse.getData().getProductData(), getProductsResponse.getData().getmTotal());

                        } else {
                            discoverView.showErrorDialog(getProductsResponse.getMessage());
                        }
                    }

                }

                @Override
                public void onError(Throwable e) {
                    discoverView.hideProgressDialog();
                    discoverView.showErrorDialog((((Fragment) discoverView).getActivity()).getResources().getString(R.string.something_went_wrong));
                }

                @Override
                public void onComplete() {

                }
            });
        } else {
            discoverView.hideProgressDialog();
            CommonUtils.check_your_network_available((((Fragment) discoverView).getActivity()));
        }
    }

    @Override
    public void addToWishlist(String userId, String productId, String type) {

    }
}
