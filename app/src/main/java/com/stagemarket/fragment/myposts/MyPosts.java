package com.stagemarket.fragment.myposts;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.stagemarket.AppClass;
import com.stagemarket.R;
import com.stagemarket.adapter.MyPostsAdapter;
import com.stagemarket.fragment.BaseFragment;
import com.stagemarket.pojo.DeleteMyPostsPojo;
import com.stagemarket.pojo.myPosts.Datum;
import com.stagemarket.pojo.myPosts.MyPostsPojo;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.CommonListeners;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.DialogPopUps;
import com.stagemarket.utils.SharedPreff;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MyPosts extends BaseFragment implements MyPostsView, MyPostsAdapter.CallBackLoadMore {

    @BindView(R.id.title_name)
    TextView titleName;
    @BindView(R.id.recyclerViewMyPosts)
    RecyclerView recyclerViewMyPosts;
    Unbinder unbinder;
    @BindView(R.id.header)
    View header;
    @BindView(R.id.img_header_back)
    ImageView imgHeaderBack;
    MyPostsAdapter mMyPostsAdapter;
    ArrayList<MyPostsPojo> mArrayListMyPostsPojo = new ArrayList<>();
    @BindView(R.id.tv_noPostsFound)
    TextView tvNoPostsFound;
    @BindView(R.id.swipe_view)
    SwipeRefreshLayout swipeView;

    @BindView(R.id.radio_purchased)
    RadioButton radioPurchased;
    @BindView(R.id.radio_sold)
    RadioButton radioSold;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    private MyPostsPresenter myPostsPresenter;
    private NetWorkingService netWorkingService;
    private int PAGE_COUNT = 1;
    private static int DEAL_LIST_SIZE = 5;
    private ArrayList<Datum> my_posts_list1 = new ArrayList<>();
    private int type = 2;
    private boolean hasMore = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View screen = inflater.inflate(R.layout.myposts_view, container, false);
        unbinder = ButterKnife.bind(this, screen);
        initToolbar();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        return screen;
    }

    private void initRecyclerView() {
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerViewMyPosts.setLayoutManager(mLayoutManager);
        mMyPostsAdapter = new MyPostsAdapter(this, recyclerViewMyPosts, my_posts_list1, getActivity(),
                this,type);
        recyclerViewMyPosts.setAdapter(mMyPostsAdapter);
        recyclerViewMyPosts.setHasFixedSize(true);
        recyclerViewMyPosts.setItemAnimator(new DefaultItemAnimator());
        recyclerViewMyPosts.setVisibility(View.VISIBLE);

       /* mMyPostsAdapter.setOnLoadMoreListener(new CommonListeners.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (my_posts_list1.size() <= DEAL_LIST_SIZE) {
                    my_posts_list1.add(null);
                    mMyPostsAdapter.notifyItemInserted(my_posts_list1.size() - 1);
                    myPostsPresenter.fetchingMyPostsData(PAGE_COUNT, true);

                } else {
                    //Toast.makeText(activity, "Loading data completed", Toast.LENGTH_SHORT).show();
                }
            }
        });*/
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        netWorkingService = ((AppClass) getActivity().getApplication()).getNetworkService();
        super.onActivityCreated(savedInstanceState);

        myPostsPresenter = new MyPostsPresenterImp(this, netWorkingService,type);
        initRecyclerView();
        clickRadioButton();

         //swipeView.setRefreshing(true);

      myPostsPresenter.fetchingMyPostsData(PAGE_COUNT, false, type);


        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (type == 1) {
                    swipeView.setRefreshing(true);
                    PAGE_COUNT = 1;
                    myPostsPresenter.fetchingMyPostsData(PAGE_COUNT, false, type);
                } else {
                    swipeView.setRefreshing(true);
                    PAGE_COUNT = 1;
                    myPostsPresenter.fetchingMyPostsData(PAGE_COUNT, false, type);
                }
            }
        });


    }

    private void clickRadioButton() {
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                View radioButton = radioGroup.findViewById(checkedId);
                int idx = radioGroup.indexOfChild(radioButton);
                if (idx == 0) {
                    tvNoPostsFound.setVisibility(View.GONE);
                    my_posts_list1.clear();
                    swipeView.setRefreshing(true);
                    PAGE_COUNT = 1;
                    type = 2;
                    mMyPostsAdapter.setType(type);
                    myPostsPresenter.fetchingMyPostsData(PAGE_COUNT, false, type);
                } else {
                    tvNoPostsFound.setVisibility(View.GONE);
                    my_posts_list1.clear();
                     swipeView.setRefreshing(true);
                    PAGE_COUNT = 1;
                    type = 1;
                    mMyPostsAdapter.setType(type);
                    myPostsPresenter.fetchingMyPostsData(PAGE_COUNT, false, type);
                }
            }
        });
    }

    private void initToolbar() {
        header.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.greynew));
        imgHeaderBack.setImageResource(R.drawable.ic_back);
        DrawableCompat.setTint(imgHeaderBack.getDrawable(), ContextCompat.getColor(getActivity(), R.color.black));
        imgHeaderBack.setVisibility(View.GONE);

        titleName.setText(R.string.my_ads);
        titleName.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
        unbinder.unbind();
        myPostsPresenter.unSubscribe();
    }

    @Override
    public void showProgressDialog() {
        DialogPopUps.showProgressDialog(getActivity(), getResources().getString(R.string.please_wait));
    }

    @Override
    public void showErrorDialog(String msg) {
        tvNoPostsFound.setVisibility(View.VISIBLE);
        // CommonUtils.showTopSnackBar(getActivity(), getActivity().findViewById(android.R.id.content), msg);
        if (swipeView != null) {
            swipeView.setRefreshing(false);
        }
    }

    @Override
    public void showMessageDialog(String msg) {
    }

    @Subscribe
    public void onDeleteAd(DeleteMyPostsPojo deleteMyPostsPojo) {
        my_posts_list1.remove(deleteMyPostsPojo.getPosition());
        mMyPostsAdapter.notifyDataSetChanged();

        if (my_posts_list1.size() > 0) {
            tvNoPostsFound.setVisibility(View.GONE);
        } else {
            tvNoPostsFound.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideProgressDialog() {
        DialogPopUps.hideDialog();
        if (swipeView.isRefreshing()) {
            swipeView.setRefreshing(false);
        }
    }

    @Override
    public void setListData(ArrayList<Datum> my_posts_list, int totalSize, boolean isLoadMore) {
        DEAL_LIST_SIZE = totalSize;
        swipeView.setRefreshing(false);
        if (PAGE_COUNT == 1) {
            my_posts_list1.clear();
        }

        if (my_posts_list.size() > 0) {
            PAGE_COUNT = PAGE_COUNT + 1;
            my_posts_list1.addAll(my_posts_list);
            mMyPostsAdapter.notifyDataSetChanged();
            // mMyPostsAdapter.addingMyPostsData(my_posts_list);
            tvNoPostsFound.setVisibility(View.GONE);
        } else {
            if (PAGE_COUNT == 1) {
                tvNoPostsFound.setVisibility(View.VISIBLE);
            }
        }

    }

    @Override
    public void setFromPullToRefresh(ArrayList<Datum> my_posts_list, int totalSize) {
        swipeView.setRefreshing(false);
        DEAL_LIST_SIZE = totalSize;
        if (my_posts_list.size() > 0) {

            if ((my_posts_list.size() < 10)) {
                hasMore = true;
            }
            PAGE_COUNT = 2;
            my_posts_list1 = my_posts_list;
            mMyPostsAdapter.addingMyPostsData(my_posts_list);
            recyclerViewMyPosts.setVisibility(View.VISIBLE);
            tvNoPostsFound.setVisibility(View.GONE);
            mMyPostsAdapter.setLoaded();

        } else {
            tvNoPostsFound.setVisibility(View.VISIBLE);
            recyclerViewMyPosts.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBottomReach() {
        if (!hasMore) {
            PAGE_COUNT = PAGE_COUNT + 1;
            swipeView.setRefreshing(true);
            myPostsPresenter.fetchingMyPostsData(PAGE_COUNT, false, type);
        }

    }
}
