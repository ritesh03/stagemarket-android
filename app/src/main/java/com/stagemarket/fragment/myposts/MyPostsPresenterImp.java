package com.stagemarket.fragment.myposts;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.stagemarket.AppClass;
import com.stagemarket.R;
import com.stagemarket.fragment.discover.DiscoverView;
import com.stagemarket.pojo.SaveLoginDetailsModel;
import com.stagemarket.pojo.myPosts.GetMyPostsResponse;
import com.stagemarket.retrofit.NetWorkingService;
import com.stagemarket.utils.CommonUtils;
import com.stagemarket.utils.Constants;
import com.stagemarket.utils.SharedPreff;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MyPostsPresenterImp implements MyPostsPresenter {

    NetWorkingService netWorkingService;
    MyPostsView myPostsView;
    SaveLoginDetailsModel saveLoginDetailsModel;
    private final CompositeDisposable disposables = new CompositeDisposable();
    Context context;
int type;

    public MyPostsPresenterImp(MyPostsView myPostsView, NetWorkingService netWorkingService,int type) {
        this.myPostsView = myPostsView;
        this.netWorkingService = netWorkingService;
        this.type=type;
        saveLoginDetailsModel = SharedPreff.getLoginDetailsModel();
        context = AppClass.getInstance().getApplicationContext();
    }


    @Override
    public void subscribe() {
    }

    @Override
    public void unSubscribe() {
        if (disposables != null) {
            disposables.clear();
        }
        myPostsView = null;
        netWorkingService = null;
    }


    @Override
    public void fetchingMyPostsData(int page_count, final boolean isLoadMore,int type) {
        if (CommonUtils.isConnectedToInternet(((Fragment) myPostsView).getActivity())) {


            netWorkingService.getAPI().getMyPostsService(saveLoginDetailsModel.getId(), page_count,type)
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<GetMyPostsResponse>() {
                @Override
                public void onSubscribe(Disposable d) {
                    disposables.add(d);
                }

                @Override
                public void onNext(GetMyPostsResponse getMyPostsResponse) {
                    if (myPostsView != null) {
                        if (getMyPostsResponse.getResult().equalsIgnoreCase(Constants.SUCCESS)) {

                            myPostsView.setListData(getMyPostsResponse.getData().getData(), getMyPostsResponse.getData().getmTotal(), isLoadMore);
                        } else {
                            myPostsView.showErrorDialog(getMyPostsResponse.getMessage());
                        }
                    }
                }

                @Override
                public void onError(Throwable e) {
                    myPostsView.hideProgressDialog();
                    myPostsView.showErrorDialog((((Fragment) myPostsView).getActivity()).getResources().getString(R.string.something_went_wrong));
                }

                @Override
                public void onComplete() {

                }
            });



        } else {
            myPostsView.hideProgressDialog();
            CommonUtils.showTopSnackBar(context,
                    ((FragmentActivity) ((Fragment) myPostsView).getActivity()).findViewById(android.R.id.content), context.getString(R.string.please_check_internet));
        }
    }

    @Override
    public void pullToRefreshCalled(int page) {
        if (CommonUtils.isConnectedToInternet(((Fragment) myPostsView).getActivity())) {

            netWorkingService.getAPI().getMyPostsService(saveLoginDetailsModel.getId(), page,1)
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<GetMyPostsResponse>() {

                @Override
                public void onSubscribe(Disposable d) {
                    disposables.add(d);
                }

                @Override
                public void onNext(GetMyPostsResponse getMyPostsResponse) {

                    if (myPostsView != null) {
                        if (getMyPostsResponse.getResult().equalsIgnoreCase(Constants.SUCCESS)) {

                            myPostsView.setFromPullToRefresh(getMyPostsResponse.getData().getData(), getMyPostsResponse.getData().getmTotal());
                        } else {
                            myPostsView.showErrorDialog(getMyPostsResponse.getMessage());
                        }
                    }

                }

                @Override
                public void onError(Throwable e) {
                    myPostsView.showErrorDialog((((Fragment) myPostsView).getActivity()).getResources().getString(R.string.something_went_wrong));
                }

                @Override
                public void onComplete() {

                }
            });



        } else {
            CommonUtils.showTopSnackBar(context,
                    ((FragmentActivity) ((Fragment) myPostsView).getActivity()).findViewById(android.R.id.content), context.getString(R.string.please_check_internet));
        }
    }
}
