package com.stagemarket.fragment.myposts;

import com.stagemarket.CommonPresenter;

public interface MyPostsPresenter extends CommonPresenter{
    void fetchingMyPostsData(int page_count, boolean isLoadMore, int type);

    void pullToRefreshCalled(int page);
}
