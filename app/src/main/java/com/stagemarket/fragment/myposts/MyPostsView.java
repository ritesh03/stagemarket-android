package com.stagemarket.fragment.myposts;

import com.stagemarket.BasePresenter;
import com.stagemarket.pojo.myPosts.Datum;

import java.util.ArrayList;
import java.util.List;

public interface MyPostsView extends BasePresenter{
    void setListData(ArrayList<Datum> data, int  totalSize, boolean isLoadMore);

    void setFromPullToRefresh(ArrayList<Datum> data, int totalSize);
}
